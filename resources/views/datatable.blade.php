<html>
<head>
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css"> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
</head>

<body>
    <div class="aa">
  <!-- Content here -->
</div>
<div class="container">
    <br>
    <table class="table table-striped table-bordered dt-responsive nowrap" id="myTable">
        <thead>
            <tr>
            <th scope="col">Menu ID</th>
            <th scope="col">Menu Name</th>
            <th scope="col">root</th>
            <th scope="col">weight</th>
            <th scope="col">icon</th>
            <th scope="col">active</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
    
    <script
    src="https://code.jquery.com/jquery-3.6.0.js"
    integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
    crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('/datamenu?token=hkfnsifsdkfksdkfksdfkd') }}",
                }, 
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'menu_name', name: 'menu_name'},
                    {data: 'root', name: 'root'},
                    {data: 'weight', name: 'weight'},
                    {data: 'icon', name: 'icon'},
                    {data: 'active', name: 'active'},
                ]
            });

            $.ajax({
                url : "{{url('v1/role-access/1/edit?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3QvYXBpX3Bvcy9wdWJsaWMvdjEvc2lnbmluIiwiaWF0IjoxNjI5MzYyNTg5LCJleHAiOjE2Mjk0NDg5ODksIm5iZiI6MTYyOTM2MjU4OSwianRpIjoicDBncUNZU0k4S1BZZTNLTiJ9.tIc2iFMsR0X0eECkHT_Gj1_7aJ6wxvNP1dN1oi8b-sI')}}",
                type : "GET",
                contentType : 'JSON',
                success : function(res) {
                    var a = "";
                    $.each( res,function(key, val){
                    console.log(val)
                        a += val.menu_name+"<br>";
                    });
                    $('.aa').append(a);
                }
            });
        } );
    </script>
</body>
</html>