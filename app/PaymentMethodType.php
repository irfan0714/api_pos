<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethodType extends Model
{
    protected $table = 'mst_payment_method_types';
    protected $keyType = 'string';
}
