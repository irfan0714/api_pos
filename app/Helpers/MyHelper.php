<?php
namespace App\Helpers;
 
use Illuminate\Support\Facades\DB;
use App\Stock;
 
class MyHelper {
    
    public function createHistory() {

        $root = $param['root'];
        $desc = $param['desc'];
        $ip = $param['ip'];
        $os = $param['os'];
        $os = $param['os'];
    }

    public function stockManagement($param=array())
    {
        $year = $param['year'];
        $month = $param['month'];
        $warehouseId = $param['warehouseId'];
        $productId = $param['productId'];
        $qty = $param['qty'];
        $value = $param['value'];
        $mutationType = $param['mutationType'];

        $earlyStockField = "early_".$month;
        $inStockField = "in_".$month;
        $outStockField = "out_".$month;
        $endStockField = "end_".$month;

        $earlyValueField = "early_value_".$month;
        $inValueField = "in_value_".$month;
        $outValueField = "out_value_".$month;
        $endValueField = "end_value_".$month;

        $earlyStockResponse = '';
        $inStockResponse = '';
        $outStockResponse = '';
        $endStockResponse = '';

        DB::beginTransaction();
        try
        {
            //insert row stok
            $checkStockRow = DB::table('stocks')->select('*')
                            ->where('year', $year)
                            ->where('warehouse_id', $warehouseId)
                            ->where('product_id', $productId)
                            ->count();
            
            if($checkStockRow == 0) {
                if($month == '01') {
                    $iMonth = '12';
                    $iYear = intval($year) - 1;
                }else {
                    $iMonth = intval($month) - 1;
                    $iYear = $year;
                }
    
                if($iMonth < 10 ) {
                    $iMonth = '0'.$iMonth;
                }else {
                    $iMonth = $iMonth;
                }

                $arrIStock = DB::table('stocks')->select('*')
                            ->where('year', $iYear)
                            ->where('warehouse_id', $warehouseId)
                            ->where('product_id', $productId)
                            ->get();

                $arrData = $this->getArray($arrIStock);

                if(count($arrData) > 0) {
                    $iStock = $arrData[0]["end_".$iMonth];
                    $iValue = $arrData[0]["end_value_".$iMonth];
                }else {
                    $iStock = 0;
                    $iValue = 0;
                }

                DB::table('stocks')->insert([
                    'year' => $year,
                    'warehouse_id' => $warehouseId,
                    'product_id' => $productId,
                    'early_'.$month => $iStock,
                    'early_value_'.$month => $iValue,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
            //end insert row stok

            //update stok awal, ambil dari stok akhir month sebelumnya
            if($month == '01') {
                $uMonth = '12';
                $uYear = intval($year) - 1;
            }else {
                $uMonth = intval($month) - 1;
                $uYear = $year;
            }
    
            if($uMonth < 10 ) {
                $uMonth = '0'.$uMonth;
            }else {
                $uMonth = $uMonth;
            }

            $arrUStock = DB::table('stocks')->select('*')
                        ->where('year', $uYear)
                        ->where('warehouse_id', $warehouseId)
                        ->where('product_id', $productId)
                        ->get();

            $arrData = $this->getArray($arrUStock);
            
            if(count($arrData) > 0) {
                $uStock = $arrData[0]["end_".$uMonth];
                $uValue = $arrData[0]["end_value_".$uMonth];
            }else {
                $uStock = 0;
                $uValue = 0;
            }

            $earlyStockAffected = DB::table('stocks')
                        ->where('year', $year)
                        ->where('warehouse_id', $warehouseId)
                        ->where('product_id', $productId)
                        ->update([
                            $earlyStockField => $uStock,
                            $earlyValueField => $uValue,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);

            if($earlyStockAffected > 0) { $earlyStockResponse = 'Early stock updated'; }

            $arrStock = Stock::select('*')
                        ->where('year', $year)
                        ->where('warehouse_id', $warehouseId)
                        ->where('product_id', $productId)
                        ->get();
            
            $arrData = $arrStock->toArray();
                    
            if($mutationType == 'I') {

                if(count($arrData) > 0) {
                    $stockIn = intval($arrData[0]["in_".$month]) + intval($qty);
                    $valueIn = intval($arrData[0]["in_value_".$month]) + 0;
                } else {
                    $stockIn = intval($qty);
                    $valueIn = 0;
                }

                //insert stock masuk
                $inStockAffected = DB::table('stocks')
                        ->where('year', $year)
                        ->where('warehouse_id', $warehouseId)
                        ->where('product_id', $productId)
                        ->update([
                            $inStockField => $stockIn,
                            $inValueField => $valueIn,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                
                if($inStockAffected > 0) { $inStockResponse = 'In stock updated'; }

                if(count($arrData) > 0) {
                    $stockEnd = intval($arrData[0]["early_".$month]) + $stockIn - intval($arrData[0]["out_".$month]);
                    $valueEnd = intval($arrData[0]["early_value_".$month]) + $valueIn - intval($arrData[0]["out_value_".$month]);
                } else {
                    $stockEnd = $stockIn;
                    $valueEnd = $valueIn;
                }
    
            } else if($mutationType == 'O') {

                if(count($arrData) > 0) {
                    $stockOut = intval($arrData[0]["out_".$month]) + intval($qty);
                    $valueOut = intval($arrData[0]["out_value_".$month]) + 0;
                } else {
                    $stockOut = intval($qty);
                    $valueOut = 0;
                }

                //insert stock keluar
                $outStockAffected = DB::table('stocks')
                        ->where('year', $year)
                        ->where('warehouse_id', $warehouseId)
                        ->where('product_id', $productId)
                        ->update([
                            $outStockField => $stockOut,
                            $outValueField => $valueOut,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);

                if($outStockAffected > 0) { $outStockResponse = 'Out stock updated'; }

                if(count($arrData) > 0) {
                    $stockEnd = intval($arrData[0]["early_".$month]) + intval($arrData[0]["in_".$month]) - $stockOut;
                    $valueEnd = intval($arrData[0]["early_value_".$month]) + intval($arrData[0]["in_value_".$month]) - $valueOut;
                } else {
                    $stockEnd = $stockOut;
                    $valueEnd = $valueOut;
                }
            } else {
                $stockEnd = intval($arrData[0]["early_".$month]);
                $valueEnd = intval($arrData[0]["early_value_".$month]);
            }

            $endStockAffected = DB::table('stocks')
                        ->where('year', $year)
                        ->where('warehouse_id', $warehouseId)
                        ->where('product_id', $productId)
                        ->update([
                            $endStockField => $stockEnd,
                            $endValueField => $valueEnd,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);

            if($endStockAffected > 0) { $endStockResponse = 'End stock updated'; }

            DB::commit();
            $arrStockAfter = DB::table('stocks')->select('*')
                            ->where('year', $year)
                            ->where('warehouse_id', $warehouseId)
                            ->where('product_id', $productId)
                            ->get();

            $response = [
                'message' => [
                    'early_stock_response' => $earlyStockResponse,
                    'in_stock_response' => $inStockResponse,
                    'out_stock_response' => $outStockResponse,
                    'end_stock_response' => $endStockResponse
                ],
                'status' => [
                    'code' => 201,
                    'description' => 'updated'
                ],
                'results' => $arrStockAfter
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e) {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    public function getArray($dataStocks)
    {
        $index = 0;
        $arrStock = [];
        foreach($dataStocks as $stock) {
            $arrStock[$index] = [
                "year" => $stock->year,
                "warehouse_id" => $stock->warehouse_id,
                "product_id" => $stock->product_id,
                "early_01" => $stock->early_01,
                "early_value_01" => $stock->early_value_01,
                "in_01" => $stock->in_01,
                "in_value_01" => $stock->in_value_01,
                "out_01" => $stock->out_01,
                "out_value_01" => $stock->out_value_01,
                "end_01" => $stock->end_01,
                "end_value_01" => $stock->end_value_01,
                "early_02" => $stock->early_02,
                "early_value_02" => $stock->early_value_02,
                "in_02" => $stock->in_02,
                "in_value_02" => $stock->in_value_02,
                "out_02" => $stock->out_02,
                "out_value_02" => $stock->out_value_02,
                "end_02" => $stock->end_02,
                "end_value_02" => $stock->end_value_02,
                "early_03" => $stock->early_03,
                "early_value_03" => $stock->early_value_03,
                "in_03" => $stock->in_03,
                "in_value_03" => $stock->in_value_03,
                "out_03" => $stock->out_03,
                "out_value_03" => $stock->out_value_03,
                "end_03" => $stock->end_03,
                "end_value_03" => $stock->end_value_03,
                "early_04" => $stock->early_04,
                "early_value_04" => $stock->early_value_04,
                "in_04" => $stock->in_04,
                "in_value_04" => $stock->in_value_04,
                "out_04" => $stock->out_04,
                "out_value_04" => $stock->out_value_04,
                "end_04" => $stock->end_04,
                "end_value_04" => $stock->end_value_04,
                "early_05" => $stock->early_05,
                "early_value_05" => $stock->early_value_05,
                "in_05" => $stock->in_05,
                "in_value_05" => $stock->in_value_05,
                "out_05" => $stock->out_05,
                "out_value_05" => $stock->out_value_05,
                "end_05" => $stock->end_05,
                "end_value_05" => $stock->end_value_05,
                "early_06" => $stock->early_06,
                "early_value_06" => $stock->early_value_06,
                "in_06" => $stock->in_06,
                "in_value_06" => $stock->in_value_06,
                "out_06" => $stock->out_06,
                "out_value_06" => $stock->out_value_06,
                "end_06" => $stock->end_06,
                "end_value_06" => $stock->end_value_06,
                "early_07" => $stock->early_07,
                "early_value_07" => $stock->early_value_07,
                "in_07" => $stock->in_07,
                "in_value_07" => $stock->in_value_07,
                "out_07" => $stock->out_07,
                "out_value_07" => $stock->out_value_07,
                "end_07" => $stock->end_07,
                "end_value_07" => $stock->end_value_07,
                "early_08" => $stock->early_08,
                "early_value_08" => $stock->early_value_08,
                "in_08" => $stock->in_08,
                "in_value_08" => $stock->in_value_08,
                "out_08" => $stock->out_08,
                "out_value_08" => $stock->out_value_08,
                "end_08" => $stock->end_08,
                "end_value_08" => $stock->end_value_08,
                "early_09" => $stock->early_09,
                "early_value_09" => $stock->early_value_09,
                "in_09" => $stock->in_09,
                "in_value_09" => $stock->in_value_09,
                "out_09" => $stock->out_09,
                "out_value_09" => $stock->out_value_09,
                "end_09" => $stock->end_09,
                "end_value_09" => $stock->end_value_09,
                "early_10" => $stock->early_10,
                "early_value_10" => $stock->early_value_10,
                "in_10" => $stock->in_10,
                "in_value_10" => $stock->in_value_10,
                "out_10" => $stock->out_10,
                "out_value_10" => $stock->out_value_10,
                "end_10" => $stock->end_10,
                "end_value_10" => $stock->end_value_10,
                "early_11" => $stock->early_11,
                "early_value_11" => $stock->early_value_11,
                "in_11" => $stock->in_11,
                "in_value_11" => $stock->in_value_11,
                "out_11" => $stock->out_11,
                "out_value_11" => $stock->out_value_11,
                "end_11" => $stock->end_11,
                "end_value_11" => $stock->end_value_11,
                "early_12" => $stock->early_12,
                "early_value_12" => $stock->early_value_12,
                "in_12" => $stock->in_12,
                "in_value_12" => $stock->in_value_12,
                "out_12" => $stock->out_12,
                "out_value_12" => $stock->out_value_12,
                "end_12" => $stock->end_12,
                "end_value_12" => $stock->end_value_12,
                "created_at" => $stock->created_at,
                "updated_at" => $stock->updated_at
            ];

            $index++;
        }

        return $arrStock;
    }

}