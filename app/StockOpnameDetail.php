<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOpnameDetail extends Model
{
    protected $table = "stock_opname_details";

    protected $fillable = ['stock_opname_id','product_id','qty_real','qty_program'];
}
