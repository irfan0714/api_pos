<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstCashier extends Model
{
    protected $guarded = [];
    
    protected $casts = [    
        'counter_id' => 'integer'
    ];
}
