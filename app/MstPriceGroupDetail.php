<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstPriceGroupDetail extends Model
{
    protected $table = "mst_price_group_details";

    protected $fillable = ['price_group_id','product_id','price'];
}
