<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptVoucher extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
