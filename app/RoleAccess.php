<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleAccess extends Model
{
    protected $table = 'role_access';
    protected $fillable = ['role_id', 'menu_id', 'view', 'read', 'read_all', 'create', 'update', 'delete', 'approve', 'download', 'created_by', 'updated_by'];
    
    // public function menu() {
    //     return $this->belongsTo(Menu::class);
    // }
}
