<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProduct extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
