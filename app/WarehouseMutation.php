<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseMutation extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
