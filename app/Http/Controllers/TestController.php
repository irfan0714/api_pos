<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\ExistingSalesTransactions;
use App\ExistingSalesTransactionDetails;
use App\ExistingSalesTransactionPayments;
use App\SalesTransactionDetails;
use App\SalesTransactionPayments;
use App\MstCashier;
use App\MstCounter;
use App\MstWarehouse;
use App\Customer;
use App\CustomerType;
use Illuminate\Support\Facades\Validator;
use App\MstBranch;
use App\MstPaymentMethodTypes;
use App\MstPaymentMethods;
use App\MstEdcMachine;
use App\User;

class TestController extends Controller
{
    public function testBcrypt($string) {
        $result = md5($string);
        return  response()->json([$result], 200);
    }

    public function testStrRandom() {
        $random = strtolower(Str::random(6));
        return  response()->json([$random], 200);

    }

    public function printQuery() {
        DB::enableQueryLog(); 

        dd(DB::getQueryLog());
    }
    
    public function bcryptString($string) {
        $data = bcrypt($string);
        $response = [
            'message' => 'Becrypt Password',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);

    }

    public function testCron()
    {
        echo "test cron success"."\n";
    }

    public function testDataReseller() {
        $resellerData = DB::table('resellers')->get();
        foreach ($resellerData as $key => $value) {
            if($value->ttl != '') {
                $string = str_replace(' ', '', $value->ttl);
                $exp1 = explode(',',$string);
                $tempat = $exp1[0];
                if(isset($exp1[1])) {
                    $exp2 = explode('-',$exp1[1]);
                    $dd = isset($exp2[0]) ? $exp2[0] : '00';
                    $mm = isset($exp2[1]) ? $exp2[1] : '00';
                    $yy = isset($exp2[2]) ? $exp2[2] : '0000';
                    if($dd != '00' AND $mm != '00' AND $yy != '0000') {
                        $tglLahir = $yy.'-'.$mm.'-'.$dd;
                        DB::table('resellers')
                        ->where('kode_voucher', $value->kode_voucher)
                        ->update(['tempat_lahir' => $tempat, 'tgl_lahir' => $tglLahir]);
                    }
                }
            }
        }
        echo "selesai";
    }

    public function testInjectDataReseller() {
        $resellerData = DB::table('resellers')->get();
        foreach ($resellerData as $key => $value) {
            $cekData = DB::table('customers')->where(['referral_code'=>$value->kode_voucher])->get();
            if(count($cekData) == 0){
                DB::table('customers')->insert([
                        'customer_type_id' => 2,
                        'referral_code' => $value->kode_voucher,
                        'customer_name' => $value->nama,
                        'identity_number' => $value->ktp,
                        'phone' => $value->no_hp,
                        'birth_place' => $value->tempat_lahir,
                        'birth_date' => $value->tgl_lahir,
                        'gender' => $value->jk,
                        'profession' => $value->pekerjaan,
                        'area_code' => $value->kode_area,
                        'area' => $value->area,
                        'address' => $value->alamat,
                        'map_location' => $value->map_location,
                        'join_date' => $value->tgl_join,
                        'internal_employee' => ($value->karyawan_vci == 'Ya') ? 1 : 0
                    ]
                );
            }else {
                DB::table('customers')
                        ->where('referral_code', $value->kode_voucher)
                        ->update([
                            'customer_type_id' => 2,
                            'referral_code' => $value->kode_voucher,
                            'customer_name' => $value->nama,
                            'identity_number' => $value->ktp,
                            'phone' => $value->no_hp,
                            'birth_place' => $value->tempat_lahir,
                            'birth_date' => $value->tgl_lahir,
                            'gender' => $value->jk,
                            'profession' => $value->pekerjaan,
                            'area_code' => $value->kode_area,
                            'area' => $value->area,
                            'address' => $value->alamat,
                            'map_location' => $value->map_location,
                            'join_date' => $value->tgl_join,
                            'internal_employee' => ($value->karyawan_vci == 'Ya') ? 1 : 0
                        ]);
            }
        }
        echo "selesai";
    }
    
    public function mstBranch($id='') {
        if(!empty($id)) {
            $mstBranch = mstBranch::select('id', 'branch_name', 'active')
                                  ->orderBy('id', 'asc')
                                  ->where('id', $id)
                                  ->get();
        }else {
            $mstBranch = mstBranch::select('id', 'branch_name', 'active')
                                  ->orderBy('id', 'asc')
                                  ->get();
        }
        $response = [
            'message' => 'List of master branches',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstBranch
        ];
        return response()->json($response, 200);
    }

    public function mstCounter($id='') {
        if(!empty($id)) {
            $mstCounter = MstCounter::select('mst_counters.id',
                                             'mst_counters.counter_name',
                                             'mst_counters.counter_code',
                                             'mst_counters.branch_id',
                                             'mst_branches.branch_name',
                                             'mst_counters.active')
                                    ->join('mst_branches', 'mst_counters.branch_id', '=', 'mst_branches.id')
                                    ->where('mst_counters.id', $id)
                                    ->get();
        }else {
            $mstCounter = MstCounter::select('mst_counters.id',
                                             'mst_counters.counter_name',
                                             'mst_counters.counter_code',
                                             'mst_counters.branch_id',
                                             'mst_branches.branch_name',
                                             'mst_counters.active')
                                    ->join('mst_branches', 'mst_counters.branch_id', '=', 'mst_branches.id')
                                    ->orderBy('mst_counters.id', 'asc')
                                    ->get();
        }
        $response = [
            'message' => 'List of master counters',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstCounter
        ];
        return response()->json($response, 200);
    }

    public function mstCashier($id='', Request $request) {
        if($request->get('counter_code')) {
            $mstCashier = MstCashier::select('mst_cashiers.id',
                                                 'mst_cashiers.cashier_name',
                                                 'mst_cashiers.counter_id',
                                                 'mst_counters.counter_name',
                                                 'mst_counters.counter_code')
                                        ->join('mst_counters', 'mst_cashiers.counter_id', '=', 'mst_counters.id')
                                        ->where('mst_counters.counter_code', $request->get('counter_code'))
                                        ->get();
        }else {
            if(!empty($id)) {
                $mstCashier = MstCashier::select('mst_cashiers.id',
                                                 'mst_cashiers.cashier_name',
                                                 'mst_cashiers.counter_id',
                                                 'mst_counters.counter_name',
                                                 'mst_counters.counter_code')
                                        ->join('mst_counters', 'mst_cashiers.counter_id', '=', 'mst_counters.id')
                                        ->where('mst_cashiers.id', $id)
                                        ->get();
            }else {
                $mstCashier = MstCashier::select('mst_cashiers.id',
                                                 'mst_cashiers.cashier_name',
                                                 'mst_cashiers.counter_id',
                                                 'mst_counters.counter_name',
                                                 'mst_counters.counter_code')
                                        ->join('mst_counters', 'mst_cashiers.counter_id', '=', 'mst_counters.id')
                                        ->orderBy('mst_cashiers.id', 'asc')
                                        ->get();
            }

        }

        $response = [
            'message' => 'List of master cashiers',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstCashier
        ];
        return response()->json($response, 200);
    }

    public function mstWarehouse($id='') {
        if(!empty($id)) {
            $mstWarehouse = MstWarehouse::select('mst_warehouses.id',
                                                 'mst_warehouses.warehouse_name',
                                                 'mst_warehouses.counter_id',
                                                 'mst_counters.counter_name')
                                        ->join('mst_counters', 'mst_warehouses.counter_id', '=', 'mst_counters.id')
                                        ->where('mst_warehouses.id', $id)
                                        ->get();
        }else {
            $mstWarehouse = MstWarehouse::select('mst_warehouses.id',
                                                 'mst_warehouses.warehouse_name',
                                                 'mst_warehouses.counter_id',
                                                 'mst_counters.counter_name')
                                        ->join('mst_counters', 'mst_warehouses.counter_id', '=', 'mst_counters.id')
                                        ->orderBy('mst_warehouses.id', 'asc')
                                        ->get();
        }
        $response = [
            'message' => 'List of master warehouses',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstWarehouse
        ];
        return response()->json($response, 200);
    }

    public function mstCustomer($id='', Request $request) {
        DB::enableQueryLog(); 
        if($request->get('phone')) {
            $mstCustomer = Customer::select('*')
                                    ->where('phone', $request->get('phone'))
                                    ->get();
        }else if($request->get('referral_code')) {
            $mstCustomer = Customer::select('*')
                                    ->where('referral_code', $request->get('referral_code'))
                                    ->get();
        }else {
            if(!empty($id)) {
                $mstCustomer = Customer::select('*')
                                        ->where('id', $id)
                                        ->get();
            }else {
                if($request->get('limit') || $request->get('offset')) {
                    $mstCustomer = Customer::select('*')
                                            ->orderBy('id', 'asc')
                                            ->limit($request->get('limit'))
                                            ->offset($request->get('offset'))
                                            ->get();

                }else {
                    $mstCustomer = Customer::select('*')
                                            ->orderBy('id', 'asc')
                                            ->get();

                }
            }
        }
        $response = [
            'message' => 'List of master customers',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstCustomer
        ];
        return response()->json($response, 200);
    }

    public function mstCustomerType($id='') {
        if(!empty($id)) {
            $mstCustomerType = CustomerType::select('id', 'customer_type_name', 'active')
                                           ->where('id', $id)
                                           ->get();
        }else {
            $mstCustomerType = CustomerType::select('id', 'customer_type_name', 'active')
                                           ->orderBy('id', 'asc')
                                           ->get();
        }
        $response = [
            'message' => 'List of master customer types',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstCustomerType
        ];
        return response()->json($response, 200);
    }

    public function mstUser($id='', Request $request) {
        if(!empty($id)) {
            $mstUser = User::select('*')
                           ->where('id', $id)
                           ->get();
        }else {
            $mstUser = User::select('*')
                           ->orderBy('id', 'asc')
                           ->get();
        }
        $response = [
            'message' => 'List of master users',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstUser
        ];
        return response()->json($response, 200);
    }

    public function mstPaymentMethodType($id='') {
        if(!empty($id)) {
            $mstPaymentMethodType = MstPaymentMethodTypes::select('id', 'payment_type_name')
                                                         ->where('id', $id)
                                                         ->get();
        }else {
            $mstPaymentMethodType = MstPaymentMethodTypes::select('id', 'payment_type_name')
                                                         ->orderBy('id', 'asc')
                                                         ->get();
        }
        $response = [
            'message' => 'List of master payment method types',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstPaymentMethodType
        ];
        return response()->json($response, 200);
    }

    public function mstPaymentMethod($id='') {
        if(!empty($id)) {
            $mstPaymentMethod = MstPaymentMethods::select('mst_payment_methods.id',
                                                                  'mst_payment_methods.payment_name',
                                                                  'mst_payment_methods.payment_method_type_id',
                                                                  'mst_payment_method_types.payment_type_name')
                                                 ->join('mst_payment_method_types', 'mst_payment_methods.payment_method_type_id', '=', 'mst_payment_method_types.id')
                                                 ->where('mst_payment_methods.id', $id)
                                                 ->get();
        }else {
            $mstPaymentMethod = MstPaymentMethods::select('mst_payment_methods.id',
                                                                  'mst_payment_methods.payment_name',
                                                                  'mst_payment_methods.payment_method_type_id',
                                                                  'mst_payment_method_types.payment_type_name')
                                                 ->join('mst_payment_method_types', 'mst_payment_methods.payment_method_type_id', '=', 'mst_payment_method_types.id')
                                                 ->orderBy('mst_payment_methods.id', 'asc')
                                                 ->get();
        }
        $response = [
            'message' => 'List of master payment methods',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstPaymentMethod
        ];
        return response()->json($response, 200);
    }

    public function mstEdcMechine($id='') {
        if(!empty($id)) {
            $mstEdcMechine = MstEdcMachine::select('id', 'edc_name', 'status AS active')
                                          ->where('id', $id)
                                          ->get();
        }else {
            $mstEdcMechine = MstEdcMachine::select('id', 'edc_name', 'status AS active')
                                          ->orderBy('id', 'asc')
                                          ->get();
        }
        $response = [
            'message' => 'List of master edc mechine',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $mstEdcMechine
        ];
        return response()->json($response, 200);
    }

    public function apiDocumentation() {
        $data = [
            [
                "sales_transactions" =>  [
                    "cashier_id" => "id kassa|number|required",
                    "user_id" => "id user kasir|number|required",
                    "counter_id" => "id counter|number|required",
                    "warehouse_id" => "Id Gudang|number|required",
                    "customer_id" => "Id Kustomer|number|required",
                    "gsa_id" => "null",
                    "agent_id" => "null",
                    "guide_id" => "null",
                    "receipt_no" => "no struk|string|required",
                    "trans_date" => "tanggal transaksi|string|required|format:yyyy-mm-dd",
                    "trans_time" => "waktu transaksi|string|required|format:h:i:s",
                    "total_item" => "total item|number|required|default:0",
                    "total_value" => "total nilai|number|required|default:0",
                    "total_payment" => "total bayar|number|required|default:0",
                    "discount" => "total diskon|number|required|default:0",
                    "change" => "kembalian|number|required|default:0",
                    "cash" => "tunai|number|",
                    "credit_card" => "kartu kredit|number|required|default:0",
                    "debit_card" => "kartu debit|number|required|default:0",
                    "transfer" => "transfer|number|required|default:0",
                    "voucher" => "voucher|number|required|default:0",
                    "member_point" => "member point moninal|number|required|default:0",
                    "reward" => "reward nominal|number|required|default:0",
                    "travel_voucher" => "voucher travel|number|required|default:0",
                    "tax_basis" => "DPP|number|required|default:0",
                    "vat" => "PPN|number|required|default:0",
                    "description" => "keterangan|string",
                    "discount_code" => "kode diskon|string",
                    "discount_value" => "nilai diskon|number|required|default:0",
                    "commission_status" => "status komisi|string|required|enum:['0','1']",
                    "commission_extra_status" => "status komisi extra|string|required|enum:['0','1']",
                    "foreign_currency_1" => "mata uang asing|number|required|default:0",
                    "exchange_rate_1" => "nilai kurs|number|required|default:0",
                    "currency_1" => "mata uang|string",
                    "foreign_currency_2" => "mata uang asing|number|required|default:0",
                    "exchange_rate_2" => "nilai kurs|number|required|default:0",
                    "currency_2" => "mata uang|string",
                    "foreign_currency_3" => "mata uang asing|number|required|default:0",
                    "exchange_rate_3" => "nilai kurs|number|required|default:0",
                    "currency_3" => "mata uang|string",
                    "currency_change" => "mata uang asing kembalian|string",
                    "change_rate_usd" => "kurs dolar kembalian|number|required|default:0",
                    "change_rate_cny" => "kurs yuan kembalian|number|required|default:0",
                    "change_fc_usd" => "nilai dolar kembalian|number|required|default:0",
                    "change_fc_cny" => "nilai yuan kembalian|number|required|default:0",
                    "desc_void" => "keterangan void|string",
                    "transaction_type" => "id type transaksi|string|required|enum:['1','2']|'1':transaksi reguler,'2':transaksi reseller",
                    "status" => "status transaksi|string|required|enum=['0','1','2']|'0':pending,'1':oke,'2':void",
                    "created_by" => "username|string|required",
                    "updated_by" => "username|string|required"
                ],
                "sales_transaction_details" =>  [
                    [
                        "product_id" => "pcode|string|required",
                        "qty" => "qty|number|required|default:0",
                        "free_qty" => "qty bonus|number|required|default:0",
                        "price" => "harga per item|number|required|default:0",
                        "percent_disc_1" => "angka persen diskon 1|number|required|default:0",
                        "disc_1" => "nilai diskon 1|number|required|default:0",
                        "percent_disc_2" => "angka persen diskon 2|number|required|default:0",
                        "disc_2" => "nilai diskon 2|number|required|default:0",
                        "percent_disc_3" => "angka persen diskon 3|number|required|default:0",
                        "disc_3" => "nilai diskon 3|number|required|default:0",
                        "percent_disc_4" => "angka persen diskon 3|number|required|default:0",
                        "disc_4" => "nilai diskon 4|number|required|default:0",
                        "net" => "netto|number|required|default:0",
                        "cogs" => "HPP|number|required|default:0",
                        "percent_vat" => "persen PPN|number|required|default:0",
                        "percent_commission" => "persen komisi|number|required|default:0",
                        "description" => "keterangan detail|string",
                        "status" => "status transaksi|string|required|enum=['0','1','2']|'0':pending,'1':oke,'2':void",
                        "created_by" => "username|string|required",
                        "updated_by" => "username|string|required"
                    ],
                    [
                        "product_id" => "pcode|string|required",
                        "qty" => "qty|number|required|default:0",
                        "free_qty" => "qty bonus|number|required|default:0",
                        "price" => "harga per item|number|required|default:0",
                        "percent_disc_1" => "angka persen diskon 1|number|required|default:0",
                        "disc_1" => "nilai diskon 1|number|required|default:0",
                        "percent_disc_2" => "angka persen diskon 2|number|required|default:0",
                        "disc_2" => "nilai diskon 2|number|required|default:0",
                        "percent_disc_3" => "angka persen diskon 3|number|required|default:0",
                        "disc_3" => "nilai diskon 3|number|required|default:0",
                        "percent_disc_4" => "angka persen diskon 3|number|required|default:0",
                        "disc_4" => "nilai diskon 4|number|required|default:0",
                        "net" => "netto|number|required|default:0",
                        "cogs" => "HPP|number|required|default:0",
                        "percent_vat" => "persen PPN|number|required|default:0",
                        "percent_commission" => "persen komisi|number|required|default:0",
                        "description" => "keterangan detail|string",
                        "status" => "status transaksi|string|required|enum=['0','1','2']|'0':pending,'1':oke,'2':void",
                        "created_by" => "username|string|required",
                        "updated_by" => "username|string|required"
                    ]
                ],
                "sales_transaction_payments" =>  [
                    [
                        "payment_method_id" => "id payment method|number|required",
                        "edc_id" => "id mesin edc|number",
                        "point" => "point|number|required|default:0",
                        "payment_value" => "nilai payment|number|required",
                        "voucher_code" => "kode voucher|string",
                        "exchange_rate" => "nilai kurs|number|required"
                    ],
                    [
                        "payment_method_id" => "id payment method|number|required",
                        "edc_id" => "id mesin edc|number",
                        "point" => "point|number|required|default:0",
                        "payment_value" => "nilai payment|number|required",
                        "voucher_code" => "kode voucher|string",
                        "exchange_rate" => "nilai kurs|number|required"
                    ]
                ]
            ]
        ];
        $response = [
            'message' => 'Api dokumentation',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        return response()->json($response, 200);
    }

    public function testCreateFile() {
        $myfile = fopen("logs/log-api.txt", "a+");
        $txt = "John Doe\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }
}
