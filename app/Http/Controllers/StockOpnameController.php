<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\StockOpname;
use App\StockOpnameDetail;
use App\MstProduct;
use App\MstPriceGroup;
use App\MstCounter;
use App\MstWarehouse;
use App\User;
use App\Stock;
use App\ApprovalHistory;
use App\StockMutation;
use App\Imports\StockOpnameDetailImport;
use MyHelper;

class StockOpnameController extends Controller
{
    protected $myHelper;

    public function __construct(MyHelper $myHelper) 
    {
        // $this->middleware('jwt.auth');
        $this->myHelper = $myHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $counterId = $request->input('counter_id') != 0 ? "stock_opnames.counter_id IN (".$request->input('counter_id').")" : 'stock_opnames.counter_id is not null';
        $data = StockOpname::join('mst_warehouses', 'mst_warehouses.id', '=', 'stock_opnames.warehouse_id')
                ->join('mst_counters', 'mst_counters.id', '=', 'stock_opnames.counter_id')
                ->select('stock_opnames.*', 'mst_counters.counter_name', 'mst_warehouses.warehouse_name')
                ->whereRaw($counterId)
                ->orderByDesc('stock_opnames.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of stock opname',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';
        
        $counterData = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->orderBy('warehouse_name', 'asc')
                         ->get();

        $response = [
            'message' => 'Create stock opname',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stockOpnameData = $request->input('stockOpname');
        $stockOpnameDetailData = $request->input('stockOpnameDetail');

        $counterId = $stockOpnameData["counter_id"];
        $stockOpnameId = $this->getLatestId($counterId);

        DB::beginTransaction();
        try
        {
            $stockOpname = new StockOpname();
            $stockOpname->id = $stockOpnameId;
            $stockOpname->counter_id = $stockOpnameData["counter_id"];
            $stockOpname->warehouse_id = $stockOpnameData["warehouse_id"];
            $stockOpname->doc_date = $stockOpnameData["doc_date"];
            $stockOpname->desc = $stockOpnameData["desc"];
            $stockOpname->status = $stockOpnameData["status"];
            $stockOpname->created_by = $stockOpnameData["created_by"];
            $stockOpname->save();

            $arrProductId = $stockOpnameDetailData["product_id"];
            $arrQtyReal = $stockOpnameDetailData["qty_real"];
            $arrQtyProgram = $stockOpnameDetailData["qty_program"];
            $arrPrice = $stockOpnameDetailData["price"];

            if (count($arrProductId) > 0) {
                StockOpnameDetail::where(['stock_opname_id'=> $stockOpnameId])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $stockOpnameDetail = new StockOpnameDetail();
                    $stockOpnameDetail->stock_opname_id = $stockOpnameId; 
                    $stockOpnameDetail->product_id = $arrProductId[$index];
                    $stockOpnameDetail->qty_real = intval($arrQtyReal[$index]);
                    $stockOpnameDetail->qty_program = intval($arrQtyProgram[$index]);
                    $stockOpnameDetail->price = intval($arrPrice[$index]);
                    $stockOpnameDetail->save();
                }
            }

            DB::commit();
            $response = [
                'message' => 'Stock opname created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $stockOpname
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $counterData = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();
                       
        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->orderBy('warehouse_name', 'asc')
                         ->get();

        $stockOpnameData = StockOpname::select('*')->where('id', $id)->get();
        $stockOpnameDetailData = StockOpnameDetail::join('mst_products', 'mst_products.id', '=', 'stock_opname_details.product_id')
                                ->selectRaw('stock_opname_details.*, mst_products.product_name, mst_products.barcode, mst_products.unit, mst_products.price')
                                ->whereRaw("stock_opname_details.stock_opname_id = '".$id."'")
                                ->get();

        $arrStockOpname = $stockOpnameData->toArray();
        if(count($arrStockOpname) > 0) {
            $idCounter = $arrStockOpname[0]["counter_id"];
            $idWarehouse = $arrStockOpname[0]["warehouse_id"];
        } else {
            $idCounter = 0;
            $idWarehouse = 0;
        }

        $priceGroup = MstPriceGroup::select('*')
                      ->where('counter_id', $idCounter)
                      ->where('price_group_types', 'RG')
                      ->first();

        $priceGroupId = $priceGroup != null ? $priceGroup->id : 0;
        if($priceGroupId != 0) {
            $first = MstProduct::leftJoin('mst_price_group_details', 'mst_products.id', '=', 'mst_price_group_details.product_id')
                    ->selectRaw("mst_products.id, mst_products.barcode, mst_products.product_name,
                    mst_products.initial_name, mst_products.price AS mst_product_price, mst_price_group_details.price AS group_detail_price,
                    IF(mst_price_group_details.product_id IS NULL, mst_products.price, mst_price_group_details.price) AS price, mst_products.unit")
                    ->where('mst_price_group_details.price_group_id', $priceGroup->id);

            $second = MstProduct::selectRaw("mst_products.id, mst_products.barcode, mst_products.product_name,
                      mst_products.initial_name, mst_products.price AS mst_product_price,
                      mst_products.price AS group_detail_price, mst_products.price AS price, mst_products.unit")
                      ->whereRaw("mst_products.id NOT IN (SELECT product_id
                                                          FROM mst_price_group_details
                                                          WHERE mst_price_group_details.price_group_id = '".$priceGroup->id."')");

            $productData = $first->union($second)->get();
        } else {
            $productData = MstProduct::select('id', 'barcode', 'product_name', 'initial_name', 'unit', 'price')
                           ->orderBy('id', 'asc')
                           ->get();
        }

        $stockField = $request->input('stock_field');
        $year = date("Y");
        $stockData = Stock::select("product_id", "".$stockField." as end_stock")
                    ->whereRaw($idWarehouse)
                    ->where('year', $year)
                    ->get();
                    
        $arrStock = $stockData->toArray();
        $productWithStockData = array();
        if(count($arrStock) > 0) {
            foreach($productData as $product) {
                $qtyProgram = $this->findInArrayStock($product->id, $stockData);
                $data = [
                    'id' => $product->id,
                    'barcode' => $product->barcode,
                    'product_name' => $product->product_name,
                    'initial_name' => $product->initial_name,
                    'unit' => $product->unit,
                    'price' => $product->price,
                    'qty_program' => $qtyProgram
                ];
                array_push($productWithStockData, $data);
            }
        } else {
            foreach($productData as $product) {
                $data = [
                    'id' => $product->id,
                    'barcode' => $product->barcode,
                    'product_name' => $product->product_name,
                    'initial_name' => $product->initial_name,
                    'unit' => $product->unit,
                    'price' => $product->price,
                    'qty_program' => 0
                ];
                
                array_push($productWithStockData, $data);
            }
        }

        foreach($stockOpnameData as $opname) {
            $createdBy = $opname->created_by ? $opname->created_by : null;
            $updatedBy = $opname->updated_by ? $opname->updated_by : null;
        }

        $userCreatedData = null;
        $userUpdatedData = null;
        if(isset($createdBy)) {
            $userCreatedData = User::select('*')->where('username', $createdBy)->get();
        }

        if(isset($updatedBy)) {
            $userUpdatedData = User::select('*')->where('username', $updatedBy)->get();
        }

        $approvalHistoryData = ApprovalHistory::join('users', 'users.username', '=', 'approval_histories.username')
            ->select('approval_histories.*', 'users.name')
            ->whereRaw("approval_histories.transaction_id = '".$id."'")
            ->get();

        $response = [
            'message' => 'Stock opname details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData,
                'stock_opname_data' => $stockOpnameData,
                'stock_opname_detail_data' => $stockOpnameDetailData,
                'product_data' => $productWithStockData,
                'user_create_data' => $userCreatedData,
                'user_update_data' => $userUpdatedData,
                'approval_history_data' => $approvalHistoryData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stockOpnameData = $request->input('stockOpname');
        $stockOpnameDetailData = $request->input('stockOpnameDetail');
        $approvalHistoryData = $request->input('approvalHistory');

        DB::beginTransaction();
        try
        {
            $stockOpname = StockOpname::find($id);
            $stockOpname->counter_id = $stockOpnameData["counter_id"];
            $stockOpname->warehouse_id = $stockOpnameData["warehouse_id"];
            $stockOpname->doc_date = $stockOpnameData["doc_date"];
            $stockOpname->desc = $stockOpnameData["desc"];
            $stockOpname->status = $stockOpnameData["status"];
            $stockOpname->updated_by = $stockOpnameData["updated_by"];
            $stockOpname->save();

            $arrStockOpnameId = $stockOpnameDetailData["stock_opname_id"];
            $arrProductId = $stockOpnameDetailData["product_id"];
            $arrQtyReal = $stockOpnameDetailData["qty_real"];
            $arrQtyProgram = $stockOpnameDetailData["qty_program"];
            $arrPrice = $stockOpnameDetailData["price"];

            if (count($arrProductId) > 0) {
                StockOpnameDetail::where(['stock_opname_id'=> $id])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $stockOpnameDetail = new StockOpnameDetail();
                    $stockOpnameDetail->stock_opname_id = $arrStockOpnameId[$index];
                    $stockOpnameDetail->product_id = $arrProductId[$index];
                    $stockOpnameDetail->qty_real = intval($arrQtyReal[$index]);
                    $stockOpnameDetail->qty_program = intval($arrQtyProgram[$index]);
                    $stockOpnameDetail->price = intval($arrPrice[$index]);
                    $stockOpnameDetail->save();

                    $qtyDiffRealAndProgram = 0;
                    $mutationType = 'X';

                    if(intval($arrQtyReal[$index]) > intval($arrQtyProgram[$index])) {
                        $qtyDiffRealAndProgram = intval($arrQtyReal[$index]) - intval($arrQtyProgram[$index]);
                        $mutationType = 'I';
                    }
                    
                    if(intval($arrQtyReal[$index]) < intval($arrQtyProgram[$index])) {
                        $qtyDiffRealAndProgram = intval($arrQtyProgram[$index]) - intval($arrQtyReal[$index]);
                        $mutationType = 'O';
                    }

                    if($mutationType != 'X') {
                        $stockMutation = new StockMutation;
                        $stockMutation->warehouse_id = $stockOpnameData["warehouse_id"];
                        $stockMutation->transaction_id = $id;
                        $stockMutation->stock_mutation_type_id = 'SO';
                        $stockMutation->product_id = $arrProductId[$index];
                        $stockMutation->qty = $qtyDiffRealAndProgram;
                        $stockMutation->value = 0;
                        $stockMutation->stock_move = $mutationType;
                        $stockMutation->trans_date = date('Y-m-d');
                        $stockMutation->save();

                        $docDate = explode('-', $stockOpnameData["doc_date"]);
                        $year = $docDate[0];
                        $month = $docDate[1];

                        $paramManageStock = array(
                            'year'=> $year,
                            'month'=> $month,
                            'warehouseId'=> $stockOpnameData["warehouse_id"],
                            'productId'=> $arrProductId[$index],
                            'qty'=> $qtyDiffRealAndProgram,
                            'value'=> 0,
                            'mutationType'=> $mutationType
                        );
                
                        $response = $this->myHelper->stockManagement($paramManageStock);
                    }
                }
            }

            if($stockOpnameData["status"] == '1') {
                $approvalHistory = new ApprovalHistory();
                $approvalHistory->transaction_id = $approvalHistoryData["transaction_id"];
                $approvalHistory->level = 0;
                $approvalHistory->username = $approvalHistoryData["username"];
                $approvalHistory->status = $approvalHistoryData["status"];
                $approvalHistory->trans_date = $approvalHistoryData["trans_date"];
                $approvalHistory->save();
            }

            DB::commit();
            $response = [
                'message' => 'Stock opname updated',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $stockOpname
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getLatestId($counterId)
    {
        $year = date('y');
        $month = date('m');

        $data = StockOpname::select('id')
                ->where('counter_id', $counterId)
                ->orderByDesc('created_at')
                ->get();

        $arrData = $data->toArray();
        if(count($arrData) > 0) {
            $latestStockOpnameId = $arrData[0]["id"];
            $splitId = explode("-", $latestStockOpnameId);
            $lastCounter = substr($splitId[0], 6, 3);
            $lastMonth = intval(substr($splitId[0], 4, 2));
            $newCounter = intval($lastCounter) + 1;
            if($lastMonth === intval($month)) {
                if($newCounter <= 9) {
                    $newCounter = '00' . strval($newCounter); 
                }
                if($newCounter > 9 && $newCounter <= 99) { 
                    $newCounter = '0' . strval($newCounter); 
                }
            } else {
              $newCounter = '001';
            }
      
            $stockOpnameId = 'SO' . strval($year) . strval($month) . $newCounter . '-' . $splitId[1];
        } else {
            $newCounterId;
            if(intval($counterId) <= 9) {
                $newCounterId = '00' . strval($counterId); 
            }
            if(intval($counterId) > 9 && intval($counterId) <= 99) { 
                $newCounterId = '0' . strval($counterId); 
            }
            $stockOpnameId = 'SO' . strval($year) . strval($month) . '001' . '-' . $newCounterId;
        }

        return $stockOpnameId;
    }

    private function findInArrayStock($id, $stockData)
    {
        $result = 0;
        foreach($stockData as $stock) {
            if ($id == $stock->product_id) {
                $result = $stock->end_stock;
            }
        }

        return $result;
    }

    public function uploadFile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'counter_id' => 'required|numeric',
            'warehouse_id' => 'required|numeric',
            'doc_date' => 'required',
            'desc' => 'required',
            'status' => 'required',
            'created_by' => 'required',
            'file' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        }

        DB::beginTransaction();
        try
        {
            $stockOpname = new StockOpname();
            $stockOpname->id = $request->input('id');
            $stockOpname->counter_id = $request->input('counter_id');
            $stockOpname->warehouse_id = $request->input('warehouse_id');
            $stockOpname->doc_date = $request->input('doc_date');
            $stockOpname->desc = $request->input('desc');
            $stockOpname->status = $request->input('status');
            $stockOpname->created_by = $request->input('created_by');
            $stockOpname->save();

            $file = $request->file('file');
            $fileName = rand().$file->getClientOriginalName();
            $file->move('stock_opname_file', $fileName);
            Excel::import(new StockOpnameDetailImport, public_path('/stock_opname_file/'.$fileName));

            DB::commit();
            $response = [
                'message' => 'Stock opname created',
                'status' => [
                    'code' => 200,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $stockOpname
                ]
            ];

            return response()->json($response, 200);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }

        $response = [
            'message' => 'Stock opname created',
            'status' => [
                'code' => 200,
                'description' => 'Created'
            ],
            'results' => [
                'data' => []
            ]
        ];

        return response()->json($response, 200);
    }

    public function getDetailProductSO(Request $request) 
    {
        $warehouseId = $request->input('warehouse_id') ? "warehouse_id = '".$request->input('warehouse_id')."'" : 'warehouse_id is not null';
        $stockField = $request->input('stock_field');
        $counterId = $request->input('counter_id');
        $year = date("Y");
        $stockOpnameData = array();
        
        $stockOpnameId = $this->getLatestId($counterId);

        $priceGroup = MstPriceGroup::select('*')
                      ->where('counter_id', $counterId)
                      ->where('price_group_types', 'RG')
                      ->where('active', '1')
                      ->first();

        $priceGroupId = $priceGroup != null ? $priceGroup->id : 0;
        $q1 = '';
        if($priceGroupId != 0) {
            DB::enableQueryLog(); 
            // $first = MstProduct::leftJoin('mst_price_group_details', 'mst_products.id', '=', 'mst_price_group_details.product_id')
            //         ->selectRaw("mst_products.id, mst_products.barcode, mst_products.product_name, mst_products.unit,
            //         mst_products.initial_name, mst_products.price AS mst_product_price, mst_price_group_details.price AS group_detail_price,
            //         IF(mst_price_group_details.product_id IS NULL, mst_products.price, mst_price_group_details.price) AS price")
            //         ->where('mst_price_group_details.price_group_id', $priceGroup->id)
            //         ->orderBy('mst_products.id', 'ASC');

            // $second = MstProduct::selectRaw("mst_products.id, mst_products.barcode, mst_products.product_name,
            //           mst_products.initial_name, mst_products.price AS mst_product_price, mst_products.unit,
            //           mst_products.price AS group_detail_price, mst_products.price AS price")
            //           ->whereRaw("mst_products.id NOT IN (SELECT product_id
            //                                               FROM mst_price_group_details
            //                                               WHERE mst_price_group_details.price_group_id = '".$priceGroup->id."')")
            //           ->orderBy('mst_products.id', 'ASC');

            // $productData = $first->union($second)->get();

            $productData = DB::select("
                SELECT * FROM (
                    (SELECT
                    mst_products.id,
                    mst_products.barcode,
                    mst_products.product_name,
                    mst_products.unit,
                    mst_products.initial_name,
                    mst_products.price AS mst_product_price,
                    mst_price_group_details.price AS group_detail_price,
                    IF(
                        mst_price_group_details.product_id IS NULL,
                        mst_products.price,
                        mst_price_group_details.price
                    ) AS price
                    FROM
                    `mst_products`
                    LEFT JOIN `mst_price_group_details`
                        ON `mst_products`.`id` = `mst_price_group_details`.`product_id`
                    WHERE `mst_price_group_details`.`price_group_id` = '".$priceGroup->id."')
                    UNION
                    (SELECT
                    mst_products.id,
                    mst_products.barcode,
                    mst_products.product_name,
                    mst_products.initial_name,
                    mst_products.price AS mst_product_price,
                    mst_products.unit,
                    mst_products.price AS group_detail_price,
                    mst_products.price AS price
                    FROM
                    `mst_products`
                    WHERE mst_products.id NOT IN
                    (SELECT
                        product_id
                    FROM
                        mst_price_group_details
                    WHERE mst_price_group_details.price_group_id = '".$priceGroup->id."'))
                    ) AS tabel ORDER BY id ASC                
            ");
            $q1 = DB::getQueryLog();
        } else {

            $productData = MstProduct::select('id', 'barcode', 'product_name', 'initial_name', 'unit', 'price')
                           ->orderBy('id', 'ASC')
                           ->get();

        }
        
        $stockData = Stock::select("product_id", "".$stockField." as end_stock")
                    ->whereRaw($warehouseId)
                    ->where('year', $year)
                    ->get();
                    
        $arrStock = $stockData->toArray();
       
        if(count($arrStock) > 0) {
            foreach($productData as $product) {
                $qtyProgram = $this->findInArrayStock($product->id, $stockData);
                $data = [
                    'id' => $product->id,
                    'barcode' => $product->barcode,
                    'product_name' => $product->product_name,
                    'initial_name' => $product->initial_name,
                    'unit' => $product->unit,
                    'price' => $product->price,
                    'qty_program' => $qtyProgram
                ];
                array_push($stockOpnameData, $data);
            }
        } else {
            foreach($productData as $product) {
                $data = [
                    'id' => $product->id,
                    'barcode' => $product->barcode,
                    'product_name' => $product->product_name,
                    'initial_name' => $product->initial_name,
                    'unit' => $product->unit,
                    'price' => $product->price,
                    'qty_program' => 0
                ];
                
                array_push($stockOpnameData, $data);
            }
        }

        $response = [
            'q1' => $q1,
            'message' => 'Detail product for stock opname',
            'status' => [
                'code' => 200,
                'description' => 'Created'
            ],
            'results' => $stockOpnameData,
            'stock_opname_id' => $stockOpnameId
        ];

        return response()->json($response, 200);
    }

    public function getDetailSOForPrint(Request $request)
    {
        $stockOpnameId = $request->input('stock_opname_id');

        $stockOpnameDetail = StockOpnameDetail::join('stock_opnames', 'stock_opname_details.stock_opname_id', '=', 'stock_opnames.id')
                             ->join('mst_products', 'stock_opname_details.product_id', '=', 'mst_products.id')
                             ->join('mst_counters', 'stock_opnames.counter_id', '=', 'mst_counters.id')
                             ->join('mst_warehouses', 'stock_opnames.warehouse_id', '=', 'mst_warehouses.id')
                             ->select('stock_opname_details.stock_opname_id', 'stock_opnames.doc_date', 'mst_counters.counter_name', 'mst_warehouses.warehouse_name', 
                             'stock_opname_details.product_id', 'mst_products.product_name', 'stock_opname_details.qty_real AS qty_physics',
                             'stock_opname_details.qty_program', 'stock_opname_details.price')
                             ->where('stock_opnames.id', $stockOpnameId)
                             ->get();

        $response = [
            'message' => 'Stock opname for print',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $stockOpnameDetail
        ];
    
        return response()->json($response, 200);
    }
}
