<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\MstCounter;
use App\MstWarehouse;
use App\ProductRequest;
use App\ProductRequestDetail;

class ReportProductRequestController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                       ->whereRaw($counterIdforWarehouse)
                       ->where('active', '1')
                       ->orderBy('warehouse_name', 'asc')
                       ->get();
                    
        $response = [
            'message' => 'Product request report index',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData
            ]
        ];

        return response()->json($response, 200);
    }

    public function getReportProductRequest(Request $request)
    {
        DB::enableQueryLog();
        $productRequestId = $request->input('product_request_id');
        $counterId = $request->input('counter_id');
        $warehouseId = $request->input('warehouse_id');
        $docDateStart = $request->input('doc_date_start');
        $docDateEnd = $request->input('doc_date_end');
        $needDateStart = $request->input('need_date_start');
        $needDateEnd = $request->input('need_date_end');

        $warehouseIdList = '';
        if($warehouseId == '0') {
            $warehouseData = MstWarehouse::select('id')
                             ->where('counter_id', $counterId)
                             ->where('active', '1')
                             ->get();
                            
            if(count($warehouseData->toArray()) > 0) {
                foreach($warehouseData as $warehouse) {
                    if($warehouseIdList == '') {
                        $warehouseIdList = $warehouse->id;
                    } else {
                        $warehouseIdList = $warehouseIdList . ',' . $warehouse->id;
                    }
                }
            }
        }

        $whereProductRequestId = $productRequestId != 'null' ? "product_requests.id = '".$productRequestId."'" : 'product_requests.id is not null';
        $whereCounterId = "product_requests.counter_id = '".$counterId."'";
        $whereWarehouseId = $warehouseId == '0' ? "product_requests.warehouse_id IN (".$warehouseIdList.")" : "product_requests.warehouse_id = '".$warehouseId."'";
        $whereDocDate = "product_requests.doc_date BETWEEN '".$docDateStart."' AND '".$docDateEnd."'";
        $whereNeedDate = "product_requests.need_date BETWEEN '".$needDateStart."' AND '".$needDateEnd."'";

        if($productRequestId != 'null') {
            $whereCounterId = "product_requests.counter_id is not null";
            $whereWarehouseId = "product_requests.warehouse_id is not null";
            $whereDocDate = "product_requests.doc_date is not null";
            $whereNeedDate = "product_requests.need_date is not null";
        }

        $reportData = ProductRequest::join('mst_counters', 'product_requests.counter_id', '=', 'mst_counters.id')
                      ->join('mst_warehouses', 'product_requests.warehouse_id', '=', 'mst_warehouses.id')
                      ->selectRaw('product_requests.id AS product_request_id,
                        product_requests.counter_id,
                        mst_counters.counter_name,
                        product_requests.warehouse_id,
                        mst_warehouses.warehouse_name,
                        product_requests.doc_date,
                        product_requests.need_date,
                        product_requests.status')
                      ->whereRaw($whereProductRequestId)
                      ->whereRaw($whereCounterId)
                      ->whereRaw($whereWarehouseId)
                      ->whereRaw($whereDocDate)
                      ->whereRaw($whereNeedDate)
                      ->orderByRaw('product_requests.created_at DESC')
                      ->get();

        $response = [
            'message' => 'Report Product Request',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'report_data' => $reportData
        ];
        
        return response()->json($response, 200);
    }

    public function getProductRequestDetail(Request $request)
    {
        $productRequestId = $request->input('product_request_id');

        $reportData = ProductRequestDetail::join('product_requests', 'product_request_details.product_request_id', '=', 'product_requests.id')
                      ->join('mst_counters', 'product_requests.counter_id', '=', 'mst_counters.id')
                      ->join('mst_warehouses', 'product_requests.warehouse_id', '=', 'mst_warehouses.id')
                      ->join('mst_products', 'product_request_details.product_id', '=', 'mst_products.id')
                      ->selectRaw('product_request_details.product_request_id,
                        product_requests.counter_id,
                        mst_counters.counter_name,
                        product_requests.warehouse_id,
                        mst_warehouses.warehouse_name,
                        product_requests.doc_date,
                        product_requests.need_date,
                        product_requests.desc,
                        product_requests.status,
                        product_request_details.product_id,
                        mst_products.product_name,
                        product_request_details.qty')
                      ->where('product_request_details.product_request_id', $productRequestId)
                      ->orderByRaw('product_request_details.product_id ASC')
                      ->get();

        $response = [
            'message' => 'Report Product Request Detail',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'report_data' => $reportData
        ];
        
        return response()->json($response, 200);
    }
}
