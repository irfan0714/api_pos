<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstCashBank;
use App\MstCoa;

class MstCashBankController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = MstCashBank::select('*')->paginate($limit);

        $response = [
            'message' => 'List of cash bank',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = MstCoa::select('id', 'name')->get();
        $response = [
            'message' => 'Create cash bank',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'coa_data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'coa_id' => 'required',
            'name' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstCashBank = new MstCashBank();
        $mstCashBank->coa_id = $request->input('coa_id');
        $mstCashBank->name = $request->input('name');
        $mstCashBank->created_by = $request->input('created_by');

        if($mstCashBank->save()) {
            $mstCashBank->show_cash_bank = [
                'url' => url('/v1/cash-bank/'.$mstCashBank->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Cash bank created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $mstCashBank
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MstCashBank::find($id);

        $response = [
            'message' => 'Show cash bank',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MstCashBank::find($id);
        
        $response = [
            'message' => 'Edit cash bank',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'coa_id' => 'required',
            'name' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstCashBank = MstCashBank::find($id);
        $mstCashBank->coa_id = $request->input('coa_id');
        $mstCashBank->name = $request->input('name');
        $mstCashBank->updated_by = $request->input('updated_by');

        if($mstCashBank->save()) {
            $mstCashBank->show_cash_bank = [
                'url' => url('/v1/cash-bank/'.$mstCashBank->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Cash bank updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $mstCashBank
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstCashBank = MstCashBank::find($id);
        $mstCashBank->delete();
        $response = [
            'message' => 'Cash bank deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function getAllCashBank()
    {
        $data = MstCashBank::select('*')->get();

        $response = [
            'message' => 'List of cash bank',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }
}
