<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\OtherReceiving;
use App\OtherReceivingDetail;

class OtherReceivingDetailController extends Controller
{
    public function index(Request $request)
    {
        //
    }

    public function create(Request $request)
    {
        //
    }

    public function show(Request $request)
    {
        $otherReceivingId = $request->input('other_receiving_id') ? "other_receivings.id = '".$request->input('other_receiving_id')."'" : 'other_receivings.id is not null';
        $data = OtherReceivingDetail::join('other_receivings', 'other_receivings.id', '=', 'other_receiving_details.other_receiving_id')
                ->join('mst_products', 'mst_products.id', '=', 'other_receiving_details.product_id')
                ->select('other_receiving_details.*', 'mst_products.product_name')
                ->whereRaw($otherReceivingId)
                ->get();

        $response = [
            'message' => 'List of other receiving details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function edit($id)
    {
        //
    }

    public function store(Request $request)
    {
        $arrOtherReceivingId = $request->input('other_receiving_id');
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrUnit = $request->input('unit');
        $arrDesc = $request->input('description');

        if (count($arrOtherReceivingId) > 0) {
            OtherReceivingDetail::where(['other_receiving_id'=> $arrOtherReceivingId[0]])->delete();
            for ($index=0; $index < count($arrOtherReceivingId); $index++) {
                $otherReceivingDetail = new OtherReceivingDetail;
                $otherReceivingDetail->other_receiving_id = $arrOtherReceivingId[$index]; 
                $otherReceivingDetail->product_id = $arrProductId[$index];
                $otherReceivingDetail->qty = intval($arrQty[$index]);
                $otherReceivingDetail->unit = $arrUnit[$index];
                $otherReceivingDetail->price = 0;
                $otherReceivingDetail->description = $arrDesc[$index];
                $otherReceivingDetail->save();
            }
            $otherReceivingData = OtherReceiving::select('id')->where('id', $arrOtherReceivingId[0])->get();
            $otherReceivingDetailsData = OtherReceivingDetail::where('other_receiving_id', $arrOtherReceivingId[0])->get();
            $response = [
                'message' => 'Other receiving details created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'other_receiving_data' => $otherReceivingData,
                    'other_receiving_detail_data' => $otherReceivingDetailsData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    public function update(Request $request, $id)
    {
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrUnit = $request->input('unit');
        $arrDesc = $request->input('description');

        if (count($arrProductId) > 0) {
            OtherReceivingDetail::where(['other_receiving_id'=> $id])->delete();
            for ($index=0; $index < count($arrProductId); $index++) {
                $otherReceivingDetail = new OtherReceivingDetail;
                $otherReceivingDetail->other_receiving_id = $id; 
                $otherReceivingDetail->product_id = $arrProductId[$index];
                $otherReceivingDetail->qty = intval($arrQty[$index]);
                $otherReceivingDetail->unit = $arrUnit[$index];
                $otherReceivingDetail->price = 0;
                $otherReceivingDetail->description = $arrDesc[$index];
                $otherReceivingDetail->save();
            }
            $otherReceivingData = OtherReceiving::select('id')->where('id', $id[0])->get();
            $otherReceivingDetailsData = OtherReceivingDetail::where('other_receiving_id', $id[0])->get();
            $response = [
                'message' => 'Other receiving details updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'other_receiving_data' => $otherReceivingData,
                    'other_receiving_detail_data' => $otherReceivingDetailsData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    public function destroy($id)
    {
        //
    }
}
