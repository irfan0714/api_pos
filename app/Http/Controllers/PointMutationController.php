<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\PointMutation;

class PointMutationController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = PointMutation::select('*')->paginate($limit);

        $response = [
            'message' => 'List of point mutation',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $arrCustomerId = $request->input('customer_id');
        $arrPointMutationTypeId = $request->input('point_mutation_type_id');
        $arrSalesTransactionId = $request->input('sales_transaction_id');
        $arrPoint = $request->input('point');
        $arrPointvalue = $request->input('point_value');
        $arrTransDate = $request->input('trans_date');
        $arrExpiredDate = $request->input('expired_date');
        $arrStatus = $request->input('status');
        $arrCreatedBy = $request->input('created_by');

        if(count($arrSalesTransactionId) > 0) {
            PointMutation::where(['sales_transaction_id'=> $arrSalesTransactionId[0]])->delete();
            for ($index=0; $index < count($arrSalesTransactionId); $index++) {
                $pointMutation = new PointMutation();
                $pointMutation->customer_id = intval($arrCustomerId[$index]);
                $pointMutation->point_mutation_type_id = $arrPointMutationTypeId[$index];
                $pointMutation->sales_transaction_id = $arrSalesTransactionId[$index];
                $pointMutation->point = intval($arrPoint[$index]);
                $pointMutation->point_value = intval($arrPointvalue[$index]);
                $pointMutation->trans_date = $arrTransDate[$index];
                $pointMutation->expired_date = $arrExpiredDate[$index];
                $pointMutation->status = intval($arrStatus[$index]);
                $pointMutation->created_by = $arrCreatedBy[$index];
                $pointMutation->save();
            }

            $pointMutationData = PointMutation::where('sales_transaction_id', $arrSalesTransactionId[0])->get();
            $response = [
                'message' => 'Point mutation created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $pointMutationData
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
