<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\ResellerMonthlyReward;

class ResellerMonthlyRewardController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = ResellerMonthlyReward::join('customers', 'reseller_monthly_rewards.customer_id', '=', 'customers.id')
                ->select('reseller_monthly_rewards.*', 'customers.customer_name')
                ->orderByDesc('reseller_monthly_rewards.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of Reseller Monthly Reward',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'period_transaction_start' => 'required|date_format:Y-m-d',
            'period_transaction_end' => 'required|date_format:Y-m-d',
            'total_transaction' => 'required|numeric',
            'paid_date' => 'required|date_format:Y-m-d',
            'paid_amount' => 'required|numeric',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        }

        $resellerMonthlyRewardId = $this->getLatestId();

        DB::beginTransaction();
        try
        {
            $resellerMonthlyReward = new ResellerMonthlyReward();
            $resellerMonthlyReward->id = $resellerMonthlyRewardId;
            $resellerMonthlyReward->customer_id = $request->input('customer_id');
            $resellerMonthlyReward->period_transaction_start = $request->input('period_transaction_start');
            $resellerMonthlyReward->period_transaction_end = $request->input('period_transaction_end');
            $resellerMonthlyReward->total_transaction = $request->input('total_transaction');
            $resellerMonthlyReward->paid_date = $request->input('paid_date');
            $resellerMonthlyReward->paid_amount = $request->input('paid_amount');
            $resellerMonthlyReward->created_by = $request->input('created_by');
            $resellerMonthlyReward->save();

            DB::commit();
            $response = [
                'message' => 'Reseller monthly reward created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $resellerMonthlyReward
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ResellerMonthlyReward::join('customers', 'reseller_monthly_rewards.customer_id', '=', 'customers.id')
                ->select('reseller_monthly_rewards.*', 'customers.customer_name')
                ->where('reseller_monthly_rewards.id', $id)
                ->get();

        $response = [
            'message' => 'Reseller monthly reward details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'reseller_monthly_reward_data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getLatestId()
    {
        $year = date('y');
        $month = date('m');

        $data = ResellerMonthlyReward::select('id')
                ->orderByDesc('created_at')
                ->get();

        $arrData = $data->toArray();
        if(count($arrData) > 0) {
            $latestStockOpnameId = $arrData[0]["id"];
            $lastCounter = substr($latestStockOpnameId, 6, 3);
            $lastMonth = intval(substr($latestStockOpnameId, 4, 2));
            $newCounter = intval($lastCounter) + 1;
            if($lastMonth === intval($month)) {
                if($newCounter <= 9) {
                    $newCounter = '00' . strval($newCounter); 
                }
                if($newCounter > 9 && $newCounter <= 99) { 
                    $newCounter = '0' . strval($newCounter); 
                }
            } else {
              $newCounter = '001';
            }
      
            $resellerMonthlyRewardId = 'RC' . strval($year) . strval($month) . $newCounter;
        } else {
            $resellerMonthlyRewardId = 'RC' . strval($year) . strval($month) . '001';
        }

        return $resellerMonthlyRewardId;
    }
}
