<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\MstBranch;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $keywords = $request->input('keywords') != 'null' ? 
                    "users.username LIKE '%".$request->input('keywords')."%'
                     OR users.email LIKE '%".$request->input('keywords')."%'
                     OR users.name LIKE '%".$request->input('keywords')."%'" : 'users.username is not null';

        $data = User::leftJoin('roles', 'roles.id', '=', 'users.role_id')
                ->leftJoin('mst_counters', 'mst_counters.id', '=', 'users.counter_id')
                ->leftJoin('mst_branches', 'mst_branches.id', '=', 'users.branch_id')
                ->select('users.*', 'roles.role_name', 'mst_counters.counter_name', 'mst_branches.branch_name')
                ->whereRaw($keywords)
                ->paginate($limit);

        $response = [
            'message' => 'List of user',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pull(Request $request)
    {
        $validator = Validator::make($request->all(), ['created_by' => 'required']);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $createdBy = $request->input('created_by');
        $userData = [];
        $limit = 100;
		for ($offset=0; $offset < 9999; $offset++) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://139.255.39.26:92/vci/api?action=employee&offset='.$offset.'&limit='.$limit,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));
    
            $response = curl_exec($curl);
            $dataResponse = json_decode($response);  
            curl_close($curl);
            
            if (count($dataResponse) > 0) {
                foreach ($dataResponse as $data) {
                    $check = User::where('id', $data->user_id)->orWhere('email', $data->email)->count();
                    if (empty($check)) {
                        $random = strtolower("123456"); // strtolower(Str::random(6));
                        $user = new User;
                        $user->id = $data->user_id;
                        $user->role_id = 0;
                        $user->branch_id = 0;
                        $user->counter_id = 0;
                        $user->username = empty($data->username) ? $data->user_id : $data->username;
                        $user->name = $data->employee_name;
                        $user->email = empty($data->email) ? $data->user_id : $data->email;
                        $user->password = bcrypt($random);
                        $user->password_history = bcrypt($random);
                        $user->online = "0";
                        $user->status = "1";
                        $user->created_by = $createdBy;
                        if ($user->save()) {
                            $userData[] = $user;
                        } 
                    }
                }
            } else {
                $response = [
                    'message' => 'User created',
                    'status' => [
                        'code' => 200,
                        'description' => 'OK'
                    ],
                    'results' => $userData
                ];
    
                return response()->json($response, 200);
                exit();
            }
            $offset +=99;
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userData = User::find($id);
        $branchData = MstBranch::select('id', 'branch_name')->orderBy('branch_name', 'asc')->get();
        $response = [
            'message' => 'Edit user',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'user_data' => $userData,
                'branch_data' => $branchData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'role_id' => 'required|numeric',
            'counter_id' => 'required|numeric',
            'branch_id' => 'required|numeric',
            'name' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $user = User::find($id);
        $user->role_id = $request->input('role_id');
        $user->counter_id = $request->input('counter_id');
        $user->branch_id = $request->input('branch_id');
        $user->name = $request->input('name');
        $user->updated_by = $request->input('updated_by');

        if($user->save()) {
            $user->show_user = [
                'url' => url('/v1/user/'.$user->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'User updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $user
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
