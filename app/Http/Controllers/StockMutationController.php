<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\StockMutation;
use App\MstProduct;

class StockMutationController extends Controller
{
    public function store(Request $request)
    {
        $arrWarehouseId = $request->input('warehouse_id');
        $arrTransactionId = $request->input('transaction_id');
        $arrStockMutationTypeId = $request->input('stock_mutation_type_id');
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrValue = $request->input('value');
        $arrStockMove = $request->input('stock_move');
        $arrTransDate = $request->input('trans_date');

        if (count($arrTransactionId) > 0) {
            for ($index=0; $index < count($arrTransactionId); $index++) {
                $stockMutation = new StockMutation;
                $stockMutation->warehouse_id = $arrWarehouseId[$index];  
                $stockMutation->transaction_id = $arrTransactionId[$index];
                $stockMutation->stock_mutation_type_id = $arrStockMutationTypeId[$index];
                $stockMutation->product_id = $arrProductId[$index];
                $stockMutation->qty = intval($arrQty[$index]);
                $stockMutation->value = intval($arrValue[$index]);
                $stockMutation->stock_move = $arrStockMove[$index];
                $stockMutation->trans_date = $arrTransDate[$index];
                $stockMutation->save();
            }
            $stockMutationData = StockMutation::where('transaction_id', $arrTransactionId[0])->get();
            $response = [
                'message' => 'Stock mutation created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'stock_mutation_data' => $stockMutationData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    public function getMutationStock(Request $request)
    {
        $warehouseId = $request->input('warehouse_id');
        $productId = $request->input('product_id');
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $data = StockMutation::join('mst_warehouses', 'mst_warehouses.id', '=', 'stock_mutations.warehouse_id')
                ->join('mst_products', 'mst_products.id', '=', 'stock_mutations.product_id')
                ->selectRaw('mst_warehouses.warehouse_name, stock_mutations.transaction_id, mst_products.product_name,
                mst_products.unit, stock_mutations.transaction_id, stock_mutations.trans_date,
                stock_mutations.stock_move, stock_mutations.qty')
                ->whereRaw("stock_mutations.warehouse_id = '".$warehouseId."'")
                ->whereRaw("stock_mutations.product_id = '".$productId."'")
                ->whereRaw("stock_mutations.trans_date >= '".$startDate."'")
                ->whereRaw("stock_mutations.trans_date <= '".$endDate."'")
                ->get();

        $productData = MstProduct::select('*')->where('id', $productId)->get();
        
        $response = [
            'message' => 'List of stock mutation',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'stock_mutation_data' => $data,
                'product_data' => $productData
            ]
        ];
        
        return response()->json($response, 200);
    }
}
