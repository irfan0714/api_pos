<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\OtherExpences;
use App\OtherExpencesDetail;

class OtherExpencesDetailController extends Controller
{
    public function index(Request $request)
    {
        //
    }

    public function create(Request $request)
    {
        //
    }

    public function show(Request $request)
    {
        $otherExpencesId = $request->input('other_expences_id') ? "other_expences.id = '".$request->input('other_expences_id')."'" : 'other_expences.id is not null';
        $data = OtherExpencesDetail::join('other_expences', 'other_expences.id', '=', 'other_expences_details.other_expences_id')
                ->join('mst_products', 'mst_products.id', '=', 'other_expences_details.product_id')
                ->select('other_expences_details.*', 'mst_products.product_name')
                ->whereRaw($otherExpencesId)
                ->get();

        $response = [
            'message' => 'List of other expences details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function edit($id)
    {
        //
    }

    public function store(Request $request)
    {
        $arrOtherExpencesId = $request->input('other_expences_id');
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrUnit = $request->input('unit');
        $arrDesc = $request->input('description');

        if (count($arrOtherExpencesId) > 0) {
            OtherExpencesDetail::where(['other_expences_id'=> $arrOtherExpencesId[0]])->delete();
            for ($index=0; $index < count($arrOtherExpencesId); $index++) {
                $otherExpencesDetail = new OtherExpencesDetail;
                $otherExpencesDetail->other_expences_id = $arrOtherExpencesId[$index]; 
                $otherExpencesDetail->product_id = $arrProductId[$index];
                $otherExpencesDetail->qty = intval($arrQty[$index]);
                $otherExpencesDetail->unit = $arrUnit[$index];
                $otherExpencesDetail->price = 0;
                $otherExpencesDetail->description = $arrDesc[$index];
                $otherExpencesDetail->save();
            }
            $otherExpencesData = OtherExpences::select('id')->where('id', $arrOtherExpencesId[0])->get();
            $otherExpencesDetailsData = OtherExpencesDetail::where('other_expences_id', $arrOtherExpencesId[0])->get();
            $response = [
                'message' => 'Other expences details created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'other_expences_data' => $otherExpencesData,
                    'other_expences_detail_data' => $otherExpencesDetailsData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    public function update(Request $request, $id)
    {
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrUnit = $request->input('unit');
        $arrDesc = $request->input('description');

        if (count($arrProductId) > 0) {
            OtherExpencesDetail::where(['other_expences_id'=> $id])->delete();
            for ($index=0; $index < count($arrProductId); $index++) {
                $otherExpencesDetail = new OtherExpencesDetail;
                $otherExpencesDetail->other_expences_id = $id; 
                $otherExpencesDetail->product_id = $arrProductId[$index];
                $otherExpencesDetail->qty = intval($arrQty[$index]);
                $otherExpencesDetail->unit = $arrUnit[$index];
                $otherExpencesDetail->price = 0;
                $otherExpencesDetail->description = $arrDesc[$index];
                $otherExpencesDetail->save();
            }
            $otherExpencesData = OtherExpences::select('id')->where('id', $id[0])->get();
            $otherExpencesDetailsData = OtherExpencesDetail::where('other_expences_id', $id[0])->get();
            $response = [
                'message' => 'Other expences details updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'other_expences_data' => $otherExpencesData,
                    'other_expences_detail_data' => $otherExpencesDetailsData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    public function destroy($id)
    {
        //
    }
}
