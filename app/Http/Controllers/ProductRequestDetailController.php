<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ProductRequest;
use App\ProductRequestDetail;

class ProductRequestDetailController extends Controller
{
    public function index(Request $request)
    {
        //
    }

    public function create(Request $request)
    {
        //
    }

    public function show(Request $request)
    {
        $productRequestId = $request->input('product_request_id') ? "product_requests.id = '".$request->input('product_request_id')."'" : 'product_requests.id is not null';
        $data = ProductRequestDetail::join('product_requests', 'product_requests.id', '=', 'product_request_details.product_request_id')
                ->join('mst_products', 'mst_products.id', '=', 'product_request_details.product_id')
                ->select('product_request_details.*', 'mst_products.product_name')
                ->whereRaw($productRequestId)
                ->get();

        $response = [
            'message' => 'List of product request details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function edit($id)
    {
        //
    }

    public function store(Request $request)
    {
        $arrProductRequestId = $request->input('product_request_id');
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrUnit = $request->input('unit');

        if (count($arrProductRequestId) > 0) {
            ProductRequestDetail::where(['product_request_id'=> $arrProductRequestId[0]])->delete();
            for ($index=0; $index < count($arrProductRequestId); $index++) {
                $productRequestDetail = new ProductRequestDetail;
                $productRequestDetail->product_request_id = $arrProductRequestId[$index]; 
                $productRequestDetail->product_id = $arrProductId[$index];
                $productRequestDetail->qty = intval($arrQty[$index]);
                $productRequestDetail->unit = $arrUnit[$index];
                $productRequestDetail->qty_receipt = 0;
                $productRequestDetail->save();
            }
            $productRequestData = ProductRequest::select('id')->where('id', $arrProductRequestId[0])->get();
            $productRequestDetailsData = ProductRequestDetail::where('product_request_id', $arrProductRequestId[0])->get();
            $response = [
                'message' => 'Product request details created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'product_request_data' => $productRequestData,
                    'product_request_detail_data' => $productRequestDetailsData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    public function update(Request $request, $id)
    {
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrUnit = $request->input('unit');

        if (count($arrProductId) > 0) {
            ProductRequestDetail::where(['product_request_id'=> $id])->delete();
            for ($index=0; $index < count($arrProductId); $index++) {
                $productRequestDetail = new ProductRequestDetail;
                $productRequestDetail->product_request_id = $id; 
                $productRequestDetail->product_id = $arrProductId[$index];
                $productRequestDetail->qty = intval($arrQty[$index]);
                $productRequestDetail->unit = $arrUnit[$index];
                $productRequestDetail->qty_receipt = 0;
                $productRequestDetail->save();
            }
            $productRequestData = ProductRequest::select('id')->where('id', $id[0])->get();
            $productRequestDetailsData = ProductRequestDetail::where('product_request_id', $id[0])->get();
            $response = [
                'message' => 'Product request details updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'product_request_data' => $productRequestData,
                    'product_request_detail_data' => $productRequestDetailsData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    public function destroy($id)
    {
        //
    }
}
