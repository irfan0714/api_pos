<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstProductBrand;

class MstProductBrandController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = MstProductBrand::select('*')->paginate($limit);

        $response = [
            'message' => 'List of product brand',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'product_brand_name' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstProductBrand = new MstProductBrand();
        $mstProductBrand->id = $request->input('id');
        $mstProductBrand->product_brand_name = $request->input('product_brand_name');
        $mstProductBrand->created_by = $request->input('created_by');

        if($mstProductBrand->save()) {
            $mstProductBrand->show_product_brand = [
                'url' => url('/v1/product-brand/'.$mstProductBrand->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Product Brand created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $mstProductBrand
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MstProductBrand::find($id);

        $response = [
            'message' => 'Show product brand',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MstProductBrand::find($id);
        $response = [
            'message' => 'Edit product brand',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'product_brand_name' => 'required',
            'active' => 'required|numeric|max:1',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstProductBrand = MstProductBrand::find($id);
        $mstProductBrand->product_brand_name = $request->input('product_brand_name');
        $mstProductBrand->active = $request->input('active');
        $mstProductBrand->updated_by = $request->input('updated_by');

        if($mstProductBrand->save()) {
            $mstProductBrand->show_product_brand = [
                'url' => url('/v1/product-brand/'.$mstProductBrand->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Product brand updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $mstProductBrand
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstProductBrand = MstProductBrand::find($id);
        $mstProductBrand->delete();
        $response = [
            'message' => 'Product brand deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }
}
