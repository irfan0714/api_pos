<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\MstCounter;
use App\MstWarehouse;
use App\ProductRequest;
use App\ProductRequestDetail;

class ProductRequestFromBranchController extends Controller
{
    public function storeFromDistribution(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_request_no' => 'required',
            'counter_code' => 'required',
            'document_date' => 'required|date_format:Y-m-d',
            'need_date' => 'required|date_format:Y-m-d',
            'product_id' => 'required',
            'qty' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $productRequestNo = $request->input('product_request_no');
        $counterCode = $request->input('counter_code');
        $docDate = $request->input('document_date');
        $needDate = $request->input('need_date');

        $productId = $request->input('product_id');
        $qty = $request->input('qty');

        $getCounter = MstCounter::select('*')
                      ->where('counter_code', $counterCode)
                      ->get();

        $getCounterId = '';
        $arrCounter = $getCounter->toArray();
        if(count($arrCounter) > 0) {
            $getCounterId = $arrCounter[0]["id"];
        }
        
        if($getCounterId != '') {
            $productRequestData = ProductRequest::select('*')
                                  ->where('id', $productRequestNo)
                                  ->first();

            if($productRequestData == null) {
                $warehouseData = MstWarehouse::select('id', 'warehouse_name')
                            ->where('counter_id', $getCounterId)
                            ->where('warehouse_type_id', '1') // gudang reguler
                            ->where('active', '1')
                            ->first();

                DB::beginTransaction();
                try
                {
                    $productRequest = new ProductRequest();
                    $productRequest->id = $productRequestNo;
                    $productRequest->counter_id = $getCounterId;
                    $productRequest->warehouse_id = $warehouseData ? $warehouseData->id : 0;
                    $productRequest->doc_date = $docDate;
                    $productRequest->need_date = $needDate;
                    $productRequest->desc = "Product Request Input from Branch";
                    $productRequest->status = "6";
                    $productRequest->created_by = "system";
                    $productRequest->save();

                    $arrProductId = explode(",", $productId);
                    $arrQty = explode(",", $qty);
                    if (count($arrProductId) > 0) {
                        ProductRequestDetail::where(['product_request_id'=> $productRequestNo])->delete();
                        for ($index=0; $index < count($arrProductId); $index++) {
                            $productRequestDetail = new ProductRequestDetail;
                            $productRequestDetail->product_request_id = $productRequestNo; 
                            $productRequestDetail->product_id = $arrProductId[$index];
                            $productRequestDetail->qty = intval($arrQty[$index]);
                            $productRequestDetail->unit = 0;
                            $productRequestDetail->qty_receipt = 0;
                            $productRequestDetail->save();
                        }
                    }
        
                    DB::commit();
                    $response = [
                        'message' => 'Product request created',
                        'status' => [
                            'code' => 201,
                            'description' => 'Created'
                        ],
                        'results' => [
                            'product_request_data' => $productRequest,
                            'product_request_detail_data' => $productRequestDetail
                        ]
                    ];
        
                    return response()->json($response, 201);
                }
                catch (Exception $e)
                {
                    DB::rollback();

                    $response = [
                        'message' => 'An error occured',
                        'status' => [
                            'code' => 500,
                            'description' => 'internal server error'
                        ],
                    ];

                    return response()->json($response, 500);
                }
            } else {
                $message = "Nomor Permintaan Barang: ".$productRequestNo." , sudah ter-input di POS Online!";
                $response = [
                    'message' => $message,
                    'status' => [
                        'code' => 500,
                        'description' => 'internal server error'
                    ],
                ];
    
                return response()->json($response, 500);
            }
        } else {
            $response = [
                'message' => 'Kode Counter belum terdaftar di POS Online!',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }
}
