<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstPromotions;
use App\MstPromotionTypes;
use App\MstCounter;
use App\MstWarehouse;

class MstPromotionsController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $counterId = $request->input('counter_id') != 0 ? "mst_promotions.counter_id IN (".$request->input('counter_id').")" : 'mst_promotions.counter_id is not null';
        $data = MstPromotions::join('mst_counters', 'mst_counters.id', '=', 'mst_promotions.counter_id')
                ->join('mst_warehouses', 'mst_warehouses.id', '=', 'mst_promotions.warehouse_id')
                ->join('mst_promotion_types', 'mst_promotion_types.id', '=', 'mst_promotions.promotion_type_id')
                ->select('mst_promotions.*', 'mst_counters.counter_name', 'mst_warehouses.warehouse_name', 'mst_promotion_types.promotion_type_name')
                ->whereRaw($counterId)
                ->orderByDesc('mst_promotions.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of promotions',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->orderBy('warehouse_name', 'asc')
                         ->get();

        $promotionTypeData = MstPromotionTypes::select('id', 'promotion_type_name')->get();

        $response = [
            'message' => 'Create promotions',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'promotion_type_data' => $promotionTypeData,
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required',
            'warehouse_id' => 'required',
            'promotion_name' => 'required',
            'early_period' => 'required|date_format:Y-m-d',
            'end_period' => 'required|date_format:Y-m-d',
            'promotion_type_id' => 'required|numeric',
            'promotion_cashier_type' => 'required',
            'minimum' => 'required|numeric',
            'valid_on_su' => 'required|numeric|max:1',
            'valid_on_mo' => 'required|numeric|max:1',
            'valid_on_tu' => 'required|numeric|max:1',
            'valid_on_we' => 'required|numeric|max:1',
            'valid_on_th' => 'required|numeric|max:1',
            'valid_on_fr' => 'required|numeric|max:1',
            'valid_on_sa' => 'required|numeric|max:1',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstPromotions = new MstPromotions();
        $mstPromotions->counter_id = $request->input('counter_id');
        $mstPromotions->warehouse_id = $request->input('warehouse_id');
        $mstPromotions->promotion_name = $request->input('promotion_name');
        $mstPromotions->early_period = $request->input('early_period');
        $mstPromotions->end_period = $request->input('end_period');
        $mstPromotions->promotion_type_id = $request->input('promotion_type_id');
        $mstPromotions->promotion_cashier_type = $request->input('promotion_cashier_type');
        $mstPromotions->minimum = $request->input('minimum');
        $mstPromotions->valid_on_su = $request->input('valid_on_su');
        $mstPromotions->valid_on_mo = $request->input('valid_on_mo');
        $mstPromotions->valid_on_tu = $request->input('valid_on_tu');
        $mstPromotions->valid_on_we = $request->input('valid_on_we');
        $mstPromotions->valid_on_th = $request->input('valid_on_th');
        $mstPromotions->valid_on_fr = $request->input('valid_on_fr');
        $mstPromotions->valid_on_sa = $request->input('valid_on_sa');
        $mstPromotions->status = '1';
        $mstPromotions->created_by = $request->input('created_by');

        if($mstPromotions->save()) {
            $mstPromotions->show_promotions = [
                'url' => url('/v1/promotions/'.$mstPromotions->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Promotions created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $mstPromotions
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MstPromotions::find($id);

        $response = [
            'message' => 'Show promotions',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->orderBy('warehouse_name', 'asc')
                         ->get();
        
        $data = MstPromotions::find($id);
        $promotionTypesData = MstPromotionTypes::select('id', 'promotion_type_name')->get();

        $response = [
            'message' => 'Edit promotions',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data,
                'promotion_type_data' => $promotionTypesData,
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'promotion_name' => 'required',
            'early_period' => 'required|date_format:Y-m-d',
            'end_period' => 'required|date_format:Y-m-d',
            'promotion_type_id' => 'required|numeric',
            'promotion_cashier_type' => 'required',
            'minimum' => 'required|numeric',
            'valid_on_su' => 'required|numeric|max:1',
            'valid_on_mo' => 'required|numeric|max:1',
            'valid_on_tu' => 'required|numeric|max:1',
            'valid_on_we' => 'required|numeric|max:1',
            'valid_on_th' => 'required|numeric|max:1',
            'valid_on_fr' => 'required|numeric|max:1',
            'valid_on_sa' => 'required|numeric|max:1',
            'status' => 'required|numeric|max:1',
            'updated_by' => 'required',
        ]);

        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        }

        $mstPromotions = MstPromotions::find($id);
        $mstPromotions->promotion_name = $request->input('promotion_name');
        $mstPromotions->early_period = $request->input('early_period');
        $mstPromotions->end_period = $request->input('end_period');
        $mstPromotions->promotion_type_id = $request->input('promotion_type_id');
        $mstPromotions->promotion_cashier_type = $request->input('promotion_cashier_type');
        $mstPromotions->minimum = $request->input('minimum');
        $mstPromotions->valid_on_su = $request->input('valid_on_su');
        $mstPromotions->valid_on_mo = $request->input('valid_on_mo');
        $mstPromotions->valid_on_tu = $request->input('valid_on_tu');
        $mstPromotions->valid_on_we = $request->input('valid_on_we');
        $mstPromotions->valid_on_th = $request->input('valid_on_th');
        $mstPromotions->valid_on_fr = $request->input('valid_on_fr');
        $mstPromotions->valid_on_sa = $request->input('valid_on_sa');
        $mstPromotions->status = $request->input('status');
        $mstPromotions->updated_by = $request->input('updated_by');

        if($mstPromotions->save()) {
            $mstPromotions->show_promotions = [
                'url' => url('/v1/promotions/'.$mstPromotions->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Promotions updated',
                'status' => [
                    'code' => 200,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $mstPromotions
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstPromotions = MstPromotions::find($id);
        $mstPromotions->delete();
        $response = [
            'message' => 'Promotions deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }
}
