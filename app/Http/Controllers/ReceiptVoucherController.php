<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ReceiptVoucher;
use App\ReceiptVoucherDetail;
use App\MstProduct;
use App\MstCounter;
use App\User;
use App\ApprovalHistory;

class ReceiptVoucherController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = ReceiptVoucher::join('mst_cash_banks', 'mst_cash_banks.id', '=', 'receipt_vouchers.cash_bank_id')
                ->join('mst_foreign_currencies', 'mst_foreign_currencies.id', '=', 'receipt_vouchers.currency')
                ->selectRaw('receipt_vouchers.*, mst_cash_banks.name as cash_bank_name, mst_foreign_currencies.foreign_currency_name')
                ->orderByDesc('receipt_vouchers.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of receipt voucher',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = ReceiptVoucher::select('id')
                ->orderByDesc('created_at')
                ->get();

        $response = [
            'message' => 'Last receipt voucher id',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'cash_bank_id' => 'required',
            'doc_date' => 'required|date_format:Y-m-d',
            'receipt_from' => 'required',
            'currency' => 'required',
            'exchange_rate' => 'required',
            'amount' => 'required',
            'receipt_proof_no' => 'required',
            'status' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $receiptVoucher = new ReceiptVoucher();
        $receiptVoucher->id = $request->input('id');
        $receiptVoucher->cash_bank_id = $request->input('cash_bank_id');
        $receiptVoucher->doc_date = $request->input('doc_date');
        $receiptVoucher->receipt_from = $request->input('receipt_from');
        $receiptVoucher->currency = $request->input('currency');
        $receiptVoucher->exchange_rate = $request->input('exchange_rate');
        $receiptVoucher->amount = $request->input('amount');
        $receiptVoucher->receipt_proof_no = $request->input('receipt_proof_no');
        $receiptVoucher->status = $request->input('status');
        $receiptVoucher->created_by = $request->input('created_by');

        if($receiptVoucher->save()) {
            $receiptVoucher->show_receipt_voucher = [
                'url' => url('/v1/receipt-voucher/'.$receiptVoucher->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Receipt voucher created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $receiptVoucher
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $receiptVoucherData = ReceiptVoucher::select('*')
            ->where('id', $id)
            ->get();

        foreach($receiptVoucherData as $receipt) {
            $createdBy = $receipt->created_by ? $receipt->created_by : null;
            $updatedBy = $receipt->updated_by ? $receipt->updated_by : null;
        }

        $userCreatedData = null;
        $userUpdatedData = null;
        if(isset($createdBy)) {
            $userCreatedData = User::select('*')->where('username', $createdBy)->get();
        }

        if(isset($updatedBy)) {
            $userUpdatedData = User::select('*')->where('username', $updatedBy)->get();
        }

        $approvalHistoryData = ApprovalHistory::join('users', 'users.username', '=', 'approval_histories.username')
            ->select('approval_histories.*', 'users.name')
            ->whereRaw("approval_histories.transaction_id = '".$id."'")
            ->get();

        $receiptVoucherDetailData = ReceiptVoucherDetail::join('receipt_vouchers', 'receipt_vouchers.id', '=', 'receipt_voucher_details.receipt_voucher_id')
                ->join('mst_coas', 'mst_coas.id', '=', 'receipt_voucher_details.coa')
                ->join('mst_counters', 'mst_counters.id', '=', 'receipt_voucher_details.counter_id')
                ->selectRaw('receipt_voucher_details.*, mst_coas.name as coa_name, mst_counters.counter_name')
                ->whereRaw("receipt_vouchers.id = '".$id."'")
                ->get();

        $response = [
            'message' => 'Receipt voucher details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'receipt_voucher_data' => $receiptVoucherData,
                'receipt_voucher_detail_data' => $receiptVoucherDetailData,
                'user_create_data' => $userCreatedData,
                'user_update_data' => $userUpdatedData,
                'approval_history_data' => $approvalHistoryData

            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'cash_bank_id' => 'required',
            'doc_date' => 'required|date_format:Y-m-d',
            'receipt_from' => 'required',
            'currency' => 'required',
            'exchange_rate' => 'required',
            'amount' => 'required',
            'receipt_proof_no' => 'required',
            'status' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $receiptVoucher = ReceiptVoucher::find($id);
        $receiptVoucher->cash_bank_id = $request->input('cash_bank_id');
        $receiptVoucher->doc_date = $request->input('doc_date');
        $receiptVoucher->receipt_from = $request->input('receipt_from');
        $receiptVoucher->currency = $request->input('currency');
        $receiptVoucher->exchange_rate = $request->input('exchange_rate');
        $receiptVoucher->amount = $request->input('amount');
        $receiptVoucher->receipt_proof_no = $request->input('receipt_proof_no');
        $receiptVoucher->status = $request->input('status');
        $receiptVoucher->updated_by = $request->input('updated_by');

        if($receiptVoucher->save()) {
            $receiptVoucher->show_receipt_voucher = [
                'url' => url('/v1/receipt-voucher/'.$receiptVoucher->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Receipt voucher updated',
                'status' => [
                    'code' => 201,
                    'description' => 'updated'
                ],
                'results' => [
                    'data' => $receiptVoucher
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
