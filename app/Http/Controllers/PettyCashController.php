<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PettyCash;
use App\PettyCashCategory;

class PettyCashController extends Controller
{
    public function __construct() 
    {
        // $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = PettyCash::join('petty_cash_categories', 'petty_cashes.petty_cash_category_id', '=', 'petty_cash_categories.id')
                            ->join('mst_counters', 'petty_cashes.counter_id', '=', 'mst_counters.id')
                            ->orderBy('petty_cashes.created_at', 'desc')
                            ->paginate($limit,['petty_cashes.*', 'mst_counters.counter_name','petty_cash_categories.petty_cash_category_name']);
        $response = [
            'message' => 'List of petty cash',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = PettyCashCategory::all();
        $dataType = [
            ['id' => 'I','type_name' => 'Pemasukan'],
            ['id' => 'O','type_name' => 'Pengeluaran']
        ];
        $response = [
            'message' => 'Create petty cash',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'petty_cash_category_data' => $data,
                'type_data' => $dataType
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required',
            'petty_cash_category_id' => 'required|numeric',
            'doc_date' => 'required|date_format:Y-m-d',
            'type' => 'required|max:1',
            'amount' => 'required|numeric',
            'desc' => 'required',
            'created_by' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $year = date('Y', strtotime($request->doc_date)); 
        $_year = date('y', strtotime($request->doc_date)); 
        $month = date('m', strtotime($request->doc_date));
        $pettyCashgetCode = PettyCash::select(DB::raw('SUBSTRING(id, 8, 4) AS last_counter'))
                                            ->whereYear('doc_date', $year)
                                            ->whereMonth('doc_date', $month)
                                            ->orderBy('id', 'desc')
                                            ->first();
        $prefix = "PTY";
        $counterPlus = isset($pettyCashgetCode->last_counter) ? intval($pettyCashgetCode->last_counter) + 1 : 1;
        $counter = sprintf("%04s", $counterPlus);
        $code = $prefix.$_year.$month.$counter;

        $pettyCash = new PettyCash();
        $pettyCash->id = $code;
        $pettyCash->counter_id = $request->counter_id;
        $pettyCash->petty_cash_category_id = $request->petty_cash_category_id;
        $pettyCash->doc_date = $request->doc_date;
        $pettyCash->type = $request->type;
        $pettyCash->amount = $request->amount;
        $pettyCash->desc = $request->desc;
        $pettyCash->created_by = $request->created_by;

        if($pettyCash->save()) {
            $pettyCash->show_petty_cash = [
                'url' => url('/v1/petty-cash/'.$pettyCash->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Petty cash created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $pettyCash
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PettyCash::find($id);

        $response = [
            'message' => 'Show petty cash',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pettyCashData = PettyCash::find($id);
        $pettyCashCategoryData = PettyCashCategory::all();
        $dataType = [
            ['id' => 'I','type_name' => 'Pemasukan'],
            ['id' => 'O','type_name' => 'Pengeluaran']
        ];
        $response = [
            'message' => 'Edit petty cash',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'petty_cash_data' => $pettyCashData,
                'petty_cash_category_data' => $pettyCashCategoryData,
                'type_data' => $dataType
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required',
            'petty_cash_category_id' => 'required|numeric',
            'doc_date' => 'required|date_format:Y-m-d',
            'type' => 'required|max:1',
            'amount' => 'required|numeric',
            'desc' => 'required',
            'created_by' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $pettyCash = PettyCash::find($id);
        $pettyCash->counter_id = $request->counter_id;
        $pettyCash->petty_cash_category_id = $request->petty_cash_category_id;
        $pettyCash->doc_date = $request->doc_date;
        $pettyCash->type = $request->type;
        $pettyCash->amount = $request->amount;
        $pettyCash->desc = $request->desc;
        $pettyCash->created_by = $request->created_by;

        if($pettyCash->save()) {
            $pettyCash->show_petty_cash = [
                'url' => url('/v1/petty-cash/'.$pettyCash->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Petty cash updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $pettyCash
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pettyCash = PettyCash::find($id);
        $pettyCash->delete();
        $response = [
            'message' => 'Petty Cash deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];
        return response()->json($response, 200);
    }
}
