<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ExistingSalesTransactions;
use App\ExistingSalesTransactionDetails;
use App\ExistingSalesTransactionPayments;
use App\SalesTransactionDetails;
use App\SalesTransactionPayments;
use App\MstCashier;
use App\MstCounter;
use App\MstWarehouse;
use App\Customer;
use App\CustomerType;
use Illuminate\Support\Facades\Validator;
use App\MstBranch;
use App\MstPaymentMethodTypes;
use App\MstPaymentMethods;
use App\MstEdcMachine;
use App\User;
use App\ExistingMapPaymentMethodTypes;
use Exception;

class SalesExistingController extends Controller
{
    public function index(Request $request) {
        $postJson = $request->json()->all();
        $counterCode = $postJson['counter'];
        $data = $postJson['data'];
        DB::beginTransaction();
        try
        {
            $errors = [];
            $counter = MstCounter::select('*')->where('counter_code', $counterCode)->first();
            if(!$counter) {
                $_counterId = '';
                $errors['counter'] = ['The Counter is not found.'];
            }else {
                $_counterId = $counter->id;
                $chasier = MstCashier::select('*')->where('counter_id', $_counterId)->first();
                $warehouse = MstWarehouse::select('*')->where('counter_id', $_counterId)->first();
                if(!$chasier || !$warehouse) {
                    $errors['counter'] = [];
                }
                if(!$chasier) {
                    $_cashierId = '';
                    array_push($errors['counter'], 'This Cashier by counter is not found.');
                }else {
                    $_cashierId = $chasier->id;
                }
                if(!$warehouse) {
                    $_warehouseId = '';
                    array_push($errors['counter'], 'This Warehouse by counter is not found.');
                }else {
                    $_warehouseId = $warehouse->id;
                }
            }
            $validatorTransaksiHeader = Validator::make($data['transaksi_header'], [
                'NoKassa' => 'required|numeric',
                'Gudang' => 'required',
                'NoStruk' => 'required',
                'Tanggal' => 'required|date_format:Y-m-d',
                'Waktu' => 'required|date_format:H:i:s',
                'Kasir' => 'required',
                'KdStore' => '',
                'TotalItem' => 'required|numeric',
                'TotalNilai' => 'required|numeric',
                'TotalBayar' => 'required|numeric',
                'Kembali' => 'required|numeric',
                'Tunai' => 'required|numeric',
                'KKredit' => 'required|numeric',
                'KDebit' => 'required|numeric',
                'Voucher' => 'required|numeric',
                'VoucherTravel' => 'required|numeric',
                'Discount' => 'required|numeric',
                'Status' => 'required|numeric',
                'KdCustomer' => '',
                'NamaCustomer' => '',
                'Gender' => '',
                'TglLahir' => '',
                'Keterangan' => '',
                'EditDate' => '',
                'EditUser' => '',
                'KdAgent' => '',
                'KdGsa' => '',
                'KdGuide' => '',
                'Ttl_Charge' => '',
                'DPP' => 'required|numeric',
                'TAX' => 'required|numeric',
                'KdMeja' => '',
                'userdisc' => '',
                'nilaidisc' => 'required|numeric',
                'statuskomisi' => 'required|numeric',
                'statuskomisiextra' => 'required|numeric',
                'Valas' => 'required|numeric',
                'Kurs' => 'required|numeric',
                'Valuta' => '',
                'Valas2' => 'required|numeric',
                'Kurs2' => 'required|numeric',
                'Valuta2' => '',
                'Valas3' => 'required|numeric',
                'Kurs3' => 'required|numeric',
                'Valuta3' => '',
                'ValutaKembali' => '',
                'KursKembaliUSD' => 'required|numeric',
                'KursKembaliCNY' => 'required|numeric',
                'SelisihBayar' => 'required|numeric',
                'KembaliValasUSD' => 'required|numeric',
                'KembaliValasCNY' => 'required|numeric',
                'KetVoid' => '',
                'TipeTransaksi' => 'required|numeric',
            ]);
            if ($validatorTransaksiHeader->fails()) {
                $errorValidasiTransaksiHeader['errors'] = json_decode(json_encode($validatorTransaksiHeader->messages()), true);
                $error = array_merge($data['transaksi_header'],$errorValidasiTransaksiHeader);
                $errors['data']['transaksi_header'] = $error;
            } 
            foreach ($data['transaksi_detail'] as $key => $transaksiDetail) {
                $validatorTransaksiDetail = null;
                $validatorTransaksiDetail = Validator::make($transaksiDetail, [
                    'NoKassa' => 'required|numeric',
                    'Gudang' => 'required',
                    'NoStruk' => 'required',
                    'Tanggal' => 'required|date_format:Y-m-d',
                    'Waktu' => 'required|date_format:H:i:s',
                    'Kasir' => 'required',
                    'KdStore' => '',
                    'PCode' => 'required',
                    'Qty' => 'required|numeric',
                    'QtyFree' => 'required|numeric',
                    'Harga' => 'required|numeric',
                    'HargaBeli' => 'required|numeric',
                    'Disc_persen' => 'required|numeric',
                    'Ketentuan1' => '',
                    'Disc1' => 'required|numeric',
                    'Jenis1' => '',
                    'Ketentuan2' => '',
                    'Disc2' => 'required|numeric',
                    'Jenis2' => '',
                    'Ketentuan3' => '',
                    'Disc3' => 'required|numeric',
                    'Jenis3' => '',
                    'Ketentuan4' => '',
                    'Disc4' => 'required|numeric',
                    'Jenis4' => '',
                    'Netto' => 'required|numeric',
                    'Hpp' => 'required|numeric',
                    'Status' => 'required|numeric',
                    'Keterangan' => '',
                    'Service_charge' => 'required|numeric',
                    'Komisi' => 'required|numeric',
                    'PPN' => 'required|numeric',
                    'Printer' => '',
                    'KdMeja' => '',
                    'KdAgent' => ''
                ]);
                if ($validatorTransaksiDetail->fails()) {
                    $errorValidasiTransaksiDetail['errors'] = json_decode(json_encode($validatorTransaksiDetail->messages()), true);
                    $error = array_merge($transaksiDetail,$errorValidasiTransaksiDetail);
                    $errors['data']['transaksi_detail'][] = $error;
                }
            }
            foreach ($data['transaksi_detail_bayar'] as $key => $transaksiDetailBayar) {
                $validatorTransaksiDetailBayar = null;
                $validatorTransaksiDetailBayar = Validator::make($transaksiDetailBayar, [
                    'Gudang' => 'required',
                    'NoKassa' => 'required|numeric',
                    'NoStruk' => 'required',
                    'Jenis' => 'required',
                    'Kode' => '',
                    'NomorKKredit' => '',
                    'NomorKDebet' => '',
                    'NomorVoucher' => '',
                    'Status' => '',
                    'Keterangan' => '',
                    'ExpDate' => '',
                    'NilaiTunai' => 'required|numeric',
                    'NilaiKredit' => 'required|numeric',
                    'NilaiDebet' => 'required|numeric',
                    'NilaiVoucher' => 'required|numeric',
                    'Currency' => 'required',
                    'Valas' => 'required|numeric',
                    'Kurs' => 'required|numeric',
                    'Valuta' => '',
                    'Valas1' => '',
                    'Kurs1' => '',
                    'Valuta1' => '',
                    'Valas2' => '',
                    'Kurs2' => '',
                    'Valuta2' => '',
                    'Valas3' => '',
                    'Kurs3' => '',
                    'Valuta3' => ''
                ]);
                if ($validatorTransaksiDetailBayar->fails()) {
                    $errorValidasiTransaksiDetailBayar['errors'] = json_decode(json_encode($validatorTransaksiDetailBayar->messages()), true);
                    $error = array_merge($transaksiDetail,$errorValidasiTransaksiDetailBayar);
                    $errors['data']['transaksi_detail_bayar'][] = $error;
                } 
            }
            if(count($errors) > 0) {
                $response = [
                    'message' => 'The given data was invalid.',
                    'status' => [
                        'code' => 422,
                        'description' => 'Unprocessable entity'
                    ],
                    'results' =>  $errors,
                ];
                return response()->json($response, 422);
            }
            if(!empty($data['transaksi_header']['KdCustomer'])) {
                if($data['transaksi_header']['TipeTransaksi'] == '1') {
                    $customerTypeReguler = '1';
                    $customerReguler = Customer::select('*')->where(['phone'=> $data['transaksi_header']['KdCustomer'], 'customer_type_id'=> $customerTypeReguler])->first(); 
                    if(!$customerReguler) {
                        $_customerId = '';
                    }else {
                        $_customerId = $customerReguler->id;
                    }
                }else {
                    $customerTypeReseller = '2';
                    $customerReseller = Customer::select('*')->where(['referral_code'=> $data['transaksi_header']['KdCustomer'], 'customer_type_id'=> $customerTypeReseller])->first(); 
                    if(!$customerReseller) {
                        $_customerId = '';
                    }else {
                        $_customerId = $customerReseller->id;
                    }
                }
            }else {
                $_customerId = '';
            }
            $this->deleteSales($counterCode, $data['transaksi_header']['NoStruk']);
            $username = 'posoffline';
            $user = User::select('*')->where('username', $username)->first();
            $cashierId = $_cashierId;
            $userId = $user->id;
            $counterId = $_counterId;
            $warehouseId = $_warehouseId;
            $customerId = $_customerId;
            $gsaId = $data['transaksi_header']['KdGsa'];
            $agentId = $data['transaksi_header']['KdAgent'];
            $guideId = $data['transaksi_header']['KdGuide'];
            $receiptNo = $data['transaksi_header']['NoStruk'];
            $transDate = $data['transaksi_header']['Tanggal'];
            $transTime = $data['transaksi_header']['Waktu'];
            $totalItem = $data['transaksi_header']['TotalItem'];
            $totalValue = $data['transaksi_header']['TotalNilai'];
            $totalPayment = $data['transaksi_header']['TotalBayar'];
            $discount = $data['transaksi_header']['Discount'];
            $change = $data['transaksi_header']['Kembali'];
            $cash = $data['transaksi_header']['Tunai'];
            $creditCard = $data['transaksi_header']['KKredit'];
            $debitCard = $data['transaksi_header']['KDebit'];
            $transfer = 0;
            $voucher = $data['transaksi_header']['Voucher'];
            $memberPoint = 0;
            $reward = 0;
            $travelVoucher = $data['transaksi_header']['VoucherTravel'];
            $taxBasis = $data['transaksi_header']['DPP'];
            $vat = $data['transaksi_header']['TAX'];
            $description = $data['transaksi_header']['Keterangan'];
            $discountCode = $data['transaksi_header']['userdisc'];
            $discountValue = $data['transaksi_header']['nilaidisc'];
            $commissionStatus = $data['transaksi_header']['statuskomisi'];
            $commissionExtraStatus = $data['transaksi_header']['statuskomisiextra'];
            $foreignCurrency1 = $data['transaksi_header']['Valas'];
            $exchangeRate1 = $data['transaksi_header']['Kurs'];
            $currency1 = $data['transaksi_header']['Valuta'];
            $foreignCurrency2 = $data['transaksi_header']['Valas2'];
            $exchangeRate2 = $data['transaksi_header']['Kurs2'];
            $currency2 = $data['transaksi_header']['Valuta2'];
            $foreignCurrency3 = $data['transaksi_header']['Valas3'];
            $exchangeRate3 = $data['transaksi_header']['Kurs3'];
            $currency3 = $data['transaksi_header']['Valuta3'];
            $currencyChange = $data['transaksi_header']['ValutaKembali'];
            $changeRateUsd = $data['transaksi_header']['KursKembaliUSD'];
            $changeRateCny = $data['transaksi_header']['KursKembaliCNY'];
            $changeFcUsd = $data['transaksi_header']['KembaliValasUSD'];
            $changeFcCny = $data['transaksi_header']['KembaliValasCNY'];
            $descVoid = $data['transaksi_header']['KetVoid'];
            $transactionType = $data['transaksi_header']['TipeTransaksi'];
            $status = $data['transaksi_header']['Status'];
            $createdBy = $user->username;
            $updatedBy = $user->username;
            $existingSalesTransactions = new ExistingSalesTransactions();
            $existingSalesTransactions->cashier_id = $cashierId;
            $existingSalesTransactions->user_id = $userId;
            $existingSalesTransactions->counter_id = $counterId;
            $existingSalesTransactions->warehouse_id = $warehouseId;
            $existingSalesTransactions->customer_id = $customerId;
            $existingSalesTransactions->gsa_id = $gsaId;
            $existingSalesTransactions->agent_id = $agentId;
            $existingSalesTransactions->guide_id = $guideId;
            $existingSalesTransactions->receipt_no = $receiptNo;
            $existingSalesTransactions->trans_date = $transDate;
            $existingSalesTransactions->trans_time = $transTime;
            $existingSalesTransactions->total_item = $totalItem;
            $existingSalesTransactions->total_value = $totalValue;
            $existingSalesTransactions->total_payment = $totalPayment;
            $existingSalesTransactions->discount = $discount;
            $existingSalesTransactions->change = $change;
            $existingSalesTransactions->cash = $cash;
            $existingSalesTransactions->credit_card = $creditCard;
            $existingSalesTransactions->debit_card = $debitCard;
            $existingSalesTransactions->transfer = $transfer;
            $existingSalesTransactions->voucher = $voucher;
            $existingSalesTransactions->member_point = $memberPoint;
            $existingSalesTransactions->reward = $reward;
            $existingSalesTransactions->travel_voucher = $travelVoucher;
            $existingSalesTransactions->tax_basis = $taxBasis;
            $existingSalesTransactions->vat = $vat;
            $existingSalesTransactions->description = $description;
            $existingSalesTransactions->discount_code = $discountCode;
            $existingSalesTransactions->discount_value = $discountValue;
            $existingSalesTransactions->commission_status = $commissionStatus;
            $existingSalesTransactions->commission_extra_status = $commissionExtraStatus;
            $existingSalesTransactions->foreign_currency_1 = $foreignCurrency1;
            $existingSalesTransactions->exchange_rate_1 = $exchangeRate1;
            $existingSalesTransactions->currency_1 = $currency1;
            $existingSalesTransactions->foreign_currency_2 = $foreignCurrency2;
            $existingSalesTransactions->exchange_rate_2 = $exchangeRate2;
            $existingSalesTransactions->currency_2 = $currency2;
            $existingSalesTransactions->foreign_currency_3 = $foreignCurrency3;
            $existingSalesTransactions->exchange_rate_3 = $exchangeRate3;
            $existingSalesTransactions->currency_3 = $currency3;
            $existingSalesTransactions->currency_change = $currencyChange;
            $existingSalesTransactions->change_rate_usd = $changeRateUsd;
            $existingSalesTransactions->change_rate_cny = $changeRateCny;
            $existingSalesTransactions->change_fc_usd = $changeFcUsd;
            $existingSalesTransactions->change_fc_cny = $changeFcCny;
            $existingSalesTransactions->desc_void = $descVoid;
            $existingSalesTransactions->transaction_type = $transactionType;
            $existingSalesTransactions->status = $status;
            $existingSalesTransactions->created_by = $createdBy;
            $existingSalesTransactions->updated_by = $updatedBy;
            $existingSalesTransactions->save();
            $existingSalesTransactionId = $existingSalesTransactions->id;
            foreach ($data['transaksi_detail'] as $key => $transaksiDetail) {
                $productid = $transaksiDetail['PCode'];
                $qty = $transaksiDetail['Qty'];
                $freeQty = $transaksiDetail['QtyFree'];
                $price = $transaksiDetail['Harga'];
                $percentDisc1 = $transaksiDetail['Disc_persen'];
                $disc1 = $transaksiDetail['Disc1'];
                $percentDisc2 = 0;
                $disc2 = $transaksiDetail['Disc2'];
                $percentDisc3 = 0;
                $disc3 = $transaksiDetail['Disc3'];
                $percentDisc4 = 0;
                $disc4 = $transaksiDetail['Disc4'];
                $net = $transaksiDetail['Netto'];
                $cogs = $transaksiDetail['Hpp'];
                $percentVat = $transaksiDetail['PPN'];
                $percentCommission = 0;
                $description = $transaksiDetail['Keterangan'];
                $status = $transaksiDetail['Status'];
                $createdBy = $user->username;
                $updatedBy = $user->username;
                $existingSalesTransactionDetails = new ExistingSalesTransactionDetails;
                $existingSalesTransactionDetails->sales_transaction_id = $existingSalesTransactionId;
                $existingSalesTransactionDetails->product_id = $productid;
                $existingSalesTransactionDetails->qty = $qty;
                $existingSalesTransactionDetails->free_qty = $freeQty;
                $existingSalesTransactionDetails->price = $price;
                $existingSalesTransactionDetails->percent_disc_1 = $percentDisc1;
                $existingSalesTransactionDetails->disc_1 = $disc1;
                $existingSalesTransactionDetails->percent_disc_2 = $percentDisc2;
                $existingSalesTransactionDetails->disc_2 = $disc2;
                $existingSalesTransactionDetails->percent_disc_3 = $percentDisc3;
                $existingSalesTransactionDetails->disc_3 = $disc3;
                $existingSalesTransactionDetails->percent_disc_4 = $percentDisc4;
                $existingSalesTransactionDetails->disc_4 = $disc4;
                $existingSalesTransactionDetails->net = $net;
                $existingSalesTransactionDetails->cogs = $cogs;
                $existingSalesTransactionDetails->percent_vat = $percentVat;
                $existingSalesTransactionDetails->percent_commission = $percentCommission;
                $existingSalesTransactionDetails->description = $description;
                $existingSalesTransactionDetails->status = $status;
                $existingSalesTransactionDetails->created_by = $createdBy;
                $existingSalesTransactionDetails->updated_by = $updatedBy;
                $existingSalesTransactionDetails->save();
            }
            foreach ($data['transaksi_detail_bayar'] as $key => $transaksiDetailBayar) {
                $tunai = 'T';
                $kredit = 'K';
                $debit = 'D';
                $tunaiVoucher = 'TV';
                $kreditVoucher = 'KV';
                $debitVoucher = 'DV';
                $tunaiKredit = 'TK';
                $tunaiDebit = 'TD';
                $tunaiKreditVoucher = 'TKV';
                $tunaiDebitVoucher = 'TDV';
                if($transaksiDetailBayar['Jenis'] == $tunai || $transaksiDetailBayar['Jenis'] == $tunaiVoucher) {
                    $existingMapPaymentMethodTypes = ExistingMapPaymentMethodTypes::where('code', $transaksiDetailBayar['Jenis'])->first();
                    $paymentMethodTypeId = $existingMapPaymentMethodTypes->payment_method_type_id;
                    $mstPaymentMethods = MstPaymentMethods::where('payment_method_type_id', $paymentMethodTypeId)->first();
                    $paymentMethodId = $mstPaymentMethods->id;
                    $edcId = 0;
                    $point = 0;
                    $paymentValue = $transaksiDetailBayar['NilaiTunai'];
                    $voucherCode = $transaksiDetailBayar['NomorVoucher'];
                    $exchangeRate = $transaksiDetailBayar['Kurs'];
                    $existingSalesTransactionPayments = new ExistingSalesTransactionPayments;
                    $existingSalesTransactionPayments->sales_transaction_id = $existingSalesTransactionId;
                    $existingSalesTransactionPayments->payment_method_id = $paymentMethodId;
                    $existingSalesTransactionPayments->edc_id = $edcId;
                    $existingSalesTransactionPayments->point = $point;
                    $existingSalesTransactionPayments->payment_value = $paymentValue;
                    $existingSalesTransactionPayments->voucher_code = $voucherCode;
                    $existingSalesTransactionPayments->exchange_rate = $exchangeRate;
                    $existingSalesTransactionPayments->save();
                }else if($transaksiDetailBayar['Jenis'] == $kredit || $transaksiDetailBayar['Jenis'] == $kreditVoucher) {
                    $existingMapPaymentMethodTypes = ExistingMapPaymentMethodTypes::where('code', $transaksiDetailBayar['Jenis'])->first();
                    $paymentMethodTypeId = $existingMapPaymentMethodTypes->payment_method_type_id;
                    $mstPaymentMethods = MstPaymentMethods::where(['payment_method_type_id' => $paymentMethodTypeId, 'payment_name' => $transaksiDetailBayar['NomorKKredit']])->first();
                    $paymentMethodId = $mstPaymentMethods->id;
                    $edcId = 0;
                    $point = 0;
                    $paymentValue = $transaksiDetailBayar['NilaiKredit'];
                    $voucherCode = $transaksiDetailBayar['NomorVoucher'];
                    $exchangeRate = $transaksiDetailBayar['Kurs'];
                    $existingSalesTransactionPayments = new ExistingSalesTransactionPayments;
                    $existingSalesTransactionPayments->sales_transaction_id = $existingSalesTransactionId;
                    $existingSalesTransactionPayments->payment_method_id = $paymentMethodId;
                    $existingSalesTransactionPayments->edc_id = $edcId;
                    $existingSalesTransactionPayments->point = $point;
                    $existingSalesTransactionPayments->payment_value = $paymentValue;
                    $existingSalesTransactionPayments->voucher_code = $voucherCode;
                    $existingSalesTransactionPayments->exchange_rate = $exchangeRate;
                    $existingSalesTransactionPayments->save();
                }else if($transaksiDetailBayar['Jenis'] == $debit || $transaksiDetailBayar['Jenis'] == $debitVoucher) {
                    $existingMapPaymentMethodTypes = ExistingMapPaymentMethodTypes::where('code', $transaksiDetailBayar['Jenis'])->first();
                    $paymentMethodTypeId = $existingMapPaymentMethodTypes->payment_method_type_id;
                    if($transaksiDetailBayar['NomorKDebet'] == 'TRANSFER') {
                        $paymentMethodTypeId = '08';
                    }
                    $mstPaymentMethods = MstPaymentMethods::where(['payment_method_type_id' => $paymentMethodTypeId, 'payment_name' => $transaksiDetailBayar['NomorKDebet']])->first();
                    $paymentMethodId = $mstPaymentMethods->id;
                    $edcId = 0;
                    $point = 0;
                    $paymentValue = $transaksiDetailBayar['NilaiDebet'];
                    $voucherCode = $transaksiDetailBayar['NomorVoucher'];
                    $exchangeRate = $transaksiDetailBayar['Kurs'];
                    $existingSalesTransactionPayments = new ExistingSalesTransactionPayments;
                    $existingSalesTransactionPayments->sales_transaction_id = $existingSalesTransactionId;
                    $existingSalesTransactionPayments->payment_method_id = $paymentMethodId;
                    $existingSalesTransactionPayments->edc_id = $edcId;
                    $existingSalesTransactionPayments->point = $point;
                    $existingSalesTransactionPayments->payment_value = $paymentValue;
                    $existingSalesTransactionPayments->voucher_code = $voucherCode;
                    $existingSalesTransactionPayments->exchange_rate = $exchangeRate;
                    $existingSalesTransactionPayments->save();
                }else if($transaksiDetailBayar['Jenis'] == $tunaiKredit || $transaksiDetailBayar['Jenis'] == $tunaiKreditVoucher) {
                    $existingMapPaymentMethodTypes = ExistingMapPaymentMethodTypes::where('code', $transaksiDetailBayar['Jenis'])->first();
                    $paymentMethodTypeId = $existingMapPaymentMethodTypes->payment_method_type_id;
                    $explodePaymentMethodTypeId = explode(',', $paymentMethodTypeId);
                    //tunai
                    $paymentMethodTypeIdTunai = $explodePaymentMethodTypeId[0];
                    $mstPaymentMethods = MstPaymentMethods::where('payment_method_type_id', $paymentMethodTypeIdTunai)->first();
                    $paymentMethodId = $mstPaymentMethods->id;
                    $edcId = 0;
                    $point = 0;
                    $paymentValue = $transaksiDetailBayar['NilaiTunai'];
                    $voucherCode = $transaksiDetailBayar['NomorVoucher'];
                    $exchangeRate = $transaksiDetailBayar['Kurs'];
                    $existingSalesTransactionPayments = new ExistingSalesTransactionPayments;
                    $existingSalesTransactionPayments->sales_transaction_id = $existingSalesTransactionId;
                    $existingSalesTransactionPayments->payment_method_id = $paymentMethodId;
                    $existingSalesTransactionPayments->edc_id = $edcId;
                    $existingSalesTransactionPayments->point = $point;
                    $existingSalesTransactionPayments->payment_value = $paymentValue;
                    $existingSalesTransactionPayments->voucher_code = $voucherCode;
                    $existingSalesTransactionPayments->exchange_rate = $exchangeRate;
                    $existingSalesTransactionPayments->save();
                    //kredit
                    $paymentMethodTypeIdKredit = $explodePaymentMethodTypeId[1];
                    $mstPaymentMethods = MstPaymentMethods::where(['payment_method_type_id' => $paymentMethodTypeIdKredit, 'payment_name' => $transaksiDetailBayar['NomorKKredit']])->first();
                    $paymentMethodId = $mstPaymentMethods->id;
                    $edcId = 0;
                    $point = 0;
                    $paymentValue = $transaksiDetailBayar['NilaiKredit'];
                    $voucherCode = $transaksiDetailBayar['NomorVoucher'];
                    $exchangeRate = $transaksiDetailBayar['Kurs'];
                    $existingSalesTransactionPayments = new ExistingSalesTransactionPayments;
                    $existingSalesTransactionPayments->sales_transaction_id = $existingSalesTransactionId;
                    $existingSalesTransactionPayments->payment_method_id = $paymentMethodId;
                    $existingSalesTransactionPayments->edc_id = $edcId;
                    $existingSalesTransactionPayments->point = $point;
                    $existingSalesTransactionPayments->payment_value = $paymentValue;
                    $existingSalesTransactionPayments->voucher_code = $voucherCode;
                    $existingSalesTransactionPayments->exchange_rate = $exchangeRate;
                    $existingSalesTransactionPayments->save();
                }else if($transaksiDetailBayar['Jenis'] == $tunaiDebit || $transaksiDetailBayar['Jenis'] == $tunaiDebitVoucher) {
                    $existingMapPaymentMethodTypes = ExistingMapPaymentMethodTypes::where('code', $transaksiDetailBayar['Jenis'])->first();
                    $paymentMethodTypeId = $existingMapPaymentMethodTypes->payment_method_type_id;
                    $explodePaymentMethodTypeId = explode(',', $paymentMethodTypeId);
                    //tunai
                    $paymentMethodTypeIdTunai = $explodePaymentMethodTypeId[0];
                    $mstPaymentMethods = MstPaymentMethods::where('payment_method_type_id', $paymentMethodTypeIdTunai)->first();
                    $paymentMethodId = $mstPaymentMethods->id;
                    $edcId = 0;
                    $point = 0;
                    $paymentValue = $transaksiDetailBayar['NilaiTunai'];
                    $voucherCode = $transaksiDetailBayar['NomorVoucher'];
                    $exchangeRate = $transaksiDetailBayar['Kurs'];
                    $existingSalesTransactionPayments = new ExistingSalesTransactionPayments;
                    $existingSalesTransactionPayments->sales_transaction_id = $existingSalesTransactionId;
                    $existingSalesTransactionPayments->payment_method_id = $paymentMethodId;
                    $existingSalesTransactionPayments->edc_id = $edcId;
                    $existingSalesTransactionPayments->point = $point;
                    $existingSalesTransactionPayments->payment_value = $paymentValue;
                    $existingSalesTransactionPayments->voucher_code = $voucherCode;
                    $existingSalesTransactionPayments->exchange_rate = $exchangeRate;
                    $existingSalesTransactionPayments->save();
                    //debit
                    $paymentMethodTypeIdDebit = $explodePaymentMethodTypeId[1];
                    $mstPaymentMethods = MstPaymentMethods::where(['payment_method_type_id' => $paymentMethodTypeIdDebit, 'payment_name' => $transaksiDetailBayar['NomorKDebet']])->first();
                    $paymentMethodId = $mstPaymentMethods->id;
                    $edcId = 0;
                    $point = 0;
                    $paymentValue = $transaksiDetailBayar['NilaiDebet'];
                    $voucherCode = $transaksiDetailBayar['NomorVoucher'];
                    $exchangeRate = $transaksiDetailBayar['Kurs'];
                    $existingSalesTransactionPayments = new ExistingSalesTransactionPayments;
                    $existingSalesTransactionPayments->sales_transaction_id = $existingSalesTransactionId;
                    $existingSalesTransactionPayments->payment_method_id = $paymentMethodId;
                    $existingSalesTransactionPayments->edc_id = $edcId;
                    $existingSalesTransactionPayments->point = $point;
                    $existingSalesTransactionPayments->payment_value = $paymentValue;
                    $existingSalesTransactionPayments->voucher_code = $voucherCode;
                    $existingSalesTransactionPayments->exchange_rate = $exchangeRate;
                    $existingSalesTransactionPayments->save();
                }
            }
            DB::commit();
            $response = [
                'message' => 'Success',
                'status' => [
                    'code' => 200,
                    'description' => 'OK'
                ],
                'results' => [
                    'sales_transactions' => $existingSalesTransactions,
                    'sales_transaction_details' => ExistingSalesTransactionDetails::where('sales_transaction_id', $existingSalesTransactionId)->get(),
                    'sales_transaction_payments' => ExistingSalesTransactionPayments::where('sales_transaction_id', $existingSalesTransactionId)->get()
                ]
            ];
            return response()->json($response, 200);
        }catch (Exception $e) {
            DB::rollback();
            $response = [
                'message' => 'An error occured. '.$e,
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ]
            ];
            $response = json_encode($response);
            $myfile = fopen("logs/log-api ".date('Y-m-d').".txt", "a+");
            fwrite($myfile, "log-api ". date('Y-m-d h:i:s')."\n");
            fwrite($myfile, $response."\n\n");
            fclose($myfile);
            return response()->json($response, 500);
        }
        
    }

    public function deleteSales($counterCode, $noStruk) {
        $counter = MstCounter::select('*')->where('counter_code', $counterCode)->first();
        $existingSalesTransactions = ExistingSalesTransactions::where(['counter_id' => $counter->id, 'receipt_no' => $noStruk])->first();
        if($existingSalesTransactions) {
            $existingSalesTransactionsId = $existingSalesTransactions->id;
            ExistingSalesTransactionDetails::where('sales_transaction_id', '=', $existingSalesTransactionsId)->delete();
            ExistingSalesTransactionPayments::where('sales_transaction_id', '=', $existingSalesTransactionsId)->delete();
            ExistingSalesTransactions::where('id', '=', $existingSalesTransactionsId)->delete();
        }
    }

    public function getData(Request $request) {
        if($request->get('nostruk')) {
            $existingSalesTransactions = DB::table('existing_sales_transactions')->where('receipt_no', $request->get('nostruk'))->get();
            $data = [];
            foreach($existingSalesTransactions as $key => $value) {
                $existingSalesTransactionDetails =  DB::table('existing_sales_transaction_details')->where('sales_transaction_id', $value->id)->get();
                $existingSalesTransactionPayments =  DB::table('existing_sales_transaction_payments')->where('sales_transaction_id', $value->id)->get();
                $results = [
                    'sales_transactions' => $existingSalesTransactions,
                    'sales_transaction_details' => $existingSalesTransactionDetails,
                    'sales_transaction_payments' => $existingSalesTransactionPayments
                ];
                if($request->get('strukonly') === 'true') {
                    $data[] = $value->receipt_no;
                }else {
                    $data[] = $results;
                }
            }
        }else {
            $validator = Validator::make($request->all(), [
                'kdgu' => 'required',
                'offset' => 'required|numeric',
                'limit' => 'required|numeric'
            ]);
            if ($validator->fails()) {
                $response = [
                    'message' => 'The given data was invalid.',
                    'status' => [
                        'code' => 422,
                        'description' => 'Unprocessable entity'
                    ],
                    'errors' => $validator->messages(),
                ];
                return response()->json($response, 422);
            } 

            $counterCode = $request->get('kdgu');
            $offset = $request->get('offset');
            $limit = $request->get('limit');
            $counter = MstCounter::select('*')->where('counter_code', $counterCode)->first();
            if(!$counter) {
                $response = [
                    'message' => 'The given data was invalid.',
                    'status' => [
                        'code' => 422,
                        'description' => 'Unprocessable entity'
                    ],
                    'errors' => ['kdgu '.$counterCode.' tidak ditemukan'],
                ];
                return response()->json($response, 422);
            }
            $existingSalesTransactions = DB::table('existing_sales_transactions')->where('counter_id', $counter->id)->orderBy('id')->offset($offset)->limit($limit)->get();
            $data = [];
            foreach($existingSalesTransactions as $key => $value) {
                $existingSalesTransactionDetails =  DB::table('existing_sales_transaction_details')->where('sales_transaction_id', $value->id)->get();
                $existingSalesTransactionPayments =  DB::table('existing_sales_transaction_payments')->where('sales_transaction_id', $value->id)->get();
                $results = [
                    'sales_transactions' => $existingSalesTransactions,
                    'sales_transaction_details' => $existingSalesTransactionDetails,
                    'sales_transaction_payments' => $existingSalesTransactionPayments
                ];
                if($request->get('strukonly') === 'true') {
                    $data[] = $value->receipt_no;
                }else {
                    $data[] = $results;
                }
            }
        }

        $response = [
            'message' => 'List of sales existing',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'params' => $request->all(),
                'data' => $data 
            ]
        ];
        return response()->json($response, 200);
    }

    public function check(Request $request) {
        $data = $request->json()->all();
        $response = [
            'message' => 'Check post data sales',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        return response()->json($response, 200);
    }

    public function apiDocumentation() {
        $data = [
            [
                "transaksi_header" =>  [
                    'NoKassa' => 'required|numeric',
                    'Gudang' => 'required',
                    'NoStruk' => 'required',
                    'Tanggal' => 'required|date_format:Y-m-d',
                    'Waktu' => 'required|date_format:H:i:s',
                    'Kasir' => 'required',
                    'KdStore' => '',
                    'TotalItem' => 'required|numeric',
                    'TotalNilai' => 'required|numeric',
                    'TotalBayar' => 'required|numeric',
                    'Kembali' => 'required|numeric',
                    'Tunai' => 'required|numeric',
                    'KKredit' => 'required|numeric',
                    'KDebit' => 'required|numeric',
                    'Voucher' => 'required|numeric',
                    'VoucherTravel' => 'required|numeric',
                    'Discount' => 'required|numeric',
                    'Status' => 'required|numeric',
                    'KdCustomer' => '',
                    'NamaCustomer' => '',
                    'Gender' => '',
                    'TglLahir' => '',
                    'Keterangan' => '',
                    'EditDate' => '',
                    'EditUser' => '',
                    'KdAgent' => '',
                    'KdGsa' => '',
                    'KdGuide' => '',
                    'Ttl_Charge' => '',
                    'DPP' => 'required|numeric',
                    'TAX' => 'required|numeric',
                    'KdMeja' => '',
                    'userdisc' => '',
                    'nilaidisc' => 'required|numeric',
                    'statuskomisi' => 'required|numeric',
                    'statuskomisiextra' => 'required|numeric',
                    'Valas' => 'required|numeric',
                    'Kurs' => 'required|numeric',
                    'Valuta' => '',
                    'Valas2' => 'required|numeric',
                    'Kurs2' => 'required|numeric',
                    'Valuta2' => '',
                    'Valas3' => 'required|numeric',
                    'Kurs3' => 'required|numeric',
                    'Valuta3' => '',
                    'ValutaKembali' => '',
                    'KursKembaliUSD' => 'required|numeric',
                    'KursKembaliCNY' => 'required|numeric',
                    'SelisihBayar' => 'required|numeric',
                    'KembaliValasUSD' => 'required|numeric',
                    'KembaliValasCNY' => 'required|numeric',
                    'KetVoid' => '',
                    'TipeTransaksi' => 'required|numeric',
                ],
                "transaksi_detail" =>  [
                    [
                        'NoKassa' => 'required|numeric',
                        'Gudang' => 'required',
                        'NoStruk' => 'required',
                        'Tanggal' => 'required|date_format:Y-m-d',
                        'Waktu' => 'required|date_format:H:i:s',
                        'Kasir' => 'required',
                        'KdStore' => '',
                        'PCode' => 'required',
                        'Qty' => 'required|numeric',
                        'QtyFree' => 'required|numeric',
                        'Harga' => 'required|numeric',
                        'HargaBeli' => 'required|numeric',
                        'Disc_persen' => 'required|numeric',
                        'Ketentuan1' => '',
                        'Disc1' => 'required|numeric',
                        'Jenis1' => '',
                        'Ketentuan2' => '',
                        'Disc2' => 'required|numeric',
                        'Jenis2' => '',
                        'Ketentuan3' => '',
                        'Disc3' => 'required|numeric',
                        'Jenis3' => '',
                        'Ketentuan4' => '',
                        'Disc4' => 'required|numeric',
                        'Jenis4' => '',
                        'Netto' => 'required|numeric',
                        'Hpp' => 'required|numeric',
                        'Status' => 'required|numeric',
                        'Keterangan' => '',
                        'Service_charge' => 'required|numeric',
                        'Komisi' => 'required|numeric',
                        'PPN' => 'required|numeric',
                        'Printer' => '',
                        'KdMeja' => '',
                        'KdAgent' => ''
                    ]
                ],
                "transaksi_detail_bayar" =>  [
                    [
                        'Gudang' => 'required',
                        'NoKassa' => 'required|numeric',
                        'NoStruk' => 'required',
                        'Jenis' => 'required',
                        'Kode' => '',
                        'NomorKKredit' => '',
                        'NomorKDebet' => '',
                        'NomorVoucher' => '',
                        'Status' => '',
                        'Keterangan' => '',
                        'ExpDate' => '',
                        'NilaiTunai' => 'required|numeric',
                        'NilaiKredit' => 'required|numeric',
                        'NilaiDebet' => 'required|numeric',
                        'NilaiVoucher' => 'required|numeric',
                        'Currency' => 'required',
                        'Valas' => 'required|numeric',
                        'Kurs' => 'required|numeric',
                        'Valuta' => '',
                        'Valas1' => '',
                        'Kurs1' => '',
                        'Valuta1' => '',
                        'Valas2' => '',
                        'Kurs2' => '',
                        'Valuta2' => '',
                        'Valas3' => '',
                        'Kurs3' => '',
                        'Valuta3' => ''
                    ]
                ]
            ]
        ];
        $response = [
            'message' => 'Api dokumentation',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        return response()->json($response, 200);
    }

    public function apiDocumentationResponse(Request $request) {
        $postJson = $request->json()->all();
        $response = [
            [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' => [] 
            ],
            [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
                'results' => []
            ],
            [
                'message' => 'Success',
                'status' => [
                    'code' => 200,
                    'description' => 'OK'
                ],
                'results' => []
            ],
            
        ];
        return response()->json($response, 200);
    }
}
