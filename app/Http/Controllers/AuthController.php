<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\User;
use JWTAuth;
use JWTAuthException;
use App\MstCounter;
use App\MstBranch;
use App\Role;

class AuthController extends Controller
{
   
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        $user = new User;
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);

        $credentials = [
            'email' => $email,
            'password' => $password
        ];
        
        if($user->save())
        {   
            DB::beginTransaction();
            $token = null;
            try {
                if(!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'message' => 'Email or password are incorrect'
                    ], 500);
                }

                $userUpdate = User::find($user->id);
                $userUpdate->remember_token = $token;
                $userUpdate->save();
                DB::commit();
            }catch (JWTException $e) {
                DB::rollback();
                return response()->json([
                    'message' => 'Failed to create token'
                ], 500);
            }

            $user->signin = [
                'url' => url('/signin'),
                'method' => 'POST',
                'params' => 'email, password'
            ];

            $response = [
                'message' => 'User created',
                'user' => $user,
                'token' => $token
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured'
        ];

        return response()->json($response, 500);
    }

    public function signin(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required|min:5'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $username = $request->input('username');
        $password = $request->input('password');

        if ($user = User::where('username', $username)->first()) {
            $credentials = [
                'username' => $username,
                'password' => $password,
            ];

            DB::beginTransaction();
            $token = null;
            try {
                if(!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'message' => 'Username or password are incorrect'
                    ], 500);
                }

                $userUpdate = User::find($user->id);
                $userUpdate->remember_token = $token;
                $userUpdate->save();
                DB::commit();
                
            }catch (JWTException $e) {
                DB::rollback();
                return response()->json([
                    'message' => 'Failed to create token'
                ], 500);
            }

            $mstCounter = $user->counter_id != '0' ? MstCounter::where(['id' => $user->counter_id])->first() : array();
            $mstBranch = $user->branch_id != '0' ? MstBranch::where(['id' => $user->branch_id])->first() : array();
            $roleData = $user->role_id != '0' ? Role::where(['id' => $user->role_id])->first() : array();
            $arrCounterId = array();

            if($user->counter_id == '0') {
                $branchId = $user->branch_id != '0' ? "branch_id = '".$user->branch_id."'" : "branch_id is not null";
                $mstCounterList = MstCounter::select('id')
                                  ->whereRaw($branchId)
                                  ->get();

                $arrMstCounterList = $mstCounterList->toArray();
                
                if(count($arrMstCounterList) > 0) {
                    foreach($mstCounterList as $counter) {
                        array_push($arrCounterId, $counter->id);
                    }
                }
            }

            $user->counter_detail = $mstCounter;
            $user->branch_detail = $mstBranch;
            $user->counter_id_list = $arrCounterId;
            $user->role_detail = $roleData;

            $response = [
                'message' => 'User signin',
                'user' => $user,
                'token' => $token
            ];

            return response()->json($response, 200);
        } else {
            return response()->json([
                'message' => 'Username or password are incorrect'
            ], 500);
        }

        $response = [
            'message' => 'An error occured'
        ];

        return response()->json($response, 500);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'old_password' => 'required|min:6',
            'new_password' => 'required|min:6',
            'updated_by' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        }

        $userId = $request->input('user_id');
        $oldPassword = $request->input('old_password');
        $newPassword = $request->input('new_password');
        $updatedBy = $request->input('updated_by');

        DB::beginTransaction();
        try 
        {
            $user = User::find($userId);
            $user->password = bcrypt($newPassword);
            $user->password_history = bcrypt($oldPassword);
            $user->updated_by = $updatedBy;
            $user->save();

            DB::commit();
            $response = [
                'message' => 'Password updated',
                'status' => [
                    'code' => 201,
                    'description' => 'updated'
                ],
                'results' => [
                    'data' => $user
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }
}
