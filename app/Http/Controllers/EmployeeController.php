<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Employee;
use App\MstBranch;
use App\MstDepot;
use App\MstPosition;
use App\MstDivision;
use App\MstDepartment;
use App\MstLevel;

class EmployeeController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = Employee::select('*')->paginate($limit);

        $response = [
            'message' => 'List of employee',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function pull(Request $request)
    {
        $validator = Validator::make($request->all(), ['created_by' => 'required']);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 
        
        $createdBy = $request->input('created_by');

        $branchData = $this->getBranch($createdBy);
        $depotData = $this->getDepot($createdBy);
        $devisionData = $this->getDevision($createdBy);
        $positionData = $this->getPosition($createdBy);
        $departmentData = $this->getDepartment($createdBy);
        $levelData = $this->getLevel($createdBy);


        $employeeData = [];
        $limit = 100;
		for ($offset=0; $offset < 9999; $offset++) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://139.255.39.26:92/vci/api?action=employee&offset='.$offset.'&limit='.$limit,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));
    
            $response = curl_exec($curl);
            $dataResponse = json_decode($response);  
            curl_close($curl);
            
            if (count($dataResponse) > 0) {
                foreach ($dataResponse as $data) {
                    $check = Employee::where('id', $data->employee_id)->count();
                    if (empty($check)) {
                        $employee = new Employee;
                        $employee->id = $data->employee_id;
                        $employee->user_id = $data->user_id;
                        $employee->branch_id = $data->cabang_id;
                        $employee->depot_id = $data->depo_id;
                        $employee->division_id = $data->divisi_id;
                        $employee->departement_id = $data->departemen_id;
                        $employee->position_id = $data->jabatan_id;
                        $employee->level_id = $data->level_id;
                        $employee->employee_nik = $data->employee_nik;
                        $employee->employee_name = $data->employee_name;
                        $employee->gender = ($data->gender == 'Laki - Laki') ? "male" : "female";
                        $employee->birthday = ($data->birthday == '0000-00-00') ? null : $data->birthday;
                        $employee->email = $data->email;
                        $employee->active = ($data->v_status == 'join') ? 1 : 1;
                        $employee->created_by = $createdBy;
                        if ($employee->save()) {
                            $employeeData[] = $employee;
                        } 
                    }
                }
            } else {
                $response = [
                    'message' => 'Employee created',
                    'status' => [
                        'code' => 200,
                        'description' => 'OK'
                    ],
                    'results' => [
                        'employee_data' => $employeeData,
                        'branch_data' => $branchData,
                        'depot_data' => $depotData,
                        'devision_data' => $devisionData,
                        'position_data' => $positionData,
                        'department_data' => $departmentData,
                        'level_data' => $levelData,
                    ]
                ];
    
                return response()->json($response, 200);
                exit();
            }
            $offset +=99;
        }

    }

    public function getBranch($createdBy) 
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://139.255.39.26:92/vci/api?action=cabang',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response);  
        curl_close($curl);

        $mstBranchData = [];
        if (count($dataResponse) > 0) {
            MstBranch::truncate();
            foreach ($dataResponse as $data) {
                $mstBranch = new MstBranch;
                $mstBranch->id = $data->cabang_id;
                $mstBranch->branch_name = $data->cabang_name;
                $mstBranch->active = 1;
                $mstBranch->created_by = $createdBy;
                if ($mstBranch->save()) {
                    $mstBranchData[] = $mstBranch;
                } 
            }
        }

        return $mstBranchData;
    }

    public function getDepot($createdBy) 
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://139.255.39.26:92/vci/api?action=depo',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response);  
        curl_close($curl);

        $mstDepotData = [];
        if (count($dataResponse) > 0) {
            MstDepot::truncate();
            foreach ($dataResponse as $data) {
                $mstBranch = new MstDepot;
                $mstBranch->id = $data->depo_id;
                $mstBranch->branch_id = $data->cabang_id;
                $mstBranch->depot_name = $data->depo_name;
                $mstBranch->active = 1;
                $mstBranch->created_by = $createdBy;
                if ($mstBranch->save()) {
                    $mstDepotData[] = $mstBranch;
                } 
            }
        }
        return $mstDepotData;
    }

    public function getDevision($createdBy) 
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://139.255.39.26:92/vci/api?action=divisi',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response);  
        curl_close($curl);
        
        $mstDivisionData = [];
        if (count($dataResponse) > 0) {
            MstDivision::truncate();
            foreach ($dataResponse as $data) {
                $mstDivision = new MstDivision;
                $mstDivision->id = $data->divisi_id;
                $mstDivision->devision_name = $data->divisi_name;
                $mstDivision->active = 1;
                $mstDivision->created_by = $createdBy;
                if ($mstDivision->save()) {
                    $mstDivisionData[] = $mstDivision;
                } 
            }
        }
        return $mstDivisionData;
    }

    public function getPosition($createdBy) 
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://139.255.39.26:92/vci/api?action=jabatan',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response);  
        curl_close($curl);
        
        $mstPositionData = [];
        if (count($dataResponse) > 0) {
            MstPosition::truncate();
            foreach ($dataResponse as $data) {
                $mstPosition = new MstPosition;
                $mstPosition->id = $data->jabatan_id;
                $mstPosition->position_name = $data->jabatan_name;
                $mstPosition->active = 1;
                $mstPosition->created_by = $createdBy;
                if ($mstPosition->save()) {
                    $mstPositionData[] = $mstPosition;
                } 
            }
        }
        return $mstPositionData;
    }

    public function getDepartment($createdBy) 
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://139.255.39.26:92/vci/api?action=departemen',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response);  
        curl_close($curl);
        
        $mstDepartmentData = [];
        if (count($dataResponse) > 0) {
            MstDepartment::truncate();
            foreach ($dataResponse as $data) {
                $mstDepartment = new MstDepartment;
                $mstDepartment->id = $data->departemen_id;
                $mstDepartment->department_name = $data->departemen_name;
                $mstDepartment->active = 1;
                $mstDepartment->created_by = $createdBy;
                if ($mstDepartment->save()) {
                    $mstDepartmentData[] = $mstDepartment;
                } 
            }
        }
        return $mstDepartmentData;
    }

    public function getLevel($createdBy) 
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://139.255.39.26:92/vci/api?action=level',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response);  
        curl_close($curl);
        
        $mstLevelData = [];
        if (count($dataResponse) > 0) {
            MstLevel::truncate(); 
            foreach ($dataResponse as $data) {
                $mstLevel = new MstLevel;
                $mstLevel->id = $data->level_id;
                $mstLevel->level_name = $data->level_name;
                $mstLevel->active = 1;
                $mstLevel->created_by = $createdBy;
                if ($mstLevel->save()) {
                    $mstLevelData[] = $mstLevel;
                } 
            }
        }
        return $mstLevelData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
