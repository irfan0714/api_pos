<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Customer;
use App\SalesTransactions;

class CronController extends Controller
{
    public function countSixMonthResellerTransaction()
    {
        $startTime = date('Y-m-d H:i:s');
        
        $currentDate = date('Y-m-d');
        $sixMonthAgo = date("Y-m-d", mktime(0, 0, 0, date("m")-6, date('d')));
        $transDate = "trans_date BETWEEN '".$sixMonthAgo."' AND '".$currentDate."'";

        $resellerData = Customer::select('*')
                        ->where('customer_type_id', '2') // reseller type = 2
                        ->where('status', '1') // active
                        ->get();

        if(count($resellerData) > 0) {
            foreach($resellerData as $reseller) {
                $salesTransactionData = SalesTransactions::selectRaw("customer_id, SUM(total_value) AS sum_total_value, COUNT(id) AS total_receipt")
                                        ->where('customer_id', $reseller->id)
                                        ->whereRaw($transDate)
                                        ->first();

                $sumTotalValue = $salesTransactionData->sum_total_value;
                $totalReceipt = $salesTransactionData->total_receipt;
                $resultUpdate = 1;
                if($sumTotalValue != null) {
                    $avgTotalSales = intval($sumTotalValue) / intval($totalReceipt);
                    if($avgTotalSales < 200000) {
                        $this->deactiveReseller($reseller->id);
                    }
                } else {
                    $this->deactiveReseller($reseller->id);
                }
            }
        }

        $endTime = date('Y-m-d H:i:s');
        $response = [
            'start_time' => $startTime,
            'end_time' => $endTime,
            'message' => 'Reseller status updated',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        // return response()->json($response, 200);
        echo json_encode($response);
        echo "\n";
    }

    private function deactiveReseller($resellerId)
    {
        DB::beginTransaction();
        try
        {
            $reseller = Customer::find($resellerId);
            $reseller->status = '0';
            $reseller->save();
            DB::commit();
        }
        catch (Exception $e)
        {
            DB::rollback();
            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            // return response()->json($response, 500);
            echo json_encode($response);
            echo "\n";
        }
    }
}
