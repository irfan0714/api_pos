<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ApprovalHistory;

class ApprovalHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transaction_id' => 'required',
            'username' => 'required',
            'status' => 'required',
            'trans_date' => 'required|date_format:Y-m-d H:i:s'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $approvalHistory = new ApprovalHistory();
        $approvalHistory->transaction_id = $request->input('transaction_id');
        $approvalHistory->level = 0;
        $approvalHistory->username = $request->input('username');
        $approvalHistory->status = $request->input('status');
        $approvalHistory->trans_date = $request->input('trans_date');
        $approvalHistory->remark = $request->input('remark') ? $request->input('remark') : null;

        if($approvalHistory->save()) {
            $approvalHistory->show_approval_history = [
                'url' => url('/v1/approval-history/'.$approvalHistory->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Approval history created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $approvalHistory
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
