<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstCounter;
use App\MstBranch;

class MstCounterController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $keywords = $request->input('keywords') != 'null' ? 
                    "mst_counters.counter_code LIKE '%".$request->input('keywords')."%'
                     OR mst_counters.counter_name LIKE '%".$request->input('keywords')."%'
                     OR mst_branches.branch_name LIKE '%".$request->input('keywords')."%'" : 'mst_counters.counter_name is not null';

        $data = MstCounter::join('mst_branches', 'mst_counters.branch_id', '=', 'mst_branches.id')
                ->select('mst_counters.*', 'mst_branches.branch_name')
                ->orderBy('mst_counters.counter_name', 'asc')
                ->whereRaw($keywords)
                ->paginate($limit);

        $response = [
            'message' => 'List of counter',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = MstBranch::select('id', 'branch_name')->get();
        $response = [
            'message' => 'Create counter',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'branch_data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'branch_id' => 'required|numeric',
            'counter_code' => 'required',
            'counter_name' => 'required',
            'trans_date' => 'required|date_format:Y-m-d',
            'stock_period' => 'required|date_format:Y-m-d',
            'first_address' => 'required',
            'last_address' => 'required',
            'phone' => 'required',
            'footer_text' => 'required',
            'timezone' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstCounter = new MstCounter();
        $mstCounter->branch_id = $request->input('branch_id');
        $mstCounter->counter_code = $request->input('counter_code');
        $mstCounter->counter_name = $request->input('counter_name');
        $mstCounter->trans_date = $request->input('trans_date');
        $mstCounter->stock_period = $request->input('stock_period');
        $mstCounter->first_address = $request->input('first_address');
        $mstCounter->last_address = $request->input('last_address');
        $mstCounter->phone = $request->input('phone');
        $mstCounter->footer_text = $request->input('footer_text');
        $mstCounter->timezone = $request->input('timezone');
        $mstCounter->created_by = $request->input('created_by');

        if($mstCounter->save()) {
            $mstCounter->show_counter = [
                'url' => url('/v1/counter/'.$mstCounter->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Counter created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $mstCounter
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MstCounter::find($id);

        $response = [
            'message' => 'Show counter',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MstCounter::find($id);
        $branchData = MstBranch::select('id', 'branch_name')->get();
        $response = [
            'message' => 'Edit counter',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data,
                'branch_data' => $branchData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'branch_id' => 'required|numeric',
            'counter_code' => 'required',
            'counter_name' => 'required',
            'trans_date' => 'required|date_format:Y-m-d',
            'stock_period' => 'required|date_format:Y-m-d',
            'first_address' => 'required',
            'last_address' => 'required',
            'footer_text' => 'required',
            'timezone' => 'required',
            'active' => 'required|numeric|max:1',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstCounter = MstCounter::find($id);
        $mstCounter->branch_id = $request->input('branch_id');
        $mstCounter->counter_code = $request->input('counter_code');
        $mstCounter->counter_name = $request->input('counter_name');
        $mstCounter->trans_date = $request->input('trans_date');
        $mstCounter->stock_period = $request->input('stock_period');
        $mstCounter->first_address = $request->input('first_address');
        $mstCounter->last_address = $request->input('last_address');
        $mstCounter->phone = $request->input('phone');
        $mstCounter->footer_text = $request->input('footer_text');
        $mstCounter->timezone = $request->input('timezone');
        $mstCounter->active = $request->input('active');
        $mstCounter->updated_by = $request->input('updated_by');

        if($mstCounter->save()) {
            $mstCounter->show_counter = [
                'url' => url('/v1/counter/'.$mstCounter->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Counter updated',
                'status' => [
                    'code' => 200,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $mstCounter
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstCounter = MstCounter::find($id);
        $mstCounter->delete();
        $response = [
            'message' => 'Counter deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function getAllCounter()
    {
        $data = MstCounter::select('*')->orderBy('counter_name', 'asc')->get();

        $response = [
            'message' => 'List of counter',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }
}
