<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\CustomerDownline;
use Illuminate\Support\Str;
use App\MemberPoint;
use App\CustomerReward;
use App\SalesTransactions;
use App\MstCounter;
use Illuminate\Support\Facades\DB;

class CustomerResellerController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        DB::enableQueryLog(); 
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $keywords = $request->input('keywords') != 'null' ? 
                    "customers.referral_code LIKE '%".$request->input('keywords')."%'
                     OR customers.customer_name LIKE '%".$request->input('keywords')."%'
                     OR customers.phone LIKE '%".$request->input('keywords')."%'" : 'customers.customer_name is not null';

        $data = Customer::leftJoin('customer_downlines', 'customers.id', '=', 'customer_downlines.downline_id')
                ->leftJoin(DB::raw('(SELECT id, referral_code AS referral_code_upline  FROM customers) AS cust_upline'),'customer_downlines.upline_id', '=', 'cust_upline.id')
                ->where('customers.customer_type_id', '2')
                ->whereRaw($keywords)
                ->orderBy('customers.updated_at', 'desc')
                ->select('customers.*','customer_downlines.upline_id', 'cust_upline.referral_code_upline')
                ->paginate($limit,['customers.*', 'customer_types.customer_type_name']);

        $response = [
            'message' => 'List of customer reseller',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'query' =>DB::getQueryLog()
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->where('active', '1')
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $response = [
            'message' => 'Customer reseller for create data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData
            ]
        ];

        return response()->json($response, 200);
    }
    
    private function getReferralCode($latestIncrement) 
    {
        $getLatestCounter = Customer::select('referral_code')
                            ->where('customer_type_id', '2')
                            ->orderByDesc('referral_code')
                            ->first();

        $latestCounter = intval($getLatestCounter->referral_code);
        $incrementCounter = $latestCounter + $latestIncrement;
        $counterNow = sprintf("%05s", $incrementCounter);

        $referralCode = $counterNow;
        $check = Customer::where('referral_code', $referralCode)->first();
        if($check != null) {
            $newIncrement = $latestIncrement + 1;
            $this->getReferralCode($newIncrement);
        } else {
            return $referralCode;
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required',
            'customer_name' => 'required',
            'identity_number' => 'required',
            'phone' => 'required',
            'gender' => 'required',
            'profession' => 'required',
            'area_code' => 'required',
            'area' => 'required',
            'address' => 'required',
            'join_date' => 'required',
            'internal_employee' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $referralCodeUpline = $request->input('referral_code_upline');
        if($referralCodeUpline != '') {
            $findResellerByReferralCode = Customer::where('referral_code', $referralCodeUpline)->first();
            if($findResellerByReferralCode == null) {
                $response = [
                    'message' => 'Referral code not valid.',
                    'status' => [
                        'code' => 200,
                        'description' => 'Unprocessable entity'
                    ],
                    'errors' =>  'Referral code not valid.',
                ];
                return response()->json($response, 200);
            }
        }

        $newReferralCode = $this->getReferralCode(1);
        if($newReferralCode != 'Failed get Counter Data') {
            $customerTypeReseller = 2;  
            $customer = new Customer();
            $customer->customer_type_id = $customerTypeReseller;
            $customer->referral_code = $newReferralCode;
            $customer->customer_name = $request->input('customer_name');
            $customer->identity_number = $request->input('identity_number');
            $customer->phone = $request->input('phone');
            $customer->birth_place = $request->input('birth_place');
            $customer->birth_date = $request->input('birth_date');
            $customer->gender = $request->input('gender');
            $customer->profession = $request->input('profession');
            $customer->area_code = $request->input('area_code');
            $customer->area = $request->input('area');
            $customer->address = $request->input('address');
            $customer->join_date = $request->input('join_date');
            $customer->internal_employee = $request->input('internal_employee');
            $customer->created_by = $request->input('created_by');

            if($customer->save()) {
                $newResellerId = $customer->id;
                $memberPoint = new MemberPoint();
                $memberPoint->customer_id = $newResellerId;
                $memberPoint->created_by = $request->input('created_by');
                $memberPoint->save();

                $customerReward = new CustomerReward();
                $customerReward->customer_id = $newResellerId;
                $customerReward->currency = 'IDR';
                $customerReward->created_by = $request->input('created_by');
                $customerReward->save();
                    
                //set reseller upline
                if($referralCodeUpline != '') {
                    $findResellerByReferralCode  = Customer::where('referral_code', $referralCodeUpline)->first();
                    if($findResellerByReferralCode != null) {
                        $resellerUplineId = $findResellerByReferralCode->id;
                        $customerDownline = new CustomerDownline();
                        $customerDownline->upline_id = $resellerUplineId;
                        $customerDownline->downline_id = $newResellerId;
                        $customerDownline->currency = 'IDR';
                        $customerDownline->created_by = $request->input('created_by');
                        $customerDownline->save();
                    }
                }
                
                $customer->show_customer = [
                    'url' => url('/v1/customer-reseller/'.$customer->id),
                    'method' => 'GET'
                ];

                $response = [
                    'message' => 'Customer Reseller created',
                    'status' => [
                        'code' => 201,
                        'description' => 'Created'
                    ],
                    'results' => [
                        'data' => $customer
                    ]
                ];

                return response()->json($response, 201);
            }

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        } else {
            $response = [
                'message' => $newReferralCode,
                'status' => [
                    'code' => 200,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $newReferralCode,
            ];
            return response()->json($response, 200);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        $customerUpline = CustomerDownline::join('customers', 'customer_downlines.upline_id', '=', 'customers.id')
                                            ->where('customer_downlines.downline_id', $customer->id)
                                            ->first();  
        $customer->referral_code_upline = "";
        if($customerUpline !== null) {
            $customer->referral_code_upline = $customerUpline->referral_code;
        }
        $response = [
            'message' => 'Edit customer reseller',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $customer
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'customer_name' => 'required',
            'identity_number' => 'required',
            'phone' => 'required',
            'gender' => 'required',
            'profession' => 'required',
            'area_code' => 'required',
            'area' => 'required',
            'address' => 'required',
            'join_date' => 'required',
            'internal_employee' => 'required',
            'updated_by' => 'required',
        ]);
        
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $customer = Customer::find($id);
        $customer->customer_name = $request->input('customer_name');
        $customer->identity_number = $request->input('identity_number');
        $customer->phone = $request->input('phone');
        $customer->birth_place = $request->input('birth_place');
        $customer->birth_date = $request->input('birth_date');
        $customer->gender = $request->input('gender');
        $customer->profession = $request->input('profession');
        $customer->area_code = $request->input('area_code');
        $customer->area = $request->input('area');
        $customer->address = $request->input('address');
        $customer->join_date = $request->input('join_date');
        $customer->internal_employee = $request->input('internal_employee');
        $customer->updated_by = $request->input('updated_by');
        if($customer->save()) {
            //update jika ganti upline
            $referralCodeUplineNew = $request->input('referral_code_upline');
            if($referralCodeUplineNew != '') {
                $findResellerUplineByReferralCodeNew  = Customer::where('referral_code', $referralCodeUplineNew)->first();
                if($findResellerUplineByReferralCodeNew != null) {
                    $resellerUplineBefore = CustomerDownline::join('customers', 'customer_downlines.upline_id', '=', 'customers.id')
                    ->where('customer_downlines.downline_id', $customer->id)
                    ->first();
                    if($resellerUplineBefore != null) {
                        $resellerUplineReferralCodeBefore = $resellerUplineBefore->referral_code;
                        if($resellerUplineReferralCodeBefore != $referralCodeUplineNew) {
                            $customerDownline = CustomerDownline::where('downline_id', $customer->id)
                                                                    ->update(['upline_id' => $findResellerUplineByReferralCodeNew->id]);
                        }
                    }else {
                        $findResellerByReferralCode  = Customer::where('referral_code', $referralCodeUplineNew)->first();
                        if($findResellerByReferralCode != null) {
                            $resellerUplineId = $findResellerByReferralCode->id;
                            $newResellerId = $customer->id;
                            $customerDownline = new CustomerDownline();
                            $customerDownline->upline_id = $resellerUplineId;
                            $customerDownline->downline_id = $newResellerId;
                            $customerDownline->currency = 'IDR';
                            $customerDownline->created_by = $request->input('created_by');
                            $customerDownline->save();
                        }
                    }

                }else {
                    $response = [
                        'message' => 'Referral code not valid.',
                        'status' => [
                            'code' => 200,
                            'description' => 'Unprocessable entity'
                        ],
                        'errors' =>  'Referral code not valid.',
                    ];
                    return response()->json($response, 200);
                }
            }
            $customer->show_customer = [
                'url' => url('/v1/customer-reseller/'.$customer->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Customer reseller updated',
                'status' => [
                    'code' => 200,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $customer
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        $response = [
            'message' => 'Customer deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function countResellerMonthlyTransactionByPhoneNumber(Request $request)
    {
        $phone = $request->input('phone') ? $request->input('phone') : '0';
        $resellerData = Customer::where('customer_type_id', '2')
                        ->where('phone', $phone)
                        ->first();

        if($resellerData != null) {
            $firstDate = date('Y-m-01');
            $dateNow = date('Y-m-d');
            $transDate = "trans_date BETWEEN '".$firstDate."' AND '".$dateNow."'";
            $totalTransaction = SalesTransactions::selectRaw('SUM(total_value) as total_sales')
                                ->where('customer_id', $resellerData->id)
                                ->whereRaw($transDate)
                                ->where('status', '1')
                                ->get();

            $total = intval($totalTransaction[0]['total_sales']);
            if($total >= 5000000) {
                $countCashback = $total / 5000000;
                $cashbackAmount = 0;
                for($i = 0; $i < floor($countCashback); $i++) {
                    $cashbackAmount = $cashbackAmount + 250000;
                }
            } else {
                $cashbackAmount = 0;
            }

            $response = [
                'message' => 'Total reseller transaction monthly',
                'status' => [
                    'code' => 200,
                    'description' => 'OK'
                ],
                'results' => [
                    'start_date' => $firstDate,
                    'end_date' => $dateNow,
                    'customer_id' => $resellerData->id,
                    'name' => $resellerData->customer_name,
                    'total_sales' => $totalTransaction[0]['total_sales'],
                    'cashback_amount' => $cashbackAmount
                ]
            ];
            
            return response()->json($response, 200);
        } else {
            $response = [
                'message' => 'Reseller not found!',
                'status' => [
                    'code' => 200,
                    'description' => 'OK'
                ],
                'results' => [
                    'start_date' => null,
                    'end_date' => null,
                    'customer_id' => 0,
                    'name' => null,
                    'total_sales' => 0,
                    'cashback_amount' => 0
                ]
            ];
            
            return response()->json($response, 200);
        }
    }
}
