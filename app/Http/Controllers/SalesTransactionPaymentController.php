<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesTransactions;
use App\SalesTransactionPayments;

class SalesTransactionPaymentController extends Controller
{
    public function store(Request $request)
    {
        $arrSalesTransactionId = $request->input('sales_transaction_id');
        $arrPaymentMethodId = $request->input('payment_method_id');
        $arrEdcId = $request->input('edc_id');
        $arrPoint = $request->input('point');
        $arrPaymentValue = $request->input('payment_value');
        $arrVoucherCode = $request->input('voucher_code');
        $arrExchangeRate = $request->input('exchange_rate');

        if (count($arrSalesTransactionId) > 0) {
            SalesTransactionPayments::where(['sales_transaction_id'=> $arrSalesTransactionId[0]])->delete();
            for ($index=0; $index < count($arrSalesTransactionId); $index++) {
                $salesTransactionPayments = new SalesTransactionPayments;
                $salesTransactionPayments->sales_transaction_id = $arrSalesTransactionId[$index]; 
                $salesTransactionPayments->payment_method_id = $arrPaymentMethodId[$index];
                $salesTransactionPayments->edc_id = $arrEdcId[$index];
                $salesTransactionPayments->point = intval($arrPoint[$index]);
                $salesTransactionPayments->payment_value = intval($arrPaymentValue[$index]);
                $salesTransactionPayments->voucher_code = $arrVoucherCode[$index];
                $salesTransactionPayments->exchange_rate = intval($arrExchangeRate[$index]);
                $salesTransactionPayments->save();
            }
            $salesTransactionData = SalesTransactions::where('id', $arrSalesTransactionId[0])->get();
            $salesTransactionPaymentsData = SalesTransactionPayments::where('sales_transaction_id', $arrSalesTransactionId[0])->get();
            $response = [
                'message' => 'Sales transaction payments created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'sales_transaction_data' => $salesTransactionData,
                    'sales_transaction_payments_data' => $salesTransactionPaymentsData
                ]
            ];

            return response()->json($response, 201);
        }
    }
}
