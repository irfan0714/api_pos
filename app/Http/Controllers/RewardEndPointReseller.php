<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MemberPoint;
use App\Customer;
use App\PointMutation;

class RewardEndPointReseller extends Controller
{
    public function __construct() 
    {
        // $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $customerTypeReseller = 2;
        $data = Customer::join('member_points','customers.id', '=', 'member_points.customer_id')
                            ->join('customer_rewards','customers.id', '=', 'customer_rewards.customer_id')
                            ->where('customers.customer_type_id', $customerTypeReseller)
                            ->orderBy('customers.updated_at', 'desc')
                            ->paginate($limit,['customers.*','member_points.point_remains','customer_rewards.reward_remains']);
 
    
        $response = [
            'message' => 'List of reseller reward end point',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    public function redemPoint(Request $request) {
        $todayDate = date('Y-m-d');
        $point = new PointMutation();
        $point->customer_id = $request->input('customerId');
        $point->point_mutation_type_id = 'O';
        $point->sales_transaction_id = null;
        $point->point = $request->input('pointRedem');
        $point->point_value = 0;
        $point->trans_date = $todayDate;
        $point->expired_date = null;
        $point->status = 1;
        $point->save();

        $memberPoint = MemberPoint::where('customer_id', $request->input('customerId'))->first();
        $memberPoint->point_used = intval($memberPoint->point_used) + intval($request->input('pointRedem'));
        $memberPoint->point_remains = intval($memberPoint->point_remains) - intval($request->input('pointRedem'));
        $memberPoint->save();

        $response = [
            'message' => 'Success redem point',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [] 
        ];

        return response()->json($response, 200);
    }
}
