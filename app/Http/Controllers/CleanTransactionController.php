<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CleanTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'counter_id' => 'required',
            'trans_date' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 
        $userCashierId = $request->input('user_id');
        $counterId = $request->input('counter_id');
        $transDate = $request->input('trans_date');
        
        DB::enableQueryLog();

        $salesSummaryData = DB::select("SELECT
                                            sales_transactions.trans_date,
                                            sales_transactions.counter_id,
                                            COUNT(sales_transactions.receipt_no) AS total_receipt_no,
                                            SUM(sales_transactions.total_item) AS total_item,
                                            SUM(sales_transactions.total_value) AS total_value,
                                            SUM(sales_transactions.total_payment) AS total_payment,
                                            SUM(sales_transactions.cash) AS cash,
                                            SUM(sales_transactions.credit_card) AS credit_card,
                                            SUM(sales_transactions.debit_card) AS debit_card,
				                            SUM(sales_transactions.transfer) AS `transfer`,
                                            SUM(sales_transactions.voucher) AS voucher,
                                            SUM(sales_transactions.member_point) AS member_point,
                                            SUM(sales_transactions.change) AS `change`,
                                            SUM(IF(sales_transactions.currency_1 = 'USD', sales_transactions.foreign_currency_1, 0)) AS USD,
                                            SUM(IF(sales_transactions.currency_1 = 'CNY', sales_transactions.foreign_currency_1, 0)) AS CNY,
                                            SUM(IF(sales_transactions.currency_1 = 'WNY', sales_transactions.foreign_currency_1, 0)) AS WNY,
                                            SUM(sales_transactions.change_fc_cny) AS change_fc_cny,
                                            SUM(sales_transactions.change_fc_usd) AS change_fc_usd
                                        FROM
                                            sales_transactions
                                        WHERE sales_transactions.counter_id = '".$counterId."' AND sales_transactions.user_id = '".$userCashierId."' AND sales_transactions.trans_date = '".$transDate."' AND sales_transactions.status = '1'");
        // $user = DB::table('users')->select('name')->where(['id' => $userCashierId])->first();
        // $salesSummaryData[0]->cashier_name = $user->name;



        $query = DB::getQueryLog();
        $response = [
            'query' => $query,
            'message' => 'Clean transaction data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $salesSummaryData[0]
            ]
        ];
        return response()->json($response, 200);
    }

   
}
