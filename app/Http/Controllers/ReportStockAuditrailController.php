<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\MstCounter;
use App\MstProduct;
use App\MstWarehouse;
use App\StockMutation;
use App\Stock;

class ReportStockAuditrailController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                       ->whereRaw($counterIdforWarehouse)
                       ->where('active', '1')
                       ->orderBy('warehouse_name', 'asc')
                       ->get();

        $productData = MstProduct::select('id', 'product_name')->get();
                    
        $response = [
            'message' => 'Stock auditrail report index',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData,
                'product_data' => $productData
            ]
        ];

        return response()->json($response, 200);
    }

    public function getReportStockAuditrail(Request $request)
    {
        DB::enableQueryLog();
        $counterId = $request->input('counter_id');
        $warehouseId = $request->input('warehouse_id');
        $year = $request->input('year');
        $month = $request->input('month');

        $warehouseIdList = '';
        if($warehouseId == '0') {
            $warehouseData = MstWarehouse::select('id')
                             ->where('counter_id', $counterId)
                             ->where('active', '1')
                             ->get();
                            
            if(count($warehouseData->toArray()) > 0) {
                foreach($warehouseData as $warehouse) {
                    if($warehouseIdList == '') {
                        $warehouseIdList = $warehouse->id;
                    } else {
                        $warehouseIdList = $warehouseIdList . ',' . $warehouse->id;
                    }
                }
            }
        }

        $whereWarehouseIdSM = $warehouseId == '0' ? "stock_mutations.warehouse_id IN (".$warehouseIdList.")" : "stock_mutations.warehouse_id = '".$warehouseId."'";
        $whereWarehouseIdS = $warehouseId == '0' ? "stocks.warehouse_id IN (".$warehouseIdList.")" : "stocks.warehouse_id = '".$warehouseId."'";
        $productId = $request->input('product_id') != 'null' ? $request->input('product_id') : '0';

        $selectedDate = date('Y') ."-". $month ."-". date('d');
        $firstOfTheMonth = date('Y') ."-". $month ."-01";
        $endOfTheMonth = date("Y-m-t", strtotime($selectedDate));
        $transDateMutation = "stock_mutations.trans_date BETWEEN '".$firstOfTheMonth."' AND '".$endOfTheMonth."'";

        $reportData = StockMutation::join('stock_mutation_types', 'stock_mutations.stock_mutation_type_id', '=', 'stock_mutation_types.id')
                      ->join('mst_warehouses', 'stock_mutations.warehouse_id', '=', 'mst_warehouses.id')
                      ->selectRaw('stock_mutations.transaction_id,
                        stock_mutations.warehouse_id,
                        mst_warehouses.warehouse_name,
                        stock_mutations.product_id,
                        stock_mutations.trans_date,
                        stock_mutations.stock_mutation_type_id,
                        stock_mutations.stock_move,
                        stock_mutation_types.stock_mutation_type_name,
                        stock_mutations.qty,
                        stock_mutations.value')
                      ->whereRaw($transDateMutation)
                      ->where('stock_mutations.product_id', $productId)
                      ->whereRaw($whereWarehouseIdSM)
                      ->orderByRaw('stock_mutations.warehouse_id, stock_mutations.trans_date, stock_mutations.transaction_id ASC')
                      ->get();

        $stockData = Stock::selectRaw("year, warehouse_id, product_id, early_".$month." AS early_stock, 
                     in_".$month." AS in_stock, out_".$month." AS out_stock, end_".$month." AS end_stock")
                     ->where('year', $year)
                     ->whereRaw($whereWarehouseIdS)
                     ->where('product_id', $productId)
                     ->get();

        $response = [
            'message' => 'Report Stock Auditrail',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'report_data' => $reportData,
            'stock_data' => $stockData
        ];
        
        return response()->json($response, 200);
    }
}
