<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ProductReceiving;
use App\ProductReceivingDetail;

class ProductReceivingDetailController extends Controller
{
    public function store(Request $request)
    {
        $arrProductReceivingId = $request->input('product_receiving_id');
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrUnit = $request->input('unit');
        $arrQtyRequest = $request->input('qty_request');

        if (count($arrProductReceivingId) > 0) {
            ProductReceivingDetail::where(['product_receiving_id'=> $arrProductReceivingId[0]])->delete();
            for ($index=0; $index < count($arrProductReceivingId); $index++) {
                $productReceivingDetail = new ProductReceivingDetail;
                $productReceivingDetail->product_receiving_id = $arrProductReceivingId[$index]; 
                $productReceivingDetail->product_id = $arrProductId[$index];
                $productReceivingDetail->qty = intval($arrQty[$index]);
                $productReceivingDetail->unit = $arrUnit[$index];
                $productReceivingDetail->qty_request = intval($arrQtyRequest[$index]);
                $productReceivingDetail->save();
            }
            $productReceivingData = ProductReceiving::select('id')->where('id', $arrProductReceivingId[0])->get();
            $productReceivingDetailData = ProductReceivingDetail::where('product_receiving_id', $arrProductReceivingId[0])->get();
            $response = [
                'message' => 'Product receiving details created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'product_receiving_data' => $productReceivingData,
                    'product_receiving_detail_data' => $productReceivingDetailData
                ]
            ];

            return response()->json($response, 201);
        }
    }
}
