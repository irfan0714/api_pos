<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstVoucher;
use App\MstForeignCurrency;

class MstVoucherController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = MstVoucher::select('batch_no', 'expired_date', 'currency', 'nominal',
                'created_at', 'updated_at')
                ->groupBy('batch_no')
                ->orderByDesc('created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of voucher',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $foreignCurrencyData = MstForeignCurrency::select('id', 'foreign_currency_name')->get();

        $response = [
            'message' => 'Get data for voucher create',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $foreignCurrencyData
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = MstVoucher::max('batch_no');
        $newBatchNo = intval($data) + 1;

        $arrVoucherCode = $request->input('voucher_code');
        $nominal = $request->input('nominal');
        $currency = $request->input('currency');
        $expiredDate = $request->input('expired_date');
        $status = $request->input('status');
        $createdBy = $request->input('created_by');

        if (count($arrVoucherCode) > 0) {
            for ($index=0; $index < count($arrVoucherCode); $index++) {
                $mstVoucher = new MstVoucher;
                $mstVoucher->voucher_code = $arrVoucherCode[$index]; 
                $mstVoucher->nominal = $nominal;
                $mstVoucher->currency = $currency;
                $mstVoucher->expired_date = $expiredDate;
                $mstVoucher->batch_no = $newBatchNo;
                $mstVoucher->status = $status;
                $mstVoucher->created_by = $createdBy;
                $mstVoucher->save();
            }

            $voucherData = MstVoucher::where('batch_no', $newBatchNo)->get();
            $response = [
                'message' => 'Voucher created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => $voucherData
            ];

            return response()->json($response, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $headerData = MstVoucher::select('batch_no', 'expired_date', 'currency', 'nominal',
                        'created_at', 'updated_at')
                        ->where('batch_no', $id)
                        ->groupBy('batch_no')
                        ->get();

        $detailData = MstVoucher::select('*')->where('batch_no', $id)->get();

        $foreignCurrencyData = MstForeignCurrency::select('id', 'foreign_currency_name')->get();
        $response = [
            'message' => 'Edit voucher',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'header_data' => $headerData,
                'detail_data' => $detailData,
                'currency_data' => $foreignCurrencyData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nominal' => 'required|numeric',
            'currency' => 'required',
            'expired_date' => 'required|date_format:Y-m-d',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $data = MstVoucher::where('batch_no', $id)->get();

        if (count($data) > 0) {
            foreach($data as $voucher) {
                $mstVoucher = MstVoucher::find($voucher->id);
                $mstVoucher->nominal = $request->input('nominal');
                $mstVoucher->currency = $request->input('currency');
                $mstVoucher->expired_date = $request->input('expired_date');
                $mstVoucher->updated_by = $request->input('updated_by');
                $mstVoucher->save();
            }
            
            $voucherData = MstVoucher::where('batch_no', $id)->get();
            $response = [
                'message' => 'Voucher updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => $voucherData
            ];

            return response()->json($response, 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstVoucherData = MstVoucher::select('*')->where('batch_no', $id)->get();
        foreach($mstVoucherData as $voucher) {
            $mstVoucher = MstVoucher::find($voucher->id);
            $mstVoucher->delete();
        }
        $response = [
            'message' => 'Voucher deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function generate(Request $request)
    {
        $minValue = $request->input('min_value');
        $maxValue = $request->input('max_value');
        $totalCode = $request->input('total_code');
        $voucherCode = array();
        $x = 0;
        while($x < intval($totalCode)) {
            $randomNumber = random_int(intval($minValue), intval($maxValue));
            $checkCode = MstVoucher::where('voucher_code', $randomNumber)->count();
            if($checkCode == 0) {
                array_push($voucherCode, $randomNumber);
                $x++;
            }
        }

        $response = [
            'message' => 'Voucher generated',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $voucherCode
        ];

        return response()->json($response, 200);
    }

    public function updateUsedStatus(Request $request) {
        
        $arrVoucherId = $request->input('id');
        $arrStatus = $request->input('status');
        $arrUpdatedBy = $request->input('updated_by');
        $voucherData = [];

        if (count($arrVoucherId) > 0) {
            try {
                for ($index = 0; $index < count($arrVoucherId); $index++) {
                    $mstVoucher = MstVoucher::find($arrVoucherId[$index]);
                    $mstVoucher->status = $arrStatus[$index];
                    $mstVoucher->updated_by = $arrUpdatedBy[$index];

                    if($mstVoucher->save()) {
                        array_push($voucherData, $mstVoucher);
                    }
                }

                $response = [
                    'message' => 'Voucher updated',
                    'status' => [
                        'code' => 201,
                        'description' => 'Updated'
                    ],
                    'results' => [
                        'voucher_data' => $voucherData
                    ]
                ];
    
                return response()->json($response, 201);
            }
            catch (Exception $e) {
                $response = [
                    'message' => 'An error occured',
                    'status' => [
                        'code' => 500,
                        'description' => 'internal server error'
                    ],
                ];
    
                return response()->json($response, 500);
            }
        }
    }
}
