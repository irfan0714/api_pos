<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstCoa;

class MstCoaController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = MstCoa::select('*')->paginate($limit);

        $response = [
            'message' => 'List of coa',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required',
            'position' => 'required',
            'level' => 'required',
            'parent' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstCoa = new MstCoa();
        $mstCoa->id = $request->input('id');
        $mstCoa->name = $request->input('name');
        $mstCoa->position = $request->input('position');
        $mstCoa->level = $request->input('level');
        $mstCoa->parent = $request->input('parent');
        $mstCoa->created_by = $request->input('created_by');

        if($mstCoa->save()) {
            $mstCoa->show_coa = [
                'url' => url('/v1/coa/'.$mstCoa->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Coa created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $mstCoa
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MstCoa::find($id);

        $response = [
            'message' => 'Show coa',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MstCoa::find($id);
        
        $response = [
            'message' => 'Edit coa',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'level' => 'required',
            'parent' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstCoa = MstCoa::find($id);
        $mstCoa->name = $request->input('name');
        $mstCoa->position = $request->input('position');
        $mstCoa->level = $request->input('level');
        $mstCoa->parent = $request->input('parent');
        $mstCoa->updated_by = $request->input('updated_by');

        if($mstCoa->save()) {
            $mstCoa->show_coa = [
                'url' => url('/v1/coa/'.$mstCoa->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Coa updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $mstCoa
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstCoa = MstCoa::find($id);
        $mstCoa->delete();
        $response = [
            'message' => 'Coa deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function getAllCoa()
    {
        $data = MstCoa::select('*')->get();

        $response = [
            'message' => 'List of coa',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    public function getMstCoaConcat()
    {
        $data = MstCoa::selectRaw("id, CONCAT(id,' - ',name) AS name")
                ->orderBy('name')
                ->get();

        $response = [
            'message' => 'List of coa',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }
}
