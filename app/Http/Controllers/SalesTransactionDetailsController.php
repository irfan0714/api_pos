<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesTransactions;
use App\SalesTransactionDetails;

class SalesTransactionDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrSalesTransactionId = $request->input('sales_transaction_id');
        $arrProductId = $request->input('product_id');
        $arrQty = $request->input('qty');
        $arrPrice = $request->input('price');
        $arrPercentDisc = $request->input('percent_disc_1');
        $arrDisc = $request->input('disc_1');
        $arrNet = $request->input('net');

        if (count($arrSalesTransactionId) > 0) {
            SalesTransactionDetails::where(['sales_transaction_id'=> $arrSalesTransactionId[0]])->delete();
            for ($index=0; $index < count($arrSalesTransactionId); $index++) {
                $salesTransactionDetails = new SalesTransactionDetails;
                $salesTransactionDetails->sales_transaction_id = intval($arrSalesTransactionId[$index]); 
                $salesTransactionDetails->product_id = $arrProductId[$index];
                $salesTransactionDetails->qty = $arrQty[$index];
                $salesTransactionDetails->price = intval($arrPrice[$index]);
                $salesTransactionDetails->percent_disc_1 = intval($arrPercentDisc[$index]);
                $salesTransactionDetails->disc_1 = intval($arrDisc[$index]);
                $salesTransactionDetails->net = intval($arrNet[$index]);
                $salesTransactionDetails->status = 1;
                $salesTransactionDetails->save();
            }
            $salesTransactionData = SalesTransactions::where('id', $arrSalesTransactionId[0])->get();
            $salesTransactionDetailsData = SalesTransactionDetails::where('sales_transaction_id', $arrSalesTransactionId[0])->get();
            $response = [
                'message' => 'Sales transaction details created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'sales_transaction_data' => $salesTransactionData,
                    'sales_transaction_detail_data' => $salesTransactionDetailsData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
