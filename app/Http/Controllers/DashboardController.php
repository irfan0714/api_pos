<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct() 
    {
        // $this->middleware('jwt.auth');
    }

    public function getData() {

        $querySalesPerbulan = DB::select("
            SELECT 
            SUM(IF(bulan = 1, total, 0)) AS bulan1,
            SUM(IF(bulan = 2, total, 0)) AS bulan2,
            SUM(IF(bulan = 3, total, 0)) AS bulan3,
            SUM(IF(bulan = 4, total, 0)) AS bulan4,
            SUM(IF(bulan = 5, total, 0)) AS bulan5,
            SUM(IF(bulan = 6, total, 0)) AS bulan6,
            SUM(IF(bulan = 7, total, 0)) AS bulan7,
            SUM(IF(bulan = 8, total, 0)) AS bulan8,
            SUM(IF(bulan = 9, total, 0)) AS bulan9,
            SUM(IF(bulan = 10, total, 0)) AS bulan10,
            SUM(IF(bulan = 11, total, 0)) AS bulan11,
            SUM(IF(bulan = 12, total, 0)) AS bulan12
            FROM (
            SELECT
            trans_date,
            MONTH(trans_date) AS bulan,
            YEAR(trans_date) AS tahun,
            counter_id,
            total_value AS total
            FROM
            sales_transactions
            WHERE STATUS = '1' AND YEAR(trans_date) = 2022
            UNION ALL
            SELECT
            trans_date,
            MONTH(trans_date) AS bulan,
            YEAR(trans_date) AS tahun,
            counter_id,
            total_value AS total
            FROM
            existing_sales_transactions
            WHERE STATUS = '1' AND YEAR(trans_date) = 2022
            ) sales
        ");
        $arrSalesPerbulan = json_decode(json_encode($querySalesPerbulan[0]), true);
        $dataSalesPerbulan = [];
        for ($i=1; $i <= 12 ; $i++) {
            $dataSalesPerbulan[] = $arrSalesPerbulan['bulan'.$i];
        }

        $querySalesPerbulanReseller = DB::select("
            SELECT 
            SUM(IF(bulan = 1, total, 0)) AS bulan1,
            SUM(IF(bulan = 2, total, 0)) AS bulan2,
            SUM(IF(bulan = 3, total, 0)) AS bulan3,
            SUM(IF(bulan = 4, total, 0)) AS bulan4,
            SUM(IF(bulan = 5, total, 0)) AS bulan5,
            SUM(IF(bulan = 6, total, 0)) AS bulan6,
            SUM(IF(bulan = 7, total, 0)) AS bulan7,
            SUM(IF(bulan = 8, total, 0)) AS bulan8,
            SUM(IF(bulan = 9, total, 0)) AS bulan9,
            SUM(IF(bulan = 10, total, 0)) AS bulan10,
            SUM(IF(bulan = 11, total, 0)) AS bulan11,
            SUM(IF(bulan = 12, total, 0)) AS bulan12
            FROM (
            SELECT
            trans_date,
            MONTH(trans_date) AS bulan,
            YEAR(trans_date) AS tahun,
            counter_id,
            total_value AS total
            FROM
            sales_transactions
            WHERE STATUS = '1' AND YEAR(trans_date) = 2022 AND transaction_type = '2'
            UNION ALL
            SELECT
            trans_date,
            MONTH(trans_date) AS bulan,
            YEAR(trans_date) AS tahun,
            counter_id,
            total_value AS total
            FROM
            existing_sales_transactions
            WHERE STATUS = '1' AND YEAR(trans_date) = 2022 AND transaction_type = '2'
            ) sales
        ");
        $arrSalesPerbulanReseller = json_decode(json_encode($querySalesPerbulanReseller[0]), true);
        $dataSalesPerbulanReseller = [];
        for ($i=1; $i <= 12 ; $i++) {
            $dataSalesPerbulanReseller[] = $arrSalesPerbulanReseller['bulan'.$i];
        }
        $dataSelesPerCounter  = DB::select("
            SELECT a.id, a.counter_code, a.counter_name, b.last_date, IF(b.total IS NULL, 0, b.total) total FROM mst_counters a
            INNER JOIN (SELECT counter_id, MAX(trans_date) AS last_date, SUM(total_value) AS total FROM sales_transactions WHERE STATUS='1' AND YEAR(trans_date) = 2022 AND MONTH(trans_date) = 10 GROUP BY counter_id) b
            ON a.id = b.counter_id
            UNION ALL
            SELECT c.id, c.counter_code, c.counter_name, d.last_date, IF(d.total IS NULL, 0, d.total) total FROM mst_counters c
            LEFT JOIN (SELECT counter_id, MAX(trans_date) AS last_date, SUM(total_value) AS total FROM existing_sales_transactions WHERE STATUS='1' AND YEAR(trans_date) = 2022 AND MONTH(trans_date) = 10 GROUP BY counter_id ) d
            ON c.id = d.counter_id
        ");
        $response = [
            'message' => 'List of data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data_perbulan' => $dataSalesPerbulan,
                'data_perbulan_reseller' => $dataSalesPerbulanReseller,
                'data_percounter' => $dataSelesPerCounter
            ]
        ];
        
        return response()->json($response, 200);
    }
}
