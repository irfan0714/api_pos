<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Menu;

class MenuController extends Controller
{   
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $keywords = $request->input('keywords') != 'null' ? 
                    "menu_name LIKE '%".$request->input('keywords')."%'" : 'menu_name is not null';
        $data = Menu::select('*')->whereRaw($keywords)->paginate($limit);

        $response = [
            'message' => 'List of menu',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Menu::select('id','menu_name')->get();
        $response = [
            'message' => 'Create menu',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data_perent_menu' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'menu_name' => 'required',
            'root' => 'required',
            'weight' => 'required|numeric',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $menu = new Menu();
        $menu->parent_id = $request->input('parent_id');
        $menu->menu_name = $request->input('menu_name');
        $menu->root = $request->input('root');
        $menu->weight = $request->input('weight');
        $menu->icon = $request->input('icon');
        $menu->active = '1';
        $menu->created_by = $request->input('created_by');

        if($menu->save()) {
            $menu->show_menu = [
                'url' => url('/v1/menu/'.$menu->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Menu created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $menu
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Menu::find($id);

        $response = [
            'message' => 'Show menu',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Menu::find($id);
        $dataParentMenu = Menu::select('id','menu_name')->where('parent_id', 0)->get();
        $parentIdMenuSingleArray = Menu::select('id')->where('parent_id', 0)->pluck('id');
        $child1 = Menu::select('id','menu_name')->wherein('parent_id', $parentIdMenuSingleArray)->get();
        //tambah child1
        $dataParentMenu = array_merge($dataParentMenu->toArray(), $child1->toArray());
        $response = [
            'message' => 'Edit menu',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data,
                'data_parent_menu' => $dataParentMenu
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'parent_id' => 'required|numeric',
            'menu_name' => 'required',
            'root' => 'required',
            'weight' => 'required|numeric',
            'active' => 'required|numeric',
            'updated_by' => 'required' 
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 
        
        $menu = Menu::find($id);
        $menu->parent_id = $request->input('parent_id');
        $menu->menu_name = $request->input('menu_name');
        $menu->root = $request->input('root');
        $menu->weight = $request->input('weight');
        $menu->icon = $request->input('icon');
        $menu->active = $request->input('active');
        $menu->updated_by = $request->input('updated_by');

        if($menu->save()) {
            $menu->show_menu = [
                'url' => url('/v1/menu/'.$menu->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Menu updated',
                'status' => [
                    'code' => 200,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $menu
                ]
            ];

            return response()->json($response, 200);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'Internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        $menu->delete();
        $response = [
            'message' => 'Menu deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }
    
}
