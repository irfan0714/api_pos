<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\ProductReceiving;
use App\ProductReceivingDetail;
use App\ProductRequest;
use App\User;
use App\ApprovalHistory;
use App\MstProduct;
use App\MstCounter;
use App\MstWarehouse;
use App\StockMutation;
use MyHelper;

class ProductReceivingController extends Controller
{
    protected $myHelper;

    public function __construct(MyHelper $myHelper) 
    {
        $this->middleware('jwt.auth');
        $this->myHelper = $myHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $counterId = $request->input('counter_id') != 0 ? "product_receivings.counter_id IN (".$request->input('counter_id').")" : 'product_receivings.counter_id is not null';
        $data = ProductReceiving::join('mst_counters', 'mst_counters.id', '=', 'product_receivings.counter_id')
                ->join('mst_warehouses', 'mst_warehouses.id', '=', 'product_receivings.warehouse_id')
                ->join('product_requests', 'product_requests.id', '=', 'product_receivings.product_request_id')
                ->select('product_receivings.*', 'mst_counters.counter_name', 'mst_warehouses.warehouse_name')
                ->whereRaw($counterId)
                ->orderByDesc('product_receivings.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of product receiving',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $productData = MstProduct::select('id', 'product_name')->get();
        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->orderBy('warehouse_name', 'asc')
                         ->get();

        $response = [
            'message' => 'Product receiving for create',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'product_data' => $productData,
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'product_request_id' => 'required',
            'counter_id' => 'required',
            'warehouse_id' => 'required',
            'doc_date' => 'required|date_format:Y-m-d',
            'receipt_no' => 'required',
            'courier' => 'required',
            'desc' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $productReceiving = new ProductReceiving();
        $productReceiving->id = $request->input('id');
        $productReceiving->product_request_id = $request->input('product_request_id');
        $productReceiving->counter_id = $request->input('counter_id');
        $productReceiving->warehouse_id = $request->input('warehouse_id');
        $productReceiving->doc_date = $request->input('doc_date');
        $productReceiving->receipt_no = $request->input('receipt_no');
        $productReceiving->courier = $request->input('courier');
        $productReceiving->desc = $request->input('desc');
        $productReceiving->created_by = $request->input('created_by');

        if($productReceiving->save()) {
            $productReceiving->show_product_receiving = [
                'url' => url('/v1/product-receiving/'.$productReceiving->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Product receiving created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $productReceiving
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $productReceivingData = ProductReceiving::select('*')
            ->where('id', $id)
            ->get();

        foreach($productReceivingData as $receive) {
            $createdBy = $receive->created_by ? $receive->created_by : null;
            $updatedBy = $receive->updated_by ? $receive->updated_by : null;
        }

        $userCreatedData = null;
        $userUpdatedData = null;
        if(isset($createdBy)) {
            $userCreatedData = User::select('*')->where('username', $createdBy)->get();
        }

        if(isset($updatedBy)) {
            $userUpdatedData = User::select('*')->where('username', $updatedBy)->get();
        }

        $approvalHistoryData = ApprovalHistory::join('users', 'users.username', '=', 'approval_histories.username')
            ->select('approval_histories.*', 'users.name')
            ->whereRaw("approval_histories.transaction_id = '".$id."'")
            ->get();

        $productReceivingDetailData = ProductReceivingDetail::join('product_receivings', 'product_receivings.id', '=', 'product_receiving_details.product_receiving_id')
                ->join('mst_products', 'mst_products.id', '=', 'product_receiving_details.product_id')
                ->select('product_receiving_details.*', 'mst_products.product_name')
                ->whereRaw("product_receivings.id = '".$id."'")
                ->get();

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                        ->whereRaw($counterId)
                        ->orderBy('counter_name', 'asc')
                        ->get();

        $warehouseData = MstWarehouse::select('id', 'warehouse_name')
                        ->whereRaw($counterIdforWarehouse)
                        ->orderBy('warehouse_name', 'asc')
                        ->get();

        $response = [
            'message' => 'Product receiving details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'product_receiving_data' => $productReceivingData,
                'product_receiving_detail_data' => $productReceivingDetailData,
                'user_create_data' => $userCreatedData,
                'user_update_data' => $userUpdatedData,
                'approval_history_data' => $approvalHistoryData,
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $productReceiving = ProductReceiving::find($id);
        $productReceiving->status = $request->input('status');
        $productReceiving->updated_by = $request->input('updated_by');

        if($productReceiving->save()) {
            $productReceiving->show_product_receiving = [
                'url' => url('/v1/product-receiving/'.$productReceiving->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Product receiving updated',
                'status' => [
                    'code' => 201,
                    'description' => 'updated'
                ],
                'results' => [
                    'data' => $productReceiving
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeBundle(Request $request)
    {
        $productReceivingData = $request->input('productReceiving');
        $productReceivingDetailData = $request->input('productReceivingDetail');
        
        $productReceivingId = $this->getLatestId($productReceivingData["counter_id"]);

        DB::beginTransaction();
        try
        {
            $productReceiving = new ProductReceiving();
            $productReceiving->id = $productReceivingId;
            $productReceiving->product_request_id = $productReceivingData["product_request_id"];
            $productReceiving->counter_id = $productReceivingData["counter_id"];
            $productReceiving->warehouse_id = $productReceivingData["warehouse_id"];
            $productReceiving->doc_date = $productReceivingData["doc_date"];
            $productReceiving->receipt_no = $productReceivingData["receipt_no"];
            $productReceiving->courier = $productReceivingData["courier"];
            $productReceiving->desc = $productReceivingData["desc"];
            $productReceiving->created_by = $productReceivingData["created_by"];
            $productReceiving->save();

            $arrProductId = $productReceivingDetailData["product_id"];
            $arrQtyReceive = $productReceivingDetailData["qty"];
            $arrUnit = $productReceivingDetailData["unit"];
            $arrQtyRequest = $productReceivingDetailData["qty_request"];
            $arrDesc = $productReceivingDetailData["descriptions"];

            if (count($arrProductId) > 0) {
                ProductReceivingDetail::where(['product_receiving_id'=> $productReceivingId])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $productReceivingDetail = new ProductReceivingDetail;
                    $productReceivingDetail->product_receiving_id = $productReceivingId; 
                    $productReceivingDetail->product_id = $arrProductId[$index];
                    $productReceivingDetail->qty = intval($arrQtyReceive[$index]);
                    $productReceivingDetail->unit = $arrUnit[$index];
                    $productReceivingDetail->qty_request = intval($arrQtyRequest[$index]);
                    $productReceivingDetail->descriptions = $arrDesc[$index];
                    $productReceivingDetail->save();
                }
            }

            $productRequest = ProductRequest::find($productReceivingData["product_request_id"]);
            $productRequest->status = "1";
            $productRequest->updated_by = $productReceivingData["created_by"];
            $productRequest->save();

            DB::commit();
            $response = [
                'message' => 'Product receiving bundle created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'product_receiving_data' => $productReceiving,
                    'product_receiving_detail_data' => $productReceivingDetail
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    public function closeProductReceivingBundle(Request $request)
    {
        $productReceivingData = $request->input('productReceiving');
        $stockMutationData = $request->input('stockMutation');
        $approvalHistoryData = $request->input('approvalHistory');

        DB::beginTransaction();
        try
        {
            $productReceiving = ProductReceiving::find($productReceivingData["id"]);
            $productReceiving->status = $productReceivingData["status"];
            $productReceiving->updated_by = $productReceivingData["updated_by"];
            $productReceiving->save();

            $arrWarehouseId = $stockMutationData["warehouse_id"];
            $arrTransactionId = $stockMutationData["transaction_id"];
            $arrStockMutationTypeId = $stockMutationData["stock_mutation_type_id"];
            $arrProductId = $stockMutationData["product_id"];
            $arrQty = $stockMutationData["qty"];
            $arrValue = $stockMutationData["value"];
            $arrStockMove = $stockMutationData["stock_move"];
            $arrTransDate = $stockMutationData["trans_date"];

            if (count($arrProductId) > 0) {
                for ($index=0; $index < count($arrProductId); $index++) {
                    $stockMutation = new StockMutation;
                    $stockMutation->warehouse_id = $arrWarehouseId[$index];
                    $stockMutation->transaction_id = $arrTransactionId[$index];
                    $stockMutation->stock_mutation_type_id = $arrStockMutationTypeId[$index];
                    $stockMutation->product_id = $arrProductId[$index];
                    $stockMutation->qty = intval($arrQty[$index]);
                    $stockMutation->value = intval($arrValue[$index]);
                    $stockMutation->stock_move = $arrStockMove[$index];
                    $stockMutation->trans_date = $arrTransDate[$index];
                    $stockMutation->save();

                    $paramManageStock = array(
                        'year'=> date('Y'),
                        'month'=> date('m'),
                        'warehouseId'=> $arrWarehouseId[$index],
                        'productId'=> $arrProductId[$index],
                        'qty'=> $arrQty[$index],
                        'value'=> $arrValue[$index],
                        'mutationType'=> "I"
                    );
            
                    $response = $this->myHelper->stockManagement($paramManageStock);
                }
            }

            if($approvalHistoryData["transaction_id"] != null) {
                $approvalHistory = new ApprovalHistory();
                $approvalHistory->transaction_id = $approvalHistoryData["transaction_id"];
                $approvalHistory->level = 0;
                $approvalHistory->username = $approvalHistoryData["username"];
                $approvalHistory->status = $approvalHistoryData["status"];
                $approvalHistory->trans_date = $approvalHistoryData["trans_date"];
                $approvalHistory->remark = $approvalHistoryData["remark"] ? $approvalHistoryData["remark"] : null;
                $approvalHistory->save();
            }

            DB::commit();
            $response = [
                'message' => 'Product receiving closed',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'product_receiving_data' => $productReceiving
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    private function getLatestId($counterId)
    {
        $year = date('y');
        $month = date('m');

        $data = ProductReceiving::select('id')
                ->where('counter_id', $counterId)
                ->orderByDesc('id')
                ->first();

        if($data != null) {
            $latestProductReceiveId = $data->id;
            $splitId = explode("-", $latestProductReceiveId);
            $lastCounter = substr($splitId[0], 6, 3);
            $lastMonth = intval(substr($splitId[0], 4, 2));
            $newCounter = intval($lastCounter) + 1;
            if($lastMonth === intval($month)) {
                if($newCounter <= 9) {
                    $newCounter = '00' . strval($newCounter); 
                }
                if($newCounter > 9 && $newCounter <= 99) { 
                    $newCounter = '0' . strval($newCounter); 
                }
            } else {
              $newCounter = '001';
            }
      
            $productReceiveId = 'RG' . strval($year) . strval($month) . $newCounter . '-' . $splitId[1];
        } else {
            $newCounterId;
            if(intval($counterId) <= 9) {
                $newCounterId = '00' . strval($counterId); 
            }
            if(intval($counterId) > 9 && intval($counterId) <= 99) { 
                $newCounterId = '0' . strval($counterId); 
            }
            $productReceiveId = 'RG' . strval($year) . strval($month) . '001' . '-' . $newCounterId;
        }

        return $productReceiveId;
    }
}
