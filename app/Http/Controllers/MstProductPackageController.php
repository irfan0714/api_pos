<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\MstProductPackage;
use App\StockMutation;
use App\MstWarehouse;
use App\MstProduct;
use App\MstProductBrand;
use App\MstProductSubBrand;
use App\MstProductType;
use App\MstProductSubType;
use App\MstProductCategory;
use App\MstProductSubCategory;
use App\MstProductMarketingType;
use App\User;
use App\MstCounter;

use MyHelper;

class MstProductPackageController extends Controller
{
    protected $myHelper;

    public function __construct(MyHelper $myHelper) 
    {
        $this->middleware('jwt.auth');
        $this->myHelper = $myHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $keywords = $request->input('keywords') != 'null' ? 
                    "mst_products.product_name LIKE '%".$request->input('keywords')."%'
                    OR mst_products.id LIKE '%".$request->input('keywords')."%'" : 'mst_products.product_name is not null';

        $data = MstProductPackage::join('mst_products', 'mst_products.id', '=', 'mst_product_packages.product_package_id')
                ->select('mst_product_packages.product_package_id', 'mst_products.product_name', 'mst_products.price', 'mst_products.active', 
                'mst_products.created_at', 'mst_products.updated_at')
                ->whereRaw($keywords)
                ->orderByDesc('mst_product_packages.created_at')
                ->groupBy('mst_product_packages.product_package_id')
                ->paginate($limit);

        $response = [
            'message' => 'List of product package',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';
        
        $counterData = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();
        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->where('active', '1')
                         ->get();

        $mstProduct = MstProduct::select('id', 'product_name')->get();
        $mstProductBrand = MstProductBrand::select('id', 'product_brand_name')->orderBy('product_brand_name', 'asc')->get();
        $mstProductSubBrand = MstProductSubBrand::select('id', 'product_brand_id', 'product_sub_brand_name')->orderBy('product_sub_brand_name', 'asc')->get();
        $mstProductType = MstProductType::select('id', 'product_type_name')->orderBy('product_type_name', 'asc')->get();
        $mstProductSubType = MstProductSubType::select('id', 'product_type_id', 'product_sub_type_name')->orderBy('product_sub_type_name', 'asc')->get();
        $mstProductCategory = MstProductCategory::select('id', 'product_category_name')->orderBy('product_category_name', 'asc')->get();
        $mstProductSubCategory = MstProductSubCategory::select('id', 'product_category_id', 'product_sub_category_name')->orderBy('product_sub_category_name', 'asc')->get();
        $mstProductMarketingType = MstProductMarketingType::select('id', 'product_marketing_type_name')->orderBy('product_marketing_type_name', 'asc')->get();

        $response = [
            'message' => 'Data for product package create',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'mst_counter_data' => $counterData,
                'mst_warehouse_data' => $warehouseData,
                'mst_product_data' => $mstProduct,
                'mst_product_brand_data' => $mstProductBrand,
                'mst_product_sub_brand_data' => $mstProductSubBrand,
                'mst_product_type_data' => $mstProductType,
                'mst_product_sub_type_data' => $mstProductSubType,
                'mst_product_category_data' => $mstProductCategory,
                'mst_product_sub_category_data' => $mstProductSubCategory,
                'mst_product_marketing_type_data' => $mstProductMarketingType
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allocationQty = $request->input('allocationQty');
        $mstProductPackageData = $request->input('mstProductPackage');
        $mstProductData = $request->input('mstProduct');
        $stockMutationData = $request->input('stockMutation');
        $mstCounterData = $request->input('mstCounter');
        $mstWarehouseData = $request->input('mstWarehouse');

        $year = date('y');
        $month = date('m');
        $date = date('d');

        $productIdLatest = MstProduct::select('*')
                           ->whereRaw("id LIKE '99%'")
                           ->where('product_brand_id', $mstProductData["product_brand_id"])
                           ->whereRaw("id NOT LIKE '9999%'")
                           ->orderByDesc('id')
                           ->first();

        $arrData = isset($productIdLatest) ? $productIdLatest->toArray() : [];
        $productPackageId = '';
        if(count($arrData) > 0) {
            $duplicate = true;
            $incrementNumber = 1;
            while($duplicate == true) {
                $productPackageId = $this->generatePcode($productIdLatest->id, $incrementNumber);
                $checkDuplicate = MstProduct::select('*')
                                  ->where('id', $productPackageId)
                                  ->first();
                if($checkDuplicate == null) {
                    $duplicate = false;
                } else {
                    $incrementNumber = $incrementNumber + 1;
                }
            }
        } else {
            $productPackageId = "990001";
        }

        DB::beginTransaction();
        try
        {
            $mstProduct = new MstProduct();
            $mstProduct->id = $productPackageId;
            $mstProduct->product_brand_id = $mstProductData["product_brand_id"];
            $mstProduct->product_sub_brand_id = $mstProductData["product_sub_brand_id"];
            $mstProduct->product_type_id = $mstProductData["product_type_id"];
            $mstProduct->product_sub_type_id = $mstProductData["product_sub_type_id"];
            $mstProduct->product_category_id = $mstProductData["product_category_id"];
            $mstProduct->product_sub_category_id = $mstProductData["product_sub_category_id"];
            $mstProduct->barcode = $mstProductData["barcode"];
            $mstProduct->product_name = $mstProductData["product_name"];
            $mstProduct->initial_name = $mstProductData["initial_name"];
            $mstProduct->product_marketing_type_id = $mstProductData["product_marketing_type_id"];
            $mstProduct->unit = $mstProductData["unit"];
            $mstProduct->price = $mstProductData["price"];
            $mstProduct->unit_size = "CM";
            $mstProduct->length_size = $mstProductData["length_size"];
            $mstProduct->width_size = $mstProductData["width_size"];
            $mstProduct->height_size = $mstProductData["height_size"];
            $mstProduct->active = "1";
            $mstProduct->created_by = $mstProductData["created_by"];
            $mstProduct->save();

            $arrProductId = $mstProductPackageData["product_id"];
            $arrPrice = $mstProductPackageData["price"];
            $arrQty = $mstProductPackageData["qty"];
            $arrCreatedBy = $mstProductPackageData["created_by"];

            if (count($arrProductId) > 0) {
                MstProductPackage::where(['product_package_id'=> $productPackageId])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $mstProductPackage = new MstProductPackage();
                    $mstProductPackage->product_package_id = $productPackageId; 
                    $mstProductPackage->product_id = $arrProductId[$index];
                    $mstProductPackage->price = $arrPrice[$index];
                    $mstProductPackage->qty = $arrQty[$index];
                    $mstProductPackage->active = "1";
                    $mstProductPackage->created_by = $arrCreatedBy[$index];
                    $mstProductPackage->save();
                }
            }

            // $warehouseData = MstWarehouse::where('counter_id', $mstCounterData["id"])->first();
            $arrStockMutationTypeId = $stockMutationData["stock_mutation_type_id"];
            $arrProductId = $stockMutationData["product_id"];
            $arrQty = $stockMutationData["qty"];
            $arrValue = $stockMutationData["value"];
            $arrStockMove = $stockMutationData["stock_move"];
            $arrTransDate = $stockMutationData["trans_date"];

            if (count($arrProductId) > 0) {
                for ($index = 0; $index < count($arrProductId); $index++) {
                    $qtyMutation = intval($arrQty[$index]) * intval($allocationQty);

                    $stockMutation = new StockMutation();
                    $stockMutation->warehouse_id = $mstWarehouseData["id"];  
                    $stockMutation->transaction_id = $productPackageId;
                    $stockMutation->stock_mutation_type_id = $arrStockMutationTypeId[$index];
                    $stockMutation->product_id = $arrProductId[$index];
                    $stockMutation->qty = $qtyMutation;
                    $stockMutation->value = intval($arrValue[$index]);
                    $stockMutation->stock_move = $arrStockMove[$index];
                    $stockMutation->trans_date = $arrTransDate[$index];
                    $stockMutation->save();

                    $paramManageStock = array(
                        'year'=> date('Y'),
                        'month'=> $month,
                        'warehouseId'=> $mstWarehouseData["id"],
                        'productId'=> $arrProductId[$index],
                        'qty'=> $qtyMutation,
                        'value'=> $arrValue[$index],
                        'mutationType'=> "O"
                    );
            
                    $response = $this->myHelper->stockManagement($paramManageStock);
                }
            }

            $stockMutation = new StockMutation();
            $stockMutation->warehouse_id = $mstWarehouseData["id"];  
            $stockMutation->transaction_id = $productPackageId;
            $stockMutation->stock_mutation_type_id = "MP";
            $stockMutation->product_id = $productPackageId;
            $stockMutation->qty = intval($allocationQty);
            $stockMutation->value = 0;
            $stockMutation->stock_move = "I";
            $stockMutation->trans_date = date('Y-m-d');
            $stockMutation->save();

            $paramAddStock = array(
                'year'=> date('Y'),
                'month'=> $month,
                'warehouseId'=> $mstWarehouseData["id"],
                'productId'=> $productPackageId,
                'qty'=> $allocationQty,
                'value'=> '0',
                'mutationType'=> "I"
            );
    
            $response = $this->myHelper->stockManagement($paramAddStock);

            DB::commit();
            $response = [
                'message' => 'Product package created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $mstProduct
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mstProductAll = MstProduct::select('id', 'product_name')->get();
        $mstProductBrand = MstProductBrand::select('id', 'product_brand_name')->orderBy('product_brand_name', 'asc')->get();
        $mstProductSubBrand = MstProductSubBrand::select('id', 'product_brand_id', 'product_sub_brand_name')->orderBy('product_sub_brand_name', 'asc')->get();
        $mstProductType = MstProductType::select('id', 'product_type_name')->orderBy('product_type_name', 'asc')->get();
        $mstProductSubType = MstProductSubType::select('id', 'product_type_id', 'product_sub_type_name')->orderBy('product_sub_type_name', 'asc')->get();
        $mstProductCategory = MstProductCategory::select('id', 'product_category_name')->orderBy('product_category_name', 'asc')->get();
        $mstProductSubCategory = MstProductSubCategory::select('id', 'product_category_id', 'product_sub_category_name')->orderBy('product_sub_category_name', 'asc')->get();
        $mstProductMarketingType = MstProductMarketingType::select('id', 'product_marketing_type_name')->orderBy('product_marketing_type_name', 'asc')->get();

        $mstProductData = MstProduct::select('*')
            ->where('id', $id)
            ->get();

        foreach($mstProductData as $product) {
            $createdBy = $product->created_by ? $product->created_by : null;
            $updatedBy = $product->updated_by ? $product->updated_by : null;
        }

        $userCreatedData = null;
        $userUpdatedData = null;
        if(isset($createdBy)) {
            $userCreatedData = User::select('*')->where('username', $createdBy)->get();
        }

        if(isset($updatedBy)) {
            $userUpdatedData = User::select('*')->where('username', $updatedBy)->get();
        }

        $mstProductPackageData = MstProductPackage::join('mst_products', 'mst_products.id', '=', 'mst_product_packages.product_id')
                ->select('mst_product_packages.*', 'mst_products.product_name')
                ->whereRaw("mst_product_packages.product_package_id = '".$id."'")
                ->get();

        $response = [
            'message' => 'Product package details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'mst_product_data' => $mstProductData,
                'mst_product_package_data' => $mstProductPackageData,
                'mst_product_all_data' => $mstProductAll,
                'mst_product_brand_data' => $mstProductBrand,
                'mst_product_sub_brand_data' => $mstProductSubBrand,
                'mst_product_type_data' => $mstProductType,
                'mst_product_sub_type_data' => $mstProductSubType,
                'mst_product_category_data' => $mstProductCategory,
                'mst_product_sub_category_data' => $mstProductSubCategory,
                'mst_product_marketing_type_data' => $mstProductMarketingType,
                'user_create_data' => $userCreatedData,
                'user_update_data' => $userUpdatedData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generatePcode($latestProductPackageId, $incrementNumber)
    {
        $lastCounter = substr($latestProductPackageId, 2, 4);
        $newCounter = intval($lastCounter) + $incrementNumber;

        if($newCounter <= 9) { $newCounter = "000" . strval($newCounter); }
        if($newCounter > 9 && $newCounter <= 99) { $newCounter = "00" . strval($newCounter); }
        if($newCounter > 99 && $newCounter <= 999) { $newCounter = "0" . strval($newCounter); }

        $productPackageId = "99" . strval($newCounter);

        return $productPackageId;
    }
}
