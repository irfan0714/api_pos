<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstWarehouse;
use App\MstCounter;
use App\MstWarehouseType;

class MstWarehouseController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $counterId = $request->input('counter_id') != 0 ? "mst_warehouses.counter_id IN (".$request->input('counter_id').")" : 'mst_warehouses.counter_id is not null';
        $keywords = $request->input('keywords') != 'null' ? 
                    "mst_warehouses.warehouse_name LIKE '%".$request->input('keywords')."%'
                     OR mst_counters.counter_name LIKE '%".$request->input('keywords')."%'" : 'mst_warehouses.warehouse_name is not null';

        $data = MstWarehouse::join('mst_warehouse_types', 'mst_warehouse_types.id', '=', 'mst_warehouses.warehouse_type_id')
                ->join('mst_counters', 'mst_counters.id', '=', 'mst_warehouses.counter_id')
                ->select('mst_warehouses.*', 'mst_counters.counter_name', 'mst_warehouse_types.warehouse_type_name')
                ->whereRaw($counterId)
                ->whereRaw($keywords)
                ->orderByDesc('mst_warehouses.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of warehouse',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $counterData = MstCounter::select('id', 'counter_name')->get();
        $warehouseTypeData = MstWarehouseType::select('id', 'warehouse_type_name')->get();
        $response = [
            'message' => 'Create warehouse',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData,
                'warehouse_type_data' => $warehouseTypeData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required|numeric',
            'warehouse_name' => 'required',
            'warehouse_type_id' => 'required|numeric',
            'unit_size' => 'required',
            'length_size' => 'required|numeric',
            'width_size' => 'required|numeric',
            'height_size' => 'required|numeric',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstWarehouse = new MstWarehouse();
        $mstWarehouse->counter_id = $request->input('counter_id');
        $mstWarehouse->warehouse_name = $request->input('warehouse_name');
        $mstWarehouse->warehouse_type_id = $request->input('warehouse_type_id');
        $mstWarehouse->unit_size = $request->input('unit_size');
        $mstWarehouse->length_size = $request->input('length_size');
        $mstWarehouse->width_size = $request->input('width_size');
        $mstWarehouse->height_size = $request->input('height_size');
        $mstWarehouse->created_by = $request->input('created_by');

        if($mstWarehouse->save()) {
            $mstWarehouse->show_warehouse = [
                'url' => url('/v1/warehouse/'.$mstWarehouse->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Warehouse created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $mstWarehouse
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MstWarehouse::find($id);

        $response = [
            'message' => 'Show warehouse',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MstWarehouse::find($id);
        $cashierData = MstCounter::select('id', 'counter_name')->get();
        $warehouseTypeData = MstWarehouseType::select('id', 'warehouse_type_name')->get();
        $response = [
            'message' => 'Edit warehouse',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data,
                'counter_data' => $cashierData,
                'warehouse_type_data' => $warehouseTypeData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required|numeric',
            'warehouse_name' => 'required',
            'warehouse_type_id' => 'required|numeric',
            'unit_size' => 'required',
            'length_size' => 'required|numeric',
            'width_size' => 'required|numeric',
            'height_size' => 'required|numeric',
            'active' => 'required|numeric|max:1',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstWarehouse = MstWarehouse::find($id);
        $mstWarehouse->counter_id = $request->input('counter_id');
        $mstWarehouse->warehouse_name = $request->input('warehouse_name');
        $mstWarehouse->warehouse_type_id = $request->input('warehouse_type_id');
        $mstWarehouse->unit_size = $request->input('unit_size');
        $mstWarehouse->length_size = $request->input('length_size');
        $mstWarehouse->width_size = $request->input('width_size');
        $mstWarehouse->height_size = $request->input('height_size');
        $mstWarehouse->active = $request->input('active');
        $mstWarehouse->updated_by = $request->input('updated_by');

        if($mstWarehouse->save()) {
            $mstWarehouse->show_warehouse = [
                'url' => url('/v1/warehouse/'.$mstWarehouse->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Warehouse updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $mstWarehouse
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstWarehouse = MstWarehouse::find($id);
        $mstWarehouse->delete();
        $response = [
            'message' => 'Warehouse deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function getAllWarehouse()
    {
        $data = MstWarehouse::select('*')->orderBy('warehouse_name', 'asc')->get();

        $response = [
            'message' => 'List of warehouse',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }
}
