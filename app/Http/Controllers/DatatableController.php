<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use \DataTables;

class DatatableController extends Controller
{   

    public function index() {
        return view('datatable');
    }

    public function getDataMenu() {
        $menu = Menu::select('id', 'menu_name', 'root', 'weight', 'icon', 'active');

        return DataTables::eloquent($menu)->toJson();
    }

    public function getDataMenu2() {
        print_r($_POST);
        // $menu = Menu::select('id', 'menu_name', 'root', 'weight', 'icon', 'active');

        // return DataTables::eloquent($menu)->toJson();
    }
}
