<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\StockOpname;
use App\StockOpnameDetail;

class StockOpnameDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrStockOpnameId = $request->input('stock_opname_id');
        $arrProductId = $request->input('product_id');
        $arrQtyReal = $request->input('qty_real');
        $arrQtyProgram = $request->input('qty_program');
        $arrPrice = $request->input('price');

        if (count($arrStockOpnameId) > 0) {
            StockOpnameDetail::where(['stock_opname_id'=> $arrStockOpnameId[0]])->delete();
            for ($index=0; $index < count($arrStockOpnameId); $index++) {
                $stockOpnameDetail = new StockOpnameDetail;
                $stockOpnameDetail->stock_opname_id = $arrStockOpnameId[$index]; 
                $stockOpnameDetail->product_id = $arrProductId[$index];
                $stockOpnameDetail->qty_real = intval($arrQtyReal[$index]);
                $stockOpnameDetail->qty_program = intval($arrQtyProgram[$index]);
                $stockOpnameDetail->price = intval($arrPrice[$index]);
                $stockOpnameDetail->save();
            }
            $stockOpnameData = StockOpname::select('*')->where('id', $arrStockOpnameId[0])->get();
            $stockOpnameDetailData = StockOpnameDetail::where('stock_opname_id', $arrStockOpnameId[0])->get();
            $response = [
                'message' => 'Stock opname detail created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'stock_opname_data' => $stockOpnameData,
                    'stock_opname_detail_data' => $stockOpnameDetailData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
