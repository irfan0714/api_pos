<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MstCounter;
use App\MstWarehouse;
use App\Stock;
use App\SalesTransactions;
use App\SalesTransactionDetails;
use App\StockOpnameDetail;
use App\ProductRequestDetail;

class ReportBranchDistributionController extends Controller
{
    public function getReportStockForBranchDistribution(Request $request)
    {
        DB::enableQueryLog();
        $year = $request->input('year');
        $month = $request->input('month');
        $earlyField = "early_".$month;
        $inField = "in_".$month;
        $outField = "out_".$month;
        $endField = "end_".$month;
        $warehouseId = array();

        $counterCode = $request->input('counter_code');
        $getCounter = MstCounter::select('*')
                        ->where('counter_code', $counterCode)
                        ->get();
            
        $arrCounter = $getCounter->toArray();
        if(count($arrCounter) > 0) {
            $getCounterId = $arrCounter[0]["id"];
        } else {
            $getCounterId = '';
        }

        if($getCounterId != '') {
            $getWarehouse = MstWarehouse::select('*')
                            ->where('counter_id', $getCounterId)
                            ->get();
            
            $arrWarehouse = $getWarehouse->toArray();
            if(count($arrWarehouse) > 0) {
                for($i = 0; $i < count($arrWarehouse); $i++) {
                    array_push($warehouseId, $arrWarehouse[$i]["id"]);
                }
            }
        }

        $stockData = Stock::join('mst_warehouses', 'mst_warehouses.id', '=', 'stocks.warehouse_id')
                    ->join('mst_products', 'mst_products.id', '=', 'stocks.product_id')
                    ->selectRaw("stocks.warehouse_id, stocks.year, mst_warehouses.warehouse_name, mst_products.product_name,
                    stocks.product_id, mst_products.unit, stocks.".$earlyField.", 
                    stocks.".$inField.", stocks.".$outField.", stocks.".$endField."")
                    ->whereRaw("stocks.year = '".$year."'")
                    ->whereIn('stocks.warehouse_id', $warehouseId)
                    ->get();

        $response = [
            'message' => 'Create stock report',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'stock_data' => $stockData,
            ]
        ];

        return response()->json($response, 200);
    }

    public function getReportTransactionCash(Request $request)
    {
        DB::enableQueryLog();
        $counterCode = $request->input('counter_code');
        $getCounter = MstCounter::select('*')
                        ->where('counter_code', $counterCode)
                        ->get();
            
        $arrCounter = $getCounter->toArray();
        if(count($arrCounter) > 0) {
            $getCounterId = $arrCounter[0]["id"];
        } else {
            $getCounterId = '';
        }

        $counterId = $getCounterId != '' ? "sales_transactions.counter_id = '".$getCounterId."'" : 'sales_transactions.counter_id is not null';
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $first = SalesTransactionDetails::join('mst_products', 'sales_transaction_details.product_id', '=', 'mst_products.id')
                ->join('sales_transactions', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
                ->selectRaw('sales_transaction_details.product_id AS PCode,
                            mst_products.product_name AS NamaLengkap,
                            SUM(sales_transaction_details.qty) AS Qty,
                            (sales_transaction_details.price * SUM(sales_transaction_details.qty)) AS Bruto,
                            SUM(sales_transaction_details.net) AS Netto')
                ->whereRaw($counterId)
                ->whereRaw($transDate)
                ->where('sales_transactions.status', '1')
                ->whereRaw("sales_transactions.cash != '0'")
                ->whereRaw("sales_transaction_details.qty != '0'")
                ->groupBy('sales_transaction_details.product_id')
                ->orderByRaw('sales_transaction_details.product_id ASC');

        $second = SalesTransactionDetails::join('mst_products', 'sales_transaction_details.product_id', '=', 'mst_products.id')
                ->join('sales_transactions', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
                ->selectRaw('sales_transaction_details.product_id AS PCode,
                            mst_products.product_name AS NamaLengkap,
                            SUM(sales_transaction_details.free_qty) AS Qty,
                            (sales_transaction_details.price * SUM(sales_transaction_details.qty)) AS Bruto,
                            SUM(sales_transaction_details.net) AS Netto')
                ->whereRaw($counterId)
                ->whereRaw($transDate)
                ->where('sales_transactions.status', '1')
                ->whereRaw("sales_transactions.cash != '0'")
                ->whereRaw("sales_transaction_details.qty = '0'")
                ->groupBy('sales_transaction_details.product_id')
                ->orderByRaw('sales_transaction_details.product_id ASC');

        $resultData = $first->union($second)->get();

        $response = [
            'message' => 'Report Transaction Cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $resultData,
            'sql_query' => $first->union($second)->toSql()
        ];
        
        return response()->json($response, 200);
    }
    
    public function getReportTransactionNonCash(Request $request)
    {
        DB::enableQueryLog();
        $counterCode = $request->input('counter_code');
        $getCounter = MstCounter::select('*')
                        ->where('counter_code', $counterCode)
                        ->get();
            
        $arrCounter = $getCounter->toArray();
        if(count($arrCounter) > 0) {
            $getCounterId = $arrCounter[0]["id"];
        } else {
            $getCounterId = '';
        }

        $counterId = $getCounterId != '' ? "sales_transactions.counter_id = '".$getCounterId."'" : 'sales_transactions.counter_id is not null';
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $data = SalesTransactionDetails::join('mst_products', 'sales_transaction_details.product_id', '=', 'mst_products.id')
                ->join('sales_transactions', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
                ->selectRaw('sales_transactions.trans_date AS Tanggal,
                            sales_transactions.receipt_no AS NoStruk,
                            sales_transaction_details.product_id AS PCode,
                            mst_products.product_name AS NamaLengkap,
                            IF (sales_transaction_details.qty != 0, sales_transaction_details.qty, sales_transaction_details.free_qty) AS Qty,
                            (sales_transaction_details.price * sales_transaction_details.qty) AS Bruto,
                            sales_transaction_details.net AS Netto')
                ->whereRaw($counterId)
                ->whereRaw($transDate)
                ->where('sales_transactions.status', '1')
                ->whereRaw("sales_transactions.credit_card != '0' OR sales_transactions.debit_card != '0' OR sales_transactions.voucher != '0' OR sales_transactions.member_point != '0'")
                ->orderByRaw('sales_transactions.trans_date, sales_transactions.receipt_no ASC')
                ->get();

        $sql = SalesTransactionDetails::join('mst_products', 'sales_transaction_details.product_id', '=', 'mst_products.id')
        ->join('sales_transactions', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
        ->selectRaw('sales_transactions.trans_date AS Tanggal,
                    sales_transactions.receipt_no AS NoStruk,
                    sales_transaction_details.product_id AS PCode,
                    mst_products.product_name AS NamaLengkap,
                    IF (sales_transaction_details.qty != 0, sales_transaction_details.qty, sales_transaction_details.free_qty) AS Qty,
                    (sales_transaction_details.price * sales_transaction_details.qty) AS Bruto,
                    sales_transaction_details.net AS Netto')
        ->whereRaw($counterId)
        ->whereRaw($transDate)
        ->where('sales_transactions.status', '1')
        ->whereRaw("sales_transactions.credit_card != '0' OR sales_transactions.debit_card != '0' OR sales_transactions.voucher != '0' OR sales_transactions.member_point != '0'")
        ->orderByRaw('sales_transactions.trans_date, sales_transactions.receipt_no ASC')->toSql();

        $response = [
            'message' => 'Report Transaction Cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'sql_query' => $sql
        ];
        
        return response()->json($response, 200);
    }

    public function getReportStockOpname(Request $request)
    {
        DB::enableQueryLog();
        $counterCode = $request->input('counter_code');
        $getCounter = MstCounter::select('*')
                        ->where('counter_code', $counterCode)
                        ->get();
            
        $arrCounter = $getCounter->toArray();
        if(count($arrCounter) > 0) {
            $getCounterId = $arrCounter[0]["id"];
        } else {
            $getCounterId = '';
        }

        $counterId = $getCounterId != '' ? "stock_opnames.counter_id = '".$getCounterId."'" : 'stock_opnames.counter_id is not null';
        $docDate = "stock_opnames.doc_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $data = StockOpnameDetail::join('mst_products', 'stock_opname_details.product_id', '=', 'mst_products.id')
                ->join('stock_opnames', 'stock_opname_details.stock_opname_id', '=', 'stock_opnames.id')
                ->join('mst_counters', 'stock_opnames.counter_id', '=', 'mst_counters.id')
                ->join('mst_warehouses', 'stock_opnames.warehouse_id', '=', 'mst_warehouses.id')
                ->selectRaw('mst_counters.counter_code as KdCounter,
                            mst_counters.counter_name as NamaCounter,
                            mst_warehouses.warehouse_name as NamaGudang,
                            stock_opname_details.stock_opname_id as KdStockOpname,
                            stock_opnames.doc_date as TanggalDokumen,
                            stock_opname_details.product_id AS PCode,
                            stock_opname_details.qty_real AS QtyFisik,
                            stock_opname_details.qty_program AS QtyProgram')
                ->whereRaw($counterId)
                ->whereRaw($docDate)
                ->where('stock_opnames.status', '1')
                ->orderByRaw('stock_opname_details.product_id ASC')
                ->get();

        $response = [
            'message' => 'Report Stock Opname',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    public function getReportProductRequest(Request $request)
    {
        DB::enableQueryLog();
        $counterCode = $request->input('counter_code');
        $getCounter = MstCounter::select('*')
                        ->where('counter_code', $counterCode)
                        ->get();
            
        $arrCounter = $getCounter->toArray();
        if(count($arrCounter) > 0) {
            $getCounterId = $arrCounter[0]["id"];
        } else {
            $getCounterId = '';
        }

        $counterId = $getCounterId != '' ? "product_requests.counter_id = '".$getCounterId."'" : 'product_requests.counter_id is not null';
        $docDate = "product_requests.doc_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $data = ProductRequestDetail::join('mst_products', 'product_request_details.product_id', '=', 'mst_products.id')
                ->join('product_requests', 'product_request_details.product_request_id', '=', 'product_requests.id')
                ->join('mst_counters', 'product_requests.counter_id', '=', 'mst_counters.id')
                ->join('mst_warehouses', 'product_requests.warehouse_id', '=', 'mst_warehouses.id')
                ->selectRaw('mst_counters.counter_code as KdCounter,
                            mst_counters.counter_name as NamaCounter,
                            mst_warehouses.warehouse_name as NamaGudang,
                            product_request_details.product_request_id as KdPermintaanBarang,
                            product_requests.doc_date as TanggalDokumen,
                            product_requests.need_date as TanggalKebutuhan,
                            product_request_details.product_id as PCode,
                            product_request_details.qty')
                ->whereRaw($counterId)
                ->whereRaw($docDate)
                ->where('product_requests.status', '1')
                ->orderByRaw('product_request_details.product_id ASC')
                ->get();

        $response = [
            'message' => 'Report Product Request',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /*public function getReportTransactionForBranchDistribution(Request $request)
    {
        DB::enableQueryLog();
        $counterCode = $request->input('counter_code');
        $getCounter = MstCounter::select('*')
                        ->where('counter_code', $counterCode)
                        ->get();
            
        $arrCounter = $getCounter->toArray();
        if(count($arrCounter) > 0) {
            $getCounterId = $arrCounter[0]["id"];
        } else {
            $getCounterId = '';
        }

        $counterId = $getCounterId != '' ? "sales_transactions.counter_id = '".$getCounterId."'" : 'sales_transactions.counter_id is not null';
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $data = SalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'sales_transactions.cashier_id')
                ->join('users', 'users.id', '=', 'sales_transactions.user_id')
                ->selectRaw('sales_transactions.id, sales_transactions.counter_id, mst_counters.counter_name, mst_counters.counter_code, 
                mst_cashiers.cashier_name, users.name as user_name,
                sales_transactions.trans_date, sales_transactions.trans_time, sales_transactions.receipt_no,
                sales_transactions.total_value AS total, sales_transactions.discount, sales_transactions.total_value AS netto,
                sales_transactions.tax_basis, (sales_transactions.total_value + sales_transactions.tax_basis) AS grand_total,
                ((sales_transactions.cash + (sales_transactions.foreign_currency_1 * sales_transactions.exchange_rate_1) + 
                (sales_transactions.foreign_currency_2 * sales_transactions.exchange_rate_2) + (sales_transactions.foreign_currency_3 * sales_transactions.exchange_rate_3)) - 
                sales_transactions.change) as nettunai, sales_transactions.cash AS tunai, sales_transactions.credit_card, sales_transactions.debit_card,
                (sales_transactions.foreign_currency_1 * sales_transactions.exchange_rate_1) AS valas_usd,
                (sales_transactions.foreign_currency_2 * sales_transactions.exchange_rate_2) AS valas_cny,
                (sales_transactions.foreign_currency_3 * sales_transactions.exchange_rate_3) AS valas_wny, sales_transactions.voucher, sales_transactions.member_point,
                (sales_transactions.cash + sales_transactions.credit_card + sales_transactions.debit_card + sales_transactions.voucher + sales_transactions.member_point +
                (sales_transactions.foreign_currency_1 * sales_transactions.exchange_rate_1) + (sales_transactions.foreign_currency_2 * sales_transactions.exchange_rate_2) +
                (sales_transactions.foreign_currency_3 * sales_transactions.exchange_rate_3)) as total_bayar,
                sales_transactions.change AS kembali')
                ->whereRaw($counterId)
                ->whereRaw($transDate)
                ->where('sales_transactions.status', '1')
                ->orderByRaw('sales_transactions.counter_id, sales_transactions.trans_date, sales_transactions.trans_time ASC')
                ->get();

        $response = [
            'message' => 'Report Transaction Cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }*/
}
