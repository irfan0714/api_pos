<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstWarehouse;
use App\MstCounter;
use App\MstProduct;
use App\Stock;

class ReportStockController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $year = $request->input('year');
        $month = $request->input('month');
        $earlyField = "early_".$month;
        $inField = "in_".$month;
        $outField = "out_".$month;
        $endField = "end_".$month;
        $arrField = array();
        array_push($arrField, $earlyField, $inField, $outField, $endField);

        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';
        $warehouseId = $request->input('warehouse_id');
        if($warehouseId == "0") {
            $warehouseFirstRow = MstWarehouse::select('*')
                                 ->whereRaw($counterIdforWarehouse)
                                 ->where('warehouse_type_id', '1') //warehouse reguler
                                 ->where('active', '1')
                                 ->first();
            $warehouseId = $warehouseFirstRow->id;
        }

        $keywords = $request->input('keywords') != 'null' ? 
                    "mst_products.product_name LIKE '%".$request->input('keywords')."%'
                     OR mst_products.id LIKE '%".$request->input('keywords')."%'
                     OR mst_products.barcode LIKE '%".$request->input('keywords')."%'" : 'mst_products.product_name is not null';

        $productData = MstProduct::select('*')
                       ->whereRaw($keywords)
                       ->first();

        if($request->input('keywords') != 'null') {
            $productId = $productData != null ? "stocks.product_id = '".$productData->id."'" : 'stocks.product_id is not null';
        } else {
            $productId = 'stocks.product_id is not null';
        }

        $stockData = Stock::join('mst_warehouses', 'mst_warehouses.id', '=', 'stocks.warehouse_id')
                    ->join('mst_products', 'mst_products.id', '=', 'stocks.product_id')
                    ->selectRaw("stocks.warehouse_id, stocks.year, mst_warehouses.warehouse_name, mst_products.product_name,
                    stocks.product_id, mst_products.unit, stocks.".$earlyField.", 
                    stocks.".$inField.", stocks.".$outField.", stocks.".$endField."")
                    ->whereRaw("stocks.year = '".$year."'")
                    ->whereRaw("stocks.warehouse_id = '".$warehouseId."'")
                    ->whereRaw($productId)
                    ->get();
        
        $warehouseData = MstWarehouse::select('*')->whereRaw($counterIdforWarehouse)->get();

        $response = [
            'message' => 'Create stock report',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'warehouse_data' => $warehouseData,
                'stock_field' => $arrField,
                'stock_data' => $stockData,
            ]
        ];

        return response()->json($response, 200);
    }
}
