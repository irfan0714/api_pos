<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MstCounter;
use App\MstWarehouse;
use App\Stock;
use MyHelper;

class ClosedMonthController extends Controller
{
    protected $myHelper;

    public function __construct(MyHelper $myHelper) 
    {
        $this->middleware('jwt.auth');
        $this->myHelper = $myHelper;
    }

    public function process(Request $request)
    {
        $year = $request->input('year');
        $month = $request->input('month');
        $yearBefore = $request->input('year_before');
        $monthBefore = $request->input('month_before');
        $counterId = $request->input('counter_id');

        $warehouseData = MstWarehouse::select('id')
                       ->where('counter_id', $counterId)
                       ->get();
        
        $arrWarehouseData = $warehouseData->toArray();
        $arrWarehouseId = array();
        if(count($arrWarehouseData) > 0) {
            foreach($warehouseData as $warehouse) {
                array_push($arrWarehouseId, $warehouse->id);
            }

            $endField = "end_".$monthBefore;
            $endValueField = "end_value_".$monthBefore;
            $stockData = Stock::select("year", "warehouse_id", "product_id",
                        "".$endField." as end_field", "".$endValueField." as end_value_field")
                        ->whereIn('warehouse_id', $arrWarehouseId)
                        ->where('year', $yearBefore)
                        ->get();

            $arrStockData = $stockData->toArray();
            if(count($arrStockData) > 0) {
                foreach($stockData as $stock) {
                    $paramManageStock = array(
                        'year'=> $year,
                        'month'=> $month,
                        'warehouseId'=> $stock->warehouse_id,
                        'productId'=> $stock->product_id,
                        'qty'=> $stock->end_field,
                        'value'=> $stock->end_value_field,
                        'mutationType'=> "X"
                    );
            
                    $response = $this->myHelper->stockManagement($paramManageStock);   
                }

                $response = [
                    'message' => 'Closed day process run successfully',
                    'status' => [
                        'code' => 200,
                        'description' => 'Updated'
                    ]
                ];
    
                return response()->json($response, 200);
            } else {
                $response = [
                    'message' => 'An error occured',
                    'status' => [
                        'code' => 500,
                        'description' => 'internal server error'
                    ],
                ];
        
                return response()->json($response, 500);
            }
        } else {
            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];
    
            return response()->json($response, 500);
        }
    }
}
