<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\CashierDeposite;
use App\CashierDepositeDetail;
use App\CashierCapital;
use App\User;

class CashierDepositeController extends Controller
{
    public function __construct() 
    {
        // $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = CashierDeposite::select('*')->paginate($limit);

        $response = [
            'message' => 'List of cashier deposite',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($counterId)
    {
        $userKasirData = User::select('id', 'name')->where('counter_id', '=', $counterId)->get();
        $response = [
            'message' => 'Cashier deposite create',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'user_kasir_data' => $userKasirData,
            ]
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required|numeric',
            'user_id' => 'required',
            'cashier_capital_id' => 'required',
            'transaction_date' => 'required|date_format:Y-m-d',
            'amount' => 'required|numeric',
            'diff' => 'required|numeric',
            'cash' => 'required|numeric',
            'credit_card' => 'required|numeric',
            'debit_card' => 'required|numeric',
            'voucher' => 'required|numeric',
            'description' => 'required',
            'cashier_capital_amount' => 'required|numeric',
            'cash_report' => 'required|numeric',
            'change' => 'required|numeric',
            'created_by' => 'required',
        ]);
        

        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, $response['status']['code']);
        } 

        $cashierDeposite = new CashierDeposite();
        $cashierDeposite->counter_id = $request->counter_id;
        $cashierDeposite->user_id = $request->user_id;
        $cashierDeposite->cashier_capital_id = $request->cashier_capital_id;
        $cashierDeposite->transaction_date = $request->transaction_date;
        $cashierDeposite->amount = $request->amount;
        $cashierDeposite->diff = $request->diff;
        $cashierDeposite->cash = $request->cash;
        $cashierDeposite->credit_card = $request->credit_card;
        $cashierDeposite->debit_card = $request->debit_card;
        $cashierDeposite->voucher = $request->voucher;
        $cashierDeposite->description = $request->description;
        $cashierDeposite->cashier_capital_amount = $request->cashier_capital_amount;
        $cashierDeposite->cash_report = $request->cash_report;
        $cashierDeposite->change = $request->change;
        $cashierDeposite->created_by = $request->created_by;

        if($cashierDeposite->save()) {
            $cashierDeposite->show_cashier_deposite = [
                'url' => url('/v1/cashier-deposite/'.$cashierDeposite->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Cashier deposite created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $cashierDeposite
                ]
            ];

        }else {
            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];
        }
        return response()->json($response, $response['status']['code']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $counterId)
    {
        $cashierDepositeData = CashierDeposite::find($id); 
        $userKasirData = User::select('name')->where(['id' => $cashierDepositeData->user_id])->first(); 
        $cashierDepositeData->user_kasir_name = $userKasirData->name;
        $response = [
            'message' => 'Edit setor kasir',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'cashier_deposite_data' => $cashierDepositeData,
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required|numeric',
            'user_id' => 'required',
            'cashier_capital_id' => 'required',
            'transaction_date' => 'required|date_format:Y-m-d',
            'amount' => 'required|numeric',
            'diff' => 'required|numeric',
            'cash' => 'required|numeric',
            'credit_card' => 'required|numeric',
            'debit_card' => 'required|numeric',
            'voucher' => 'required|numeric',
            'description' => 'required',
            'cashier_capital_amount' => 'required|numeric',
            'cash_report' => 'required|numeric',
            'change' => 'required|numeric',
            'updated_by' => 'required',
        ]);
        

        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, $response['status']['code']);
        }

        $cashierDeposite = CashierDeposite::find($id);
        $cashierDeposite->counter_id = $request->counter_id;
        $cashierDeposite->user_id = $request->user_id;
        $cashierDeposite->cashier_capital_id = $request->cashier_capital_id;
        $cashierDeposite->transaction_date = $request->transaction_date;
        $cashierDeposite->amount = $request->amount;
        $cashierDeposite->diff = $request->diff;
        $cashierDeposite->cash = $request->cash;
        $cashierDeposite->credit_card = $request->credit_card;
        $cashierDeposite->debit_card = $request->debit_card;
        $cashierDeposite->voucher = $request->voucher;
        $cashierDeposite->description = $request->description;
        $cashierDeposite->cashier_capital_amount = $request->cashier_capital_amount;
        $cashierDeposite->cash_report = $request->cash_report;
        $cashierDeposite->change = $request->change;
        $cashierDeposite->updated_by = $request->updated_by;

        if($cashierDeposite->save()) {
            $cashierDeposite->show_cashier_deposite = [
                'url' => url('/v1/cashier-deposite/'.$cashierDeposite->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Cashier deposite updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $cashierDeposite
                ]
            ];

        }else {
            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];
        }
        return response()->json($response, $response['status']['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cashierDeposite = CashierDeposite::findOrFail($id);
        $cashierDeposite->delete();
        $response = [
            'message' => 'Cashier deposite deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function depositoValueByUserCashierId($userCashierId,$transDate) {
        DB::enableQueryLog();
        $depositeData = DB::table('sales_transactions')
                                ->select(DB::raw('cashier_id, SUM(total_value) AS tot_val, SUM(total_payment) AS tot_payment, SUM(`change`) AS tot_change, SUM(cash) AS tot_cash, SUM(cash - `change`) AS tot_cash_act, SUM(credit_card) AS tot_credit_card, SUM(debit_card) AS tot_debit_card, SUM(voucher) AS tot_voucher' ))
                                ->where('trans_date', '=', $transDate)
                                ->where('user_id', '=', $userCashierId)
                                ->first();
        $query = DB::getQueryLog();
        if(!$depositeData) {
            $depositeData = [
                'user_id' => $cashierUserId,
                'tot_val' => 0,
                'tot_payment' => 0,
                'tot_change' => 0,
                'tot_cash' => 0,
                'tot_cash_act' => 0,
                'tot_credit_card' => 0,
                'tot_debit_card' => 0,
                'tot_voucher' => 0
            ];
        }
        $cashierCapitalData = CashierCapital::select('id','amount')->where(['user_id' => $userCashierId, 'transaction_date' => $transDate])->first();
        if(!$cashierCapitalData) {
            $cashierCapitalData = [
                'id' => 0,
                'amount' => 0
            ];
        }
        $response = [
            'message' => 'Deposite value by cashier id',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'deposite_data' => $depositeData,
                'cashier_capital_data' => $cashierCapitalData,
                'query' => $query
            ]
        ];
        return response()->json($response, $response['status']['code']);
    }
}
