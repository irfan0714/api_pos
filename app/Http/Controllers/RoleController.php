<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Role;

class RoleController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Role::select('*')->paginate(10);

        $response = [
            'message' => 'List of role',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role_name' => 'required',
            'all_access' => 'required|numeric|max:1',
            'created_by' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $role = new Role();
        $role->role_name = $request->input('role_name');
        $role->all_access = $request->input('all_access');
        $role->active = '1';
        $role->created_by = $request->input('created_by');

        if($role->save()) {
            $role->show_role = [
                'url' => url('/v1/role/'.$role->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Role created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $role
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'Internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Role::find($id);

        $response = [
            'message' => 'Show role',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Role::find($id);
        $response = [
            'message' => 'Edit role',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data,
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'role_name' => 'required',
            'all_access' => 'required|numeric|max:1',
            'active' => 'required|numeric',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $role = Role::find($id);
        $role->role_name = $request->input('role_name');
        $role->all_access = $request->input('all_access');
        $role->active = $request->input('active');
        $role->updated_by = $request->input('updated_by');

        if($role->save()) {
            $role->show_role = [
                'url' => url('/v1/role/'.$role->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Role updated',
                'status' => [
                    'code' => 200,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $role
                ]
            ];

            return response()->json($response, 200);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'Internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        $response = [
            'message' => 'Role deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function getAllRole()
    {
        $data = Role::get();

        $response = [
            'message' => 'List of role',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }
}
