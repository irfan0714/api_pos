<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ReceiptVoucher;
use App\ReceiptVoucherDetail;

class ReceiptVoucherDetailController extends Controller
{
    public function store(Request $request)
    {
        $arrReceiptVoucherId = $request->input('receipt_voucher_id');
        $arrCounterId = $request->input('counter_id');
        $arrCoa = $request->input('coa');
        $arrAmount = $request->input('amount');
        $arrDesc = $request->input('desc');

        if (count($arrReceiptVoucherId) > 0) {
            ReceiptVoucherDetail::where(['receipt_voucher_id'=> $arrReceiptVoucherId[0]])->delete();
            for ($index=0; $index < count($arrReceiptVoucherId); $index++) {
                $receiptVoucherDetail = new ReceiptVoucherDetail;
                $receiptVoucherDetail->receipt_voucher_id = $arrReceiptVoucherId[$index];
                $receiptVoucherDetail->counter_id = $arrCounterId[$index]; 
                $receiptVoucherDetail->coa = $arrCoa[$index];
                $receiptVoucherDetail->amount = intval($arrAmount[$index]);
                $receiptVoucherDetail->desc = $arrDesc[$index];
                $receiptVoucherDetail->save();
            }
            $receiptVoucherData = ReceiptVoucher::select('id')->where('id', $arrReceiptVoucherId[0])->get();
            $receiptVoucherDetailData = ReceiptVoucherDetail::where('receipt_voucher_id', $arrReceiptVoucherId[0])->get();
            $response = [
                'message' => 'Receipt voucher details created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'receipt_voucher_data' => $receiptVoucherData,
                    'receipt_voucher_detail_data' => $receiptVoucherDetailData
                ]
            ];

            return response()->json($response, 201);
        }
    }
}
