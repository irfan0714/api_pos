<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\WarehouseMutation;
use App\WarehouseMutationDetail;
use App\WarehouseMutationType;
use App\StockMutation;
use App\User;
use App\ApprovalHistory;
use App\MstCounter;
use App\MstWarehouse;
use App\MstBranch;
use MyHelper;

class WarehouseMutationController extends Controller
{
    protected $myHelper;

    public function __construct(MyHelper $myHelper) 
    {
        $this->middleware('jwt.auth');
        $this->myHelper = $myHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $counterId = $request->input('counter_id') != 0 ? "warehouse_mutations.from_counter_id IN (".$request->input('counter_id').")" : 'warehouse_mutations.from_counter_id is not null';
        $menuName = $request->input('menu_name') ? $request->input('menu_name') : null;
        
        if($menuName === 'warehouse_out') {
            $statusQuery = "warehouse_mutations.status is not null";
            $receiveStatusQuery = "warehouse_mutations.receive_status is not null";
        } else if($menuName === 'warehouse_in') {
            $statusQuery = "warehouse_mutations.status = '1'";
            $receiveStatusQuery = "warehouse_mutations.receive_status is not null";
        } else {
            $statusQuery = "warehouse_mutations.status is not null";
            $receiveStatusQuery = "warehouse_mutations.receive_status is not null";
        }
        $data = WarehouseMutation::join('mst_counters as mst_counter_from', 'mst_counter_from.id', '=', 'warehouse_mutations.from_counter_id')
                ->join('mst_counters as mst_counter_to', 'mst_counter_to.id', '=', 'warehouse_mutations.to_counter_id')
                ->join('mst_warehouses as mst_warehouse_from', 'mst_warehouse_from.id', '=', 'warehouse_mutations.from_warehouse_id')
                ->join('mst_warehouses as mst_warehouse_to', 'mst_warehouse_to.id', '=', 'warehouse_mutations.to_warehouse_id')
                ->join('warehouse_mutation_types', 'warehouse_mutation_types.id', '=', 'warehouse_mutations.warehouse_mutation_type_id')
                ->selectRaw('warehouse_mutations.*, mst_counter_from.counter_name as from_counter_name, mst_counter_to.counter_name as to_counter_name,
                mst_warehouse_from.warehouse_name as from_warehouse_name, mst_warehouse_to.warehouse_name as to_warehouse_name,
                warehouse_mutation_types.warehouse_mutation_type_name')
                ->whereRaw($counterId)
                ->whereRaw($statusQuery)
                ->whereRaw($receiveStatusQuery)
                ->orderByDesc('warehouse_mutations.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of warehouse mutation',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';
        
        $data = WarehouseMutationType::select('*')->get();
        $counterData = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();
        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->where('active', '1')
                         ->get();

        $response = [
            'message' => 'Warehouse mutation for create data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'counter_data' => $counterData,
            'warehouse_data' => $warehouseData
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $warehouseMutationData = $request->input('warehouseMutation');
        $warehouseMutationDetailData = $request->input('warehouseMutationDetail');

        DB::beginTransaction();
        try
        {
            //id = WM2202110001 (prefix + year + month + counter_id + warehouse_id + counter)
            $year = date('y');
            $month = date('m');
            $formCounterId = $warehouseMutationData["from_counter_id"];
            $fromWarehouseId = $warehouseMutationData["from_warehouse_id"];
            $warehouseMutationGetCode = WarehouseMutation::select(DB::raw('SUBSTRING(id, 8, 4) AS last_counter'))
                                                        ->where('from_counter_id', $warehouseMutationData["from_counter_id"])
                                                        ->where('from_warehouse_id', $warehouseMutationData["from_warehouse_id"])
                                                        ->orderBy('id', 'desc')
                                                        ->first();
            $prefix = "WM";
            $counterPlus = isset($warehouseMutationGetCode->last_counter) ? intval($warehouseMutationGetCode->last_counter) + 1 : 1;
            $counter = sprintf("%04s", $counterPlus);
            $code = $prefix.$formCounterId.$fromWarehouseId.$year.$month.$counter;

            $warehouseMutation = new WarehouseMutation();
            $warehouseMutation->id = $code;
            $warehouseMutation->from_counter_id = $warehouseMutationData["from_counter_id"];
            $warehouseMutation->to_counter_id = $warehouseMutationData["to_counter_id"];
            $warehouseMutation->from_warehouse_id = $warehouseMutationData["from_warehouse_id"];
            $warehouseMutation->to_warehouse_id = $warehouseMutationData["to_warehouse_id"];
            $warehouseMutation->warehouse_mutation_type_id = $warehouseMutationData["warehouse_mutation_type_id"];
            $warehouseMutation->desc = $warehouseMutationData["desc"];
            $warehouseMutation->status = $warehouseMutationData["status"];
            $warehouseMutation->receive_status = $warehouseMutationData["receive_status"];
            $warehouseMutation->created_by = $warehouseMutationData["created_by"];
            $warehouseMutation->save();

            $arrProductId = $warehouseMutationDetailData["product_id"];
            $arrQty = $warehouseMutationDetailData["qty"];
            $arrDescDetail = $warehouseMutationDetailData["desc_detail"];

            if (count($arrProductId) > 0) {
                WarehouseMutationDetail::where(['warehouse_mutation_id'=> $code])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $warehouseMutationDetails = new WarehouseMutationDetail;
                    $warehouseMutationDetails->warehouse_mutation_id = $code;
                    $warehouseMutationDetails->product_id = $arrProductId[$index];
                    $warehouseMutationDetails->qty = intval($arrQty[$index]);
                    $warehouseMutationDetails->desc_detail = $arrDescDetail[$index];
                    $warehouseMutationDetails->save();
                }
            }

            DB::commit();
            $response = [
                'message' => 'Warehouse mutation created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $warehouseMutation
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $warehouseMutationData = WarehouseMutation::select('*')
            ->where('id', $id)
            ->get();

        foreach($warehouseMutationData as $mutation) {
            $createdBy = $mutation->created_by ? $mutation->created_by : null;
            $updatedBy = $mutation->updated_by ? $mutation->updated_by : null;
        }

        $userCreatedData = null;
        $userUpdatedData = null;
        if(isset($createdBy)) {
            $userCreatedData = User::select('*')->where('username', $createdBy)->get();
        }

        if(isset($updatedBy)) {
            $userUpdatedData = User::select('*')->where('username', $updatedBy)->get();
        }

        $approvalHistoryData = ApprovalHistory::join('users', 'users.username', '=', 'approval_histories.username')
            ->select('approval_histories.*', 'users.name')
            ->whereRaw("approval_histories.transaction_id = '".$id."'")
            ->get();

        $warehouseMutationDetailData = WarehouseMutationDetail::join('warehouse_mutations', 'warehouse_mutations.id', '=', 'warehouse_mutation_details.warehouse_mutation_id')
                ->join('mst_products', 'mst_products.id', '=', 'warehouse_mutation_details.product_id')
                ->select('warehouse_mutation_details.*', 'mst_products.product_name')
                ->whereRaw("warehouse_mutations.id = '".$id."'")
                ->get();

        $warehouseMutationTypeData = WarehouseMutationType::select('*')->get();
        $mstCounterData = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();
        $mstWarehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->get();

        $response = [
            'message' => 'Warehouse mutation details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'warehouse_mutation_data' => $warehouseMutationData,
                'warehouse_mutation_detail_data' => $warehouseMutationDetailData,
                'warehouse_mutation_type_data' => $warehouseMutationTypeData,
                'user_create_data' => $userCreatedData,
                'user_update_data' => $userUpdatedData,
                'approval_history_data' => $approvalHistoryData,
                'mst_counter_data' => $mstCounterData,
                'mst_warehouse_data' => $mstWarehouseData

            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
            'receive_status' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        }

        DB::beginTransaction();
        try
        {
            $warehouseMutation = WarehouseMutation::find($id);
            $warehouseMutation->status = $request->input('status') ? $request->input('status') : $warehouseMutation->status;
            $warehouseMutation->receive_status = $request->input('receive_status') ? $request->input('receive_status') : $warehouseMutation->receive_status;
            $warehouseMutation->updated_by = $request->input('updated_by');
            $warehouseMutation->save();

            $warehouseMutationDetail = WarehouseMutationDetail::select('*')
                                        ->where('warehouse_mutation_id', $id)
                                        ->get();

            if($warehouseMutation->status == '1' && $warehouseMutation->receive_status == '0') {
                if(count($warehouseMutationDetail) > 0) {
                    foreach($warehouseMutationDetail as $mutation) {
                        $stockMutation = new StockMutation;
                        $stockMutation->warehouse_id = $warehouseMutation->form_warehouse_id;
                        $stockMutation->transaction_id = $id;
                        $stockMutation->stock_mutation_type_id = "MM"; // MM = MUTASI STOCK OUT
                        $stockMutation->product_id = $mutation->product_id;
                        $stockMutation->qty = intval($mutation->qty);
                        $stockMutation->value = 0;
                        $stockMutation->stock_move = "O";
                        $stockMutation->trans_date = date('Y-m-d');
                        $stockMutation->save();
    
                        $paramManageStock = array(
                            'year'=> date('Y'),
                            'month'=> date('m'),
                            'warehouseId'=> $warehouseMutation->form_warehouse_id,
                            'productId'=> $mutation->product_id,
                            'qty'=> $mutation->qty,
                            'value'=> 0,
                            'mutationType'=> "O"
                        );
                
                        $response = $this->myHelper->stockManagement($paramManageStock);
                    }
                }
            }
            
            if($warehouseMutation->status == '1' && $warehouseMutation->receive_status == '1') {
                if(count($warehouseMutationDetail) > 0) {
                    foreach($warehouseMutationDetail as $mutation) {
                        $stockMutation = new StockMutation;
                        $stockMutation->warehouse_id = $warehouseMutation->to_warehouse_id;
                        $stockMutation->transaction_id = $id;
                        $stockMutation->stock_mutation_type_id = "MC"; // MC = MUTASI CONFIRMATION
                        $stockMutation->product_id = $mutation->product_id;
                        $stockMutation->qty = intval($mutation->qty);
                        $stockMutation->value = 0;
                        $stockMutation->stock_move = "I";
                        $stockMutation->trans_date = date('Y-m-d');
                        $stockMutation->save();
    
                        $paramManageStock = array(
                            'year'=> date('Y'),
                            'month'=> date('m'),
                            'warehouseId'=> $warehouseMutation->to_warehouse_id,
                            'productId'=> $mutation->product_id,
                            'qty'=> $mutation->qty,
                            'value'=> 0,
                            'mutationType'=> "I"
                        );
                
                        $response = $this->myHelper->stockManagement($paramManageStock);
                    }
                }
            }

            DB::commit();

            $response = [
                'message' => 'Warehouse mutation updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $warehouseMutation
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
