<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\MstCashier;
use App\MstCounter;
use App\User;
use App\SalesTransactions;
use App\SalesTransactionDetails;
use App\MstProductBrand;
use App\StockMutation;
use App\MstProductType;
use App\Customer;
use MyHelper;
use App\ExistingSalesTransactions;
use App\ExistingSalesTransactionDetails;


class ReportCashierController extends Controller
{
    protected $myHelper;

    public function __construct(MyHelper $myHelper) 
    {
        $this->middleware('jwt.auth');
        $this->myHelper = $myHelper;
    }

    public function index(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdList = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $cashierData = MstCashier::select('id', 'counter_id', 'cashier_name')
                         ->whereRaw($counterIdList)
                         ->orderBy('cashier_name', 'asc')
                         ->get();
                         
        $userData = User::select('id', 'name')
                    ->whereRaw($counterIdList)
                    ->orderBy('name', 'asc')
                    ->get();

        $customerResellerData = Customer::select('id', 'referral_code', 'customer_name')
                                ->where('customer_type_id', '2') // 2 = reseller
                                ->orderBy('customer_name', 'asc')
                                ->get();
                    
        $typeData = MstProductType::select('id', 'product_type_name')->orderBy('product_type_name', 'asc')->get();
        $response = [
            'message' => 'Create cashier report',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData,
                'cashier_data' => $cashierData,
                'user_data' => $userData,
                'type_data' => $typeData,
                'reseller_data' => $customerResellerData
            ]
        ];

        return response()->json($response, 200);
    }

    public function getReportTransaction(Request $request)
    {
        DB::enableQueryLog();
        $receiptNo = $request->input('receipt_no') != 'null' ? "sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'sales_transactions.cashier_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "sales_transactions.transaction_type = '2'"; }
        $userId = $request->input('user_id') != 'null' ? "sales_transactions.user_id = '".$request->input('user_id')."'" : 'sales_transactions.user_id is not null';
        $resellerId = $request->input('reseller_id') != 'null' ? "sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'sales_transactions.customer_id is not null';
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $data1 = SalesTransactions::join('view_pivot_sales_transaction_payments', 'view_pivot_sales_transaction_payments.pm_sales_transaction_id', '=', 'sales_transactions.id')
                ->join('mst_counters', 'mst_counters.id', '=', 'sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'sales_transactions.cashier_id')
                ->join('users', 'users.id', '=', 'sales_transactions.user_id')
                ->leftJoin('customers', 'customers.id', '=', 'sales_transactions.customer_id')
                ->selectRaw("sales_transactions.id,
                             sales_transactions.counter_id,
                             mst_counters.counter_name,
                             mst_cashiers.cashier_name,
                             users.name as user_name,
                             customers.customer_name,
                             sales_transactions.trans_date,
                             sales_transactions.trans_time,
                             sales_transactions.receipt_no,
                             sales_transactions.total_value AS total,
                             sales_transactions.discount,
                             sales_transactions.total_value AS netto,
                             sales_transactions.tax_basis,
                             (sales_transactions.total_value + sales_transactions.tax_basis) AS grand_total,
                             ((CAST(sales_transactions.cash AS SIGNED) + (CAST(sales_transactions.foreign_currency_1 AS SIGNED) * CAST(sales_transactions.exchange_rate_1 AS SIGNED)) + (CAST(sales_transactions.foreign_currency_2 AS SIGNED) * CAST(sales_transactions.exchange_rate_2 AS SIGNED)) + (CAST(sales_transactions.foreign_currency_3 AS SIGNED) * CAST(sales_transactions.exchange_rate_3 AS SIGNED))) - CAST(sales_transactions.change AS SIGNED)) as nettunai,sales_transactions.cash AS tunai,
                             sales_transactions.credit_card,
                             sales_transactions.debit_card,
                             sales_transactions.transfer,
                             (sales_transactions.foreign_currency_1 * sales_transactions.exchange_rate_1) AS valas_usd,
                             (sales_transactions.foreign_currency_2 * sales_transactions.exchange_rate_2) AS valas_cny,
                             (sales_transactions.foreign_currency_3 * sales_transactions.exchange_rate_3) AS valas_wny,
                             sales_transactions.voucher,
                             sales_transactions.member_point,
                             sales_transactions.reward,
                             (sales_transactions.cash + sales_transactions.credit_card + sales_transactions.debit_card + sales_transactions.voucher + sales_transactions.member_point + sales_transactions.reward + (sales_transactions.foreign_currency_1 * sales_transactions.exchange_rate_1) + (sales_transactions.foreign_currency_2 * sales_transactions.exchange_rate_2) + (sales_transactions.foreign_currency_3 * sales_transactions.exchange_rate_3)) + sales_transactions.transfer as total_bayar,
                             sales_transactions.change AS kembali")
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($userId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->whereRaw($transDate)
                ->where('sales_transactions.status', '1')
                ->orderByRaw('sales_transactions.counter_id, sales_transactions.trans_date, sales_transactions.trans_time ASC')
                ->get()->toArray();
                $printQuery = DB::getQueryLog();
        $receiptNo = $request->input('receipt_no') != 'null' ? "existing_sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'existing_sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "existing_sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'existing_sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "existing_sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'existing_sales_transactions.cashier_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "existing_sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "existing_sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "existing_sales_transactions.transaction_type = '2'"; }
        $userId = $request->input('user_id') != 'null' ? "existing_sales_transactions.user_id = '".$request->input('user_id')."'" : 'existing_sales_transactions.user_id is not null';
        $resellerId = $request->input('reseller_id') != 'null' ? "existing_sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'existing_sales_transactions.customer_id is not null';
        $transDate = "existing_sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $data2 = ExistingSalesTransactions::join('view_pivot_existing_sales_transaction_payments', 'view_pivot_existing_sales_transaction_payments.pm_sales_transaction_id', '=', 'existing_sales_transactions.id')
                ->join('mst_counters', 'mst_counters.id', '=', 'existing_sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'existing_sales_transactions.cashier_id')
                ->join('users', 'users.id', '=', 'existing_sales_transactions.user_id')
                ->leftJoin('customers', 'customers.id', '=', 'existing_sales_transactions.customer_id')
                ->selectRaw("existing_sales_transactions.id,
                                existing_sales_transactions.counter_id,
                                mst_counters.counter_name,
                                mst_cashiers.cashier_name,
                                users.name as user_name,
                                customers.customer_name,
                                existing_sales_transactions.trans_date,
                                existing_sales_transactions.trans_time,
                                existing_sales_transactions.receipt_no,
                                existing_sales_transactions.total_value AS total,
                                existing_sales_transactions.discount,
                                existing_sales_transactions.total_value AS netto,
                                existing_sales_transactions.tax_basis,
                                (existing_sales_transactions.total_value + existing_sales_transactions.tax_basis) AS grand_total,
                                ((CAST(existing_sales_transactions.cash AS SIGNED) + (CAST(existing_sales_transactions.foreign_currency_1 AS SIGNED) * CAST(existing_sales_transactions.exchange_rate_1 AS SIGNED)) + (CAST(existing_sales_transactions.foreign_currency_2 AS SIGNED) * CAST(existing_sales_transactions.exchange_rate_2 AS SIGNED)) + (CAST(existing_sales_transactions.foreign_currency_3 AS SIGNED) * CAST(existing_sales_transactions.exchange_rate_3 AS SIGNED))) - CAST(existing_sales_transactions.change AS SIGNED)) as nettunai,existing_sales_transactions.cash AS tunai,
                                existing_sales_transactions.credit_card,
                                existing_sales_transactions.debit_card,
                                existing_sales_transactions.transfer,
                                (existing_sales_transactions.foreign_currency_1 * existing_sales_transactions.exchange_rate_1) AS valas_usd,
                                (existing_sales_transactions.foreign_currency_2 * existing_sales_transactions.exchange_rate_2) AS valas_cny,
                                (existing_sales_transactions.foreign_currency_3 * existing_sales_transactions.exchange_rate_3) AS valas_wny,
                                existing_sales_transactions.voucher,
                                existing_sales_transactions.member_point,
                                existing_sales_transactions.reward,
                                (existing_sales_transactions.cash + existing_sales_transactions.credit_card + existing_sales_transactions.debit_card + existing_sales_transactions.voucher + existing_sales_transactions.member_point + existing_sales_transactions.reward + (existing_sales_transactions.foreign_currency_1 * existing_sales_transactions.exchange_rate_1) + (existing_sales_transactions.foreign_currency_2 * existing_sales_transactions.exchange_rate_2) + (existing_sales_transactions.foreign_currency_3 * existing_sales_transactions.exchange_rate_3)) + existing_sales_transactions.transfer as total_bayar,
                                existing_sales_transactions.change AS kembali")
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($userId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->whereRaw($transDate)
                ->where('existing_sales_transactions.status', '1')
                ->orderByRaw('existing_sales_transactions.counter_id, existing_sales_transactions.trans_date, existing_sales_transactions.trans_time ASC')
                ->get()->toArray();
        $printQuery = DB::getQueryLog();
        $data = array_merge($data1,$data2);
        $whereRawCounter = "id IN (".$request->input('counter_id').")";
        $cashierData = MstCashier::where('id', $request->input('cashier_id'))->get();
        $counterData = MstCounter::whereRaw($whereRawCounter)->get();
        $userData = User::select('id', 'name')->where('id', $request->input('user_id'))->get();
        $resellerData = Customer::select('id', 'referral_code', 'customer_name')->where('id', $request->input('reseller_id'))->get();
        $response = [
            'message' => 'Report Transaction Cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'cashier_data' => $cashierData,
            'counter_data' => $counterData,
            'user_data' => $userData,
            'type_data' => array(),
            'reseller_data' => $resellerData
        ];
        
        return response()->json($response, 200);
    }

    public function getReportTransactionSumDate(Request $request) {
        DB::enableQueryLog();
        $periode = $request->input('periode');
        $counterId = $request->input('counter_id') != 'null' ? "sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'sales_transactions.counter_id is not null';
        $data1 = SalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'sales_transactions.counter_id')
                                    ->selectRaw("mst_counters.counter_name,
                                                sales_transactions.trans_date,
                                                SUM(sales_transactions.total_value) AS total,
                                                SUM(sales_transactions.discount) AS discount,
                                                SUM(sales_transactions.total_value) AS netto,
                                                SUM(sales_transactions.tax_basis) AS tax_basis,
                                                SUM((sales_transactions.total_value + sales_transactions.tax_basis)) AS grand_total")
                                    ->whereRaw('sales_transactions.status = \'1\'')
                                    ->whereRaw($counterId)
                                    ->whereRaw('SUBSTR(sales_transactions.trans_date,1,7) = \''.$periode.'\'')
                                    ->groupBy('sales_transactions.trans_date')
                                    ->get()->toArray();
        $periode = $request->input('periode');
        $counterId = $request->input('counter_id') != 'null' ? "existing_sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'existing_sales_transactions.counter_id is not null';
        $data2 = ExistingSalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'existing_sales_transactions.counter_id')
                                    ->selectRaw("mst_counters.counter_name,
                                                existing_sales_transactions.trans_date,
                                                SUM(existing_sales_transactions.total_value) AS total,
                                                SUM(existing_sales_transactions.discount) AS discount,
                                                SUM(existing_sales_transactions.total_value) AS netto,
                                                SUM(existing_sales_transactions.tax_basis) AS tax_basis,
                                                SUM((existing_sales_transactions.total_value + existing_sales_transactions.tax_basis)) AS grand_total")
                                    ->whereRaw('existing_sales_transactions.status = \'1\'')
                                    ->whereRaw($counterId)
                                    ->whereRaw('SUBSTR(existing_sales_transactions.trans_date,1,7) = \''.$periode.'\'')
                                    ->groupBy('existing_sales_transactions.trans_date')
                                    ->get()->toArray();
        $data = array_merge($data1,$data2);
        $response = [
            'message' => 'Report Transaction SumDate',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'QUWE'=>DB::getQueryLog()
        ];
        
        return response()->json($response, 200);
    }

    public function getReportTransactionDetail(Request $request)
    {
        $receiptNo = $request->input('receipt_no') != 'null' ? "sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'sales_transactions.cashier_id is not null';
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $resellerId = $request->input('reseller_id') != 'null' ? "sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'sales_transactions.customer_id is not null';
        $typeId = $request->input('type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('type_id')."'" : 'mst_products.product_type_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "sales_transactions.transaction_type = '2'"; }

        $data1 = SalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'sales_transactions.cashier_id')
                ->join('sales_transaction_details', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
                ->join('mst_products', 'mst_products.id', '=', 'sales_transaction_details.product_id')
                ->join('users', 'users.id', '=', 'sales_transactions.user_id')
                ->leftJoin('customers', 'customers.id', '=', 'sales_transactions.customer_id')
                ->selectRaw('sales_transactions.id, 
                             mst_counters.counter_name,
                             mst_cashiers.cashier_name,
                             users.name as user_name,
                             customers.referral_code,
                             CONCAT(customers.referral_code," - ",customers.customer_name) as customer_name,
                             CONCAT(IF(customers.referral_code IS NULL, "Kode Reseller Kosong", customers.referral_code)," - ",customers.customer_name) AS customer_name,
                             sales_transactions.trans_date,
                             sales_transactions.trans_time,
                             sales_transactions.receipt_no,
                             mst_products.id AS PCode,
                             mst_products.product_name,
                             sales_transaction_details.qty,
                             sales_transaction_details.price,
                             (sales_transaction_details.price * sales_transaction_details.qty) AS bruto,
                             (sales_transaction_details.disc_1 + sales_transaction_details.disc_2 + sales_transaction_details.disc_3 + sales_transaction_details.disc_4) AS disc,
                             (((sales_transaction_details.price * sales_transaction_details.qty) - (sales_transaction_details.price * sales_transaction_details.free_qty)) - (sales_transaction_details.disc_1 + sales_transaction_details.disc_2 + sales_transaction_details.disc_3 + sales_transaction_details.disc_4)) AS netto,
                             (((sales_transaction_details.price * sales_transaction_details.qty) - (sales_transaction_details.price * sales_transaction_details.free_qty)) - (sales_transaction_details.disc_1 + sales_transaction_details.disc_2 + sales_transaction_details.disc_3 + sales_transaction_details.disc_4)) AS total')
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($transDate)
                ->whereRaw($typeId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->where('sales_transactions.status', '1')
                ->where('sales_transaction_details.qty', '>', '0')
                ->orderByRaw('sales_transactions.counter_id, sales_transactions.trans_date, sales_transactions.trans_time ASC')
                ->get()->toArray();

        $receiptNo = $request->input('receipt_no') != 'null' ? "existing_sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'existing_sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "existing_sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'existing_sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "existing_sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'existing_sales_transactions.cashier_id is not null';
        $transDate = "existing_sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $resellerId = $request->input('reseller_id') != 'null' ? "existing_sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'existing_sales_transactions.customer_id is not null';
        $typeId = $request->input('type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('type_id')."'" : 'mst_products.product_type_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "existing_sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "existing_sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "existing_sales_transactions.transaction_type = '2'"; }
        $data2 = ExistingSalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'existing_sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'existing_sales_transactions.cashier_id')
                ->join('existing_sales_transaction_details', 'existing_sales_transaction_details.sales_transaction_id', '=', 'existing_sales_transactions.id')
                ->join('mst_products', 'mst_products.id', '=', 'existing_sales_transaction_details.product_id')
                ->join('users', 'users.id', '=', 'existing_sales_transactions.user_id')
                ->leftJoin('customers', 'customers.id', '=', 'existing_sales_transactions.customer_id')
                ->selectRaw('existing_sales_transactions.id, 
                                mst_counters.counter_name,
                                mst_cashiers.cashier_name,
                                users.name as user_name,
                                customers.referral_code,
                                CONCAT(customers.referral_code," - ",customers.customer_name) as customer_name,
                                existing_sales_transactions.trans_date,
                                existing_sales_transactions.trans_time,
                                existing_sales_transactions.receipt_no,
                                mst_products.id AS PCode,
                                mst_products.product_name,
                                existing_sales_transaction_details.qty,
                                existing_sales_transaction_details.price,
                                (existing_sales_transaction_details.price * existing_sales_transaction_details.qty) AS bruto,
                                (existing_sales_transaction_details.disc_1 + existing_sales_transaction_details.disc_2 + existing_sales_transaction_details.disc_3 + existing_sales_transaction_details.disc_4) AS disc,
                                (((existing_sales_transaction_details.price * existing_sales_transaction_details.qty) - (existing_sales_transaction_details.price * existing_sales_transaction_details.free_qty)) - (existing_sales_transaction_details.disc_1 + existing_sales_transaction_details.disc_2 + existing_sales_transaction_details.disc_3 + existing_sales_transaction_details.disc_4)) AS netto,
                                (((existing_sales_transaction_details.price * existing_sales_transaction_details.qty) - (existing_sales_transaction_details.price * existing_sales_transaction_details.free_qty)) - (existing_sales_transaction_details.disc_1 + existing_sales_transaction_details.disc_2 + existing_sales_transaction_details.disc_3 + existing_sales_transaction_details.disc_4)) AS total')
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($transDate)
                ->whereRaw($typeId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->where('existing_sales_transactions.status', '1')
                ->where('existing_sales_transaction_details.qty', '>', '0')
                ->orderByRaw('existing_sales_transactions.counter_id, existing_sales_transactions.trans_date, existing_sales_transactions.trans_time ASC')
                ->get()->toArray();
        $data = array_merge($data1,$data2);
        $whereRawCounter = "id IN (".$request->input('counter_id').")";
        $cashierData = MstCashier::where('id', $request->input('cashier_id'))->get();
        $counterData = MstCounter::whereRaw($whereRawCounter)->get();
        $userData = User::select('id', 'name')->where('id', $request->input('user_id'))->get();
        $typeData = MstProductType::select('id', 'product_type_name')->where('id', $request->input('type_id'))->get();
        $resellerData = Customer::select('id', 'referral_code', 'customer_name')->where('id', $request->input('reseller_id'))->get();

        $response = [
            'message' => 'Report Transaction Detail Cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'cashier_data' => $cashierData,
            'counter_data' => $counterData,
            'user_data' => $userData,
            'type_data' => $typeData,
            'reseller_data' => $resellerData
        ];
        
        return response()->json($response, 200);
    }

    public function getReportTransactionProduct(Request $request)
    {
        $receiptNo = $request->input('receipt_no') != 'null' ? "sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'sales_transactions.cashier_id is not null';
        $typeId = $request->input('type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('type_id')."'" : 'mst_products.product_type_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "sales_transactions.transaction_type = '2'"; }
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $resellerId = $request->input('reseller_id') != 'null' ? "sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'sales_transactions.customer_id is not null';
        $data1 = SalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'sales_transactions.cashier_id')
                ->join('sales_transaction_details', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
                ->join('mst_products', 'mst_products.id', '=', 'sales_transaction_details.product_id')
                ->selectRaw('mst_products.id AS PCode, mst_products.product_name, SUM(sales_transaction_details.qty) AS qty, sales_transaction_details.price,
                (sales_transaction_details.price * SUM(sales_transaction_details.qty)) AS bruto,
                (SUM(sales_transaction_details.disc_1) + SUM(sales_transaction_details.disc_2) + SUM(sales_transaction_details.disc_3) + SUM(sales_transaction_details.disc_4)) AS disc,
                ((sales_transaction_details.price * SUM(sales_transaction_details.qty)) - (SUM(sales_transaction_details.disc_1) + SUM(sales_transaction_details.disc_2) + SUM(sales_transaction_details.disc_3) + SUM(sales_transaction_details.disc_4))) AS netto')
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($transDate)
                ->whereRaw($typeId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->where('sales_transactions.status', '1')
                ->where('sales_transaction_details.qty', '>', '0')
                ->groupBy('sales_transaction_details.product_id')
                ->get()->toArray();
        $receiptNo = $request->input('receipt_no') != 'null' ? "existing_sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'existing_sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "existing_sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'existing_sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "existing_sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'existing_sales_transactions.cashier_id is not null';
        $typeId = $request->input('type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('type_id')."'" : 'mst_products.product_type_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "existing_sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "existing_sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "existing_sales_transactions.transaction_type = '2'"; }
        $transDate = "existing_sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $resellerId = $request->input('reseller_id') != 'null' ? "existing_sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'existing_sales_transactions.customer_id is not null';
        $data2 = ExistingSalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'existing_sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'existing_sales_transactions.cashier_id')
                ->join('existing_sales_transaction_details', 'existing_sales_transaction_details.sales_transaction_id', '=', 'existing_sales_transactions.id')
                ->join('mst_products', 'mst_products.id', '=', 'existing_sales_transaction_details.product_id')
                ->selectRaw('mst_products.id AS PCode, mst_products.product_name, SUM(existing_sales_transaction_details.qty) AS qty, existing_sales_transaction_details.price,
                (existing_sales_transaction_details.price * SUM(existing_sales_transaction_details.qty)) AS bruto,
                (SUM(existing_sales_transaction_details.disc_1) + SUM(existing_sales_transaction_details.disc_2) + SUM(existing_sales_transaction_details.disc_3) + SUM(existing_sales_transaction_details.disc_4)) AS disc,
                ((existing_sales_transaction_details.price * SUM(existing_sales_transaction_details.qty)) - (SUM(existing_sales_transaction_details.disc_1) + SUM(existing_sales_transaction_details.disc_2) + SUM(existing_sales_transaction_details.disc_3) + SUM(existing_sales_transaction_details.disc_4))) AS netto')
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($transDate)
                ->whereRaw($typeId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->where('existing_sales_transactions.status', '1')
                ->where('existing_sales_transaction_details.qty', '>', '0')
                ->groupBy('existing_sales_transaction_details.product_id')
                ->get()->toArray();
        $data = array_merge($data1,$data2);
        $whereRawCounter = "id IN (".$request->input('counter_id').")";
        $cashierData = MstCashier::where('id', $request->input('cashier_id'))->get();
        $counterData = MstCounter::whereRaw($whereRawCounter)->get();
        $userData = User::select('id', 'name')->where('id', $request->input('user_id'))->get();
        $typeData = MstProductType::select('id', 'product_type_name')->where('id', $request->input('type_id'))->get();
        $resellerData = Customer::select('id', 'referral_code', 'customer_name')->where('id', $request->input('reseller_id'))->get();

        $response = [
            'message' => 'Report Transaction Product Cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'cashier_data' => $cashierData,
            'counter_data' => $counterData,
            'user_data' => $userData,
            'type_data' => $typeData,
            'reseller_data' => $resellerData
        ];
        
        return response()->json($response, 200);
    }

    public function getReportTransactionBrand(Request $request)
    {
        $receiptNo = $request->input('receipt_no') != 'null' ? "sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'sales_transactions.cashier_id is not null';
        $typeId = $request->input('type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('type_id')."'" : 'mst_products.product_type_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "sales_transactions.transaction_type = '2'"; }
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $resellerId = $request->input('reseller_id') != 'null' ? "sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'sales_transactions.customer_id is not null';
        $data1 = SalesTransactions::join('sales_transaction_details', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
                ->join('mst_products', 'mst_products.id', '=', 'sales_transaction_details.product_id')
                ->join('mst_product_brands', 'mst_product_brands.id', '=', 'mst_products.product_brand_id')
                ->selectRaw('sales_transaction_details.product_id,
                mst_products.product_name,
                mst_product_brands.id as product_brand_id,
                mst_product_brands.product_brand_name,
                (SUM(sales_transaction_details.qty) + SUM(sales_transaction_details.free_qty)) AS total_qty,
                (
                  (
                    sales_transaction_details.price * (SUM(sales_transaction_details.qty) + SUM(sales_transaction_details.free_qty))
                  ) - (
                    SUM(sales_transaction_details.disc_1) + SUM(sales_transaction_details.disc_2) + SUM(sales_transaction_details.disc_3) + SUM(sales_transaction_details.disc_4)
                  )
                ) AS netto')
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($transDate)
                ->whereRaw($typeId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->where('sales_transactions.status', '1')
                ->groupBy('sales_transaction_details.product_id')
                ->get()->toArray();
        $receiptNo = $request->input('receipt_no') != 'null' ? "existing_sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'existing_sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "existing_sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'existing_sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "existing_sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'existing_sales_transactions.cashier_id is not null';
        $typeId = $request->input('type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('type_id')."'" : 'mst_products.product_type_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "existing_sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "existing_sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "existing_sales_transactions.transaction_type = '2'"; }
        $transDate = "existing_sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $resellerId = $request->input('reseller_id') != 'null' ? "existing_sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'existing_sales_transactions.customer_id is not null';
        $data2 = ExistingSalesTransactions::join('existing_sales_transaction_details', 'existing_sales_transaction_details.sales_transaction_id', '=', 'existing_sales_transactions.id')
                ->join('mst_products', 'mst_products.id', '=', 'existing_sales_transaction_details.product_id')
                ->join('mst_product_brands', 'mst_product_brands.id', '=', 'mst_products.product_brand_id')
                ->selectRaw('existing_sales_transaction_details.product_id,
                mst_products.product_name,
                mst_product_brands.id as product_brand_id,
                mst_product_brands.product_brand_name,
                (SUM(existing_sales_transaction_details.qty) + SUM(existing_sales_transaction_details.free_qty)) AS total_qty,
                (
                    (
                    existing_sales_transaction_details.price * (SUM(existing_sales_transaction_details.qty) + SUM(existing_sales_transaction_details.free_qty))
                    ) - (
                    SUM(existing_sales_transaction_details.disc_1) + SUM(existing_sales_transaction_details.disc_2) + SUM(existing_sales_transaction_details.disc_3) + SUM(existing_sales_transaction_details.disc_4)
                    )
                ) AS netto')
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($transDate)
                ->whereRaw($typeId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->where('existing_sales_transactions.status', '1')
                ->groupBy('existing_sales_transaction_details.product_id')
                ->get()->toArray();
        $data = array_merge($data1,$data2);
        $whereRawCounter = "id IN (".$request->input('counter_id').")";
        $cashierData = MstCashier::where('id', $request->input('cashier_id'))->get();
        $counterData = MstCounter::whereRaw($whereRawCounter)->get();
        $userData = User::select('id', 'name')->where('id', $request->input('user_id'))->get();
        $typeData = MstProductType::select('id', 'product_type_name')->where('id', $request->input('type_id'))->get();
        $resellerData = Customer::select('id', 'referral_code', 'customer_name')->where('id', $request->input('reseller_id'))->get();

        $response = [
            'message' => 'Report Transaction Brand Cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'cashier_data' => $cashierData,
            'counter_data' => $counterData,
            'user_data' => $userData,
            'type_data' => $typeData,
            'reseller_data' => $resellerData
        ];
        
        return response()->json($response, 200);
    }

    /*public function getReportTransactionBrand(Request $request)
    {
        $counterId = $request->input('counter_id') != 'null' ? "sales_transactions.counter_id = '".$request->input('counter_id')."'" : 'sales_transactions.counter_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "sales_transactions.transaction_type = '2'"; }
        $year = $request->input('year');
        $reportData = array();
        $dataPerMonth = array();
        $monthName = array('null', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $productBrand = MstProductBrand::get();
        $brandArrData = $productBrand->toArray();

        $counterData = MstCounter::where('id', $request->input('counter_id'))->get();

        if(count($brandArrData) > 0) {
            for($month=1; $month < 13; $month++) {
                for ($index=0; $index < count($brandArrData); $index++) {
                    

                    $setMonth = sprintf("%02s", strval($month));
                    $startDate = $year.'-'.$setMonth.'-01';
                    $endDate = $year.'-'.$setMonth.'-31';
                    $transDate = "sales_transactions.trans_date BETWEEN '".$startDate."' AND '".$endDate."'";

                    $data = SalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'sales_transactions.counter_id')
                            ->join('sales_transaction_details', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
                            ->join('mst_products', 'mst_products.id', '=', 'sales_transaction_details.product_id')
                            ->join('mst_product_brands', 'mst_product_brands.id', '=', 'mst_products.product_brand_id')
                            ->selectRaw('sales_transactions.counter_id, mst_counters.counter_name, mst_product_brands.id as product_brand_id,
                            mst_product_brands.product_brand_name, SUM(sales_transactions.total_value) AS sum_total_value')
                            ->whereRaw($counterId)
                            ->whereRaw($transDate)
                            ->whereRaw($transactionType)
                            ->where('mst_product_brands.id', $brandArrData[$index]["id"])
                            ->where('sales_transactions.status', '1')
                            ->where('sales_transaction_details.qty', '>', '0')
                            ->groupBy('sales_transactions.counter_id')
                            ->get();

                    $arrData = $data->toArray();
                    if(count($arrData) > 0) {
                        $newArrData = array(
                            "year" => $year,
                            "month" => $month,
                            "month_name" => $monthName[$month],
                            "counter_id" => $arrData[0]["counter_id"],
                            "product_brand_id" => $arrData[0]["product_brand_id"],
                            "sum_total_value" => $arrData[0]["sum_total_value"]
                        );
                    } else {
                        $newArrData = array(
                            "year" => $year,
                            "month" => $month,
                            "month_name" => $monthName[$month],
                            "counter_id" => $request->input('counter_id'),
                            "product_brand_id" => $brandArrData[$index]["id"],
                            "sum_total_value" => 0
                        );
                    }
                    array_push($dataPerMonth, $newArrData);
                }
                
                array_push($reportData, $dataPerMonth);
                $dataPerMonth = array();
            }
        } else {
            $response = [
                'message' => 'Report Transaction Brand',
                'status' => [
                    'code' => 200,
                    'description' => 'OK'
                ],
                'results' => array()
            ];
            
            return response()->json($response, 200);
        }

        $response = [
            'message' => 'Report Transaction Brand',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $reportData,
            'counter_data' => $counterData,
            'mst_product_brands' => $productBrand,
        ];
        
        return response()->json($response, 200);
    }*/

    public function getReportTransactionPrize(Request $request)
    {
        $receiptNo = $request->input('receipt_no') != 'null' ? "sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'sales_transactions.cashier_id is not null';
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $typeId = $request->input('type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('type_id')."'" : 'mst_products.product_type_id is not null';
        $resellerId = $request->input('reseller_id') != 'null' ? "sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'sales_transactions.customer_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "sales_transactions.transaction_type = '2'"; }
        $data1 = SalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'sales_transactions.cashier_id')
                ->join('sales_transaction_details', 'sales_transaction_details.sales_transaction_id', '=', 'sales_transactions.id')
                ->join('mst_products', 'mst_products.id', '=', 'sales_transaction_details.product_id')
                ->selectRaw('mst_products.id AS PCode, mst_products.product_name, SUM(sales_transaction_details.free_qty) AS free_qty,
                sales_transaction_details.price, SUM((sales_transaction_details.price * sales_transaction_details.free_qty)) AS netto')
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($transDate)
                ->whereRaw($typeId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->where('sales_transactions.status', '1')
                ->whereRaw("sales_transaction_details.free_qty > 0")
                ->groupBy('sales_transaction_details.product_id')
                ->orderByRaw('mst_products.product_type_id, sales_transactions.trans_date, sales_transactions.trans_time ASC')
                ->get()->toArray();
        $receiptNo = $request->input('receipt_no') != 'null' ? "existing_sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'existing_sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "existing_sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'existing_sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "existing_sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'existing_sales_transactions.cashier_id is not null';
        $transDate = "existing_sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $typeId = $request->input('type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('type_id')."'" : 'mst_products.product_type_id is not null';
        $resellerId = $request->input('reseller_id') != 'null' ? "existing_sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'existing_sales_transactions.customer_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "existing_sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "existing_sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "existing_sales_transactions.transaction_type = '2'"; }
        $data2 = ExistingSalesTransactions::join('mst_counters', 'mst_counters.id', '=', 'existing_sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'existing_sales_transactions.cashier_id')
                ->join('existing_sales_transaction_details', 'existing_sales_transaction_details.sales_transaction_id', '=', 'existing_sales_transactions.id')
                ->join('mst_products', 'mst_products.id', '=', 'existing_sales_transaction_details.product_id')
                ->selectRaw('mst_products.id AS PCode, mst_products.product_name, SUM(existing_sales_transaction_details.free_qty) AS free_qty,
                existing_sales_transaction_details.price, SUM((existing_sales_transaction_details.price * existing_sales_transaction_details.free_qty)) AS netto')
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($transDate)
                ->whereRaw($typeId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->where('existing_sales_transactions.status', '1')
                ->whereRaw("existing_sales_transaction_details.free_qty > 0")
                ->groupBy('existing_sales_transaction_details.product_id')
                ->orderByRaw('mst_products.product_type_id, existing_sales_transactions.trans_date, existing_sales_transactions.trans_time ASC')
                ->get()->toArray();        
        $data = array_merge($data1,$data2);
        $counterData = MstCounter::where('id', $request->input('counter_id'))->get();
        $cashierData = MstCashier::where('id', $request->input('cashier_id'))->get();
        $userData = User::select('id', 'name')->where('id', $request->input('user_id'))->get();
        $typeData = MstProductType::select('id', 'product_type_name')->where('id', $request->input('type_id'))->get();
        $resellerData = Customer::select('id', 'referral_code', 'customer_name')->where('id', $request->input('reseller_id'))->get();

        $response = [
            'message' => 'Report Transaction Cashier Prize',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'counter_data' => $counterData,
            'cashier_data' => $cashierData,
            'user_data' => $userData,
            'type_data' => $typeData,
            'reseller_data' => $resellerData
        ];
        
        return response()->json($response, 200);
    }

    public function getReportTransactionVoid(Request $request)
    {
        DB::enableQueryLog();
        $receiptNo = $request->input('receipt_no') != 'null' ? "sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'sales_transactions.cashier_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "sales_transactions.transaction_type = '2'"; }
        $userId = $request->input('user_id') != 'null' ? "sales_transactions.user_id = '".$request->input('user_id')."'" : 'sales_transactions.user_id is not null';
        $resellerId = $request->input('reseller_id') != 'null' ? "sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'sales_transactions.customer_id is not null';
        $transDate = "sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $data1 = SalesTransactions::join('sales_transaction_payments', 'sales_transaction_payments.sales_transaction_id', '=', 'sales_transactions.id')
                ->join('mst_counters', 'mst_counters.id', '=', 'sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'sales_transactions.cashier_id')
                ->join('users', 'users.id', '=', 'sales_transactions.user_id')
                ->leftJoin('customers', 'customers.id', '=', 'sales_transactions.customer_id')
                ->selectRaw("sales_transactions.id, sales_transactions.counter_id, mst_counters.counter_name, mst_cashiers.cashier_name, users.name as user_name,
                customers.customer_name, sales_transactions.trans_date, sales_transactions.trans_time, sales_transactions.receipt_no,
                sales_transactions.total_value AS total, sales_transactions.discount, sales_transactions.total_value AS netto,
                sales_transactions.tax_basis, (sales_transactions.total_value + sales_transactions.tax_basis) AS grand_total,
                ((sales_transactions.cash + (sales_transactions.foreign_currency_1 * sales_transactions.exchange_rate_1) + 
                (sales_transactions.foreign_currency_2 * sales_transactions.exchange_rate_2) + (sales_transactions.foreign_currency_3 * sales_transactions.exchange_rate_3)) - 
                sales_transactions.change) as nettunai, sales_transactions.cash AS tunai, sales_transactions.credit_card,
                IF(sales_transaction_payments.payment_method_id <> '1', sales_transactions.debit_card, 0) AS debit_card,
                IF(sales_transaction_payments.payment_method_id = '1', sales_transactions.debit_card, 0) AS transfer,
                (sales_transactions.foreign_currency_1 * sales_transactions.exchange_rate_1) AS valas_usd,
                (sales_transactions.foreign_currency_2 * sales_transactions.exchange_rate_2) AS valas_cny,
                (sales_transactions.foreign_currency_3 * sales_transactions.exchange_rate_3) AS valas_wny, sales_transactions.voucher, sales_transactions.member_point, sales_transactions.reward,
                (sales_transactions.cash + sales_transactions.credit_card + sales_transactions.debit_card +  sales_transactions.voucher + sales_transactions.member_point + sales_transactions.reward +
                (sales_transactions.foreign_currency_1 * sales_transactions.exchange_rate_1) + (sales_transactions.foreign_currency_2 * sales_transactions.exchange_rate_2) +
                (sales_transactions.foreign_currency_3 * sales_transactions.exchange_rate_3)) as total_bayar,
                sales_transactions.change AS kembali, sales_transactions.desc_void")
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($userId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->whereRaw($transDate)
                ->where('sales_transactions.status', '0')
                ->orderByRaw('sales_transactions.counter_id, sales_transactions.trans_date, sales_transactions.trans_time ASC')
                ->get()->toArray();
        $receiptNo = $request->input('receipt_no') != 'null' ? "existing_sales_transactions.receipt_no = '".$request->input('receipt_no')."'" : 'existing_sales_transactions.receipt_no is not null';
        $counterId = $request->input('counter_id') != 'null' ? "existing_sales_transactions.counter_id IN (".$request->input('counter_id').")" : 'existing_sales_transactions.counter_id is not null';
        $cashierId = $request->input('cashier_id') != 'null' ? "existing_sales_transactions.cashier_id = '".$request->input('cashier_id')."'" : 'existing_sales_transactions.cashier_id is not null';
        if($request->input('transaction_type') == '0') { $transactionType = "existing_sales_transactions.transaction_type is not null"; }
        if($request->input('transaction_type') == '1') { $transactionType = "existing_sales_transactions.transaction_type = '1'"; }
        if($request->input('transaction_type') == '2') { $transactionType = "existing_sales_transactions.transaction_type = '2'"; }
        $userId = $request->input('user_id') != 'null' ? "existing_sales_transactions.user_id = '".$request->input('user_id')."'" : 'existing_sales_transactions.user_id is not null';
        $resellerId = $request->input('reseller_id') != 'null' ? "existing_sales_transactions.customer_id = '".$request->input('reseller_id')."'" : 'existing_sales_transactions.customer_id is not null';
        $transDate = "existing_sales_transactions.trans_date BETWEEN '".$request->input('start_date')."' AND '".$request->input('end_date')."'";
        $data2 = ExistingSalesTransactions::join('existing_sales_transaction_payments', 'existing_sales_transaction_payments.sales_transaction_id', '=', 'existing_sales_transactions.id')
                ->join('mst_counters', 'mst_counters.id', '=', 'existing_sales_transactions.counter_id')
                ->join('mst_cashiers', 'mst_cashiers.id', '=', 'existing_sales_transactions.cashier_id')
                ->join('users', 'users.id', '=', 'existing_sales_transactions.user_id')
                ->leftJoin('customers', 'customers.id', '=', 'existing_sales_transactions.customer_id')
                ->selectRaw("existing_sales_transactions.id, existing_sales_transactions.counter_id, mst_counters.counter_name, mst_cashiers.cashier_name, users.name as user_name,
                customers.customer_name, existing_sales_transactions.trans_date, existing_sales_transactions.trans_time, existing_sales_transactions.receipt_no,
                existing_sales_transactions.total_value AS total, existing_sales_transactions.discount, existing_sales_transactions.total_value AS netto,
                existing_sales_transactions.tax_basis, (existing_sales_transactions.total_value + existing_sales_transactions.tax_basis) AS grand_total,
                ((existing_sales_transactions.cash + (existing_sales_transactions.foreign_currency_1 * existing_sales_transactions.exchange_rate_1) + 
                (existing_sales_transactions.foreign_currency_2 * existing_sales_transactions.exchange_rate_2) + (existing_sales_transactions.foreign_currency_3 * existing_sales_transactions.exchange_rate_3)) - 
                existing_sales_transactions.change) as nettunai, existing_sales_transactions.cash AS tunai, existing_sales_transactions.credit_card,
                IF(existing_sales_transaction_payments.payment_method_id <> '1', existing_sales_transactions.debit_card, 0) AS debit_card,
                IF(existing_sales_transaction_payments.payment_method_id = '1', existing_sales_transactions.debit_card, 0) AS transfer,
                (existing_sales_transactions.foreign_currency_1 * existing_sales_transactions.exchange_rate_1) AS valas_usd,
                (existing_sales_transactions.foreign_currency_2 * existing_sales_transactions.exchange_rate_2) AS valas_cny,
                (existing_sales_transactions.foreign_currency_3 * existing_sales_transactions.exchange_rate_3) AS valas_wny, existing_sales_transactions.voucher, existing_sales_transactions.member_point, existing_sales_transactions.reward,
                (existing_sales_transactions.cash + existing_sales_transactions.credit_card + existing_sales_transactions.debit_card +  existing_sales_transactions.voucher + existing_sales_transactions.member_point + existing_sales_transactions.reward +
                (existing_sales_transactions.foreign_currency_1 * existing_sales_transactions.exchange_rate_1) + (existing_sales_transactions.foreign_currency_2 * existing_sales_transactions.exchange_rate_2) +
                (existing_sales_transactions.foreign_currency_3 * existing_sales_transactions.exchange_rate_3)) as total_bayar,
                existing_sales_transactions.change AS kembali, existing_sales_transactions.desc_void")
                ->whereRaw($receiptNo)
                ->whereRaw($counterId)
                ->whereRaw($cashierId)
                ->whereRaw($userId)
                ->whereRaw($resellerId)
                ->whereRaw($transactionType)
                ->whereRaw($transDate)
                ->where('existing_sales_transactions.status', '0')
                ->orderByRaw('existing_sales_transactions.counter_id, existing_sales_transactions.trans_date, existing_sales_transactions.trans_time ASC')
                ->get()->toArray();        
        $data = array_merge($data1,$data2);
        $whereRawCounter = "id IN (".$request->input('counter_id').")";
        $cashierData = MstCashier::where('id', $request->input('cashier_id'))->get();
        $counterData = MstCounter::whereRaw($whereRawCounter)->get();
        $userData = User::select('id', 'name')->where('id', $request->input('user_id'))->get();
        $resellerData = Customer::select('id', 'referral_code', 'customer_name')->where('id', $request->input('reseller_id'))->get();

        $response = [
            'message' => 'Report Transaction Void Cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'cashier_data' => $cashierData,
            'counter_data' => $counterData,
            'user_data' => $userData,
            'type_data' => array(),
            'reseller_data' => $resellerData
        ];
        
        return response()->json($response, 200);
    }

    public function saveVoidTransaction(Request $request)
    {
        $receiptNo = $request->input('receipt_no');
        $descVoid = $request->input('desc_void');
        $created_by = $request->input('created_by');
        $updated_by = $request->input('updated_by');

        DB::beginTransaction();
        try
        {
            $salesTransaction = SalesTransactions::find($receiptNo);
            $salesTransaction->status = "0";
            $salesTransaction->desc_void = $descVoid;
            $salesTransaction->updated_by = $request->input('updated_by');
            $salesTransaction->save();

            $salesTransactionDetail = SalesTransactionDetails::where('sales_transaction_id', $receiptNo)->get();
            $arrSalesTransactionDetail = $salesTransactionDetail->toArray();

            if(count($arrSalesTransactionDetail) > 0) {
                for ($index=0; $index < count($arrSalesTransactionDetail); $index++) {
                    $salesDetail = SalesTransactionDetails::find($arrSalesTransactionDetail[$index]["id"]);
                    $salesDetail->status = "0";
                    $salesDetail->updated_by = $request->input('updated_by');
                    $salesDetail->save();
                }
            }
            
            $stockMutationData = StockMutation::where('transaction_id', $receiptNo)->get();
            $arrStockMutation = $stockMutationData->toArray();

            if(count($arrStockMutation) > 0) {
                for ($index=0; $index < count($arrStockMutation); $index++) {
                    $stockMutation = new StockMutation;
                    $stockMutation->warehouse_id = $arrStockMutation[$index]["warehouse_id"];  
                    $stockMutation->transaction_id = $receiptNo;
                    $stockMutation->stock_mutation_type_id = "VO";
                    $stockMutation->product_id = $arrStockMutation[$index]["product_id"];
                    $stockMutation->qty = intval($arrStockMutation[$index]["qty"]);
                    $stockMutation->value = intval($arrStockMutation[$index]["value"]);
                    $stockMutation->stock_move = "I";
                    $stockMutation->trans_date = date('Y-m-d');
                    $stockMutation->save();

                    $paramManageStock = array(
                        'year'=> date('Y'),
                        'month'=> date('m'),
                        'warehouseId'=> $arrStockMutation[$index]["warehouse_id"],
                        'productId'=> $arrStockMutation[$index]["product_id"],
                        'qty'=> $arrStockMutation[$index]["qty"],
                        'value'=> $arrStockMutation[$index]["value"],
                        'mutationType'=> "I"
                    );
            
                    $response = $this->myHelper->stockManagement($paramManageStock);
                }

                DB::commit();
                $response = [
                    'message' => 'Void transactions saved',
                    'status' => [
                        'code' => 201,
                        'description' => 'created'
                    ]
                ];

                return response()->json($response, 201);
            } else {
                DB::rollback();

                $response = [
                    'message' => 'An error occured',
                    'status' => [
                        'code' => 500,
                        'description' => 'internal server error'
                    ],
                ];

                return response()->json($response, 500);
            }
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }
}
