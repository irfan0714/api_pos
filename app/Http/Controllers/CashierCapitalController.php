<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\CashierCapital;
use App\User;

class CashierCapitalController extends Controller
{
    public function __construct() 
    {
        // $this->middleware('jwt.auth');
    }

    /**cls
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = CashierCapital::select('*')->paginate($limit);

        $response = [
            'message' => 'List of cashier capital',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($counterId)
    {   
        $data = User::select('id', 'name')->where('counter_id', '=', $counterId)->get();
        $response = [
            'message' => 'Create cashier capital',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'user_kasir_data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required|numeric',
            'user_id' => 'required',
            'transaction_date' => 'required|date_format:Y-m-d',
            'amount' => 'required|numeric',
            'description' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $cashierCapital = new CashierCapital();
        $cashierCapital->counter_id = $request->input('counter_id');
        $cashierCapital->user_id = $request->input('user_id');
        $cashierCapital->transaction_date = $request->input('transaction_date');
        $cashierCapital->amount = $request->input('amount');
        $cashierCapital->description = $request->input('description');
        $cashierCapital->created_by = $request->input('created_by');

        if($cashierCapital->save()) {
            $cashierCapital->show_counter = [
                'url' => url('/v1/modal-kasir/'.$cashierCapital->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Cashier capital created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $cashierCapital
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = CashierCapital::find($id);

        $response = [
            'message' => 'Show cashier capital',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $counterId)
    {
        $data = CashierCapital::find($id);
        $userKasirData = User::select('id', 'name')->where('counter_id', '=', $counterId)->get();
        $response = [
            'message' => 'Edit modal kasir',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data,
                'user_kasir_data' => $userKasirData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'transaction_date' => 'required|date_format:Y-m-d',
            'amount' => 'required|numeric',
            'description' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $cashierCapital = CashierCapital::find($id);
        $cashierCapital->counter_id = $request->input('counter_id');
        $cashierCapital->user_id = $request->input('user_id');
        $cashierCapital->transaction_date = $request->input('transaction_date');
        $cashierCapital->amount = $request->input('amount');
        $cashierCapital->description = $request->input('description');
        $cashierCapital->created_by = $request->input('created_by');

        if($cashierCapital->save()) {
            $cashierCapital->show_counter = [
                'url' => url('/v1/modal-kasir/'.$cashierCapital->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Cashier capital updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $cashierCapital
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cashierCapital = CashierCapital::findOrFail($id);
        $cashierCapital->delete();
        $response = [
            'message' => 'Cashier capital deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }
}
