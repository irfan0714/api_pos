<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\RoleAccess;
use App\Menu;
use App\Role;

class RoleAccessController extends Controller
{
    
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrRoleId = $request->input('role_id');
        $arrMenuId = $request->input('menu_id');
        $arrView = $request->input('view');
        $arrRead = $request->input('read');
        $arrReadAll = $request->input('read_all'); 
        $arrCreate = $request->input('create');
        $arrUpdate = $request->input('update');
        $arrDelete = $request->input('delete');
        $arrApprove = $request->input('approve');
        $arrDownload = $request->input('download');

        DB::beginTransaction();
        try
        {
            if (count($arrRoleId) > 0) {
                for ($index=0; $index < count($arrRoleId); $index++) { 
                    RoleAccess::where(['role_id'=> $arrRoleId[$index], 'menu_id' => $arrMenuId[$index]])->delete();
                    $roleAccess = new RoleAccess;
                    $roleAccess->role_id = intval($arrRoleId[$index]); 
                    $roleAccess->menu_id = intval($arrMenuId[$index]);
                    $roleAccess->view = $arrView[$index];
                    $roleAccess->read = $arrRead[$index];
                    $roleAccess->read_all = $arrReadAll[$index];
                    $roleAccess->create = $arrCreate[$index];
                    $roleAccess->update = $arrUpdate[$index];
                    $roleAccess->delete = $arrDelete[$index];
                    $roleAccess->approve = $arrApprove[$index];
                    $roleAccess->download = $arrDownload[$index];
                    $roleAccess->save();
                }
                $roleData = Role::select('id', 'role_name')->where('id', $arrRoleId[0])->get();
                $roleAccessData = RoleAccess::where('role_id', $arrRoleId[0])->get();
                
                DB::commit();
                $response = [
                    'message' => 'Role access created',
                    'status' => [
                        'code' => 201,
                        'description' => 'Created'
                    ],
                    'results' => [
                        'role_data' => $roleData,
                        'role_access_data' => $roleAccessData
                    ]
                ];
    
                return response()->json($response, 201);
            }
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleId = $id;
        
        $menuData = [];
        $parentData = Menu::where('parent_id','0')->get();
        foreach ($parentData as $parent) {
            $menuData[] = [
                'id' => $parent['id'],
                'menu_name' => $parent['menu_name'],
            ];

            $dataChild1 = Menu::where('parent_id', $parent['id'])->get();
            foreach ($dataChild1 as $child1) {
                $menuData[] = [
                    'id' => $child1['id'],
                    'menu_name' => "    ".$child1['menu_name'],
                ];

                $dataChild2 = Menu::where('parent_id', $child1['id'])->get();
                foreach ($dataChild2 as $child2) {
                    $menuData[] = [
                        'id' => $child2['id'],
                        'menu_name' => "       ".$child2['menu_name'],
                    ];
                }
            }
        }
        
        $roleAccessData = RoleAccess::select('id',
                                             'role_id',
                                             'menu_id',
                                             'view',
                                             'read',
                                             'read_all',
                                             'create',
                                             'update',
                                             'delete',
                                             'approve',
                                             'download')->where('role_id', $roleId)->get();
        $roleData = Role::select('id', 'role_name')->where('id', $roleId)->get();
        $response = [
            'message' => 'Setup role access',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'role_data' => $roleData,
                'menu_data' => $menuData,
                'role_access_data' => $roleAccessData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arrRoleId = $request->input('role_id');
        $arrMenuId = $request->input('menu_id');
        $arrView = $request->input('view');
        $arrRead = $request->input('read');
        $arrReadAll = $request->input('read_all'); 
        $arrCreate = $request->input('create');
        $arrUpdate = $request->input('update');
        $arrDelete = $request->input('delete');
        $arrApprove = $request->input('approve');
        $arrDownload = $request->input('download');

        DB::beginTransaction();
        try
        {
            if (count($arrRoleId) > 0) {
                for ($index=0; $index < count($arrRoleId); $index++) { 
                    $roleAccess = RoleAccess::where(['role_id'=> $arrRoleId[$index], 'menu_id' => $arrMenuId[$index]])->first();
                    $roleAccess->role_id = intval($arrRoleId[$index]); 
                    $roleAccess->menu_id = intval($arrMenuId[$index]);
                    $roleAccess->view = $arrView[$index];
                    $roleAccess->read = $arrRead[$index];
                    $roleAccess->read_all = $arrReadAll[$index];
                    $roleAccess->create = $arrCreate[$index];
                    $roleAccess->update = $arrUpdate[$index];
                    $roleAccess->delete = $arrDelete[$index];
                    $roleAccess->approve = $arrApprove[$index];
                    $roleAccess->download = $arrDownload[$index];
                    $roleAccess->save();
                }
                $roleData = Role::select('id', 'role_name')->where('id', $arrRoleId[0])->get();
                $roleAccessData = RoleAccess::where('role_id', $arrRoleId[0])->get();
                
                DB::commit();
                $response = [
                    'message' => 'Role access created',
                    'status' => [
                        'code' => 201,
                        'description' => 'Created'
                    ],
                    'results' => [
                        'role_data' => $roleData,
                        'role_access_data' => $roleAccessData
                    ]
                ];
    
                return response()->json($response, 201);
            }
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
