<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Stock;
use App\StockMutation;
use App\MstWarehouse;
use MyHelper;

class StockController extends Controller
{
    protected $myHelper;

    public function __construct(MyHelper $myHelper){
    	$this->myHelper = $myHelper;
    }

    public function dashboardStock(Request $request)
    {
        $year = $request->input('year');
        $month = $request->input('month');
        $today = $request->input('today');
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');
        $counterId = $request->input('counter_id');
        $arrCounterId = explode(',', $counterId);

        if($month == '01') { $selectEarly = 'stocks.early_01'; $selectIn = 'stocks.in_01'; $selectEnd = 'stocks.end_01'; }
        if($month == '02') { $selectEarly = 'stocks.early_02'; $selectIn = 'stocks.in_02'; $selectEnd = 'stocks.end_02'; }
        if($month == '03') { $selectEarly = 'stocks.early_03'; $selectIn = 'stocks.in_03'; $selectEnd = 'stocks.end_03'; }
        if($month == '04') { $selectEarly = 'stocks.early_04'; $selectIn = 'stocks.in_04'; $selectEnd = 'stocks.end_04'; }
        if($month == '05') { $selectEarly = 'stocks.early_05'; $selectIn = 'stocks.in_05'; $selectEnd = 'stocks.end_05'; }
        if($month == '06') { $selectEarly = 'stocks.early_06'; $selectIn = 'stocks.in_06'; $selectEnd = 'stocks.end_06'; }
        if($month == '07') { $selectEarly = 'stocks.early_07'; $selectIn = 'stocks.in_07'; $selectEnd = 'stocks.end_07'; }
        if($month == '08') { $selectEarly = 'stocks.early_08'; $selectIn = 'stocks.in_08'; $selectEnd = 'stocks.end_08'; }
        if($month == '09') { $selectEarly = 'stocks.early_09'; $selectIn = 'stocks.in_09'; $selectEnd = 'stocks.end_09'; }
        if($month == '10') { $selectEarly = 'stocks.early_10'; $selectIn = 'stocks.in_10'; $selectEnd = 'stocks.end_10'; }
        if($month == '11') { $selectEarly = 'stocks.early_11'; $selectIn = 'stocks.in_11'; $selectEnd = 'stocks.end_11'; }
        if($month == '12') { $selectEarly = 'stocks.early_12'; $selectIn = 'stocks.in_12'; $selectEnd = 'stocks.end_12'; }

        if($counterId != '0') {
            $warehouseData = MstWarehouse::select('id')->whereIn('counter_id', $arrCounterId)->get();
            $arrWarehouseData = $warehouseData->toArray();
            $warehouseId = "";
            if(count($arrWarehouseData) > 0) {
                foreach($warehouseData as $warehouse) {
                    if($warehouseId == "") {
                        $warehouseId = "'" . $warehouse->id . "'";
                    } else {
                        $warehouseId = $warehouseId . ",'" . $warehouse->id . "'";
                    }
                }

                $queryWarehouse = "stocks.warehouse_id IN (" . $warehouseId . ")";
            } else {
                $queryWarehouse = "stocks.warehouse_id IS NOT NULL";
            }

            $stockData = Stock::join('mst_products', 'stocks.product_id', '=', 'mst_products.id')
                    ->select('mst_products.id as pcode', 'mst_products.product_name', $selectEarly, $selectIn, $selectEnd)
                    ->where('stocks.year', $year)
                    ->whereRaw($queryWarehouse)
                    ->get();

            $todayTransactionQty = StockMutation::join('mst_products', 'stock_mutations.product_id', '=', 'mst_products.id')
                    ->selectRaw('stock_mutations.product_id, mst_products.product_name, SUM(stock_mutations.qty) as qty')
                    ->where('stock_mutations.stock_move', 'O')
                    ->where('stock_mutations.trans_date', $today)
                    ->groupBy('stock_mutations.product_id')
                    ->get();

            $monthTransactionQty = StockMutation::join('mst_products', 'stock_mutations.product_id', '=', 'mst_products.id')
                    ->selectRaw('stock_mutations.product_id, mst_products.product_name, SUM(stock_mutations.qty) as qty')
                    ->where('stock_mutations.stock_move', 'O')
                    ->whereBetween('stock_mutations.trans_date', [$startDate, $endDate])
                    ->groupBy('stock_mutations.product_id')
                    ->get();
        } else {
            $stockData = array();
            $todayTransactionQty = array();
            $monthTransactionQty = array();
        }
        

        $response = [
            'message' => 'Stock data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'stock_data' => $stockData,
                'today_transaction_data' => $todayTransactionQty,
                'month_transaction_data' => $monthTransactionQty
            ]
        ];

        return response()->json($response, 200);
    }

    public function manageStock(Request $request)
    {
        $arrYear = strpos($request->input('year'), ',') ? explode(',', $request->input('year')) : [$request->input('year')];
        $arrMonth = strpos($request->input('month'), ',') ? explode(',', $request->input('month')) : [$request->input('month')];
        $arrWarehouseId = strpos($request->input('warehouse_id'), ',') ? explode(',', $request->input('warehouse_id')) : [$request->input('warehouse_id')];
        $arrProductId = strpos($request->input('product_id'), ',') ? explode(',', $request->input('product_id')) : [$request->input('product_id')];
        $arrQty = strpos($request->input('qty'), ',') ? explode(',', $request->input('qty')) : [$request->input('qty')];
        $arrValue = strpos($request->input('value'), ',') ? explode(',', $request->input('value')) : [$request->input('value')];
        $arrMutationType = strpos($request->input('mutation_type'), ',') ? explode(',', $request->input('mutation_type')) : [$request->input('mutation_type')];

        if(count($arrProductId) > 0) {
            for ($index = 0; $index < count($arrProductId); $index++) {
                $paramManageStock = array(
                    'year'=> $arrYear[$index],
                    'month'=> $arrMonth[$index],
                    'warehouseId'=> $arrWarehouseId[$index],
                    'productId'=> $arrProductId[$index],
                    'qty'=> $arrQty[$index],
                    'value'=> $arrValue[$index],
                    'mutationType'=> $arrMutationType[$index]
                );
        
                $response = $this->myHelper->stockManagement($paramManageStock);
            }
        } else {
            $paramManageStock = array(
                'year'=> $request->input('year'),
                'month'=> $request->input('month'),
                'warehouseId'=> $request->input('warehouse_id'),
                'productId'=> $request->input('product_id'),
                'qty'=> $request->input('qty'),
                'value'=> $request->input('value'),
                'mutationType'=> $request->input('mutation_type')	
            );
    
            $response = $this->myHelper->stockManagement($paramManageStock);
        }

        return $response;
    }
}
