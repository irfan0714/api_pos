<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\SalesTransactions;
use App\SalesTransactionDetails;
use App\SalesTransactionPayments;
use App\PointMutation;
use App\MemberPoint;
use App\StockMutation;
use App\MstWarehouse;
use App\MstCashier;
use App\MstProduct;
use App\MstPaymentMethods;
use App\MstVoucher;
use App\MstForeignCurrency;
use App\MstPromotionDetails;
use App\MstEdcMachine;
use App\Customer;
use App\RewardTerm;
use App\RewardTermDetail;
use App\CustomerDownline;
use App\CustomerReward;
use App\RewardMutation;
use App\MstPromotionFreeItem;
use App\MstPriceGroup;
use App\Stock;
use MyHelper;

class SalesTransactionResellerController extends Controller
{
    public function __construct(MyHelper $myHelper) 
    {
        // $this->middleware('jwt.auth');
        $this->myHelper = $myHelper;
    }

    public function getSetupCashier(Request $request)
    {
        $setupDeviceId = $request->input('setup_device_id');
        $counterId = $request->input('counter_id');
        $data = MstCashier::select('*')
                ->where('counter_id', $counterId)
                ->where('setup_device_id', $setupDeviceId)->first();
        $response = [
            'message' => 'Setup cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getMstProduct() 
    {
        
        $data = MstProduct::select('id', 'barcode', 'product_name', 'initial_name','price')->get();
        $response = [
            'message' => 'List of sales product',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getMstPaymentMethods()
    {
        $data = MstPaymentMethods::select('id', 'payment_method_type_id', 'payment_name')
                ->where('active', 1)
                ->orderBy('payment_name', 'asc')
                ->get();
        $response = [
            'message' => 'List of payment methods',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getLastReceiptNo()
    {
        $todayDate = date('Y-m-d');
        $data = SalesTransactions::select('receipt_no')
                ->where('trans_date', $todayDate)
                ->orderByDesc('created_at')
                ->get();

        $response = [
            'message' => 'Last receipt no',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getVoucher()
    {
        $data = MstVoucher::get();

        $response = [
            'message' => 'Voucher data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getForeignCurrencies()
    {
        $data = MstForeignCurrency::where('status', 1)->get();

        $response = [
            'message' => 'Foreign currencies data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getTodayProductPromotion(Request $request)
    {
        $todayDate = date('Y-m-d');
        $year = date('Y');
        $month = date('m');
        $earlyField = "early_".$month;
        $inField = "in_".$month;
        $outField = "out_".$month;
        $endField = "end_".$month;
        $stockData = array();
        $productScanListForPromoData = array();

        $validDayField = "mst_promotions.valid_on_".strtolower(substr(date('l'), 0, 2))." = '1'";
        $counterId = $request->input('counter_id') ? "mst_promotions.counter_id = '".$request->input('counter_id')."'" : 'mst_promotions.counter_id is not null';
        $promotionDetailData = MstPromotionDetails::join('mst_promotions', 'mst_promotion_details.promotion_id', '=', 'mst_promotions.id')
                                ->join('mst_promotion_types', 'mst_promotion_types.id', '=', 'mst_promotions.promotion_type_id')
                                ->select('mst_promotion_details.*', 'mst_promotion_types.promotion_type_name', 'mst_promotions.counter_id', 'mst_promotions.warehouse_id')
                                ->whereRaw($counterId)
                                ->whereRaw('mst_promotions.early_period <= ?', $todayDate)
                                ->whereRaw('mst_promotions.end_period >= ?', $todayDate)
                                ->whereRaw($validDayField)
                                ->whereIn('mst_promotions.promotion_cashier_type', ['0','2'])
                                ->where('mst_promotions.status', '1')
                                ->get();

        $arrPromotionDetail = $promotionDetailData->toArray();
        if(count($arrPromotionDetail) > 0) {
            $arrPromotionDetailId = array();
            foreach($promotionDetailData as $detail) {
                if($detail->promotion_type_name == 'FREE ITEM (ITEM)' || $detail->promotion_type_name == 'FREE ITEM (NOMINAL)') {
                    array_push($arrPromotionDetailId, $detail->id);

                    $arrProductScan = explode(';', $detail->product_id);
                    if(count($arrProductScan) > 0) {
                        for($z = 0; $z < count($arrProductScan); $z++) {
                            $data = array(
                                'promotion_detail_id' => $detail->id,
                                'promotion_id' => $detail->promotion_id,
                                'product_id' => $arrProductScan[$z],
                                'free_qty' => $detail->free_qty,
                                'minimum_buy' => $detail->minimum_buy,
                                'multiple_flag' => $detail->multiple_flag
                            );

                            array_push($productScanListForPromoData, $data);
                        }
                    }

                    $freeItem = MstPromotionFreeItem::select('*')
                                ->where('promotion_detail_id', $detail->id)
                                ->get();
                    
                    $arrFreeItem = $freeItem->toArray();
                    $arrFreeItemProductId = array();
                    if(count($arrFreeItem) > 0) {
                        foreach($freeItem as $free) {
                            array_push($arrFreeItemProductId, $free->product_id);
                        }
                    }
                    
                    $stock = Stock::join('mst_warehouses', 'mst_warehouses.id', '=', 'stocks.warehouse_id')
                                ->join('mst_products', 'mst_products.id', '=', 'stocks.product_id')
                                ->selectRaw("stocks.warehouse_id, stocks.year, mst_warehouses.warehouse_name, mst_products.product_name,
                                stocks.product_id, mst_products.unit, stocks.".$earlyField.", 
                                stocks.".$inField.", stocks.".$outField.", stocks.".$endField."")
                                ->whereRaw("stocks.year = '".$year."'")
                                // ->whereRaw("stocks.product_id = '".$detail->product_id."'")
                                ->whereRaw("stocks.warehouse_id = '".$detail->warehouse_id."'")
                                ->whereIn('stocks.product_id', $arrFreeItemProductId)
                                ->get();

                    $arrStockData = $stock->toArray();
                    if(count($arrStockData) > 0) {
                        foreach($stock as $data) {
                            array_push($stockData, $data);
                        }
                    }
                }
            }

            if(count($arrPromotionDetailId) > 0) {
                $freeItemData = MstPromotionFreeItem::join('mst_products', 'mst_promotion_free_items.product_id', '=', 'mst_products.id')
                                ->select('mst_promotion_free_items.*', 'mst_products.product_name')
                                ->whereIn('mst_promotion_free_items.promotion_detail_id', $arrPromotionDetailId)
                                ->get();
            } else {
                $freeItemData = array();
            }
        } else {
            $freeItemData = array();
        }

        $response = [
            'message' => 'Promotion details data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'promotion_detail_data' => $promotionDetailData,
                'promotion_free_item_data' => $freeItemData,
                'promotion_product_scan_data' => $productScanListForPromoData,
                'stock_free_item' => $stockData
            ]
        ];

        return response()->json($response, 200);
    }

    public function getEdcMachine()
    {
        $data = MstEdcMachine::where('status', 1)->get();

        $response = [
            'message' => 'Edc machine data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getMember()
    {
        // DB::enableQueryLog(); 
        $data = Customer::leftJoin('member_points', 'customers.id', '=', 'member_points.customer_id')
                ->select('customers.*', 'member_points.point_remains', 'member_points.point_remains_value')
                ->get();
        
        $response = [
            'message' => 'Customer data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getResellerDownline() {
         $cutomerDownlineData = CustomerDownline::all();
         $response = [
            'message' => 'Reseller downline data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $cutomerDownlineData
            ]
        ];

        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'cashier_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'counter_id' => 'required|numeric',
            'customer_id' => 'required|numeric',
            'receipt_no' => 'required',
            'trans_date' => 'required|date_format:Y-m-d',
            'trans_time' => 'required',
            'total_item' => 'required|numeric',
            'total_value' => 'required|numeric',
            'total_payment' => 'required|numeric',
            'discount' => 'required|numeric',
            'change' => 'required|numeric',
            'cash' => 'required|numeric',
            'credit_card' => 'required|numeric',
            'debit_card' => 'required|numeric',
            'voucher' => 'required|numeric',
            'member_point' => 'required|numeric',
            'foreign_currency_1' => 'required|numeric',
            'exchange_rate_1' => 'required|numeric',
            'currency_1' => 'required',
            'foreign_currency_2' => 'required|numeric',
            'exchange_rate_2' => 'required|numeric',
            'currency_2' => 'required',
            'foreign_currency_3' => 'required|numeric',
            'exchange_rate_3' => 'required|numeric',
            'currency_3' => 'required',
            'status' => 'required',
            'created_by' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $salesTransactions = new SalesTransactions();
        $salesTransactions->id = $request->input('id');
        $salesTransactions->cashier_id = $request->input('cashier_id');
        $salesTransactions->user_id = $request->input('user_id');
        $salesTransactions->counter_id = $request->input('counter_id');
        $salesTransactions->customer_id = $request->input('customer_id');
        $salesTransactions->receipt_no = $request->input('receipt_no');
        $salesTransactions->trans_date = $request->input('trans_date');
        $salesTransactions->trans_time = $request->input('trans_time');
        $salesTransactions->total_item = $request->input('total_item');
        $salesTransactions->total_value = $request->input('total_value');
        $salesTransactions->total_payment = $request->input('total_payment');
        $salesTransactions->discount = $request->input('discount');
        $salesTransactions->change = $request->input('change');
        $salesTransactions->cash = $request->input('cash');
        $salesTransactions->credit_card = $request->input('credit_card');
        $salesTransactions->debit_card = $request->input('debit_card');
        $salesTransactions->voucher = $request->input('voucher');
        $salesTransactions->member_point = $request->input('member_point');
        $salesTransactions->foreign_currency_1 = $request->input('foreign_currency_1');
        $salesTransactions->exchange_rate_1 = $request->input('exchange_rate_1');
        $salesTransactions->currency_1 = $request->input('currency_1');
        $salesTransactions->foreign_currency_2 = $request->input('foreign_currency_2');
        $salesTransactions->exchange_rate_2 = $request->input('exchange_rate_2');
        $salesTransactions->currency_2 = $request->input('currency_2');
        $salesTransactions->foreign_currency_3 = $request->input('foreign_currency_3');
        $salesTransactions->exchange_rate_3 = $request->input('exchange_rate_3');
        $salesTransactions->currency_3 = $request->input('currency_3');
        $salesTransactions->status = $request->input('status');
        $salesTransactions->created_by = $request->input('created_by');

        if($salesTransactions->save()) {

            $response = [
                'message' => 'Sales transactions created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $salesTransactions
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    public function getMstProductConcat()
    {
        $data = MstProduct::selectRaw("CONCAT(id,'_',product_name,'_Rp.',ROUND(price)) AS item")
                ->orderBy('product_name')
                ->get();
        $response = [
            'message' => 'List of sales product',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function storebundle(Request $request)
    {
        $salesTransactionsData = $request->input('salesTransaction');
        $salesTransactionDetailData = $request->input('salesTransactionDetail');
        $salesTransactionPaymentsData = $request->input('salesTransactionPayments');
        $stockMutationData = $request->input('stockMutation');
        $memberPointData = $request->input('memberPoint');
        $pointMutationData = $request->input('pointMutation');
        $mstVoucherDetailData = $request->input('mstVoucherDetail');
        $freePromoMutationData = $request->input('freePromoMutation');
        $mstPromotionDetailData = $request->input('mstPromotionDetail');

        $dateExplode = explode('-', $salesTransactionsData["trans_date"]);
        $year = substr($dateExplode[0], 2, 2);
        $month = $dateExplode[1];
        $date = $dateExplode[2];

        $data = SalesTransactions::select('receipt_no')
                ->where('trans_date', $salesTransactionsData["trans_date"])
                ->where('counter_id', $salesTransactionsData["counter_id"])
                ->where('cashier_id', $salesTransactionsData["cashier_id"])
                ->orderByDesc('id')
                ->first();

        // $arrData = $data->toArray();
       
        $rewardData1 = $request->input('reward');

        $chasierId = strval($salesTransactionsData["cashier_id"]);
        $newChasierId = sprintf("%03s", $chasierId);
        if($data != null) {
            $latestReceiptNo = $data->receipt_no;
            $lastCounter = substr($latestReceiptNo, -3);
            $counterPlus = intval($lastCounter) + 1;
            $newCounter = sprintf("%03s", $counterPlus);
            
            $receiptNo = $year . $month . $date . $newChasierId . strval($newCounter);
        } else {
            $receiptNo = $year . $month . $date . $newChasierId . "001";
        }

        DB::beginTransaction();
        try
        {
            $salesTransactions = new SalesTransactions();
            $salesTransactions->cashier_id = $salesTransactionsData["cashier_id"];
            $salesTransactions->user_id = $salesTransactionsData["user_id"];
            $salesTransactions->counter_id = $salesTransactionsData["counter_id"];
            $salesTransactions->customer_id = $salesTransactionsData["customer_id"];
            $salesTransactions->receipt_no = $receiptNo;
            $salesTransactions->trans_date = $salesTransactionsData["trans_date"];
            $salesTransactions->trans_time = $salesTransactionsData["trans_time"];
            $salesTransactions->total_item = $salesTransactionsData["total_item"];
            $salesTransactions->total_value = $salesTransactionsData["total_value"];
            $salesTransactions->total_payment = $salesTransactionsData["total_payment"];
            $salesTransactions->discount = $salesTransactionsData["discount"];
            $salesTransactions->change = $salesTransactionsData["change"];
            $salesTransactions->cash = $salesTransactionsData["cash"];
            $salesTransactions->transfer = $salesTransactionsData["transfer"];
            $salesTransactions->credit_card = $salesTransactionsData["credit_card"];
            $salesTransactions->debit_card = $salesTransactionsData["debit_card"];
            $salesTransactions->voucher = $salesTransactionsData["voucher"];
            $salesTransactions->member_point = $salesTransactionsData["member_point"];
            $salesTransactions->reward = $rewardData1['downline']['reward']['pakai'];
            $salesTransactions->foreign_currency_1 = $salesTransactionsData["foreign_currency_1"];
            $salesTransactions->exchange_rate_1 = $salesTransactionsData["exchange_rate_1"];
            $salesTransactions->currency_1 = $salesTransactionsData["currency_1"];
            $salesTransactions->foreign_currency_2 = $salesTransactionsData["foreign_currency_2"];
            $salesTransactions->exchange_rate_2 = $salesTransactionsData["exchange_rate_2"];
            $salesTransactions->currency_2 = $salesTransactionsData["currency_2"];
            $salesTransactions->foreign_currency_3 = $salesTransactionsData["foreign_currency_3"];
            $salesTransactions->exchange_rate_3 = $salesTransactionsData["exchange_rate_3"];
            $salesTransactions->currency_3 = $salesTransactionsData["currency_3"];
            $salesTransactions->transaction_type = $salesTransactionsData["transaction_type"];
            $salesTransactions->status = $salesTransactionsData["status"];
            $salesTransactions->created_by = $salesTransactionsData["created_by"];
            $salesTransactions->save();
            $salesTransactionsId = $salesTransactions->id;

            $arrProductId = $salesTransactionDetailData["product_id"];
            $arrQty = $salesTransactionDetailData["qty"];
            $arrPrice = $salesTransactionDetailData["price"];
            $arrPercentDisc = $salesTransactionDetailData["percent_disc_1"];
            $arrFreeQty = $salesTransactionDetailData["free_qty"];
            $arrDisc = $salesTransactionDetailData["disc_1"];
            $arrNet = $salesTransactionDetailData["net"];

            if (count($arrProductId) > 0) {
                SalesTransactionDetails::where(['sales_transaction_id'=> $receiptNo])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $salesTransactionDetails = new SalesTransactionDetails;
                    $salesTransactionDetails->sales_transaction_id = $salesTransactionsId; 
                    $salesTransactionDetails->product_id = $arrProductId[$index];
                    $salesTransactionDetails->qty = $arrQty[$index];
                    $salesTransactionDetails->free_qty = count($arrFreeQty) > 0 ? intval($arrFreeQty[$index]) : 0;
                    $salesTransactionDetails->price = intval($arrPrice[$index]);
                    $salesTransactionDetails->percent_disc_1 = intval($arrPercentDisc[$index]);
                    $salesTransactionDetails->disc_1 = intval($arrDisc[$index]);
                    $salesTransactionDetails->net = intval($arrNet[$index]);
                    $salesTransactionDetails->status = 1;
                    $salesTransactionDetails->save();
                }
            }

            $arrPaymentMethodId = $salesTransactionPaymentsData["payment_method_id"];
            $arrEdcId = $salesTransactionPaymentsData["edc_id"];
            $arrPoint = $salesTransactionPaymentsData["point"];
            $arrPaymentValue = $salesTransactionPaymentsData["payment_value"];
            $arrVoucherCode = $salesTransactionPaymentsData["voucher_code"];
            $arrExchangeRate = $salesTransactionPaymentsData["exchange_rate"];

            if (count($arrPaymentMethodId) > 0) {
                SalesTransactionPayments::where(['sales_transaction_id'=> $receiptNo])->delete();
                for ($index=0; $index < count($arrPaymentMethodId); $index++) {
                    if(!empty($arrPaymentMethodId[$index])) {
                        $salesTransactionPayments = new SalesTransactionPayments;
                        $salesTransactionPayments->sales_transaction_id = $salesTransactionsId; 
                        $salesTransactionPayments->payment_method_id = $arrPaymentMethodId[$index];
                        $salesTransactionPayments->edc_id = $arrEdcId[$index];
                        $salesTransactionPayments->point = intval($arrPoint[$index]);
                        $salesTransactionPayments->payment_value = intval($arrPaymentValue[$index]);
                        $salesTransactionPayments->voucher_code = $arrVoucherCode[$index];
                        $salesTransactionPayments->exchange_rate = intval($arrExchangeRate[$index]);
                        $salesTransactionPayments->save();
                    }
                }
            }

            $arrCustomerId = $pointMutationData["customer_id"];
            $arrPointMutationTypeId = $pointMutationData["point_mutation_type_id"];
            $arrPoint = $pointMutationData["point"];
            $arrPointvalue = $pointMutationData["point_value"];
            $arrTransDate = $pointMutationData["trans_date"];
            $arrExpiredDate = $pointMutationData["expired_date"];
            $arrStatus = $pointMutationData["status"];
            $arrCreatedBy = $pointMutationData["created_by"];

        /*
            if(count($arrPointMutationTypeId) > 0) {
                PointMutation::where(['sales_transaction_id'=> $receiptNo])->delete();
                for ($index=0; $index < count($arrPointMutationTypeId); $index++) {
                    $pointMutation = new PointMutation();
                    $pointMutation->customer_id = intval($arrCustomerId[$index]);
                    $pointMutation->point_mutation_type_id = $arrPointMutationTypeId[$index];
                    $pointMutation->sales_transaction_id = $receiptNo;
                    $pointMutation->point = intval($arrPoint[$index]);
                    $pointMutation->point_value = intval($arrPointvalue[$index]);
                    $pointMutation->trans_date = $arrTransDate[$index];
                    $pointMutation->expired_date = $arrExpiredDate[$index];
                    $pointMutation->status = intval($arrStatus[$index]);
                    $pointMutation->created_by = $arrCreatedBy[$index];
                    $pointMutation->save();
                }

                $memberPoint = MemberPoint::where('customer_id', $memberPointData["customer_id"])->first();
                $memberPoint->point_total = intval($memberPoint->point_total) + intval($memberPointData["point_total"]);
                $memberPoint->point_total_value = intval($memberPoint->point_total_value) + intval($memberPointData["point_total_value"]);
                $memberPoint->point_used = intval($memberPoint->point_used) + intval($memberPointData["point_used"]);
                $memberPoint->point_used_value = intval($memberPoint->point_used_value) + intval($memberPointData["point_used_value"]);
                $memberPoint->point_remains = intval($memberPoint->point_remains) + intval($memberPointData["point_remains"]);
                $memberPoint->point_remains_value = intval($memberPoint->point_remains_value) + intval($memberPointData["point_remains_value"]);
                $memberPoint->updated_by = $request->input('updated_by');
                $memberPoint->save();
            }
        */

            $arrVoucherId = $mstVoucherDetailData["id"];
            $arrStatus = $mstVoucherDetailData["status"];
            $arrUpdatedBy = $mstVoucherDetailData["updated_by"];

            if (count($arrVoucherId) > 0) {
                for ($index=0; $index < count($arrVoucherId); $index++) {
                    $mstVoucher = MstVoucher::find($arrVoucherId[$index]);
                    $mstVoucher->status = $arrStatus[$index];
                    $mstVoucher->updated_by = $arrUpdatedBy[$index];
                    $mstVoucher->save();
                }
            }

            $warehouseData = MstWarehouse::where('counter_id', $salesTransactionsData["counter_id"])
                            ->where('warehouse_type_id', '1') //warehouse reguler
                            ->where('active', '1')
                            ->first();
            $arrStockMutationTypeId = $stockMutationData["stock_mutation_type_id"];
            $arrProductId = $stockMutationData["product_id"];
            $arrQty = $stockMutationData["qty"];
            $arrValue = $stockMutationData["value"];
            $arrStockMove = $stockMutationData["stock_move"];
            $arrTransDate = $stockMutationData["trans_date"];

            if (count($arrProductId) > 0) {
                for ($index=0; $index < count($arrProductId); $index++) {
                    $stockMutation = new StockMutation;
                    $stockMutation->warehouse_id = $warehouseData->id;  
                    $stockMutation->transaction_id = $receiptNo;
                    $stockMutation->stock_mutation_type_id = $arrStockMutationTypeId[$index];
                    $stockMutation->product_id = $arrProductId[$index];
                    $stockMutation->qty = intval($arrQty[$index]);
                    $stockMutation->value = intval($arrValue[$index]);
                    $stockMutation->stock_move = $arrStockMove[$index];
                    $stockMutation->trans_date = $arrTransDate[$index];
                    $stockMutation->save();

                    $paramManageStock = array(
                        'year'=> date('Y'),
                        'month'=> date('m'),
                        'warehouseId'=> $warehouseData->id,
                        'productId'=> $arrProductId[$index],
                        'qty'=> $arrQty[$index],
                        'value'=> $arrValue[$index],
                        'mutationType'=> "O"
                    );
            
                    $response = $this->myHelper->stockManagement($paramManageStock);
                }
            }

            $arrWarehouseIdFI = $freePromoMutationData["warehouse_id"];
            $arrFIStockMutationTypeId = $freePromoMutationData["stock_mutation_type_id"];
            $arrFIProductId = $freePromoMutationData["product_id"];
            $arrFIQty = $freePromoMutationData["qty"];
            $arrFIValue = $freePromoMutationData["value"];
            $arrFIStockMove = $freePromoMutationData["stock_move"];
            $arrFITransDate = $freePromoMutationData["trans_date"];

            if (count($arrFIProductId) > 0) {
                for ($index=0; $index < count($arrFIProductId); $index++) {
                    $stockMutation = new StockMutation;
                    $stockMutation->warehouse_id = $arrWarehouseIdFI[$index] != 0 ? $arrWarehouseIdFI[$index] : $warehouseData->id;
                    $stockMutation->transaction_id = $receiptNo;
                    $stockMutation->stock_mutation_type_id = $arrFIStockMutationTypeId[$index];
                    $stockMutation->product_id = $arrFIProductId[$index];
                    $stockMutation->qty = intval($arrFIQty[$index]);
                    $stockMutation->value = intval($arrFIValue[$index]);
                    $stockMutation->stock_move = $arrFIStockMove[$index];
                    $stockMutation->trans_date = $arrFITransDate[$index];
                    $stockMutation->save();

                    $paramManageStockFI = array(
                        'year'=> date('Y'),
                        'month'=> date('m'),
                        'warehouseId'=> $arrWarehouseIdFI[$index] != 0 ? $arrWarehouseIdFI[$index] : $warehouseData->id,
                        'productId'=> $arrFIProductId[$index],
                        'qty'=> $arrFIQty[$index],
                        'value'=> $arrFIValue[$index],
                        'mutationType'=> "O"
                    );
            
                    $response = $this->myHelper->stockManagement($paramManageStockFI);
                }
            }

            $arrPromotionDetailId = $mstPromotionDetailData["id"];
            $arrFreeItemUsed = $mstPromotionDetailData["free_item_used"];
            $arrFreeItemId = $mstPromotionDetailData["free_item_id"];

            if (count($arrFreeItemId) > 0) {
                for ($index=0; $index < count($arrFreeItemId); $index++) {
                    if($arrFreeItemId[$index] != 0) {
                        $promotionFree = MstPromotionFreeItem::where('id', $arrFreeItemId[$index])->first();
                    
                        if($promotionFree != null) {
                            $mstPromotionFreeItem = MstPromotionFreeItem::find($arrFreeItemId[$index]);
                            $mstPromotionFreeItem->free_item_used = intval($promotionFree->free_item_used) + intval($arrFreeItemUsed[$index]);
                            $mstPromotionFreeItem->save();
                        }
                    }
                }
            }

            //downline
            $rewardData = $request->input('reward');
            if($rewardData['downline']['pointMutation']['point'] > 0) {
                $point = new PointMutation();
                $point->customer_id = $rewardData['downline']['pointMutation']['customerId'];
                $point->point_mutation_type_id = $rewardData['downline']['pointMutation']['type'];
                $point->sales_transaction_id = $receiptNo;
                $point->point = $rewardData['downline']['pointMutation']['point'];
                $point->point_value = $rewardData['downline']['pointMutation']['pointValue'];
                $point->trans_date = date('Y-m-d');
                $point->expired_date = null;
                $point->status = 1;
                $point->created_by = $salesTransactionsData["created_by"];
                $point->save();

                $memberPoint = MemberPoint::where('customer_id', $rewardData['downline']['pointMutation']['customerId'])->first();
                if($memberPoint) {
                    $memberPoint->point_total = intval($memberPoint->point_total) + intval($rewardData['downline']['pointMutation']['point']);
                    $memberPoint->point_total_value = intval($memberPoint->point_total_value) + intval($rewardData['downline']['pointMutation']['pointValue']);
                    $memberPoint->point_remains = intval($memberPoint->point_remains) + intval($rewardData['downline']['pointMutation']['point']);
                    $memberPoint->point_remains_value = intval($memberPoint->point_remains_value) + intval($rewardData['downline']['pointMutation']['pointValue']);
                    $memberPoint->updated_by = $salesTransactionsData['created_by'];
                    $memberPoint->save();
                }
            }
            
            if($rewardData['downline']['reward']['pakai'] > 0) {
                $rewardPakai = $rewardData['downline']['reward']['pakai'];

                $salesTransactionPayments = new SalesTransactionPayments;
                $salesTransactionPayments->sales_transaction_id = $receiptNo; 
                $salesTransactionPayments->payment_method_id = $rewardData['downline']['reward']['paymentMethodId'];
                $salesTransactionPayments->payment_value = $rewardPakai;
                $salesTransactionPayments->save();

                $rewardDownline = CustomerReward::where('customer_id', $rewardData['downline']['resellerId'])->first();
                $rewardDownline->reward_used = intval($rewardDownline->reward_used) + intval($rewardPakai);
                $rewardDownline->reward_remains = intval($rewardDownline->reward_remains) - intval($rewardPakai);
                $rewardDownline->updated_by = $salesTransactionsData['created_by'];
                $rewardDownline->save();

                //reward mutation downline
                $rewardMutation = new RewardMutation();
                $rewardMutation->upline_id = $rewardData['downline']['resellerId'];
                $rewardMutation->sales_transaction_id = $receiptNo;
                $rewardMutation->reward_mutation_type_id = 'O';
                $rewardMutation->transaction_date = $todayDate;
                $rewardMutation->currency = 'IDR';
                $rewardMutation->reward_value = $rewardPakai;
                $rewardMutation->created_by = $salesTransactionsData['created_by'];
                $rewardMutation->save();
            }

            //upline
            $rewardUplineData = $rewardData['upline']['rewardMutation'];
            if(count($rewardUplineData) > 0) {
                $totalReward = 0;
                foreach($rewardUplineData as $key => $row) {
                    $rewardMutation = new RewardMutation();
                    $rewardMutation->upline_id = $row['uplineId'];
                    $rewardMutation->downline_id = $row['downlineId'];
                    $rewardMutation->sales_transaction_id = $receiptNo;
                    $rewardMutation->reward_mutation_type_id = $row['rewardMutationTypeId'];
                    $rewardMutation->transaction_date = $todayDate;
                    $rewardMutation->currency = 'IDR';
                    $rewardMutation->total_sales = $row['totalSales'];
                    $rewardMutation->reward_value = $row['rewardValue'];
                    $rewardMutation->reward_percent = $row['rewardPercent'];
                    $rewardMutation->created_by = $salesTransactionsData['created_by'];
                    $rewardMutation->save();
                    $totalReward += $row['rewardValue'];
                }

                //summary perupline
                $rewardUpline = CustomerReward::where('customer_id', $rewardData['upline']['resellerId'])->first();
                $rewardUpline->reward_value = intval($rewardUpline->reward_value) + intval($totalReward);
                $rewardUpline->reward_remains = intval($rewardUpline->reward_remains) + intval($totalReward);
                $rewardUpline->updated_by = $salesTransactionsData['created_by'];
                $rewardUpline->save();

                //summary perdownline
                $customerDownline = CustomerDownline::where(['upline_id' => $rewardData['upline']['resellerId'],'downline_id' => $rewardData['downline']['resellerId']])->first();
                $customerDownline->reward_value = intval($customerDownline->reward_value) + intval($totalReward);
                $customerDownline->updated_by = $salesTransactionsData['created_by'];
                $customerDownline->save();
            }
            //tutup tambahan
            DB::commit();
            $response = [
                'message' => 'Sales transactions created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $salesTransactions
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    public function getMstProductReseller(Request $request) 
    {
        $counterId = $request->input('counter_id');
        $priceGroup = MstPriceGroup::where('counter_id', $counterId)
                      ->where('price_group_types', 'RS') // RS = Reseller
                      ->where('active', '1')
                      ->first();
        
        if($priceGroup != null) {
            $first = MstProduct::leftJoin('mst_price_group_details', 'mst_products.id', '=', 'mst_price_group_details.product_id')
                    ->selectRaw("mst_products.id, mst_products.barcode, mst_products.product_name,
                    mst_products.initial_name, mst_products.price AS mst_product_price, mst_price_group_details.price AS group_detail_price,
                    IF(mst_price_group_details.product_id IS NULL, mst_products.price, mst_price_group_details.price) AS price")
                    ->where('mst_price_group_details.price_group_id', $priceGroup->id);

            $second = MstProduct::selectRaw("mst_products.id, mst_products.barcode, mst_products.product_name,
                      mst_products.initial_name, mst_products.price AS mst_product_price,
                      mst_products.price AS group_detail_price, mst_products.price AS price")
                      ->whereRaw("mst_products.id NOT IN (SELECT product_id
                                                          FROM mst_price_group_details
                                                          WHERE mst_price_group_details.price_group_id = '".$priceGroup->id."')");

            $data = $first->union($second)->get();
        } else {
            $data = MstProduct::select('id', 'barcode', 'product_name', 'initial_name', 'price')->get();
        }

        $response = [
            'message' => 'List of sales product reseller',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getRewardTerm() {
        $dataRewardTerm = RewardTerm::join('reward_term_details', 'reward_terms.id', '=', 'reward_term_details.reward_term_id')
                                    ->select('reward_terms.reward_term_name','reward_term_details.*')
                                    ->where('reward_terms.reward_term_name', 'Reseller')
                                    ->orderBy('reward_term_details.id', 'ASC')
                                    ->get();

        $response = [
            'message' => 'List of sales reward term',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'reward_term_data' => $dataRewardTerm,
            ]
        ];

        return response()->json($response, 200);
    }

    public function getRewardMutation() {
        $dataRewardMutation = RewardMutation::all();

        $response = [
            'message' => 'List of reward mutation',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $dataRewardMutation,
            ]
        ];

        return response()->json($response, 200);
    }

    public function getRewardReseller() {
        $dataRewardReseller = CustomerReward::all();
        $response = [
            'message' => 'List of reward reseller',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $dataRewardReseller,
            ]
        ];

        return response()->json($response, 200);
    }

    public function getReseller(Request $request) {
        $keyword = $request->get('keyword');
        DB::enableQueryLog(); 
        $reseller = DB::select('select * from customers where customer_type_id = 2 and (phone like ? or customer_name like ?)',[$keyword.'%', '%'.$keyword.'%']);
        
        $response = [
            'query' => DB::getQueryLog(),
            'message' => 'List of reseller',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $reseller,
            ]
        ];

        return response()->json($response, 200);
    }
    
    public function getProduct(Request $request) {
        $keyword = $request->get('keyword');
        DB::enableQueryLog(); 
        $product = DB::select('select * from mst_products where active = "1" and (id like ? or barcode like ? or product_name like ?)',[ $keyword.'%', $keyword.'%', '%'.$keyword.'%']);
        
        $response = [
            'query' => DB::getQueryLog(),
            'message' => 'List of product',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $product,
            ]
        ];

        return response()->json($response, 200);
    }
}
