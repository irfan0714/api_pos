<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\PettyCashCategory;

class PettyCashCategoryController extends Controller
{
    // public function __construct() 
    // {
    //     $this->middleware('jwt.auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = PettyCashCategory::select('*')->paginate($limit);

        $response = [
            'message' => 'List of petty cash category',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'petty_cash_category_name' => 'required',
            'active' => 'required',
            'created_by' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $pettyCashCategory = new PettyCashCategory();
        $pettyCashCategory->petty_cash_category_name = $request->petty_cash_category_name;
        $pettyCashCategory->active = $request->active;
        $pettyCashCategory->created_by = $request->created_by;

        if($pettyCashCategory->save()) {
            $pettyCashCategory->show_petty_cash_category = [
                'url' => url('/v1/petty-cash-category/'.$pettyCashCategory->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Petty cash category created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $pettyCashCategory
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PettyCashCategory::find($id);

        $response = [
            'message' => 'Show petty cash category',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PettyCashCategory::find($id);
        $response = [
            'message' => 'Edit petty cash category',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data,
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'petty_cash_category_name' => 'required',
            'active' => 'required',
            'updated_by' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $pettyCashCategory = PettyCashCategory::find($id);
        $pettyCashCategory->petty_cash_category_name = $request->petty_cash_category_name;
        $pettyCashCategory->active = $request->active;
        $pettyCashCategory->created_by = $request->created_by;

        if($pettyCashCategory->save()) {
            $pettyCashCategory->show_petty_cash_category = [
                'url' => url('/v1/petty-cash-category/'.$pettyCashCategory->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Petty cash category updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $pettyCashCategory
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pettyCashCategory = PettyCashCategory::find($id);
        $pettyCashCategory->delete();
        $response = [
            'message' => 'Petty Cash Category deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];
        return response()->json($response, 200);
    }
}
