<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstPromotionDetails;
use App\MstPromotions;
use App\MstPromotionFreeItem;
use App\MstProduct;
use App\Stock;

class MstPromotionDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrPromotionId = $request->input('promotion_id');
        $arrProductId = $request->input('product_id');
        $arrType = $request->input('type');
        $arrValue = $request->input('value');
        $arrFreeQty = $request->input('free_qty');
        $arrMinimumBuy = $request->input('minimum_buy');
        $arrMinimumTransaction = $request->input('minimum_transaction');
        $arrFreeItemUsed = $request->input('free_item_used');
        $arrMultipleFlag = $request->input('multiple_flag');
        $arrFreeItem = $request->input('free_item');

        if (count($arrPromotionId) > 0) {
            $mstPromotionDetailData = MstPromotionDetails::where(['promotion_id'=> $arrPromotionId[0]])->get();
            $arrPromotionDetail = $mstPromotionDetailData->toArray();
            $mstPromotionDetailId = array();
            if(count($arrPromotionDetail) > 0) {
                for($x = 0; $x < count($arrPromotionDetail); $x++) {
                    array_push($mstPromotionDetailId, $arrPromotionDetail[$x]["id"]);
                }
            }

            MstPromotionDetails::where(['promotion_id'=> $arrPromotionId[0]])->delete();
            for ($index=0; $index < count($arrPromotionId); $index++) {
                $mstPromotionDetails = new MstPromotionDetails;
                $mstPromotionDetails->promotion_id = intval($arrPromotionId[$index]); 
                $mstPromotionDetails->product_id = $arrProductId[$index];
                $mstPromotionDetails->type = strval($arrType[$index]);
                $mstPromotionDetails->value = intval($arrValue[$index]);
                $mstPromotionDetails->free_qty = intval($arrFreeQty[$index]);
                $mstPromotionDetails->minimum_buy = intval($arrMinimumBuy[$index]);
                $mstPromotionDetails->minimum_transaction = intval($arrMinimumTransaction[$index]);
                $mstPromotionDetails->free_item_used = intval($arrFreeItemUsed[$index]);
                $mstPromotionDetails->multiple_flag = $arrMultipleFlag[$index];
                $mstPromotionDetails->save();

                if(count($arrFreeItem) > 0) {
                    $freeItemArray = explode(';', $arrFreeItem[$index]);
                    MstPromotionFreeItem::whereIn('promotion_detail_id', $mstPromotionDetailId)->delete();
                    for($x=0; $x < count($freeItemArray); $x++) {
                        $mstPromotionFreeItem = new MstPromotionFreeItem;
                        $mstPromotionFreeItem->promotion_detail_id = $mstPromotionDetails->id;
                        $mstPromotionFreeItem->product_id = $freeItemArray[$x];
                        $mstPromotionFreeItem->free_item_used = 0;
                        $mstPromotionFreeItem->save();
                    }
                }
            }

            $promotionData = MstPromotions::select('id', 'promotion_name')->where('id', $arrPromotionId[0])->get();
            $promotionDetailsData = MstPromotionDetails::where('promotion_id', $arrPromotionId[0])->get();
            $response = [
                'message' => 'Promotion details created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'promotion_data' => $promotionData,
                    'promotion_detail_data' => $promotionDetailsData
                ]
            ];

            return response()->json($response, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $year = date('Y');
        $month = date('m');
        $earlyField = "early_".$month;
        $inField = "in_".$month;
        $outField = "out_".$month;
        $endField = "end_".$month;

        $promotionId = $id;

        $promotionData = MstPromotions::join('mst_counters', 'mst_counters.id', '=', 'mst_promotions.counter_id')
                        ->join('mst_warehouses', 'mst_warehouses.id', '=', 'mst_promotions.warehouse_id')
                        ->join('mst_promotion_types', 'mst_promotion_types.id', '=', 'mst_promotions.promotion_type_id')
                        ->select('mst_promotions.*', 'mst_counters.counter_name', 'mst_warehouses.warehouse_name', 'mst_promotion_types.promotion_type_name')
                        ->where('mst_promotions.id', $promotionId)
                        ->get();

        $arrPromotionData = $promotionData->toArray();
        if(count($arrPromotionData) > 0) {
            $stockData = Stock::join('mst_warehouses', 'mst_warehouses.id', '=', 'stocks.warehouse_id')
                        ->join('mst_products', 'mst_products.id', '=', 'stocks.product_id')
                        ->selectRaw("stocks.warehouse_id, stocks.year, mst_warehouses.warehouse_name, mst_products.product_name,
                        stocks.product_id, mst_products.unit, stocks.".$earlyField." as early_stock, 
                        stocks.".$inField." as in_stock, stocks.".$outField." as out_stock,
                        stocks.".$endField." as end_stock")
                        ->whereRaw("stocks.year = '".$year."'")
                        ->whereRaw("stocks.warehouse_id = '".$arrPromotionData[0]['warehouse_id']."'")
                        ->get();
        } else {
            $stockData = array();
        }
        
        $promotionDetailsData = MstPromotionDetails::where('promotion_id', $promotionId)
                                ->orderBy('id', 'asc')
                                ->get();

        $promotionDetailId = array();
        foreach($promotionDetailsData as $detail) {
            array_push($promotionDetailId, $detail->id);
        }

        $promotionFreeItemsData = MstPromotionFreeItem::whereIn('promotion_detail_id', $promotionDetailId)
                                  ->orderBy('promotion_detail_id', 'asc')
                                  ->get();
        
        $productData = MstProduct::select('id', 'product_name', 'barcode')->get();
        $response = [
            'message' => 'Edit promotion details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'promotion_data' => $promotionData,
                'promotion_details_data' => $promotionDetailsData,
                'promotion_free_items' => $promotionFreeItemsData,
                'product_data' => $productData,
                'stock_data' => $stockData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
