<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentVoucher;
use App\PaymentVoucherDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\MstCashBank;
use App\MstCoa;
use App\MstCounter;

class PaymentVoucherController extends Controller
{
    
    public function __construct() 
    {
        // $this->middleware('jwt.auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = PaymentVoucher::select('*')->paginate($limit);

        $response = [
            'message' => 'List of payment voucher',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($counterId)
    {
        $dataMstCashBank = MstCashBank::where(['counter_id' => $counterId, 'active' => 1])->get();
        $dataMstCoa = MstCoa::select('id', 'name')->where('active', 1)->get();
        $dataMstCounter = MstCounter::select('id', 'counter_name')->where('active', 1)->get();
        $dataMstCurrency= DB::table('mst_foreign_currencies')->select('id', 'foreign_currency_name', 'exchange_rate')->where('status', 1)->get();

        $response = [
            'message' => 'List of payment voucher create',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data_cash_bank' => $dataMstCashBank,
                'data_coa' => $dataMstCoa,
                'data_currency' => $dataMstCurrency,
                'data_counter' => $dataMstCounter
            ]
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'counter_id' => 'required|numeric',
            'cash_bank_id' => 'required|numeric',
            'no_referensi' => 'required',
            'recipient' => 'required',
            'desc' => 'required',
            'exchange_rate' => 'required|numeric',
            'amount' => 'required|numeric',
            'payment_proof_no' => 'required',
            'details' => 'required',
            'created_by' => 'required',
            'doc_date' => 'required|date_format:Y-m-d'
        ]);

        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, $response['status']['code']);
        }

        $cashBank = MstCashBank::find($request->cash_bank_id);
        $prefix = $cashBank->pv_code;
        $year = date('Y', strtotime($request->doc_date)); 
        $_year = date('y', strtotime($request->doc_date)); 
        $month = date('m', strtotime($request->doc_date));
        $paymentVoucher = PaymentVoucher::select(DB::raw('SUBSTRING(id, 8, 4) AS last_counter'))
                                            ->whereYear('doc_date', $year)
                                            ->whereMonth('doc_date', $month)
                                            ->orderBy('id', 'desc')
                                            ->first();
        $counterPlus = isset($paymentVoucher->last_counter) ? intval($paymentVoucher->last_counter) + 1 : 1;
        $counter = sprintf("%03s", $counterPlus);
        $code = $prefix.$_year.$month.$counter;
        $statusOke = '1';
        $paymentVoucher = new PaymentVoucher;
        $paymentVoucher->id = $code;
        $paymentVoucher->counter_id = $request->counter_id;
        $paymentVoucher->cash_bank_id = $request->cash_bank_id;
        $paymentVoucher->doc_date = $request->doc_date;
        $paymentVoucher->no_referensi = $request->no_referensi;
        $paymentVoucher->recipient = $request->recipient;
        $paymentVoucher->desc = $request->desc;
        $paymentVoucher->currency = $request->currency;
        $paymentVoucher->exchange_rate = $request->exchange_rate;
        $paymentVoucher->amount = $request->amount;
        $paymentVoucher->payment_proof_no = $request->payment_proof_no;
        $paymentVoucher->status = $statusOke;
        $paymentVoucher->created_by = $request->created_by;
        
        if($paymentVoucher->save()) {
            $details = $request->details;
            for($i=0; $i < count($details); $i++) {
                $dataInsertDetails[] = [
                    'payment_voucher_id' => $code,
                    'counter_id' => $details[$i]['counter_id'],
                    'coa_id' => $details[$i]['coa_id'],
                    'amount' => $details[$i]['amount'],
                    'desc' => $details[$i]['desc']
                ];
            }
            PaymentVoucherDetail::insert($dataInsertDetails);
            $paymentVoucher->show_payment_voucher = [
                'url' => url('/v1/payment-voucher/'.$code),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Payment voucher created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $paymentVoucher
                ]
            ];

        }else {
            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];
        }
        return response()->json($response, $response['status']['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
