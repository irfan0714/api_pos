<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\MstCounter;
use App\MstWarehouse;
use App\ProductRequest;
use App\ProductRequestDetail;
use App\User;
use App\ApprovalHistory;

class ProductRequestController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $counterId = $request->input('counter_id') != 0 ? "product_requests.counter_id IN (".$request->input('counter_id').")" : 'product_requests.counter_id is not null';
        $docDate = "product_requests.doc_date BETWEEN '".$request->input('doc_date_start')."' AND '".$request->input('doc_date_end')."'";
        $status = $request->input('status_id') != 'null' ? "product_requests.status = '".$request->input('status_id')."'" : 'product_requests.status is not null';
        $keywords = $request->input('keywords') != 'null' ? 
                    "product_requests.id LIKE '%".$request->input('keywords')."%'" : 'product_requests.id is not null';

        $data = ProductRequest::join('mst_counters', 'mst_counters.id', '=', 'product_requests.counter_id')
                ->join('mst_warehouses', 'mst_warehouses.id', '=', 'product_requests.warehouse_id')
                ->select('product_requests.*', 'mst_counters.counter_name', 'mst_warehouses.warehouse_name')
                ->whereRaw($counterId)
                ->whereRaw($keywords)
                ->whereRaw($docDate)
                ->whereRaw($status)
                ->orderByDesc('product_requests.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of product request',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->orderBy('warehouse_name', 'asc')
                         ->get();

        $response = [
            'message' => 'Product request for create data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'counter_id' => 'required',
            'warehouse_id' => 'required',
            'doc_date' => 'required|date_format:Y-m-d',
            'need_date' => 'required|date_format:Y-m-d',
            'desc' => 'required',
            'status' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $productRequest = new ProductRequest();
        $productRequest->id = $request->input('id');
        $productRequest->counter_id = $request->input('counter_id');
        $productRequest->warehouse_id = $request->input('warehouse_id');
        $productRequest->doc_date = $request->input('doc_date');
        $productRequest->need_date = $request->input('need_date');
        $productRequest->desc = $request->input('desc');
        $productRequest->status = $request->input('status');
        $productRequest->created_by = $request->input('created_by');

        if($productRequest->save()) {
            $productRequest->show_product_request = [
                'url' => url('/v1/product-request/'.$productRequest->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Product Request created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $productRequest
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $productRequestData = ProductRequest::select('*')
            ->where('id', $id)
            ->get();

        foreach($productRequestData as $request) {
            $createdBy = $request->created_by ? $request->created_by : null;
            $updatedBy = $request->updated_by ? $request->updated_by : null;
        }

        $userCreatedData = null;
        $userUpdatedData = null;
        if(isset($createdBy)) {
            $userCreatedData = User::select('*')->where('username', $createdBy)->get();
        }

        if(isset($updatedBy)) {
            $userUpdatedData = User::select('*')->where('username', $updatedBy)->get();
        }

        $approvalHistoryData = ApprovalHistory::join('users', 'users.username', '=', 'approval_histories.username')
            ->select('approval_histories.*', 'users.name')
            ->whereRaw("approval_histories.transaction_id = '".$id."'")
            ->get();

        $productRequestDetailData = ProductRequestDetail::join('product_requests', 'product_requests.id', '=', 'product_request_details.product_request_id')
                ->join('mst_products', 'mst_products.id', '=', 'product_request_details.product_id')
                ->select('product_request_details.*', 'mst_products.product_name')
                ->whereRaw("product_requests.id = '".$id."'")
                ->get();

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->orderBy('warehouse_name', 'asc')
                         ->get();

        $response = [
            'message' => 'Product request details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'product_request_data' => $productRequestData,
                'product_request_detail_data' => $productRequestDetailData,
                'user_create_data' => $userCreatedData,
                'user_update_data' => $userUpdatedData,
                'approval_history_data' => $approvalHistoryData,
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData

            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $productRequest = ProductRequest::find($id);
        $productRequest->need_date = $request->input('need_date') ? $request->input('need_date') : $productRequest->need_date;
        $productRequest->desc = $request->input('desc') ? $request->input('desc') : $productRequest->desc;
        $productRequest->status = $request->input('status');
        $productRequest->updated_by = $request->input('updated_by');

        if($productRequest->save()) {
            $productRequest->show_product_request = [
                'url' => url('/v1/product-request/'.$productRequest->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Product request updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $productRequest
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDataByParameter(Request $request)
    {
        $warehouseId = $request->input('warehouse_id') ? "product_requests.warehouse_id = '".$request->input('warehouse_id')."'" : 'product_requests.warehouse_id is not null';
        $counterId = $request->input('counter_id') ? "product_requests.counter_id = '".$request->input('counter_id')."'" : 'product_requests.counter_id is not null';
        $data = ProductRequest::join('mst_counters', 'mst_counters.id', '=', 'product_requests.counter_id')
                ->join('mst_warehouses', 'mst_warehouses.id', '=', 'product_requests.warehouse_id')
                ->select('product_requests.*', 'mst_counters.counter_name', 'mst_warehouses.warehouse_name')
                ->whereRaw($counterId)
                ->where('product_requests.status', '6')
                ->orderBy('product_requests.created_at', 'desc')
                ->get();

        $response = [
            'message' => 'List of product request',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getDataforAdvancedFilters(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';

        $counterData = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $response = [
            'message' => 'List of data for advanced filters',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData
            ]
        ];

        return response()->json($response, 200);
    }

    public function storeBundle(Request $request)
    {
        $productRequestData = $request->input('productRequest');
        $productRequestDetailData = $request->input('productRequestDetail');
        
        $productRequestId = $this->getLatestId($productRequestData["counter_id"]);

        DB::beginTransaction();
        try
        {
            $productRequest = new ProductRequest();
            $productRequest->id = $productRequestId;
            $productRequest->counter_id = $productRequestData["counter_id"];
            $productRequest->warehouse_id = $productRequestData["warehouse_id"];
            $productRequest->doc_date = $productRequestData["doc_date"];
            $productRequest->need_date = $productRequestData["need_date"];
            $productRequest->desc = $productRequestData["desc"];
            $productRequest->status = $productRequestData["status"];
            $productRequest->created_by = $productRequestData["created_by"];
            $productRequest->save();

            $arrProductId = $productRequestDetailData["product_id"];
            $arrQty = $productRequestDetailData["qty"];
            $arrUnit = $productRequestDetailData["unit"];

            if (count($arrProductId) > 0) {
                ProductRequestDetail::where(['product_request_id'=> $productRequestId])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $productRequestDetail = new ProductRequestDetail;
                    $productRequestDetail->product_request_id = $productRequestId; 
                    $productRequestDetail->product_id = $arrProductId[$index];
                    $productRequestDetail->qty = intval($arrQty[$index]);
                    $productRequestDetail->unit = $arrUnit[$index];
                    $productRequestDetail->qty_receipt = 0;
                    $productRequestDetail->save();
                }
            }

            DB::commit();
            $response = [
                'message' => 'Product request bundle created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'product_request_data' => $productRequest,
                    'product_request_detail_data' => $productRequestDetail
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    public function updateBundle(Request $request, $id)
    {
        $productRequestData = $request->input('productRequest');
        $productRequestDetailData = $request->input('productRequestDetail');
        $approvalHistoryData = $request->input('approvalHistory');

        DB::beginTransaction();
        try
        {
            $productRequest = ProductRequest::find($id);
            $productRequest->doc_date = $productRequestData["doc_date"] ? $productRequestData["doc_date"] : $productRequest->doc_date;
            $productRequest->need_date = $productRequestData["need_date"] ? $productRequestData["need_date"] : $productRequest->need_date;
            $productRequest->desc = $productRequestData["desc"] ? $productRequestData["desc"] : $productRequest->desc;
            $productRequest->status = $productRequestData["status"];
            $productRequest->updated_by = $productRequestData["updated_by"];
            $productRequest->save();

            $arrProductId = $productRequestDetailData["product_id"];
            $arrQty = $productRequestDetailData["qty"];
            $arrUnit = $productRequestDetailData["unit"];

            if (count($arrProductId) > 0) {
                ProductRequestDetail::where(['product_request_id'=> $id])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $productRequestDetail = new ProductRequestDetail;
                    $productRequestDetail->product_request_id = $id; 
                    $productRequestDetail->product_id = $arrProductId[$index];
                    $productRequestDetail->qty = intval($arrQty[$index]);
                    $productRequestDetail->unit = $arrUnit[$index];
                    $productRequestDetail->qty_receipt = 0;
                    $productRequestDetail->save();
                }
            }

            if($approvalHistoryData["transaction_id"] != null) {
                $approvalHistory = new ApprovalHistory();
                $approvalHistory->transaction_id = $approvalHistoryData["transaction_id"];
                $approvalHistory->level = 0;
                $approvalHistory->username = $approvalHistoryData["username"];
                $approvalHistory->status = $approvalHistoryData["status"];
                $approvalHistory->trans_date = $approvalHistoryData["trans_date"];
                $approvalHistory->remark = $approvalHistoryData["remark"] ? $approvalHistoryData["remark"] : null;
                $approvalHistory->save();
            }

            DB::commit();
            $response = [
                'message' => 'Product request details created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'product_request_data' => $productRequest
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    private function getLatestId($counterId)
    {
        $year = date('y');
        $month = date('m');

        $data = ProductRequest::select('id')
                ->where('counter_id', $counterId)
                ->orderByDesc('id')
                ->first();

        if($data != null) {
            $latestProductRequestId = $data->id;
            $splitId = explode("-", $latestProductRequestId);
            $lastCounter = substr($splitId[0], 6, 3);
            $lastMonth = intval(substr($splitId[0], 4, 2));
            $newCounter = intval($lastCounter) + 1;
            if($lastMonth === intval($month)) {
                if($newCounter <= 9) {
                    $newCounter = '00' . strval($newCounter); 
                }
                if($newCounter > 9 && $newCounter <= 99) { 
                    $newCounter = '0' . strval($newCounter); 
                }
            } else {
              $newCounter = '001';
            }
      
            $productRequestId = 'PR' . strval($year) . strval($month) . $newCounter . '-' . $splitId[1];
        } else {
            $newCounterId;
            if(intval($counterId) <= 9) {
                $newCounterId = '00' . strval($counterId); 
            }
            if(intval($counterId) > 9 && intval($counterId) <= 99) { 
                $newCounterId = '0' . strval($counterId); 
            }
            $productRequestId = 'PR' . strval($year) . strval($month) . '001' . '-' . $newCounterId;
        }

        return $productRequestId;
    }
}
