<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetSalesDataExistingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dbId = null)
    {
        $result = [];
        if(!empty($dbId)) {
            $listDbPosExisting =  DB::table('db_pos_existing')->where('id', $dbId)->get();
        } else {
            $listDbPosExisting =  DB::table('db_pos_existing')->where('active', '1')->get();
        }
        foreach($listDbPosExisting as $dbPosExisting) {
            $result[] = $this->getSalesData($dbPosExisting->db_database, $dbPosExisting->counter_id, $dbPosExisting->warehouse_id);
        }

        $response = [
            'results' => $result,
        ];
        return response()->json($response, 200);
    }

    public function getSalesData($dbName, $counterId, $warehouseId) {
        $dbName = strtoupper($dbName);
        $con = DB::connection('DB_'.$dbName);
        
        $countData = $con->table('transaksi_header')->count();
        $loopMax = ceil($countData/100);
        $limit = 100;
        $arrDataSales = [];
        for ($offset=0; $offset <= $loopMax; $offset++) {
            $arrGetDataSalesHeader = $con->table('transaksi_header')->skip($offset)->take($limit)->get();
            foreach($arrGetDataSalesHeader as $getDataSalesHeader) {
                $kasir = isset($getDataSalesHeader->Kasir) ? $getDataSalesHeader->Kasir : "";
                $noKassa = isset($getDataSalesHeader->NoKassa) ? $getDataSalesHeader->NoKassa : "";
                $kdKustomer =  isset($getDataSalesHeader->KdCustomer) ? $getDataSalesHeader->KdCustomer : "";
                $status =  isset($getDataSalesHeader->status) ? $getDataSalesHeader->status : "";
                $kdAgent =  isset($getDataSalesHeader->KdAgent) ? $getDataSalesHeader->KdAgent : "";
                $kdGsa =  isset($getDataSalesHeader->KdGsa) ? $getDataSalesHeader->KdGsa : "";
                $kdGuide =  isset($getDataSalesHeader->KdGuide) ? $getDataSalesHeader->KdGuide : "";
                $noStruk =  isset($getDataSalesHeader->NoStruk) ? $getDataSalesHeader->NoStruk : "";
                $tanggal =  isset($getDataSalesHeader->Tanggal) ? $getDataSalesHeader->Tanggal : "";
                $waktu =  isset($getDataSalesHeader->Waktu) ? $getDataSalesHeader->Waktu : "";
                $totalItem =  isset($getDataSalesHeader->TotalItem) ? $getDataSalesHeader->TotalItem : "";
                $totalNilai =  isset($getDataSalesHeader->TotalNilai) ? $getDataSalesHeader->TotalNilai : "";
                $totalBayar =  isset($getDataSalesHeader->TotalBayar) ? $getDataSalesHeader->TotalBayar : "";
                $discount =  isset($getDataSalesHeader->Discount) ? $getDataSalesHeader->Discount : "";
                $kembali =  isset($getDataSalesHeader->Kembali) ? $getDataSalesHeader->Kembali : "";
                $tunai =  isset($getDataSalesHeader->Tunai) ? $getDataSalesHeader->Tunai : "";
                $kKredit =  isset($getDataSalesHeader->KKredit) ? $getDataSalesHeader->KKredit : "";
                $kDebit =  isset($getDataSalesHeader->KDebit) ? $getDataSalesHeader->KDebit : "";
                $voucher =  isset($getDataSalesHeader->Voucher) ? $getDataSalesHeader->Voucher : "";
                $voucherTravel =  isset($getDataSalesHeader->VoucherTravel) ? $getDataSalesHeader->VoucherTravel : "";
                $dpp =  isset($getDataSalesHeader->DPP) ? $getDataSalesHeader->DPP : "";
                $tax =  isset($getDataSalesHeader->TAX) ? $getDataSalesHeader->TAX : "";
                $keterangan =  isset($getDataSalesHeader->Keterangan) ? $getDataSalesHeader->Keterangan : "";
                $userdisc = isset($getDataSalesHeader->userdisc) ? $getDataSalesHeader->userdisc : "";
                $nilaidisc = isset($getDataSalesHeader->nilaidisc) ? $getDataSalesHeader->nilaidisc : "";;
                $statuskomisi = isset($getDataSalesHeader->statuskomisi) ? $getDataSalesHeader->statuskomisi : "";
                $statuskomisiextra = isset($getDataSalesHeader->statuskomisiextra) ? $getDataSalesHeader->statuskomisiextra : "";
                $valuta = isset($getDataSalesHeader->Valuta) ? $getDataSalesHeader->Valuta : "";
                $valas = isset($getDataSalesHeader->Valas) ? $getDataSalesHeader->Valas : "";
                $kurs = isset($getDataSalesHeader->Kurs) ? $getDataSalesHeader->Kurs : "";
                $valuta2 = isset($getDataSalesHeader->Valuta2) ? $getDataSalesHeader->Valuta2 : "";
                $valas2 = isset($getDataSalesHeader->Valas2) ? $getDataSalesHeader->Valas2 : "";
                $kurs2 = isset($getDataSalesHeader->Kurs2) ? $getDataSalesHeader->Kurs2 : "";
                $valuta3 = isset($getDataSalesHeader->Valuta3) ? $getDataSalesHeader->Valuta3 : "";
                $valas3 = isset($getDataSalesHeader->Valas3) ? $getDataSalesHeader->Valas3 : "";
                $kurs3 = isset($getDataSalesHeader->Kurs3) ? $getDataSalesHeader->Kurs3 : "";
                $valutaKembali = isset($getDataSalesHeader->ValutaKembali) ? $getDataSalesHeader->ValutaKembali : "";
                $kursKembaliUSD = isset($getDataSalesHeader->KursKembaliUSD) ? $getDataSalesHeader->KursKembaliUSD : "";
                $kursKembaliCNY = isset($getDataSalesHeader->KursKembaliCNY) ? $getDataSalesHeader->KursKembaliCNY : "";
                $kembaliValasUSD = isset($getDataSalesHeader->KembaliValasUSD) ? $getDataSalesHeader->KembaliValasUSD : "";
                $kembaliValasCNY = isset($getDataSalesHeader->KembaliValasCNY) ? $getDataSalesHeader->KembaliValasCNY : "";
                $ketVoid = isset($getDataSalesHeader->KetVoid) ? $getDataSalesHeader->KetVoid : "";

                $kasirUser = DB::table('mapping_chasier_user')
                                ->join('users', 'mapping_chasier_user.username', '=', 'users.username')
                                ->select('mapping_chasier_user.username', 'users.id')
                                ->where('chasier_name', $kasir)
                                ->first();
                $username = $kasirUser->username;
                $userId = $kasirUser->id;
                
                DB::table('sales_transactions')->where('receipt_no', '=', $noStruk)->delete();
                $id = DB::table('sales_transactions')->insertGetId([
                        'cashier_id' => $userId,
                        'counter_id' => $counterId,
                        'warehouse_id' => $warehouseId,
                        'customer_id' => $kdKustomer,
                        'gsa_id' => $kdGsa,
                        'agent_id' => $kdAgent,
                        'guide_id' => $kdGuide,
                        'receipt_no' => $noStruk,
                        'trans_date' => $tanggal,
                        'trans_time' => $waktu,
                        'total_item' => $totalItem,
                        'total_value' => $totalNilai,
                        'total_payment' => $totalBayar,
                        'discount' => $discount,
                        'change' => $kembali,
                        'cash' => $tunai,
                        'credit_card' => $kKredit,
                        'debit_card' => $kDebit,
                        'voucher' => $voucher,
                        'travel_voucher' => $voucherTravel,
                        'tax_basis' => $dpp,
                        'vat' => $tax,
                        'description' => $keterangan,
                        'discount_code' => $userdisc,
                        'discount_value' => $nilaidisc,
                        'commission_status' => $statuskomisi,
                        'commission_extra_status' => $statuskomisiextra,
                        'foreign_currency_1' => $valuta,
                        'exchange_rate_1' => $kurs,
                        'currency_1' => $valas,
                        'foreign_currency_2' => $valuta2,
                        'exchange_rate_2' => $kurs2,
                        'currency_2' => $valas2,
                        'foreign_currency_3' => $valuta3,
                        'exchange_rate_3' => $kurs3,
                        'currency_3' => $valas3,    
                        'currency_change' => $valutaKembali,
                        'change_rate_usd' => $kursKembaliUSD,
                        'change_rate_cny' => $kursKembaliCNY,
                        'change_fc_usd' => $kembaliValasUSD,
                        'change_fc_cny' => $kembaliValasCNY,
                        'desc_void' => $ketVoid,
                        'status' => $status,
                        'created_by' => $username,
                        'created_at' => now(),
                        'updated_at' => now()
                ]);

                $arrDataSales[] = $noStruk;
            }
            $offset +=99;
        }

        $result = [
            'database' => $dbName,
            'status' => 'success',
            'data_sales' => $arrDataSales,
        ];

        return $result;
        // dump($con);
        // $ambassador = $DBAmbassador->table('aplikasi')->get();
        // $DBAtrium = DB::connection('DB_POS_ATRIUM');
        // $atrium = $DBAtrium->table('aplikasi')->get();
    }
}
