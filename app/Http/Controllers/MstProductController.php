<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\MstProduct;
use App\MstProductBrand;
use App\MstProductSubBrand;
use App\MstProductType;
use App\MstProductSubType;
use App\MstProductCategory;
use App\MstProductSubCategory;
use App\MstProductMarketingType;

class MstProductController extends Controller
{
    
    public function __construct() 
    {
        $this->middleware('jwt.auth');
        $this->api = 'http://distribusi.vci.co.id';
    }

    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $keywords = $request->input('keywords') != 'null' ? 
                    "mst_products.product_name LIKE '%".$request->input('keywords')."%'
                     OR mst_products.id LIKE '%".$request->input('keywords')."%'
                     OR mst_products.barcode LIKE '%".$request->input('keywords')."%'" : 'mst_products.product_name is not null';
        
        $productBrand = $request->input('product_brand_id') != 'null' ? "mst_products.product_brand_id = '".$request->input('product_brand_id')."'" : 'mst_products.product_brand_id is not null';
        $productType = $request->input('product_type_id') != 'null' ? "mst_products.product_type_id = '".$request->input('product_type_id')."'" : 'mst_products.product_type_id is not null';
        $productCategory = $request->input('product_category_id') != 'null' ? "mst_products.product_category_id = '".$request->input('product_category_id')."'" : 'mst_products.product_category_id is not null';
        $data = MstProduct::join('mst_product_brands', 'mst_products.product_brand_id', '=', 'mst_product_brands.id')
                ->join('mst_product_types', 'mst_products.product_type_id', '=', 'mst_product_types.id')
                ->join('mst_product_categories', 'mst_products.product_category_id', '=', 'mst_product_categories.id')
                ->select('mst_products.*', 'mst_product_brands.product_brand_name',
                'mst_product_types.product_type_name', 'mst_product_categories.product_category_name')
                ->whereRaw($productBrand)
                ->whereRaw($productType)
                ->whereRaw($productCategory)
                ->whereRaw($keywords)
                ->orderBy('mst_products.created_at', 'desc')
                ->paginate($limit);

        $response = [
            'message' => 'List of product',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function create()
    {
        $mstProductBrand = MstProductBrand::select('id', 'product_brand_name')->orderBy('product_brand_name', 'asc')->get();
        $mstProductSubBrand = MstProductSubBrand::select('id', 'product_brand_id', 'product_sub_brand_name')->orderBy('product_sub_brand_name', 'asc')->get();
        $mstProductType = MstProductType::select('id', 'product_type_name')->orderBy('product_type_name', 'asc')->get();
        $mstProductSubType = MstProductSubType::select('id', 'product_type_id', 'product_sub_type_name')->orderBy('product_sub_type_name', 'asc')->get();
        $mstProductCategory = MstProductCategory::select('id', 'product_category_name')->orderBy('product_category_name', 'asc')->get();
        $mstProductSubCategory = MstProductSubCategory::select('id', 'product_category_id', 'product_sub_category_name')->orderBy('product_sub_category_name', 'asc')->get();
        $mstProductMarketingType = MstProductMarketingType::select('id', 'product_marketing_type_name')->orderBy('product_marketing_type_name', 'asc')->get();

        $response = [
            'message' => 'Data for product create',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'mst_product_brand_data' => $mstProductBrand,
                'mst_product_sub_brand_data' => $mstProductSubBrand,
                'mst_product_type_data' => $mstProductType,
                'mst_product_sub_type_data' => $mstProductSubType,
                'mst_product_category_data' => $mstProductCategory,
                'mst_product_sub_category_data' => $mstProductSubCategory,
                'mst_product_marketing_type_data' => $mstProductMarketingType
            ]
        ];

        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'initial_name' => 'required',
            'barcode' => 'required',
            'product_brand_id' => 'required',
            'product_sub_brand_id' => 'required',
            'product_type_id' => 'required',
            'product_sub_type_id' => 'required',
            'product_category_id' => 'required',
            'product_sub_category_id' => 'required',
            'product_marketing_type_id' => 'required',
            'price' => 'required',
            'created_by' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        DB::beginTransaction();
        try
        {
            $mstProduct = new MstProduct();
            $mstProduct->id = $this->generatePcode($request->input('product_marketing_type_id'), $request->input('product_type_id'));
            $mstProduct->product_brand_id = $request->input('product_brand_id');
            $mstProduct->product_sub_brand_id = $request->input('product_sub_brand_id');
            $mstProduct->product_type_id = $request->input('product_type_id');
            $mstProduct->product_sub_type_id = $request->input('product_sub_type_id');
            $mstProduct->product_category_id = $request->input('product_category_id');
            $mstProduct->product_sub_category_id = $request->input('product_sub_category_id');
            $mstProduct->product_marketing_type_id = $request->input('product_marketing_type_id');
            $mstProduct->barcode = $request->input('barcode');
            $mstProduct->product_name = $request->input('product_name');
            $mstProduct->initial_name = $request->input('initial_name');
            $mstProduct->unit = "PCS";
            $mstProduct->price = $request->input('price');
            $mstProduct->unit_size = "CM";
            $mstProduct->length_size = 0;
            $mstProduct->width_size = 0;
            $mstProduct->height_size = 0;
            $mstProduct->active = "1";
            $mstProduct->created_by = $request->input('created_by');
            $mstProduct->save();

            DB::commit();
            $response = [
                'message' => 'Product created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $mstProduct
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    private function generatePcode($productMarketingTypeId, $productTypeId)
    {
        $frontDigit = strlen($productTypeId) + strlen($productMarketingTypeId);
        $frontCode = $productTypeId.$productMarketingTypeId;
        $queryWhere = "LEFT(id,'$frontDigit') = '".$frontCode."'";
        $getLastCounter = MstProduct::selectRaw('MAX(RIGHT(id,3)) AS last_counter')
                          ->whereRaw($queryWhere)
                          ->first();

        $lastCounter = $getLastCounter != null ? intval($getLastCounter->last_counter) + 1 : 1;
        $newPCode = $frontCode.sprintf("%03s", $lastCounter);
        return $newPCode;
    }

    public function edit($id)
    {
        $mstProduct = MstProduct::find($id);
        $mstProductBrand = MstProductBrand::select('id', 'product_brand_name')->orderBy('product_brand_name', 'asc')->get();
        $mstProductSubBrand = MstProductSubBrand::select('id', 'product_brand_id', 'product_sub_brand_name')->orderBy('product_sub_brand_name', 'asc')->get();
        $mstProductType = MstProductType::select('id', 'product_type_name')->orderBy('product_type_name', 'asc')->get();
        $mstProductSubType = MstProductSubType::select('id', 'product_type_id', 'product_sub_type_name')->orderBy('product_sub_type_name', 'asc')->get();
        $mstProductCategory = MstProductCategory::select('id', 'product_category_name')->orderBy('product_category_name', 'asc')->get();
        $mstProductSubCategory = MstProductSubCategory::select('id', 'product_category_id', 'product_sub_category_name')->orderBy('product_sub_category_name', 'asc')->get();
        $mstProductMarketingType = MstProductMarketingType::select('id', 'product_marketing_type_name')->orderBy('product_marketing_type_name', 'asc')->get();

        $response = [
            'message' => 'Data for product edit',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'mst_product' => $mstProduct,
                'mst_product_brand_data' => $mstProductBrand,
                'mst_product_sub_brand_data' => $mstProductSubBrand,
                'mst_product_type_data' => $mstProductType,
                'mst_product_sub_type_data' => $mstProductSubType,
                'mst_product_category_data' => $mstProductCategory,
                'mst_product_sub_category_data' => $mstProductSubCategory,
                'mst_product_marketing_type_data' => $mstProductMarketingType
            ]
        ];

        return response()->json($response, 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'initial_name' => 'required',
            'barcode' => 'required',
            'product_brand_id' => 'required',
            'product_sub_brand_id' => 'required',
            'product_type_id' => 'required',
            'product_sub_type_id' => 'required',
            'product_category_id' => 'required',
            'product_sub_category_id' => 'required',
            'product_marketing_type_id' => 'required',
            'price' => 'required',
            'active' => 'required',
            'updated_by' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        DB::beginTransaction();
        try
        {
            $mstProduct = MstProduct::find($id);
            $mstProduct->product_brand_id = $request->input('product_brand_id');
            $mstProduct->product_sub_brand_id = $request->input('product_sub_brand_id');
            $mstProduct->product_type_id = $request->input('product_type_id');
            $mstProduct->product_sub_type_id = $request->input('product_sub_type_id');
            $mstProduct->product_category_id = $request->input('product_category_id');
            $mstProduct->product_sub_category_id = $request->input('product_sub_category_id');
            $mstProduct->product_marketing_type_id = $request->input('product_marketing_type_id');
            $mstProduct->barcode = $request->input('barcode');
            $mstProduct->product_name = $request->input('product_name');
            $mstProduct->initial_name = $request->input('initial_name');
            $mstProduct->price = $request->input('price');
            $mstProduct->active = $request->input('active');
            $mstProduct->updated_by = $request->input('updated_by');
            $mstProduct->save();

            DB::commit();
            $response = [
                'message' => 'Product updated',
                'status' => [
                    'code' => 201,
                    'description' => 'updated'
                ],
                'results' => [
                    'data' => $mstProduct
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    public function pull(Request $request) 
    {
        $validator = Validator::make($request->all(), ['created_by' => 'required']);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 
        $createdBy = $request->input('created_by');

        $brandData = $this->pullProductBrand($createdBy);
        $subBrandData = $this->pullProductSubBrand($createdBy);
        $typeData = $this->pullProductType($createdBy);
        $subTypeData = $this->pullProductSubType($createdBy);
        $categoryData = $this->pullProductCategory($createdBy);
        $subCategoryData = $this->pullProductSubCategory($createdBy);

        $productData = [];
        $limit = 100;
		for ($offset=0; $offset < 9999; $offset++) {
            
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->api.'/api_iso?action=masterbarang&offset='.$offset.'&limit='.$limit,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);
            $dataResponse = json_decode($response); 
            curl_close($curl);
            $tempBarcode = '';
            $barcode=[];
            if (count($dataResponse) > 0) {
                foreach ($dataResponse as $data) {
                    $mstProductFind = MstProduct::where('id', $data->PCode)->first();
                    if ($mstProductFind != null) {
                        $mstProductFind->delete();
                    }
                    $mstProduct = new MstProduct;
                    $mstProduct->id = $data->PCode;
                    $mstProduct->product_brand_id = $data->Brand;
                    $mstProduct->product_sub_brand_id = $data->KdSubBrand;
                    $mstProduct->product_type_id = $data->KdDivisi;
                    $mstProduct->product_sub_type_id = $data->KdSubDivisi;
                    $mstProduct->product_category_id = $data->Kategori;
                    $mstProduct->product_sub_category_id = $data->KdSubKategori;
                    $mstProduct->product_marketing_type_id = 4; // 4 = 'BAHAN JADI' (dari table mst_product_marketing_types)
                    $mstProduct->barcode = $data->BarCode;
                    $mstProduct->product_name = $data->NamaBarang;
                    $mstProduct->initial_name = $data->NamaBarang;
                    $mstProduct->unit = $data->UnitKecil;
                    $mstProduct->cogs = 0;
                    $mstProduct->price = (!empty($data->HJualkecil)) ? $data->HJualkecil : 0;
                    $mstProduct->mrp = 0;
                    $mstProduct->percent_vat = 10;
                    $mstProduct->include_tax = ($data->FlagPPN == 'Y') ? '1' : '0';
                    $mstProduct->min_stock = 0;
                    $mstProduct->max_stock = 0;
                    $mstProduct->unit_size = 'CM';
                    $mstProduct->length_size = (!empty($data->UkuranP)) ? $data->UkuranP : 0;
                    $mstProduct->width_size = (!empty($data->UkuranL)) ? $data->UkuranL : 0;
                    $mstProduct->height_size = (!empty($data->UkuranT)) ? $data->UkuranT : 0;
                    $mstProduct->active = ($data->Active == 'Y') ? '1' : '0';
                    $mstProduct->created_by = $createdBy;
                    if ($mstProduct->save()) {
                        $productData[] = $mstProduct;
                    } 

                    //barcode double , ambil pcode terbaru save barcodenya else kosongin
                    if($tempBarcode == $data->BarCode) {
                        $barcode[] = $data->BarCode;
                    }
                    $tempBarcode = $data->BarCode;
                }
            }else {
                $response = [
                    'message' => 'Product created',
                    'status' => [
                        'code' => 200,
                        'description' => 'OK'
                    ],
                    'barcode' => $barcode,
                    'results' => [
                        'product_data' => $productData,
                        'brand_data' => $brandData,
                        'sub_brand_data' => $subBrandData,
                        'type_data' => $typeData,
                        'sub_type_data' => $subTypeData,
                        'category_data' => $categoryData,
                        'sub_category_data' => $subCategoryData
                    ]
                ];

                return response()->json($response, 200);
                exit();
            }
            $offset +=99;

        }
        
    }

    public function pullProductBrand($createdBy) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api.'/api_iso?action=brand-masterbarang',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response); 
        curl_close($curl);

        $mstProductBrandData = [];
        if (count($dataResponse) > 0) {
            MstProductBrand::truncate();
            foreach ($dataResponse as $data) {
                $mstProductBrand = new MstProductBrand;
                $mstProductBrand->id = $data->KdBrand;
                $mstProductBrand->product_brand_name = $data->NamaBrand;
                $mstProductBrand->active = 1;
                $mstProductBrand->created_by = $createdBy;
                if ($mstProductBrand->save()) {
                    $mstProductBrandData[] = $mstProductBrand;
                } 
            }
        }
        return $mstProductBrandData;
    }

    public function pullProductSubBrand($createdBy) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api.'/api_iso?action=subbrand-masterbarang',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response); 
        curl_close($curl);

        $mstProductSubBrandData = [];
        if (count($dataResponse) > 0) {
            MstProductSubBrand::truncate();
            foreach ($dataResponse as $data) {
                $mstProductSubBrand = new MstProductSubBrand;
                $mstProductSubBrand->id = $data->KdSubBrand;
                $mstProductSubBrand->product_brand_id = $data->KdBrand;
                $mstProductSubBrand->product_sub_brand_name = $data->NamaSubBrand;
                $mstProductSubBrand->active = 1;
                $mstProductSubBrand->created_by = $createdBy;
                if ($mstProductSubBrand->save()) {
                    $mstProductSubBrandData[] = $mstProductSubBrand;
                } 
            }
        }
        return $mstProductSubBrandData;
    }

    public function pullProductType($createdBy) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api.'/api_iso?action=divisi-masterbarang',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response); 
        curl_close($curl);

        $mstProductTypeData = [];
        if (count($dataResponse) > 0) {
            MstProductType::truncate();
            foreach ($dataResponse as $data) {
                $mstProductType = new MstProductType;
                $mstProductType->id = $data->KdDivisi;
                $mstProductType->product_type_name = $data->NamaDivisi;
                $mstProductType->active = 1;
                $mstProductType->created_by = $createdBy;
                if ($mstProductType->save()) {
                    $mstProductTypeData[] = $mstProductType;
                } 
            }
        }
        return $mstProductTypeData;
    }

    public function pullProductSubType($createdBy) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api.'/api_iso?action=subdivisi-masterbarang',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response); 
        curl_close($curl);

        $mstProductSubTypeData = [];
        if (count($dataResponse) > 0) {
            MstProductSubType::truncate();
            foreach ($dataResponse as $data) {
                $mstProductSubType = new MstProductSubType;
                $mstProductSubType->id = $data->KdSubDivisi;
                $mstProductSubType->product_type_id = $data->KdDivisi;
                $mstProductSubType->product_sub_type_name = $data->NamaSubDivisi;
                $mstProductSubType->active = 1;
                $mstProductSubType->created_by = $createdBy;
                if ($mstProductSubType->save()) {
                    $mstProductSubTypeData[] = $mstProductSubType;
                } 
            }
        }
        return $mstProductSubTypeData;
    }

    public function pullProductCategory($createdBy) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api.'/api_iso?action=kategori-masterbarang',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response); 
        curl_close($curl);

        $mstProductCategoryData = [];
        if (count($dataResponse) > 0) {
            MstProductCategory::truncate();
            foreach ($dataResponse as $data) {
                $mstProductCategory = new MstProductCategory;
                $mstProductCategory->id = $data->KdKategori;
                $mstProductCategory->product_category_name = $data->NamaKategori;
                $mstProductCategory->active = 1;
                $mstProductCategory->created_by = $createdBy;
                if ($mstProductCategory->save()) {
                    $mstProductCategoryData[] = $mstProductCategory;
                } 
            }
        }
        return $mstProductCategoryData;
    }
    
    public function pullProductSubCategory($createdBy) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api.'/api_iso?action=subkategori-masterbarang',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response); 
        curl_close($curl);

        $mstProductSubCategoryData = [];
        if (count($dataResponse) > 0) {
            MstProductSubCategory::truncate();
            foreach ($dataResponse as $data) {
                $mstProductSubCategory = new MstProductSubCategory;
                $mstProductSubCategory->id = $data->KdSubKategori;
                $mstProductSubCategory->product_category_id = $data->KdKategori;
                $mstProductSubCategory->product_sub_category_name = $data->NamaSubKategori;
                $mstProductSubCategory->active = 1;
                $mstProductSubCategory->created_by = $createdBy;
                if ($mstProductSubCategory->save()) {
                    $mstProductSubCategoryData[] = $mstProductSubCategory;
                } 
            }
        }
        return $mstProductSubCategoryData;
    }

    public function getAllProduct() 
    {
        $data = MstProduct::select('id', 'product_name', 'barcode')->get();
        $response = [
            'message' => 'List of product',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    public function getDataforAdvancedFilters()
    {
        $brandData = MstProductBrand::select('id', 'product_brand_name')->orderBy('product_brand_name', 'asc')->get();
        $typeData = MstProductType::select('id', 'product_type_name')->orderBy('product_type_name', 'asc')->get();
        $categoryData = MstProductCategory::select('id', 'product_category_name')->orderBy('product_category_name', 'asc')->get();
        $response = [
            'message' => 'List of data for advanced filters',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'brand_data' => $brandData,
                'type_data' => $typeData,
                'category_data' => $categoryData
            ]
        ];

        return response()->json($response, 200);
    }
}
