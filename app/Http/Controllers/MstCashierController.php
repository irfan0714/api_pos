<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MstCashier;
use App\MstCounter;

class MstCashierController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = MstCashier::join('mst_counters', 'mst_counters.id', '=', 'mst_cashiers.counter_id')
                ->select('mst_cashiers.*', 'mst_counters.branch_id', 'mst_counters.counter_name', 
                'mst_counters.trans_date')
                ->paginate($limit);

        $response = [
            'message' => 'List of cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = MstCounter::select('id', 'counter_name')->orderBy('counter_name', 'asc')->get();
        $response = [
            'message' => 'Create cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required|numeric',
            'cashier_name' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstCashier = new MstCashier();
        $mstCashier->counter_id = $request->input('counter_id');
        $mstCashier->cashier_name = $request->input('cashier_name');
        $mstCashier->created_by = $request->input('created_by');

        if($mstCashier->save()) {
            $mstCashier->show_cashier = [
                'url' => url('/v1/cashier/'.$mstCashier->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Cashier created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $mstCashier
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MstCashier::find($id);

        $response = [
            'message' => 'Show cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $data = MstCashier::find($id);
        $counterData = MstCounter::select('id', 'counter_name')->get();
        $response = [
            'message' => 'Edit cashier',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data,
                'counter_data' => $counterData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required|numeric',
            'cashier_name' => 'required',
            'active' => 'required|numeric|max:1',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $mstCashier = MstCashier::find($id);
        $mstCashier->counter_id = $request->input('counter_id');
        $mstCashier->cashier_name = $request->input('cashier_name');
        $mstCashier->active = $request->input('active');
        $mstCashier->updated_by = $request->input('updated_by');

        if($mstCashier->save()) {
            $mstCashier->show_cashier = [
                'url' => url('/v1/cashier/'.$mstCashier->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Cashier updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $mstCashier
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mstCashier = MstCashier::find($id);
        $mstCashier->delete();
        $response = [
            'message' => 'Cashier deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function setupKassa(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'setup' => 'required|numeric|max:1',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        }

        $resultValidation = 0;
        $mstCashier = MstCashier::find($id);

        if($mstCashier->setup_device_id != null) {
            if($mstCashier->setup_device_id != $request->input('setup_device_id') && $request->input('setup_device_id') != null) {
                $response = [
                    'message' => 'Kode Kassa ini sudah digunakan di device lain, silahkan pilih kode lain.',
                    'status' => [
                        'code' => 422,
                        'description' => 'Unprocessable entity'
                    ],
                    'errors' =>  $validator->messages(),
                ];
                return response()->json($response, 422);
            } else {
                $resultValidation = 1;
            }
        } else {
            $resultValidation = 1;
        }

        if($resultValidation == 1) {
            $mstCashier->setup = $request->input('setup');
            $mstCashier->setup_device_id = $request->input('setup_device_id');
            $mstCashier->updated_by = $request->input('updated_by');

            if($mstCashier->save()) {
                $mstCashier->show_cashier = [
                    'url' => url('/v1/cashier/'.$mstCashier->id),
                    'method' => 'GET'
                ];

                $response = [
                    'message' => 'Cashier updated',
                    'status' => [
                        'code' => 201,
                        'description' => 'Updated'
                    ],
                    'results' => [
                        'data' => $mstCashier
                    ]
                ];

                return response()->json($response, 201);
            }

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }
}
