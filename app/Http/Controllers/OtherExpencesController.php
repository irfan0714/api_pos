<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\OtherExpences;
use App\OtherExpencesDetail;
use App\User;
use App\ApprovalHistory;
use App\MstProduct;
use App\MstPurpose;
use App\MstCounter;
use App\MstWarehouse;
use App\StockMutation;
use MyHelper;

class OtherExpencesController extends Controller
{
    protected $myHelper;

    public function __construct(MyHelper $myHelper) 
    {
        $this->middleware('jwt.auth');
        $this->myHelper = $myHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $counterId = $request->input('counter_id') != 0 ? "other_expences.counter_id IN (".$request->input('counter_id').")" : 'other_expences.counter_id is not null';
        $data = OtherExpences::join('mst_counters', 'mst_counters.id', '=', 'other_expences.counter_id')
                ->join('mst_warehouses', 'mst_warehouses.id', '=', 'other_expences.warehouse_id')
                ->join('mst_purposes', 'mst_purposes.id', '=', 'other_expences.purpose_id')
                ->select('other_expences.*', 'mst_counters.counter_name', 'mst_warehouses.warehouse_name', 'mst_purposes.purpose_name')
                ->whereRaw($counterId)
                ->orderByDesc('other_expences.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of other expences',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $productData = MstProduct::select('id', 'product_name')->get();
        $purposeData = MstPurpose::select('*')->get();
        $counterData = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();
        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->get();

        $response = [
            'message' => 'Other expences for create',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'product_data' => $productData,
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData,
                'purpose_data' => $purposeData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'counter_id' => 'required',
            'warehouse_id' => 'required',
            'purpose_id' => 'required',
            'doc_date' => 'required|date_format:Y-m-d',
            'desc' => 'required',
            'status' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $otherExpences = new OtherExpences();
        $otherExpences->id = $request->input('id');
        $otherExpences->counter_id = $request->input('counter_id');
        $otherExpences->warehouse_id = $request->input('warehouse_id');
        $otherExpences->purpose_id = $request->input('purpose_id');
        $otherExpences->doc_date = $request->input('doc_date');
        $otherExpences->desc = $request->input('desc');
        $otherExpences->status = $request->input('status');
        $otherExpences->created_by = $request->input('created_by');

        if($otherExpences->save()) {
            $otherExpences->show_other_expences = [
                'url' => url('/v1/other-expences/'.$otherExpences->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Other expences created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $otherExpences
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    public function storeBundle(Request $request)
    {
        $otherExpencesData = $request->input('otherExpences');
        $otherExpencesDetailData = $request->input('otherExpencesDetail');
        
        $otherExpencesId = $this->getLatestId($otherExpencesData["counter_id"]);

        DB::beginTransaction();
        try
        {
            $otherExpences = new OtherExpences();
            $otherExpences->id = $otherExpencesId;
            $otherExpences->counter_id = $otherExpencesData["counter_id"];
            $otherExpences->warehouse_id = $otherExpencesData["warehouse_id"];
            $otherExpences->purpose_id = $otherExpencesData["purpose_id"];
            $otherExpences->doc_date = $otherExpencesData["doc_date"];
            $otherExpences->desc = $otherExpencesData["desc"];
            $otherExpences->status = $otherExpencesData["status"];
            $otherExpences->created_by = $otherExpencesData["created_by"];
            $otherExpences->save();

            $arrProductId = $otherExpencesDetailData["product_id"];
            $arrQty = $otherExpencesDetailData["qty"];
            $arrUnit = $otherExpencesDetailData["unit"];
            $arrDesc = $otherExpencesDetailData["description"];

            if (count($arrProductId) > 0) {
                OtherExpencesDetail::where(['other_expences_id'=> $otherExpencesId])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $otherExpencesDetail = new OtherExpencesDetail;
                    $otherExpencesDetail->other_expences_id = $otherExpencesId; 
                    $otherExpencesDetail->product_id = $arrProductId[$index];
                    $otherExpencesDetail->qty = intval($arrQty[$index]);
                    $otherExpencesDetail->unit = $arrUnit[$index];
                    $otherExpencesDetail->price = 0;
                    $otherExpencesDetail->description = $arrDesc[$index];
                    $otherExpencesDetail->save();
                }
            }

            DB::commit();
            $response = [
                'message' => 'Other expences bundle created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'other_expences_data' => $otherExpences,
                    'other_expences_detail_data' => $otherExpencesDetail
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $productData = MstProduct::select('id', 'product_name')->get();
        $purposeData = MstPurpose::select('*')->get();
        $counterData = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();
        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                         ->whereRaw($counterIdforWarehouse)
                         ->get();

        $otherExpencesData = OtherExpences::select('*')
            ->where('id', $id)
            ->get();

        $otherExpencesDetailData = OtherExpencesDetail::join('other_expences', 'other_expences.id', '=', 'other_expences_details.other_expences_id')
                ->join('mst_products', 'mst_products.id', '=', 'other_expences_details.product_id')
                ->select('other_expences_details.*', 'mst_products.product_name')
                ->whereRaw("other_expences.id = '".$id."'")
                ->get();

        foreach($otherExpencesData as $expences) {
            $createdBy = $expences->created_by ? $expences->created_by : null;
            $updatedBy = $expences->updated_by ? $expences->updated_by : null;
        }

        $userCreatedData = null;
        $userUpdatedData = null;
        if(isset($createdBy)) {
            $userCreatedData = User::select('*')->where('username', $createdBy)->get();
        }

        if(isset($updatedBy)) {
            $userUpdatedData = User::select('*')->where('username', $updatedBy)->get();
        }

        $approvalHistoryData = ApprovalHistory::join('users', 'users.username', '=', 'approval_histories.username')
            ->select('approval_histories.*', 'users.name')
            ->whereRaw("approval_histories.transaction_id = '".$id."'")
            ->get();

        $response = [
            'message' => 'Other expences details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'other_expences_data' => $otherExpencesData,
                'other_expences_detail_data' => $otherExpencesDetailData,
                'user_create_data' => $userCreatedData,
                'user_update_data' => $userUpdatedData,
                'approval_history_data' => $approvalHistoryData,
                'product_data' => $productData,
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData,
                'purpose_data' => $purposeData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'warehouse_id' => 'required',
            'purpose_id' => 'required',
            'doc_date' => 'required|date_format:Y-m-d',
            'desc' => 'required',
            'status' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $otherExpences = OtherExpences::find($id);
        $otherExpences->warehouse_id = $request->input('warehouse_id');
        $otherExpences->purpose_id = $request->input('purpose_id');
        $otherExpences->doc_date = $request->input('doc_date');
        $otherExpences->desc = $request->input('desc');
        $otherExpences->status = $request->input('status');
        $otherExpences->updated_by = $request->input('updated_by');

        if($otherExpences->save()) {
            $otherExpences->show_other_expences = [
                'url' => url('/v1/other-expences/'.$otherExpences->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Other expences updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $otherExpences
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    public function updateBundle(Request $request, $id)
    {
        $otherExpencesData = $request->input('otherExpences');
        $otherExpencesDetailData = $request->input('otherExpencesDetail');
        $stockMutationData = $request->input('stockMutation');
        $approvalHistoryData = $request->input('approvalHistory');

        DB::beginTransaction();
        try
        {
            $otherExpences = OtherExpences::find($id);
            $otherExpences->warehouse_id = $otherExpencesData["warehouse_id"];
            $otherExpences->purpose_id = $otherExpencesData["purpose_id"];
            $otherExpences->doc_date = $otherExpencesData["doc_date"];
            $otherExpences->desc = $otherExpencesData["desc"];
            $otherExpences->status = $otherExpencesData["status"];
            $otherExpences->updated_by = $otherExpencesData["updated_by"];
            $otherExpences->save();

            $arrProductId = $otherExpencesDetailData["product_id"];
            $arrQty = $otherExpencesDetailData["qty"];
            $arrUnit = $otherExpencesDetailData["unit"];
            $arrDesc = $otherExpencesDetailData["description"];

            if (count($arrProductId) > 0) {
                OtherExpencesDetail::where(['other_expences_id'=> $id])->delete();
                for ($index=0; $index < count($arrProductId); $index++) {
                    $otherExpencesDetail = new OtherExpencesDetail;
                    $otherExpencesDetail->other_expences_id = $id; 
                    $otherExpencesDetail->product_id = $arrProductId[$index];
                    $otherExpencesDetail->qty = intval($arrQty[$index]);
                    $otherExpencesDetail->unit = $arrUnit[$index];
                    $otherExpencesDetail->price = 0;
                    $otherExpencesDetail->description = $arrDesc[$index];
                    $otherExpencesDetail->save();
                }
            }

            if($approvalHistoryData["transaction_id"] != null) {
                $approvalHistory = new ApprovalHistory();
                $approvalHistory->transaction_id = $approvalHistoryData["transaction_id"];
                $approvalHistory->level = 0;
                $approvalHistory->username = $approvalHistoryData["username"];
                $approvalHistory->status = $approvalHistoryData["status"];
                $approvalHistory->trans_date = $approvalHistoryData["trans_date"];
                $approvalHistory->remark = $approvalHistoryData["remark"] ? $approvalHistoryData["remark"] : null;
                $approvalHistory->save();

                $arrWarehouseId = $stockMutationData["warehouse_id"];
                $arrTransactionId = $stockMutationData["transaction_id"];
                $arrStockMutationTypeId = $stockMutationData["stock_mutation_type_id"];
                $arrProductId = $stockMutationData["product_id"];
                $arrQty = $stockMutationData["qty"];
                $arrValue = $stockMutationData["value"];
                $arrStockMove = $stockMutationData["stock_move"];
                $arrTransDate = $stockMutationData["trans_date"];

                if (count($arrProductId) > 0) {
                    for ($index=0; $index < count($arrProductId); $index++) {
                        $stockMutation = new StockMutation;
                        $stockMutation->warehouse_id = $arrWarehouseId[$index];
                        $stockMutation->transaction_id = $arrTransactionId[$index];
                        $stockMutation->stock_mutation_type_id = $arrStockMutationTypeId[$index];
                        $stockMutation->product_id = $arrProductId[$index];
                        $stockMutation->qty = intval($arrQty[$index]);
                        $stockMutation->value = intval($arrValue[$index]);
                        $stockMutation->stock_move = $arrStockMove[$index];
                        $stockMutation->trans_date = $arrTransDate[$index];
                        $stockMutation->save();

                        $paramManageStock = array(
                            'year'=> date('Y'),
                            'month'=> date('m'),
                            'warehouseId'=> $arrWarehouseId[$index],
                            'productId'=> $arrProductId[$index],
                            'qty'=> $arrQty[$index],
                            'value'=> $arrValue[$index],
                            'mutationType'=> "O"
                        );
                
                        $response = $this->myHelper->stockManagement($paramManageStock);
                    }
                }
            }

            DB::commit();
            $response = [
                'message' => 'Other expences updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'other_expences_data' => $otherExpences
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getLatestId($counterId)
    {
        $year = date('y');
        $month = date('m');

        $data = OtherExpences::select('id')
                ->where('counter_id', $counterId)
                ->orderByDesc('id')
                ->first();

        if($data != null) {
            $latestOtherExpencesId = $data->id;
            $splitId = explode("-", $latestOtherExpencesId);
            $lastCounter = substr($splitId[0], 6, 3);
            $lastMonth = intval(substr($splitId[0], 4, 2));
            $newCounter = intval($lastCounter) + 1;
            if($lastMonth === intval($month)) {
                if($newCounter <= 9) {
                    $newCounter = '00' . strval($newCounter); 
                }
                if($newCounter > 9 && $newCounter <= 99) { 
                    $newCounter = '0' . strval($newCounter); 
                }
            } else {
              $newCounter = '001';
            }
      
            $otherExpencesId = 'PL' . strval($year) . strval($month) . $newCounter . '-' . $splitId[1];
        } else {
            $newCounterId;
            if(intval($counterId) <= 9) {
                $newCounterId = '00' . strval($counterId); 
            }
            if(intval($counterId) > 9 && intval($counterId) <= 99) { 
                $newCounterId = '0' . strval($counterId); 
            }
            $otherExpencesId = 'PL' . strval($year) . strval($month) . '001' . '-' . $newCounterId;
        }

        return $otherExpencesId;
    }
}
