<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Customer;
use App\MemberPoint;
use App\PointMutation;

class CustomerController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $data = Customer::select('*')->paginate($limit);

        $response = [
            'message' => 'List of customer',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_name' => 'required',
            'identity_number' => 'required',
            'phone' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $customer = new Customer();
        $customer->customer_name = $request->input('customer_name');
        $customer->identity_number = $request->input('identity_number');
        $customer->phone = $request->input('phone');
        $customer->created_by = $request->input('created_by');

        if($customer->save()) {
            $customer->show_customer = [
                'url' => url('/v1/customer/'.$customer->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Customer created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $customer
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Customer::find($id);

        $response = [
            'message' => 'Show customer',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Customer::find($id);
        $response = [
            'message' => 'Edit customer',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'customer_name' => 'required',
            'identity_number' => 'required',
            'phone' => 'required',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $customer = Customer::find($id);
        $customer->customer_name = $request->input('customer_name');
        $customer->identity_number = $request->input('identity_number');
        $customer->phone = $request->input('phone');
        $customer->updated_by = $request->input('updated_by');

        if($customer->save()) {
            $customer->show_customer = [
                'url' => url('/v1/customer/'.$customer->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Customer updated',
                'status' => [
                    'code' => 200,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $customer
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        $response = [
            'message' => 'Customer deleted',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ]
        ];

        return response()->json($response, 200);
    }

    public function pull(Request $request) 
    {
        $validator = Validator::make($request->all(), ['created_by' => 'required']);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        }

        $createdBy = $request->input('created_by');

        $customerData = [];
        $body = array("aksi" => "get_all_member");                                                                    
        $data_body = json_encode($body);  

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://mobile1.vci.co.id/vci_mobile_api/index.php/mobile_member/ListMember',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => $data_body,
            CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json')
        ));

        $response = curl_exec($curl);
        $dataResponse = json_decode($response); 
        curl_close($curl);
        $arrUser = $dataResponse->user;
        $arrUserPointBalance = $dataResponse->user_point_balance;
        $arrUserPointHistory = $dataResponse->user_point_history;

        DB::beginTransaction();
        try
        {
            if (count($arrUser) > 0) {
                foreach ($arrUser as $data) {
                    $check = Customer::where('card_id', $data->id_member)->count();
                    if (empty($check)) {
                        if($data->id_member != "0") {
                            $customer = new Customer;
                            $customer->customer_type_id = 1; // 1 = Member Reguler
                            $customer->card_id = $data->id_member;
                            $customer->customer_name = $data->full_name;
                            $customer->phone = $data->phone;
                            $customer->created_by = $createdBy;
                            if ($customer->save()) {
                                $customerData[] = $customer;
                            } 
                        }                    
                    }
                }
    
                if(count($arrUserPointBalance) > 0) {
                    foreach ($arrUserPointBalance as $balance) {
                        $customer = Customer::where('card_id', $balance->id_card)->first();
                        if($customer != null) {
                            $memberPoint = new MemberPoint;
                            $memberPoint->customer_id = $customer->id;
                            $memberPoint->point_remains = $balance->balance;
                            $memberPoint->point_remains_value = intval($balance->balance) * 100;
                            $memberPoint->created_by = $createdBy;
                            $memberPoint->save();
                        }
                    }
                }
    
                if(count($arrUserPointHistory) > 0) {
                    foreach ($arrUserPointHistory as $history) {
                        $customer = Customer::where('card_id', $history->id_card)->first();
                        if($customer != null) {
                            $pointMutation = new PointMutation;
                            $pointMutation->customer_id = $customer->id;
                            $pointMutation->point_mutation_type_id = strtoupper($history->type);
                            $pointMutation->sales_transaction_id = "0";
                            $pointMutation->point = $history->point;
                            $pointMutation->point_value = intval($history->point) * 100;
                            $pointMutation->trans_date = date('Y-m-d', strtotime($history->add_date));
                            $pointMutation->expired_date = date('Y-m-d', strtotime($history->expired_date));
                            $pointMutation->status = "1";
                            $pointMutation->created_by = $createdBy;
    
                            if ($pointMutation->save()) {
                                $checkMemberPoint = MemberPoint::where('customer_id', $customer->id)->first();
                                if($checkMemberPoint != null) {
                                    $pointTotal = intval($checkMemberPoint->point_total) + intval($history->point);
                                    $pointTotalValue = intval($pointTotal) * 100;
    
                                    $memberPoint = MemberPoint::find($checkMemberPoint->id);
                                    $memberPoint->point_total = $pointTotal;
                                    $memberPoint->point_total_value = $pointTotalValue;
    
                                    if(strtoupper($history->type) == 'O') {
                                        $pointUsed = intval($checkMemberPoint->point_used) + intval($history->point);
                                        $pointUsedValue = intval($pointUsed) * 100;
    
                                        $memberPoint->point_used = $pointUsed;
                                        $memberPoint->point_used_value = $pointUsedValue;
                                    }
    
                                    $memberPoint->updated_by = $createdBy;
                                    $memberPoint->save();
                                }
                            }
                        }
                    }
                }
    
                DB::commit();
                $response = [
                    'message' => 'Customer created',
                    'status' => [
                        'code' => 200,
                        'description' => 'OK'
                    ],
                    'results' => [
                        'customer_data' => $customerData
                    ]
                ];
    
                return response()->json($response, 200);
            } else {
                $response = [
                    'message' => 'Customer created',
                    'status' => [
                        'code' => 200,
                        'description' => 'OK'
                    ],
                    'results' => [
                        'customer_data' => $customerData
                    ]
                ];
    
                return response()->json($response, 200);
            }
        }
        catch (Exception $e) {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
        
    }
}
