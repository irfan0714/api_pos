<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MemberPoint;

class MemberPointController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'created_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $memberPoint = new MemberPoint();
        $memberPoint->customer_id = $request->input('customer_id');
        $memberPoint->created_by = $request->input('created_by');

        if($memberPoint->save()) {
            $memberPoint->show_member_point = [
                'url' => url('/v1/member-point/'.$memberPoint->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Member point created',
                'status' => [
                    'code' => 201,
                    'description' => 'Created'
                ],
                'results' => [
                    'data' => $memberPoint
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MemberPoint::find($id);

        $response = [
            'message' => 'Show member point',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MemberPoint::find($id);

        $response = [
            'message' => 'Show member point',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'data' => $data
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'point_total' => 'required|numeric',
            'point_total_value' => 'required|numeric',
            'point_used' => 'required|numeric',
            'point_used_value' => 'required|numeric',
            'point_remains' => 'required|numeric',
            'point_remains_value' => 'required|numeric',
            'updated_by' => 'required',
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        } 

        $memberPoint = MemberPoint::find($id);
        $memberPoint->customer_id = $request->input('customer_id');
        $memberPoint->point_total = $request->input('point_total');
        $memberPoint->point_total_value = $request->input('point_total_value');
        $memberPoint->point_used = $request->input('point_used');
        $memberPoint->point_used_value = $request->input('point_used_value');
        $memberPoint->point_remains = $request->input('point_remains');
        $memberPoint->point_remains_value = $request->input('point_remains_value');
        $memberPoint->updated_by = $request->input('updated_by');

        if($memberPoint->save()) {
            $memberPoint->show_member_point = [
                'url' => url('/v1/member-point/'.$memberPoint->id),
                'method' => 'GET'
            ];

            $response = [
                'message' => 'Member point updated',
                'status' => [
                    'code' => 200,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $memberPoint
                ]
            ];

            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occured',
            'status' => [
                'code' => 500,
                'description' => 'internal server error'
            ],
        ];

        return response()->json($response, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function findByCustomerId($id)
    {
        $data = MemberPoint::where('customer_id', $id)->get();

        $response = [
            'message' => 'Member point data',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }
}
