<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\MstPriceGroup;
use App\MstPriceGroupDetail;
use App\MstProduct;
use App\User;
use App\MstCounter;
use App\Imports\MstPriceGroupDetailImport;

class MstPriceGroupController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $counterId = $request->input('counter_id') != 0 ? "mst_price_groups.counter_id IN (".$request->input('counter_id').")" : 'mst_price_groups.counter_id is not null';
        $data = MstPriceGroup::join('mst_counters', 'mst_counters.id', '=', 'mst_price_groups.counter_id')
                ->select('mst_price_groups.*', 'mst_counters.counter_name')
                ->whereRaw($counterId)
                ->orderByDesc('mst_price_groups.created_at')
                ->paginate($limit);

        $response = [
            'message' => 'List of price group',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';        
        $mstCounter = MstCounter::select('id', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $mstProduct = MstProduct::select('id', 'product_name', 'price')->get();

        $response = [
            'message' => 'Data for price group create',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'mst_product_data' => $mstProduct,
                'mst_counter_data' => $mstCounter
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mstPriceGroupData = $request->input('mstPriceGroup');
        $mstPriceGroupDetailData = $request->input('mstPriceGroupDetail');

        DB::beginTransaction();
        try
        {
            $mstPriceGroup = new MstPriceGroup();
            $mstPriceGroup->counter_id = $mstPriceGroupData["counter_id"];
            $mstPriceGroup->price_group_types = $mstPriceGroupData["price_group_types"];
            $mstPriceGroup->description = $mstPriceGroupData["description"];
            $mstPriceGroup->active = "1";
            $mstPriceGroup->created_by = $mstPriceGroupData["created_by"];
            $mstPriceGroup->save();
            $mstPriceGroupId = $mstPriceGroup->id;

            $arrProductId = $mstPriceGroupDetailData["product_id"];
            $arrPrice = $mstPriceGroupDetailData["price"];

            if (count($arrProductId) > 0) {
                MstPriceGroupDetail::where(['price_group_id'=> $mstPriceGroupId])->delete();
                for ($index = 0; $index < count($arrProductId); $index++) {
                    $mstPriceGroupDetail = new MstPriceGroupDetail();
                    $mstPriceGroupDetail->price_group_id = $mstPriceGroupId; 
                    $mstPriceGroupDetail->product_id = $arrProductId[$index];
                    $mstPriceGroupDetail->price = $arrPrice[$index];
                    $mstPriceGroupDetail->save();
                }
            }
            DB::commit();
            $response = [
                'message' => 'Price group created',
                'status' => [
                    'code' => 201,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $mstPriceGroup
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mstPriceGroupData = MstPriceGroup::join('mst_counters', 'mst_counters.id', '=', 'mst_price_groups.counter_id')
            ->select('mst_price_groups.*', 'mst_counters.counter_name')
            ->where('mst_price_groups.id', $id)
            ->get();

        foreach($mstPriceGroupData as $group) {
            $createdBy = $group->created_by ? $group->created_by : null;
            $updatedBy = $group->updated_by ? $group->updated_by : null;
        }

        $userCreatedData = null;
        $userUpdatedData = null;
        if(isset($createdBy)) {
            $userCreatedData = User::select('*')->where('username', $createdBy)->get();
        }

        if(isset($updatedBy)) {
            $userUpdatedData = User::select('*')->where('username', $updatedBy)->get();
        }

        $mstPriceGroupDetailData = MstPriceGroupDetail::join('mst_products', 'mst_products.id', '=', 'mst_price_group_details.product_id')
                ->select('mst_price_group_details.*', 'mst_products.product_name')
                ->whereRaw("mst_price_group_details.price_group_id = '".$id."'")
                ->get();

        $response = [
            'message' => 'Price group details',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'price_group_data' => $mstPriceGroupData,
                'price_group_detail_data' => $mstPriceGroupDetailData,
                'user_create_data' => $userCreatedData,
                'user_update_data' => $userUpdatedData
            ]
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mstPriceGroupData = $request->input('mstPriceGroup');
        $mstPriceGroupDetailData = $request->input('mstPriceGroupDetail');

        DB::beginTransaction();
        try
        {
            $mstPriceGroup = MstPriceGroup::find($id);
            $mstPriceGroup->price_group_types = $mstPriceGroupData["price_group_types"];
            $mstPriceGroup->description = $mstPriceGroupData["description"];
            $mstPriceGroup->active = strval($mstPriceGroupData["active"]);
            $mstPriceGroup->updated_by = $mstPriceGroupData["updated_by"];
            $mstPriceGroup->save();

            $arrProductId = $mstPriceGroupDetailData["product_id"];
            $arrPrice = $mstPriceGroupDetailData["price"];

            if (count($arrProductId) > 0) {
                for ($index = 0; $index < count($arrProductId); $index++) {
                    $mstPriceGroupDetail = MstPriceGroupDetail::select('*')
                                           ->where('price_group_id', $id)
                                           ->where('product_id', $arrProductId[$index])
                                           ->first();
                    $mstPriceGroupDetail->price = $arrPrice[$index];
                    $mstPriceGroupDetail->save();
                }
            }
            
            DB::commit();
            $response = [
                'message' => 'Price group updated',
                'status' => [
                    'code' => 201,
                    'description' => 'Updated'
                ],
                'results' => [
                    'data' => $mstPriceGroup
                ]
            ];

            return response()->json($response, 201);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadFile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'counter_id' => 'required',
            'price_group_types' => 'required',
            'description' => 'required',
            'created_by' => 'required',
            'file' => 'required'
        ]);
    
        if ($validator->fails()) {
            $response = [
                'message' => 'The given data was invalid.',
                'status' => [
                    'code' => 422,
                    'description' => 'Unprocessable entity'
                ],
                'errors' =>  $validator->messages(),
            ];
            return response()->json($response, 422);
        }

        DB::beginTransaction();
        try
        {
            $mstPriceGroup = new MstPriceGroup();
            $mstPriceGroup->counter_id = $request->input('counter_id');
            $mstPriceGroup->price_group_types = $request->input('price_group_types');
            $mstPriceGroup->description = $request->input("description");
            $mstPriceGroup->active = "1";
            $mstPriceGroup->created_by = $request->input("created_by");
            $mstPriceGroup->save();
            $mstPriceGroupId = $mstPriceGroup->id;

            $file = $request->file('file');
            $fileName = rand().$file->getClientOriginalName();
            $file->move('mst_price_group_file', $fileName);
            Excel::import(new MstPriceGroupDetailImport, public_path('/mst_price_group_file/'.$fileName));

            $affected = DB::table('mst_price_group_details')
                        ->where('price_group_id', 0)
                        ->update(['price_group_id' => $mstPriceGroupId]);

            DB::commit();
            $response = [
                'message' => 'Mst price group created',
                'status' => [
                    'code' => 200,
                    'description' => 'created'
                ],
                'results' => [
                    'data' => $mstPriceGroup
                ]
            ];

            return response()->json($response, 200);
        }
        catch (Exception $e)
        {
            DB::rollback();

            $response = [
                'message' => 'An error occured',
                'status' => [
                    'code' => 500,
                    'description' => 'internal server error'
                ],
            ];

            return response()->json($response, 500);
        }

        $response = [
            'message' => 'Stock opname created',
            'status' => [
                'code' => 200,
                'description' => 'Created'
            ],
            'results' => [
                'data' => []
            ]
        ];

        return response()->json($response, 200);
    }
}
