<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class SidebarMenuController extends Controller
{
    public function index($username, $roleId)
    {
        $data = [];
        DB::enableQueryLog(); 

        $arrParent = DB::table('role_access')
                        ->join('roles', 'role_access.role_id', '=', 'roles.id')
                        ->join('menus', 'role_access.menu_id', '=', 'menus.id')
                        ->where(['role_access.role_id' => $roleId,'menus.parent_id' => '0', 'role_access.view' => '1'])
                        ->orderBy('menus.weight', 'asc')
                        ->get(); 
        $printQuery = DB::getQueryLog();
        $data['username'] = $username;
        $data['role_id'] = $roleId;
        $data['role_name'] = $arrParent[0]->role_name;
        foreach($arrParent as $parent) {
            $data['parent'][] = [
                'id' => $parent->id,
                'menu_name' => $parent->menu_name,
                'icon' => $parent->icon,
                'root' => $parent->root,
                'view' => $parent->view,
                'read' => $parent->read,
                'read_all' => $parent->read_all,
                'create' => $parent->create,
                'update' => $parent->update,
                'delete' => $parent->delete,
                'approve' => $parent->approve,
                'download' => $parent->download,
                'open_menu' => null
            ]; 
            
            $arrChild1 = $this->getChild($parent->id, $roleId);
            foreach($arrChild1 as $child1) {
                $data['child1'][$child1->parent_id][] = [
                    'id' => $child1->id,
                    'menu_name' => $child1->menu_name,
                    'icon' => $child1->icon,
                    'root' => $child1->root,
                    'view' => $child1->view,
                    'read' => $child1->read,
                    'read_all' => $child1->read_all,
                    'create' => $child1->create,
                    'update' => $child1->update,
                    'delete' => $child1->delete,
                    'approve' => $child1->approve,
                    'download' => $child1->download,
                    'open_menu' => null
                ]; 

                $arrChild2 = $this->getChild($child1->id, $roleId);
                foreach($arrChild2 as $child2) {
                    $data['child2'][$child2->parent_id][] = [
                        'id' => $child2->id,
                        'menu_name' => $child2->menu_name,
                        'icon' => $child2->icon,
                        'root' => $child2->root,
                        'view' => $child2->view,
                        'read' => $child2->read,
                        'read_all' => $child2->read_all,
                        'create' => $child2->create,
                        'update' => $child2->update,
                        'delete' => $child2->delete,
                        'approve' => $child2->approve,
                        'download' => $child2->download,
                        'open_menu' => null
                    ]; 
                }
            }
        }

        // dd($data);
        $response = [
            'message' => 'List of sidebar menu',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => $data,
            'query' => $printQuery
        ];

        return response()->json($response,200);
    }

    public function getChild($parentId, $roleId) 
    {
        $data = DB::table('role_access')
                        ->join('menus', 'role_access.menu_id', '=', 'menus.id')
                        ->where(['menus.parent_id' => $parentId,'role_access.role_id' => $roleId, 'role_access.view' => '1'])
                        ->orderBy('menus.weight', 'asc')
                        ->get(); 
        return $data;

    }
}
