<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\MstCounter;
use App\MstWarehouse;
use App\ProductReceiving;
use App\ProductReceivingDetail;

class ReportProductReceivingController extends Controller
{
    public function __construct() 
    {
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $counterId = $request->input('counter_id') != 0 ? "id IN (".$request->input('counter_id').")" : 'id is not null';
        $counterIdforWarehouse = $request->input('counter_id') != 0 ? "counter_id IN (".$request->input('counter_id').")" : 'counter_id is not null';

        $counterData = MstCounter::select('id', 'counter_code', 'counter_name')
                       ->whereRaw($counterId)
                       ->orderBy('counter_name', 'asc')
                       ->get();

        $warehouseData = MstWarehouse::select('id', 'counter_id', 'warehouse_name')
                       ->whereRaw($counterIdforWarehouse)
                       ->where('active', '1')
                       ->orderBy('warehouse_name', 'asc')
                       ->get();
                    
        $response = [
            'message' => 'Product receiving report index',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'results' => [
                'counter_data' => $counterData,
                'warehouse_data' => $warehouseData
            ]
        ];

        return response()->json($response, 200);
    }

    public function getReportProductReceiving(Request $request)
    {
        DB::enableQueryLog();
        $productReceivingId = $request->input('product_receiving_id');
        $productRequestId = $request->input('product_request_id');
        $counterId = $request->input('counter_id');
        $warehouseId = $request->input('warehouse_id');
        $docDateStart = $request->input('doc_date_start');
        $docDateEnd = $request->input('doc_date_end');

        $warehouseIdList = '';
        if($warehouseId == '0') {
            $warehouseData = MstWarehouse::select('id')
                             ->where('counter_id', $counterId)
                             ->where('active', '1')
                             ->get();
                            
            if(count($warehouseData->toArray()) > 0) {
                foreach($warehouseData as $warehouse) {
                    if($warehouseIdList == '') {
                        $warehouseIdList = $warehouse->id;
                    } else {
                        $warehouseIdList = $warehouseIdList . ',' . $warehouse->id;
                    }
                }
            }
        }

        $whereProductReceivingId = $productReceivingId != 'null' ? "product_receivings.id = '".$productReceivingId."'" : 'product_receivings.id is not null';
        $whereProductRequestId = $productRequestId != 'null' ? "product_receivings.product_request_id = '".$productRequestId."'" : 'product_receivings.product_request_id is not null';
        $whereCounterId = "product_receivings.counter_id = '".$counterId."'";
        $whereWarehouseId = $warehouseId == '0' ? "product_receivings.warehouse_id IN (".$warehouseIdList.")" : "product_receivings.warehouse_id = '".$warehouseId."'";
        $whereDocDate = "product_receivings.doc_date BETWEEN '".$docDateStart."' AND '".$docDateEnd."'";

        if($productReceivingId != 'null' || $productRequestId != 'null') {
            $whereCounterId = "product_receivings.counter_id is not null";
            $whereWarehouseId = "product_receivings.warehouse_id is not null";
            $whereDocDate = "product_receivings.doc_date is not null";
        }

        $reportData = ProductReceiving::join('mst_counters', 'product_receivings.counter_id', '=', 'mst_counters.id')
                      ->join('mst_warehouses', 'product_receivings.warehouse_id', '=', 'mst_warehouses.id')
                      ->selectRaw('product_receivings.id as product_receiving_id,
                        product_receivings.product_request_id,
                        product_receivings.counter_id,
                        mst_counters.counter_name,
                        product_receivings.warehouse_id,
                        mst_warehouses.warehouse_name,
                        product_receivings.doc_date,
                        product_receivings.courier,
                        product_receivings.status')
                      ->whereRaw($whereProductReceivingId)
                      ->whereRaw($whereProductRequestId)
                      ->whereRaw($whereCounterId)
                      ->whereRaw($whereWarehouseId)
                      ->whereRaw($whereDocDate)
                      ->orderByRaw('product_receivings.created_at DESC')
                      ->get();

        $response = [
            'message' => 'Report Product Receiving',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'report_data' => $reportData
        ];
        
        return response()->json($response, 200);
    }

    public function getProductReceivingDetail(Request $request)
    {
        $productReceivingId = $request->input('product_receiving_id');

        $reportData = ProductReceivingDetail::join('product_receivings', 'product_receiving_details.product_receiving_id', '=', 'product_receivings.id')
                      ->join('mst_counters', 'product_receivings.counter_id', '=', 'mst_counters.id')
                      ->join('mst_warehouses', 'product_receivings.warehouse_id', '=', 'mst_warehouses.id')
                      ->join('mst_products', 'product_receiving_details.product_id', '=', 'mst_products.id')
                      ->selectRaw('product_receiving_details.product_receiving_id,
                        product_receivings.product_request_id,
                        product_receivings.counter_id,
                        mst_counters.counter_name,
                        product_receivings.warehouse_id,
                        mst_warehouses.warehouse_name,
                        product_receivings.doc_date,
                        product_receivings.courier,
                        product_receivings.desc,
                        product_receivings.status,
                        product_receiving_details.product_id,
                        mst_products.product_name,
                        product_receiving_details.qty_request,
                        product_receiving_details.qty AS qty_receiving,
                        (product_receiving_details.qty - product_receiving_details.qty_request) AS qty_diff')
                      ->where('product_receiving_details.product_receiving_id', $productReceivingId)
                      ->orderByRaw('product_receiving_details.product_id ASC')
                      ->get();

        $response = [
            'message' => 'Report Product Receiving Detail',
            'status' => [
                'code' => 200,
                'description' => 'OK'
            ],
            'report_data' => $reportData
        ];
        
        return response()->json($response, 200);
    }
}
