<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstCoa extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
