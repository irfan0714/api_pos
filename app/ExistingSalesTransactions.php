<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExistingSalesTransactions extends Model
{
    public $incrementing = true;
}
