<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProductCategory extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
