<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstForeignCurrency extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
