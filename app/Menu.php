<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['parent_id', 'menu_name', 'root', 'weight', 'icon', 'active', 'created_by', 'updated_by'];
    
    public function roleAccess() {
        return $this->hasMany(RoleAccess::class);
    }
}
