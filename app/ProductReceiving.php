<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReceiving extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
