<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOpname extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
