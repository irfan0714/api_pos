<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentVoucher extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
