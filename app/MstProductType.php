<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProductType extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
