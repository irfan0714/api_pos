<?php

namespace App\Imports;

use App\StockOpnameDetail;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class StockOpnameDetailImport implements ToModel, WithStartRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new StockOpnameDetail([
            'stock_opname_id' => $row[1],
            'product_id'      => str_replace('`', '', $row[2]),
            'qty_real'        => $row[5],
            'qty_program'     => $row[4]
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
