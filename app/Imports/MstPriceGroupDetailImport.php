<?php

namespace App\Imports;

use App\MstPriceGroupDetail;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class MstPriceGroupDetailImport implements ToModel, WithStartRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MstPriceGroupDetail([
            'price_group_id' => '0',
            'product_id'     => str_replace('`', '', $row[1]),
            'price'          => $row[3]
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
