<?php

namespace App\Imports;

use App\StockOpname;
use Maatwebsite\Excel\Concerns\ToModel;

class StockOpnameImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new StockOpname([
            //
        ]);
    }
}
