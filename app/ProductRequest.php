<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRequest extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
