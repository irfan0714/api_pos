<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProductBrand extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
