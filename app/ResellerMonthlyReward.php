<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResellerMonthlyReward extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
}
