<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['role_name', 'all_access', 'active', 'created_by', 'updated_by'];

    public function roleAccess() {
        return $this->hasMany(RoleAccess::class);
    }
}
