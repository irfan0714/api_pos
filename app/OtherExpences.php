<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherExpences extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
    protected $casts = [    
        'warehouse_id' => 'integer',
        'counter_id' => 'integer',
        'purpose_id' => 'integer'
    ];
}
