<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstPaymentMethodTypes extends Model
{
    protected $keyType = 'string';
}
