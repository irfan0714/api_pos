<?php

use Faker\Generator as Faker;
use App\RoleAccess;

$factory->define(RoleAccess::class, function (Faker $faker) {
    return [
        'role_id' => 1,
        'view' => 1,
        'read' => 1,
        'read_all' => 1,
        'create' => 1,
        'update' => 1,
        'delete' => 1,
        'approve' => 1,
        'download' => 1,
        'created_by' => 'admin',
    ];
});
