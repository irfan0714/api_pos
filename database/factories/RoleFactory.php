<?php

use Faker\Generator as Faker;
use App\Role;

$factory->define(Role::class, function (Faker $faker) {
    return [
        'role_name' => 'admin',
        'all_access' => 1,
        'active' => 1,
        'created_by' => 'admin',
    ];
});
