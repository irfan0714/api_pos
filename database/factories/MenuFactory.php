<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Menu;

$factory->define(Menu::class, function (Faker $faker) {
    return [
        'parent_id' => 0,
        'menu_name' => $faker->name,
        'root' => $faker->name,
        'icon' => $faker->name,
        'active' => 1,
        'created_by' => 'admin',
        'weight' => 1,
    ];
});
