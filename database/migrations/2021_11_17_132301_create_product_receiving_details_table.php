<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReceivingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_receiving_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_receiving_id',15)->nullable();
            $table->string('product_id',50)->nullable();
            $table->integer('qty')->unsigned()->default(0);
            $table->string('unit',10)->nullable();
            $table->integer('qty_request')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_receiving_details');
    }
}
