<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExistingSalesTransactionPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('existing_sales_transaction_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales_transaction_id',50)->nullable();
            $table->smallInteger('payment_method_id')->unsigned()->nullable();
            $table->smallInteger('edc_id')->unsigned()->nullable();
            $table->integer('point')->unsigned()->default(0);
            $table->decimal('payment_value',11,2)->default(0.0000);
            $table->string('voucher_code',50)->nullable();
            $table->integer('exchange_rate')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('existing_sales_transaction_payments');
    }
}
