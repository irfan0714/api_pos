<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->smallInteger('year')->unsigned()->nullable();
            $table->smallInteger('warehouse_id')->unsigned()->nullable();
            $table->string('product_id',50)->nullable();
            $table->integer('early_01')->unsigned()->default(0);
            $table->integer('early_value_01')->unsigned()->default(0);
            $table->integer('in_01')->unsigned()->default(0);
            $table->integer('in_value_01')->unsigned()->default(0);
            $table->integer('out_01')->unsigned()->default(0);
            $table->integer('out_value_01')->unsigned()->default(0);
            $table->integer('end_01')->unsigned()->default(0);
            $table->integer('end_value_01')->unsigned()->default(0);
            $table->integer('early_02')->unsigned()->default(0);
            $table->integer('early_value_02')->unsigned()->default(0);
            $table->integer('in_02')->unsigned()->default(0);
            $table->integer('in_value_02')->unsigned()->default(0);
            $table->integer('out_02')->unsigned()->default(0);
            $table->integer('out_value_02')->unsigned()->default(0);
            $table->integer('end_02')->unsigned()->default(0);
            $table->integer('end_value_02')->unsigned()->default(0);
            $table->integer('early_03')->unsigned()->default(0);
            $table->integer('early_value_03')->unsigned()->default(0);
            $table->integer('in_03')->unsigned()->default(0);
            $table->integer('in_value_03')->unsigned()->default(0);
            $table->integer('out_03')->unsigned()->default(0);
            $table->integer('out_value_03')->unsigned()->default(0);
            $table->integer('end_03')->unsigned()->default(0);
            $table->integer('end_value_03')->unsigned()->default(0);
            $table->integer('early_04')->unsigned()->default(0);
            $table->integer('early_value_04')->unsigned()->default(0);
            $table->integer('in_04')->unsigned()->default(0);
            $table->integer('in_value_04')->unsigned()->default(0);
            $table->integer('out_04')->unsigned()->default(0);
            $table->integer('out_value_04')->unsigned()->default(0);
            $table->integer('end_04')->unsigned()->default(0);
            $table->integer('end_value_04')->unsigned()->default(0);
            $table->integer('early_05')->unsigned()->default(0);
            $table->integer('early_value_05')->unsigned()->default(0);
            $table->integer('in_05')->unsigned()->default(0);
            $table->integer('in_value_05')->unsigned()->default(0);
            $table->integer('out_05')->unsigned()->default(0);
            $table->integer('out_value_05')->unsigned()->default(0);
            $table->integer('end_05')->unsigned()->default(0);
            $table->integer('end_value_05')->unsigned()->default(0);
            $table->integer('early_06')->unsigned()->default(0);
            $table->integer('early_value_06')->unsigned()->default(0);
            $table->integer('in_06')->unsigned()->default(0);
            $table->integer('in_value_06')->unsigned()->default(0);
            $table->integer('out_06')->unsigned()->default(0);
            $table->integer('out_value_06')->unsigned()->default(0);
            $table->integer('end_06')->unsigned()->default(0);
            $table->integer('end_value_06')->unsigned()->default(0);
            $table->integer('early_07')->unsigned()->default(0);
            $table->integer('early_value_07')->unsigned()->default(0);
            $table->integer('in_07')->unsigned()->default(0);
            $table->integer('in_value_07')->unsigned()->default(0);
            $table->integer('out_07')->unsigned()->default(0);
            $table->integer('out_value_07')->unsigned()->default(0);
            $table->integer('end_07')->unsigned()->default(0);
            $table->integer('end_value_07')->unsigned()->default(0);
            $table->integer('early_08')->unsigned()->default(0);
            $table->integer('early_value_08')->unsigned()->default(0);
            $table->integer('in_08')->unsigned()->default(0);
            $table->integer('in_value_08')->unsigned()->default(0);
            $table->integer('out_08')->unsigned()->default(0);
            $table->integer('out_value_08')->unsigned()->default(0);
            $table->integer('end_08')->unsigned()->default(0);
            $table->integer('end_value_08')->unsigned()->default(0);
            $table->integer('early_09')->unsigned()->default(0);
            $table->integer('early_value_09')->unsigned()->default(0);
            $table->integer('in_09')->unsigned()->default(0);
            $table->integer('in_value_09')->unsigned()->default(0);
            $table->integer('out_09')->unsigned()->default(0);
            $table->integer('out_value_09')->unsigned()->default(0);
            $table->integer('end_09')->unsigned()->default(0);
            $table->integer('end_value_09')->unsigned()->default(0);
            $table->integer('early_10')->unsigned()->default(0);
            $table->integer('early_value_10')->unsigned()->default(0);
            $table->integer('in_10')->unsigned()->default(0);
            $table->integer('in_value_10')->unsigned()->default(0);
            $table->integer('out_10')->unsigned()->default(0);
            $table->integer('out_value_10')->unsigned()->default(0);
            $table->integer('end_10')->unsigned()->default(0);
            $table->integer('end_value_10')->unsigned()->default(0);
            $table->integer('early_11')->unsigned()->default(0);
            $table->integer('early_value_11')->unsigned()->default(0);
            $table->integer('in_11')->unsigned()->default(0);
            $table->integer('in_value_11')->unsigned()->default(0);
            $table->integer('out_11')->unsigned()->default(0);
            $table->integer('out_value_11')->unsigned()->default(0);
            $table->integer('end_11')->unsigned()->default(0);
            $table->integer('end_value_11')->unsigned()->default(0);
            $table->integer('early_12')->unsigned()->default(0);
            $table->integer('early_value_12')->unsigned()->default(0);
            $table->integer('in_12')->unsigned()->default(0);
            $table->integer('in_value_12')->unsigned()->default(0);
            $table->integer('out_12')->unsigned()->default(0);
            $table->integer('out_value_12')->unsigned()->default(0);
            $table->integer('end_12')->unsigned()->default(0);
            $table->integer('end_value_12')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
