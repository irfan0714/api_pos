<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseMutationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_mutation_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('warehouse_mutation_id',15)->nullable();
            $table->string('product_id',50)->nullable();
            $table->integer('qty')->unsigned()->default(0);
            $table->string('desc_detail',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_mutation_details');
    }
}
