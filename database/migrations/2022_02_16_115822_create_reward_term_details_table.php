<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardTermDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_term_details', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('reward_term_id')->unsigned()->nullable();
            $table->string('desc',250)->nullable();
            $table->integer('term')->unsigned()->default(0);
            $table->enum('reward_type',['point','nominal','percent'])->nullable();
            $table->decimal('reward',11,4)->unsigned()->default(0);
            $table->decimal('reward_value',11,4)->unsigned()->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_term_details');
    }
}
