<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_method_type_id', 20)->nullable();
            $table->string('payment_name', 20)->nullable();
            $table->integer('exchange_rate')->unsigned()->default(0);
            $table->enum('active', [1,0])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_payment_methods');
    }
}
