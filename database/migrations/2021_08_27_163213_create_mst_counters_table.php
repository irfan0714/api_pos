<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_counters', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('branch_id')->unsigned()->nullable();
            $table->string('counter_name',100)->nullable();
            $table->date('trans_date')->nullable();
            $table->string('first_address',100)->nullable();
            $table->string('last_address',100)->nullable();
            $table->string('phone',20)->nullable();
            $table->string('footer_text',200)->nullable();
            $table->string('latitude',200)->nullable();
            $table->string('longitude',200)->nullable();
            $table->enum('active',[1,0])->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_counters');
    }
}
