<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResellerMonthlyRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reseller_monthly_rewards', function (Blueprint $table) {
            $table->string('id',15);
            $table->integer('customer_id')->unsigned()->nullable();
            $table->date('period_transaction_start')->nullable();
            $table->date('period_transaction_end')->nullable();
            $table->integer('total_transaction')->unsigned()->default(0);
            $table->date('paid_date')->nullable();
            $table->integer('paid_amount')->unsigned()->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reseller_monthly_rewards');
    }
}
