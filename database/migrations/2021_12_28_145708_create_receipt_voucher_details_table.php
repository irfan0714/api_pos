<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptVoucherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_voucher_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('receipt_voucher_id',15)->nullable();
            $table->smallInteger('counter_id')->unsigned()->nullable();
            $table->string('coa',50)->nullable();
            $table->decimal('amount',11,2)->default(0.0000);
            $table->string('desc',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_voucher_details');
    }
}
