<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_access', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('role_id')->unsigned()->nullable();
            $table->tinyInteger('menu_id')->unsigned()->nullable();
            $table->enum('view',[1,0])->default(0);
            $table->enum('read',[1,0])->default(0);
            $table->enum('read_all',[1,0])->default(0);
            $table->enum('create',[1,0])->default(0);
            $table->enum('update',[1,0])->default(0);
            $table->enum('delete',[1,0])->default(0);
            $table->enum('approve',[1,0])->default(0);
            $table->enum('download',[1,0])->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_accesses');
    }
}
