<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePettyCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petty_cashes', function (Blueprint $table) {
            $table->string('id',11)->primary();
            $table->smallInteger('counter_id')->unsigned()->nullable();
            $table->smallInteger('petty_cash_category_id')->unsigned()->nullable();
            $table->date('doc_date')->nullable();
            $table->enum('type', ['I','O'])->nullable();
            $table->integer('amount')->unsigned()->default(0);
            $table->string('desc')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petty_cashes');
    }
}
