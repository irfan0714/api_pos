<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('counter_id')->unsigned()->nullable();
            $table->string('warehouse_name',100)->nullable();
            $table->string('unit_size',10)->nullable();
            $table->integer('length_size')->unsigned()->default(0);
            $table->integer('width_size')->unsigned()->default(0);
            $table->integer('height_size')->unsigned()->default(0);
            $table->enum('active', [1,0])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_warehouses');
    }
}
