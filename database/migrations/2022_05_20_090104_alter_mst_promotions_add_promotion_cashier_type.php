<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMstPromotionsAddPromotionCashierType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE mst_promotions ADD promotion_cashier_type enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 = all; 1 = reguler; 2 = reseller' AFTER promotion_type_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
