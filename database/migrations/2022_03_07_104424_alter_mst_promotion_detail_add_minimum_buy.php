<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMstPromotionDetailAddMinimumBuy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE mst_promotion_details ADD minimum_buy smallint(5) DEFAULT 0 AFTER free_qty");
        DB::statement("ALTER TABLE mst_promotion_details ADD free_item_used smallint(5) DEFAULT 0 AFTER minimum_buy");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
