<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_mutations', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('customer_id')->unsigned()->nullable();
            $table->char('point_mutation_type_id', 1)->nullable()->comment('I = IN ; O = OUT');
            $table->string('sales_transaction_id',20)->nullable();
            $table->smallInteger('point')->unsigned()->default(0);
            $table->smallInteger('point_value')->unsigned()->default(0);
            $table->date('trans_date')->nullable();
            $table->date('expired_date')->nullable();
            $table->enum('status', [1,0])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_mutations');
    }
}
