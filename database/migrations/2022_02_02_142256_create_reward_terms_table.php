<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reward_term_name')->nullable();
            $table->date('early_period')->nullable();
            $table->date('end_period')->nullable();
            $table->enum('active', [1,0])->default(1);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_terms');
    }
}
