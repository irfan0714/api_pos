<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_points', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('customer_id')->unsigned()->nullable();
            $table->integer('point_total')->unsigned()->default(0);
            $table->integer('point_total_value')->unsigned()->default(0);
            $table->integer('point_used')->unsigned()->default(0);
            $table->integer('point_used_value')->unsigned()->default(0);
            $table->integer('point_remains')->unsigned()->default(0);
            $table->integer('point_remains_value')->unsigned()->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_points');
    }
}
