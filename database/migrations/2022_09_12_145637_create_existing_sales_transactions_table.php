<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExistingSalesTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('existing_sales_transactions', function (Blueprint $table) {
            $table->string('id',20);
            $table->smallInteger('cashier_id')->unsigned()->nullable();
            $table->smallInteger('user_id')->unsigned()->nullable();
            $table->smallInteger('counter_id')->unsigned()->nullable();
            $table->smallInteger('warehouse_id')->unsigned()->nullable();
            $table->string('customer_id',20)->nullable();
            $table->smallInteger('gsa_id')->unsigned()->nullable();
            $table->smallInteger('agent_id')->unsigned()->nullable();
            $table->smallInteger('guide_id')->unsigned()->nullable();
            $table->string('receipt_no',50)->nullable();
            $table->date('trans_date')->nullable();
            $table->time('trans_time')->nullable();
            $table->integer('total_item')->unsigned()->default(0);
            $table->integer('total_value')->unsigned()->default(0);
            $table->integer('total_payment')->unsigned()->default(0);
            $table->integer('discount')->unsigned()->default(0);
            $table->integer('change')->unsigned()->default(0);
            $table->integer('cash')->unsigned()->default(0);
            $table->integer('credit_card')->unsigned()->default(0);
            $table->integer('debit_card')->unsigned()->default(0);
            $table->integer('transfer')->unsigned()->default(0);
            $table->integer('voucher')->unsigned()->default(0);
            $table->integer('member_point')->unsigned()->default(0);
            $table->integer('reward')->unsigned()->default(0);
            $table->integer('travel_voucher')->unsigned()->default(0);
            $table->integer('tax_basis')->unsigned()->default(0);
            $table->integer('vat')->unsigned()->default(0);
            $table->string('description',250)->nullable();
            $table->string('discount_code',50)->nullable();
            $table->integer('discount_value')->unsigned()->default(0);
            $table->enum('commission_status', [1,0])->default(0)->nullable();
            $table->enum('commission_extra_status', [1,0])->default(0)->nullable();
            $table->integer('foreign_currency_1')->unsigned()->default(0);
            $table->integer('exchange_rate_1')->unsigned()->default(0);
            $table->string('currency_1',10)->nullable();
            $table->integer('foreign_currency_2')->unsigned()->default(0);
            $table->integer('exchange_rate_2')->unsigned()->default(0);
            $table->string('currency_2',10)->nullable();
            $table->integer('foreign_currency_3')->unsigned()->default(0);
            $table->integer('exchange_rate_3')->unsigned()->default(0);
            $table->string('currency_3',10)->nullable();
            $table->string('currency_change',10)->nullable();
            $table->integer('change_rate_usd')->unsigned()->default(0);
            $table->integer('change_rate_cny')->unsigned()->default(0);
            $table->integer('change_fc_usd')->unsigned()->default(0);
            $table->integer('change_fc_cny')->unsigned()->default(0);
            $table->string('desc_void',200)->nullable();
            $table->enum('transaction_type', ["1","2"])->default("1")->nullable();
            $table->enum('status', ["0","1","2","3","4","5"])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('existing_sales_transactions');
    }
}
