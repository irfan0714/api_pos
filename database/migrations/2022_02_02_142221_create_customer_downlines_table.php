<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerDownlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_downlines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('upline_id')->unsigned()->nullable()->comment('upline_id: customer_id upline');
            $table->integer('downline_id')->unsigned()->nullable()->comment('downline_id: customer_id downline');
            $table->string('currency')->nullable();
            $table->integer('reward_value')->unsigned()->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_downlines');
    }
}
