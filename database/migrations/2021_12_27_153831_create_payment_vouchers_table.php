<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_vouchers', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->smallInteger('counter_id')->unsigned()->nullable();
            $table->smallInteger('cash_bank_id')->unsigned()->nullable();
            $table->date('doc_date')->nullable();
            $table->string('recipient')->nullable();
            $table->string('desc')->nullable();
            $table->string('currency',5)->nullable();
            $table->integer('exchange_rate')->unsigned()->default(0);
            $table->integer('amount')->unsigned()->default(0);
            $table->string('payment_proof_no')->nullable();
            $table->enum('status', [0,1,2])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_vouchers');
    }
}
