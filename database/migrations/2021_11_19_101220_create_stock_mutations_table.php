<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_mutations', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('warehouse_id')->unsigned()->nullable();
            $table->string('transaction_id',20)->nullable();
            $table->string('stock_mutation_type_id',2)->nullable();
            $table->string('product_id',50)->nullable();
            $table->integer('qty')->unsigned()->default(0);
            $table->decimal('value',11,2)->default(0.0000);
            $table->enum('stock_move', ['I','O'])->default('I')->nullable();
            $table->date('trans_date')->nullable();
            $table->enum('status', [0,1])->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_mutations');
    }
}
