<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherExpencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_expences', function (Blueprint $table) {
            $table->string('id',15);
            $table->smallInteger('counter_id')->unsigned()->nullable();
            $table->smallInteger('warehouse_id')->unsigned()->nullable();
            $table->smallInteger('purpose_id')->unsigned()->nullable();
            $table->date('doc_date')->nullable();
            $table->string('desc',200)->nullable();
            $table->enum('status', [0,1,2])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_expences');
    }
}
