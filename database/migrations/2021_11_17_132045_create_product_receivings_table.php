<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReceivingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_receivings', function (Blueprint $table) {
            $table->string('id',15);
            $table->string('product_request_id',15)->nullable();
            $table->smallInteger('counter_id')->unsigned()->nullable();
            $table->smallInteger('warehouse_id')->unsigned()->nullable();
            $table->date('doc_date')->nullable();
            $table->string('receipt_no',100)->nullable();
            $table->string('courier',50)->nullable();
            $table->string('desc',200)->nullable();
            $table->enum('status', [0,1,2])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_receivings');
    }
}
