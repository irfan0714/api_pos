<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersDefaultValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE users MODIFY role_id tinyint(3) UNSIGNED DEFAULT '0' NOT NULL");
        DB::statement("ALTER TABLE users MODIFY branch_id smallint(5) UNSIGNED DEFAULT '0' NOT NULL");
        DB::statement("ALTER TABLE users MODIFY counter_id smallint(5) UNSIGNED DEFAULT '0' NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
