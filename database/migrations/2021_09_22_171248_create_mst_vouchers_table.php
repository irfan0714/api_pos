<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('voucher_code', 50)->nullable();
            $table->integer('nominal')->unsigned()->default(0);
            $table->string('currency', 20)->nullable();
            $table->date('expired_date')->nullable();
            $table->enum('status', [1,0])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_vouchers');
    }
}
