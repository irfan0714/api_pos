<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSalesTransactionDetailsPercentDiscColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE sales_transaction_details MODIFY percent_disc_1 decimal(11,2) NOT NULL DEFAULT '0.00'");
        DB::statement("ALTER TABLE sales_transaction_details MODIFY percent_disc_2 decimal(11,2) NOT NULL DEFAULT '0.00'");
        DB::statement("ALTER TABLE sales_transaction_details MODIFY percent_disc_3 decimal(11,2) NOT NULL DEFAULT '0.00'");
        DB::statement("ALTER TABLE sales_transaction_details MODIFY percent_disc_4 decimal(11,2) NOT NULL DEFAULT '0.00'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
