<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashierDepositeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashier_deposite_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cashier_deposite_id')->unsigned()->default(0);
            $table->tinyInteger('payment_method_id')->unsigned()->nullable();
            $table->integer('amount')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashier_deposite_details');
    }
}
