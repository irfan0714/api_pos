<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTransactionPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_transaction_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales_transaction_id',50)->nullable();
            $table->smallInteger('payment_method_id')->unsigned()->nullable();
            $table->integer('point')->unsigned()->default(0);
            $table->decimal('payment_value',11,2)->default(0.0000);
            $table->integer('exchange_rate')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_transaction_payments');
    }
}
