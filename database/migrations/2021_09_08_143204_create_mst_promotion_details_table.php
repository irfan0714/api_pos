<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstPromotionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_promotion_details', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('promotion_id')->unsigned()->nullable();
            $table->string('product_id',50)->nullable();
            $table->enum('type', [0,1,2])->default(0)->nullable();
            $table->decimal('minimum',11,2)->default(0.0000)->nullable();
            $table->integer('free_qty')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_promotion_details');
    }
}
