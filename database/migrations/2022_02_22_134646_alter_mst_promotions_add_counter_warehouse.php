<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMstPromotionsAddCounterWarehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE mst_promotions ADD counter_id smallint(5) unsigned NULL AFTER id');
        DB::statement('ALTER TABLE mst_promotions ADD warehouse_id smallint(5) unsigned NULL AFTER counter_id');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
