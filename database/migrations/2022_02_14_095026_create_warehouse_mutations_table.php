<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_mutations', function (Blueprint $table) {
            $table->string('id',15);
            $table->smallInteger('from_counter_id')->unsigned()->nullable();
            $table->smallInteger('from_warehouse_id')->unsigned()->nullable();
            $table->smallInteger('to_counter_id')->unsigned()->nullable();
            $table->smallInteger('to_warehouse_id')->unsigned()->nullable();
            $table->smallInteger('warehouse_mutation_type_id')->unsigned()->nullable();
            $table->string('desc',200)->nullable();
            $table->enum('status', [0,1,2,3])->default(0)->nullable();
            $table->enum('receive_status', [0,1,2,3])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_mutations');
    }
}
