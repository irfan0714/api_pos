<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbPosExistingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_pos_existing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('db_host',100)->nullable();
            $table->string('db_port',100)->nullable();
            $table->string('db_database',100)->nullable();
            $table->string('db_username',100)->nullable();
            $table->string('db_password',100)->nullable();
            $table->enum('active',[1,0])->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_pos_existing');
    }
}
