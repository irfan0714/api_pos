<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE employees CHANGE branch_id branch_id smallint UNSIGNED NULL');
        DB::statement('ALTER TABLE employees CHANGE depot_id depot_id smallint UNSIGNED NULL');
        DB::statement('ALTER TABLE employees CHANGE division_id division_id smallint UNSIGNED NULL');
        DB::statement('ALTER TABLE employees CHANGE departement_id departement_id smallint UNSIGNED NULL');
        DB::statement('ALTER TABLE employees CHANGE position_id position_id smallint UNSIGNED NULL');
        DB::statement('ALTER TABLE employees CHANGE level_id level_id smallint UNSIGNED NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
