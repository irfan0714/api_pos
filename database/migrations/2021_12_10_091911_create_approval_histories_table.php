<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_id',20)->nullable();
            $table->tinyInteger('level')->unsigned()->nullable();
            $table->string('username',20)->nullable();
            $table->enum('status', [0,1,2,3,4,5,6])->default(0)->nullable();
            $table->datetime('trans_date')->nullable();
            $table->string('remark',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval_histories');
    }
}
