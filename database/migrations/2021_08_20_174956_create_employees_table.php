<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id',50)->nullable();
            $table->tinyInteger('branch_id')->unsigned()->nullable();
            $table->tinyInteger('depot_id')->unsigned()->nullable();
            $table->tinyInteger('division_id')->unsigned()->nullable();
            $table->tinyInteger('departement_id')->unsigned()->nullable();
            $table->tinyInteger('position_id')->unsigned()->nullable();
            $table->tinyInteger('level_id')->unsigned()->nullable();
            $table->string('employee_nik',50)->nullable();
            $table->string('employee_name',50)->nullable();
            $table->enum('gender',['male', 'female'])->nullable();
            $table->date('birthday',50)->nullable();
            $table->string('email',50)->nullable();
            $table->enum('active',[1, 0])->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
