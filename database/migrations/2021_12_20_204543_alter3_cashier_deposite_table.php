<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter3CashierDepositeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cashier_deposites', function (Blueprint $table) {
            $table->integer('cash')->unsigned()->default(0)->after('diff');
            $table->integer('credit_card')->unsigned()->default(0)->after('cash');
            $table->integer('debit_card')->unsigned()->default(0)->after('credit_card');
            $table->integer('voucher')->unsigned()->default(0)->after('debit_card');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
