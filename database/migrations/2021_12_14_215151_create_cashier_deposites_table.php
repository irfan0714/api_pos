<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashierDepositesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashier_deposites', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('counter_id')->unsigned()->nullable();
            $table->tinyInteger('user_id')->unsigned()->nullable();
            $table->date('transaction_date')->nullable();
            $table->time('transaction_time')->nullable();
            $table->integer('amount')->unsigned()->default(0);
            $table->integer('diff')->unsigned()->default(0);
            $table->string('description',250)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('cashier_deposites');
    }
}
