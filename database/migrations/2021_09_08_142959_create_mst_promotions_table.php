<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('promotion_name',100)->nullable();
            $table->date('early_period')->nullable();
            $table->date('end_period')->nullable();
            $table->smallInteger('promotion_type_id')->unsigned()->nullable();
            $table->decimal('minimum',11,2)->default(0.0000)->nullable();
            $table->enum('valid_on_su', [1,0])->default(0)->nullable();
            $table->enum('valid_on_mo', [1,0])->default(0)->nullable();
            $table->enum('valid_on_tu', [1,0])->default(0)->nullable();
            $table->enum('valid_on_we', [1,0])->default(0)->nullable();
            $table->enum('valid_on_th', [1,0])->default(0)->nullable();
            $table->enum('valid_on_fr', [1,0])->default(0)->nullable();
            $table->enum('valid_on_sa', [1,0])->default(0)->nullable();
            $table->enum('status', [1,0])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_promotions');
    }
}
