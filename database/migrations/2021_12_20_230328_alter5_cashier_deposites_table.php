<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter5CashierDepositesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cashier_deposites', function (Blueprint $table) {
            $table->integer('cashier_capital_amount')->unsigned()->default(0)->after('description');
            $table->integer('cash_report')->unsigned()->default(0)->after('cashier_capital_amount');
            $table->integer('change')->unsigned()->default(0)->after('cash_report');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
