<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter3CustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('birth_place',100)->nullable()->after('phone');
            $table->date('birth_date')->nullable()->after('birth_place');
            $table->enum('gender', ['Laki-laki','Perempuan'])->nullable()->after('birth_date');
            $table->string('profession',50)->nullable()->after('gender');
            $table->string('area_code',10)->nullable()->after('profession');
            $table->string('area',100)->nullable()->after('area_code');
            $table->string('address',300)->nullable()->after('area');
            $table->string('map_location',100)->nullable()->after('address');
            $table->date('join_date')->nullable()->after('map_location');
            $table->enum('internal_employee', [1,0])->default(0)->nullable()->after('join_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
