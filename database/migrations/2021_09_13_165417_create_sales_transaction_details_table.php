<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_transaction_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales_transaction_id',20)->nullable();
            $table->string('product_id',50)->nullable();
            $table->integer('qty')->unsigned()->default(0);
            $table->integer('free_qty')->unsigned()->default(0);
            $table->decimal('price',11,2)->default(0.0000);
            $table->decimal('percent_disc_1',2,2)->default(0.0000);
            $table->decimal('disc_1',11,2)->default(0.0000);
            $table->decimal('percent_disc_2',2,2)->default(0.0000);
            $table->decimal('disc_2',11,2)->default(0.0000);
            $table->decimal('percent_disc_3',2,2)->default(0.0000);
            $table->decimal('disc_3',11,2)->default(0.0000);
            $table->decimal('percent_disc_4',2,2)->default(0.0000);
            $table->decimal('disc_4',11,2)->default(0.0000);
            $table->decimal('net',11,2)->default(0.0000);
            $table->decimal('cogs',11,2)->default(0.0000);
            $table->integer('percent_vat')->unsigned()->default(0);
            $table->decimal('percent_commission',11,2)->default(0.0000);
            $table->string('description',200)->nullable();
            $table->enum('status', [1,0])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_transaction_details');
    }
}
