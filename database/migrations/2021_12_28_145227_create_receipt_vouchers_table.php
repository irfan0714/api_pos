<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_vouchers', function (Blueprint $table) {
            $table->string('id',15);
            $table->smallInteger('cash_bank_id')->unsigned()->nullable();
            $table->date('doc_date')->nullable();
            $table->string('receipt_from',100)->nullable();
            $table->string('currency',5)->nullable();
            $table->decimal('exchange_rate',11,2)->default(0.0000);
            $table->decimal('amount',11,2)->default(0.0000);
            $table->string('receipt_proof_no',50)->nullable();
            $table->enum('status', [0,1,2])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_vouchers');
    }
}
