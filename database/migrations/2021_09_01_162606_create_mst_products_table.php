<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_products', function (Blueprint $table) {
            $table->string('id',50);
            $table->string('product_brand_id',5)->nullable();
            $table->string('product_sub_brand_id',5)->nullable();
            $table->string('product_type_id',5)->nullable();
            $table->string('product_sub_type_id',5)->nullable();
            $table->string('product_category_id',5)->nullable();
            $table->string('product_sub_category_id',5)->nullable();
            $table->string('barcode',200)->nullable();
            $table->string('product_name',250)->nullable();
            $table->string('initial_name',250)->nullable();
            $table->string('unit',20)->nullable();
            $table->decimal('cogs',11,4)->default(0.0000);
            $table->integer('price')->default(0);
            $table->decimal('mrp',11,4)->default(0.0000);
            $table->tinyInteger('percent_vat')->default(0);
            $table->enum('include_tax',[1,0])->default(0);
            $table->integer('min_stock')->unsigned()->default(0);
            $table->integer('max_stock')->unsigned()->default(0);
            $table->string('unit_size',10)->nullable();
            $table->integer('length_size')->unsigned()->default(0);
            $table->integer('width_size')->unsigned()->default(0);
            $table->integer('height_size')->unsigned()->default(0);
            $table->enum('active', [1,0])->default(0)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_products');
    }
}
