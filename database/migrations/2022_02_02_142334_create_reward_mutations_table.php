<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_mutations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('upline_id')->unsigned()->nullable()->comment('upline_id: customer_id');
            $table->integer('downline_id')->unsigned()->nullable()->comment('downline_id: customer_id ');
            $table->string('sales_transaction_id')->nullable();
            $table->string('reward_mutation_type_id',5)->nullable()->comment('I: In, O: Out');;
            $table->date('transaction_date')->nullable();
            $table->string('currency')->nullable();
            $table->integer('total_sales')->unsigned()->default(0);
            $table->integer('reward_value')->unsigned()->default(0);
            $table->decimal('reward_percent',11,4)->unsigned()->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_mutations');
    }
}
