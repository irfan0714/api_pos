<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherExpencesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_expences_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('other_expences_id',15)->nullable();
            $table->string('product_id',50)->nullable();
            $table->integer('qty')->unsigned()->default(0);
            $table->string('unit',10)->nullable();
            $table->decimal('price',11,2)->default(0.0000);
            $table->string('description',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_expences_details');
    }
}
