<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTransactionPaymentsViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_pivot_sales_transaction_payments AS
            (
                SELECT
                sales_transaction_id AS pm_sales_transaction_id,
                SUM(IF(payment_method_id = 1, payment_value, 0)) AS pm_tf_transfer,
                SUM(IF(payment_method_id = 2, payment_value, 0)) AS pm_d_amex,
                SUM(IF(payment_method_id = 3, payment_value, 0)) AS pm_d_anz,
                SUM(IF(payment_method_id = 4, payment_value, 0)) AS pm_d_bankmega,
                SUM(IF(payment_method_id = 5, payment_value, 0)) AS pm_d_bca,
                SUM(IF(payment_method_id = 6, payment_value, 0)) AS pm_d_bii,
                SUM(IF(payment_method_id = 7, payment_value, 0)) AS pm_d_bjb,
                SUM(IF(payment_method_id = 8, payment_value, 0)) AS pm_d_bni,
                SUM(IF(payment_method_id = 9, payment_value, 0)) AS pm_d_bpd,
                SUM(IF(payment_method_id = 10, payment_value, 0)) AS pm_d_bri,
                SUM(IF(payment_method_id = 11, payment_value, 0)) AS pm_d_btn,
                SUM(IF(payment_method_id = 12, payment_value, 0)) AS pm_d_cimb,
                SUM(IF(payment_method_id = 13, payment_value, 0)) AS pm_d_citibank,
                SUM(IF(payment_method_id = 14, payment_value, 0)) AS pm_d_commonweal,
                SUM(IF(payment_method_id = 15, payment_value, 0)) AS pm_d_danamon,
                SUM(IF(payment_method_id = 16, payment_value, 0)) AS pm_d_gopay,
                SUM(IF(payment_method_id = 17, payment_value, 0)) AS pm_d_hsbc,
                SUM(IF(payment_method_id = 18, payment_value, 0)) AS pm_d_icbc,
                SUM(IF(payment_method_id = 19, payment_value, 0)) AS pm_d_jcb,
                SUM(IF(payment_method_id = 20, payment_value, 0)) AS pm_d_jenius,
                SUM(IF(payment_method_id = 21, payment_value, 0)) AS pm_d_mandiri,
                SUM(IF(payment_method_id = 22, payment_value, 0)) AS pm_d_mastercard,
                SUM(IF(payment_method_id = 23, payment_value, 0)) AS pm_d_maybank,
                SUM(IF(payment_method_id = 24, payment_value, 0)) AS pm_d_muamalat,
                SUM(IF(payment_method_id = 25, payment_value, 0)) AS pm_d_nisp,
                SUM(IF(payment_method_id = 26, payment_value, 0)) AS pm_d_ocbc,
                SUM(IF(payment_method_id = 27, payment_value, 0)) AS pm_d_paninbank,
                SUM(IF(payment_method_id = 28, payment_value, 0)) AS pm_d_permata,
                SUM(IF(payment_method_id = 29, payment_value, 0)) AS pm_d_scb,
                SUM(IF(payment_method_id = 30, payment_value, 0)) AS pm_d_shinhan,
                SUM(IF(payment_method_id = 31, payment_value, 0)) AS pm_d_uob,
                SUM(IF(payment_method_id = 32, payment_value, 0)) AS pm_d_visa,
                SUM(IF(payment_method_id = 33, payment_value, 0)) AS pm_k_amex,
                SUM(IF(payment_method_id = 34, payment_value, 0)) AS pm_k_anz,
                SUM(IF(payment_method_id = 35, payment_value, 0)) AS pm_k_bankmega,
                SUM(IF(payment_method_id = 36, payment_value, 0)) AS pm_k_bca,
                SUM(IF(payment_method_id = 37, payment_value, 0)) AS pm_k_bii,
                SUM(IF(payment_method_id = 38, payment_value, 0)) AS pm_k_bni,
                SUM(IF(payment_method_id = 39, payment_value, 0)) AS pm_k_bpd,
                SUM(IF(payment_method_id = 40, payment_value, 0)) AS pm_k_bri,
                SUM(IF(payment_method_id = 41, payment_value, 0)) AS pm_k_btn,
                SUM(IF(payment_method_id = 42, payment_value, 0)) AS pm_k_cimb,
                SUM(IF(payment_method_id = 43, payment_value, 0)) AS pm_k_citibank,
                SUM(IF(payment_method_id = 44, payment_value, 0)) AS pm_k_commonweal,
                SUM(IF(payment_method_id = 45, payment_value, 0)) AS pm_k_danamon,
                SUM(IF(payment_method_id = 46, payment_value, 0)) AS pm_k_gopay,
                SUM(IF(payment_method_id = 47, payment_value, 0)) AS pm_k_hsbc,
                SUM(IF(payment_method_id = 48, payment_value, 0)) AS pm_k_icbc,
                SUM(IF(payment_method_id = 49, payment_value, 0)) AS pm_k_jcb,
                SUM(IF(payment_method_id = 50, payment_value, 0)) AS pm_k_jenius,
                SUM(IF(payment_method_id = 51, payment_value, 0)) AS pm_k_mandiri,
                SUM(IF(payment_method_id = 52, payment_value, 0)) AS pm_k_mastercard,
                SUM(IF(payment_method_id = 53, payment_value, 0)) AS pm_k_maybank,
                SUM(IF(payment_method_id = 54, payment_value, 0)) AS pm_k_muamalat,
                SUM(IF(payment_method_id = 55, payment_value, 0)) AS pm_k_nisp,
                SUM(IF(payment_method_id = 56, payment_value, 0)) AS pm_k_ocbc,
                SUM(IF(payment_method_id = 57, payment_value, 0)) AS pm_k_ovo,
                SUM(IF(payment_method_id = 58, payment_value, 0)) AS pm_k_paninbank,
                SUM(IF(payment_method_id = 59, payment_value, 0)) AS pm_k_permata,
                SUM(IF(payment_method_id = 60, payment_value, 0)) AS pm_k_scb,
                SUM(IF(payment_method_id = 61, payment_value, 0)) AS pm_k_shinhan,
                SUM(IF(payment_method_id = 62, payment_value, 0)) AS pm_k_shopeepay,
                SUM(IF(payment_method_id = 63, payment_value, 0)) AS pm_k_uob,
                SUM(IF(payment_method_id = 64, payment_value, 0)) AS pm_k_visa,
                SUM(IF(payment_method_id = 65, payment_value, 0)) AS pm_v_voucher,
                SUM(IF(payment_method_id = 66, payment_value, 0)) AS pm_mp_memberpoint,
                SUM(IF(payment_method_id = 67, payment_value, 0)) AS pm_mua_usd,
                SUM(IF(payment_method_id = 68, payment_value, 0)) AS pm_mua_cny,
                SUM(IF(payment_method_id = 69, payment_value, 0)) AS pm_mua_wechat,
                SUM(IF(payment_method_id = 70, payment_value, 0)) AS pm_t_tunai,
                SUM(IF(payment_method_id = 71, payment_value, 0)) AS pm_r_reward
                FROM
                sales_transaction_payments
                GROUP BY sales_transaction_id
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
