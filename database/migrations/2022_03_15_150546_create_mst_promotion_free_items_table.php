<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstPromotionFreeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_promotion_free_items', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('promotion_detail_id')->unsigned()->nullable();
            $table->string('product_id',50)->nullable();
            $table->smallInteger('free_item_used')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_promotion_free_items');
    }
}
