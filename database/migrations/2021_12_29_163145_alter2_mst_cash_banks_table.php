<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter2MstCashBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_cash_banks', function (Blueprint $table) {
            $table->smallInteger('counter_id')->unsigned()->after('coa_id')->nullable();
            $table->enum('type',[1,2])->default(1)->after('counter_id')->remark('1= Kas Bank, 2= Giro');
            $table->string('pv_code')->nullable()->after('name');
            $table->string('rv_code')->nullable()->after('pv_code');
            $table->enum('active',[1, 0])->default(0)->after('rv_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
