<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE users ADD password_history varchar(200) NULL AFTER password');
        DB::statement("ALTER TABLE users ADD online enum('1','0') NULL AFTER password_history");
        DB::statement('ALTER TABLE users ADD image_profile varchar(200) NULL AFTER online');
        DB::statement("ALTER TABLE users ADD status enum('1','0') NULL AFTER image_profile");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
