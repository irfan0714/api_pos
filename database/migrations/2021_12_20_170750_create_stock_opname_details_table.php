<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockOpnameDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_opname_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stock_opname_id',15)->nullable();
            $table->string('product_id',50)->nullable();
            $table->integer('qty_real')->unsigned()->default(0);
            $table->integer('qty_program')->unsigned()->default(0);
            $table->decimal('price',11,2)->default(0.0000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_opname_details');
    }
}
