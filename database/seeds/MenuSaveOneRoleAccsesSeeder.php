<?php

use Illuminate\Database\Seeder;
use App\Menu;
use App\RoleAccess;

class MenuSaveOneRoleAccsesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Menu::class,10)->create()->each(function($menu) {
            $roleAccess = factory(RoleAccess::class)->make();
            $menu->roleAccess()->save($roleAccess);
        });
    }
}
