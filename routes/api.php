<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'middleware' => 'cors'], function() {
    include 'custom/routes_iso.php';
    Route::post('/register', ['uses' => 'AuthController@store']);
    Route::post('/signin', ['uses' => 'AuthController@signin']);
    Route::post('/change-password', ['uses' => 'AuthController@changePassword']);
    Route::resource('/menu', 'MenuController');
    Route::resource('/role', 'RoleController');
    Route::get('/role-get-all', ['uses' => 'RoleController@getAllRole']);
    Route::resource('/role-access', 'RoleAccessController',  ['only' => ['edit', 'store', 'update']]);
    Route::resource('/user', 'UserController');
    Route::post('/user/pull', ['uses' => 'UserController@pull']);
    Route::get('/employee', ['uses' => 'EmployeeController@index']);
    Route::post('/employee/pull', ['uses' => 'EmployeeController@pull']);
    Route::resource('/counter', 'MstCounterController');
    Route::get('/counter-all', ['uses' => 'MstCounterController@getAllCounter']);
    Route::resource('/cashier', 'MstCashierController');
    Route::put('/setup-cashier/{id}', ['uses' => 'MstCashierController@setupKassa']);
    Route::resource('/voucher', 'MstVoucherController');
    Route::get('/voucher-generate', ['uses' => 'MstVoucherController@generate']);
    Route::put('/voucher-used-update', 'MstVoucherController@updateUsedStatus');
    Route::resource('/warehouse', 'MstWarehouseController');
    Route::get('/warehouse-all', ['uses' => 'MstWarehouseController@getAllWarehouse']);
    Route::get('/product', ['uses' => 'MstProductController@index']);
    Route::get('/product-create', ['uses' => 'MstProductController@create']);
    Route::get('/product/{id}/edit', ['uses' => 'MstProductController@edit']);
    Route::put('/product/{id}', ['uses' => 'MstProductController@update']);
    Route::post('/product/pull', ['uses' => 'MstProductController@pull']);
    Route::post('/product-add', ['uses' => 'MstProductController@store']);
    Route::get('/product-all', ['uses' => 'MstProductController@getAllProduct']);
    Route::get('/advanced-filter-product', ['uses' => 'MstProductController@getDataforAdvancedFilters']);
    Route::resource('/product-brand', 'MstProductBrandController');
    Route::resource('/product-subbrand', 'MstProductSubBrandController');
    Route::resource('/product-type', 'MstProductTypeController');
    Route::resource('/product-subtype', 'MstProductSubTypeController');
    Route::resource('/product-category', 'MstProductCategoryController');
    Route::resource('/product-subcategory', 'MstProductSubCategoryController');
    Route::resource('/promotions', 'MstPromotionsController');
    Route::resource('/promotion-types', 'MstPromotionTypesController');
    Route::resource('/promotion-details', 'MstPromotionDetailsController',  ['only' => ['edit', 'store']]);
    Route::resource('/customer', 'CustomerController');
    Route::post('/customer/pull', ['uses' => 'CustomerController@pull']);
    Route::resource('/member-point', 'MemberPointController');
    Route::get('/member-point/get-by-customer/{id}', ['uses' => 'MemberPointController@findByCustomerId']);
    Route::resource('/point-mutation', 'PointMutationController');
    Route::resource('/product-request', 'ProductRequestController');
    Route::get('/product-request-get-by-parameter', ['uses' => 'ProductRequestController@getDataByParameter']);
    Route::get('/af-product-request', 'ProductRequestController@getDataforAdvancedFilters');
    Route::post('/product-request-bundle', ['uses' => 'ProductRequestController@storeBundle']);
    Route::put('/update-product-request-bundle/{id}', ['uses' => 'ProductRequestController@updateBundle']);
    Route::post('/product-request-from-branch', ['uses' => 'ProductRequestFromBranchController@storeFromDistribution']);
    Route::resource('/product-request-details', 'ProductRequestDetailController');
    Route::resource('/product-receiving', 'ProductReceivingController');
    Route::post('/product-receiving-bundle', ['uses' => 'ProductReceivingController@storeBundle']);
    Route::post('/product-receiving-closed', ['uses' => 'ProductReceivingController@closeProductReceivingBundle']);
    Route::post('/product-receiving-details/store', ['uses' => 'ProductReceivingDetailController@store']);
    Route::post('/stock-mutation/store', ['uses' => 'StockMutationController@store']);
    Route::get('/stock-mutation-get', ['uses' => 'StockMutationController@getMutationStock']);
    Route::get('/stock/dashboard-stock', ['uses' => 'StockController@dashboardStock']);
    Route::get('/stock/manage-stock', ['uses' => 'StockController@manageStock']);

    Route::resource('/stock-opname', 'StockOpnameController');
    Route::get('/stock-opname-product', 'StockOpnameController@getDetailProductSO');
    Route::resource('/stock-opname-detail', 'StockOpnameDetailController');
    Route::post('/upload-data-so', 'StockOpnameController@uploadFile');
    Route::get('/so-for-print', 'StockOpnameController@getDetailSOForPrint');
    
    Route::get('/sales-transaction-cashier', ['uses' => 'SalesTransactionController@getSetupCashier']);
    Route::get('/sales-transaction/product', ['uses' => 'SalesTransactionController@getMstProduct']);
    Route::get('/sales-transaction/product-concat', ['uses' => 'SalesTransactionController@getMstProductConcat']);
    Route::get('/sales-transaction/payment-methods', ['uses' => 'SalesTransactionController@getMstPaymentMethods']);
    Route::get('/sales-transaction/latest-receipt-no', ['uses' => 'SalesTransactionController@getLastReceiptNo']);
    Route::post('/sales-transaction/store', ['uses' => 'SalesTransactionController@store']);
    Route::post('/sales-transaction/storebundle', ['uses' => 'SalesTransactionController@storebundle']);
    Route::get('/sales-transaction/voucher', ['uses' => 'SalesTransactionController@getVoucher']);
    Route::get('/sales-transaction/promotion', ['uses' => 'SalesTransactionController@getTodayProductPromotion']);
    Route::get('/sales-transaction/currency', ['uses' => 'SalesTransactionController@getForeignCurrencies']);
    Route::get('/sales-transaction/edc', ['uses' => 'SalesTransactionController@getEdcMachine']);
    Route::get('/sales-transaction/member', ['uses' => 'SalesTransactionController@getMember']);
    Route::get('/get-sales-transaction/{receiptNo}', ['uses' => 'SalesTransactionController@getSalesByReceiptNumber']);
    Route::post('/sales-transaction-detail/store', ['uses' => 'SalesTransactionDetailsController@store']);
    Route::post('/sales-transaction-payment/store', 'SalesTransactionPaymentController@store');

    Route::post('/clean-transaction', ['uses' => 'CleanTransactionController@index']);
    Route::get('/sidebar-menu/{username}/{roleId}', ['uses' => 'SidebarMenuController@index']);

    Route::get('/get-sales-data-existing', ['uses' => 'GetSalesDataExistingController@index']);
    Route::get('/get-sales-data-existing/{dbId}', ['uses' => 'GetSalesDataExistingController@index']);

    Route::get('/report-cashier', ['uses' => 'ReportCashierController@index']);
    Route::get('/report-cashier/transaction', ['uses' => 'ReportCashierController@getReportTransaction']);
    Route::get('/report-cashier/transaction-detail', ['uses' => 'ReportCashierController@getReportTransactionDetail']);
    Route::get('/report-cashier/transaction-product', ['uses' => 'ReportCashierController@getReportTransactionProduct']);
    Route::get('/report-cashier/transaction-brand', ['uses' => 'ReportCashierController@getReportTransactionBrand']);
    Route::get('/report-cashier/transaction-prize', ['uses' => 'ReportCashierController@getReportTransactionPrize']);
    Route::get('/report-cashier/transaction-void', ['uses' => 'ReportCashierController@getReportTransactionVoid']);
    Route::get('/report-cashier/transaction-sumdate', ['uses' => 'ReportCashierController@getReportTransactionSumDate']);
    Route::post('/save-void-transaction', ['uses' => 'ReportCashierController@saveVoidTransaction']);

    Route::get('/report-stock', ['uses' => 'ReportStockController@index']);

    Route::get('/report-stock-auditrail', ['uses' => 'ReportStockAuditrailController@index']);
    Route::get('/generate-report-stock-auditrail', ['uses' => 'ReportStockAuditrailController@getReportStockAuditrail']);
    Route::get('/report-product-request', ['uses' => 'ReportProductRequestController@index']);
    Route::get('/generate-report-product-request', ['uses' => 'ReportProductRequestController@getReportProductRequest']);
    Route::get('/generate-report-product-request-detail', ['uses' => 'ReportProductRequestController@getProductRequestDetail']);
    Route::get('/report-product-receiving', ['uses' => 'ReportProductReceivingController@index']);
    Route::get('/generate-report-product-receiving', ['uses' => 'ReportProductReceivingController@getReportProductReceiving']);
    Route::get('/generate-report-product-receiving-detail', ['uses' => 'ReportProductReceivingController@getProductReceivingDetail']);
    Route::get('/cashier-capital',  ['uses' => 'CashierCapitalController@index']);

    Route::resource('/approval-history', 'ApprovalHistoryController');

    Route::resource('/other-receiving', 'OtherReceivingController');
    Route::post('/other-receiving-bundle', ['uses' => 'OtherReceivingController@storeBundle']);
    Route::put('/update-other-receiving-bundle/{id}', ['uses' => 'OtherReceivingController@updateBundle']);
    Route::resource('/other-receiving-details', 'OtherReceivingDetailController');
    Route::resource('/other-expences', 'OtherExpencesController');
    Route::resource('/other-expences-details', 'OtherExpencesDetailController');
    Route::post('/other-expences-bundle', ['uses' => 'OtherExpencesController@storeBundle']);
    Route::put('/update-other-expences-bundle/{id}', ['uses' => 'OtherExpencesController@updateBundle']);
    Route::resource('/purpose', 'MstPurposeController');
    Route::get('/purpose-all', 'MstPurposeController@getAllPurpose');
    Route::resource('/cash-bank', 'MstCashBankController');
    Route::get('/cash-bank-all', 'MstCashBankController@getAllCashBank');
    Route::resource('/coa', 'MstCoaController');
    Route::get('/coa-all', 'MstCoaController@getAllCoa');
    Route::get('/coa-concat', 'MstCoaController@getMstCoaConcat');

    Route::resource('/receipt-voucher', 'ReceiptVoucherController');
    Route::post('/receipt-voucher-details/store', 'ReceiptVoucherDetailController@store');

    Route::resource('/product-package', 'MstProductPackageController');
    Route::resource('/price-group', 'MstPriceGroupController');
    Route::post('/upload-price-group', 'MstPriceGroupController@uploadFile');
    
    Route::resource('/warehouse-mutation', 'WarehouseMutationController');

    Route::get('/stock-for-branch', 'ReportBranchDistributionController@getReportStockForBranchDistribution');
    Route::get('/transaction-cash-for-branch', 'ReportBranchDistributionController@getReportTransactionCash');
    Route::get('/transaction-noncash-for-branch', 'ReportBranchDistributionController@getReportTransactionNonCash');
    Route::get('/stock-opname-for-branch', 'ReportBranchDistributionController@getReportStockOpname');
    Route::get('/product-request-for-branch', 'ReportBranchDistributionController@getReportProductRequest');
    Route::get('/process-closed-day', 'ClosedDayController@process');
    Route::get('/process-closed-month', 'ClosedMonthController@process');
    Route::get('/total-transaction-reseller', 'CustomerResellerController@countResellerMonthlyTransactionByPhoneNumber');
    Route::resource('/reseller-monthly-rewards', 'ResellerMonthlyRewardController');
    Route::get('/reseller-six-month-sales', ['uses' => 'CronController@countSixMonthResellerTransaction']);
    Route::get('/test-stock', 'TestController@testStock');
    Route::get('/test-cron', 'TestController@testCron');

});

