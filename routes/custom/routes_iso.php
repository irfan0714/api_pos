<?php

Route::get('/cashier-capital',  ['uses' => 'CashierCapitalController@index']);
Route::get('/cashier-capital/create/{counterId}',  ['uses' => 'CashierCapitalController@create']);
Route::post('/cashier-capital/store',  ['uses' => 'CashierCapitalController@store']);
Route::get('/cashier-capital/show/{id}',  ['uses' => 'CashierCapitalController@show']);
Route::get('/cashier-capital/edit/{id}/{counterId}',  ['uses' => 'CashierCapitalController@edit']);
Route::put('/cashier-capital/{id}',  ['uses' => 'CashierCapitalController@update']);
Route::delete('/cashier-capital/{id}',  ['uses' => 'CashierCapitalController@destroy']);

Route::get('/cashier-deposite', ['uses' => 'CashierDepositeController@index']);
Route::get('/cashier-deposite/create/{counterId}', ['uses' => 'CashierDepositeController@create']);
Route::get('/cashier-deposite/deposite-value/{userCashierId}/{transDate}', ['uses' => 'CashierDepositeController@depositoValueByUserCashierId']);
Route::post('/cashier-deposite/store', ['uses' => 'CashierDepositeController@store']);
Route::delete('/cashier-deposite/{id}',  ['uses' => 'CashierDepositeController@destroy']);
Route::get('/cashier-deposite/edit/{id}/{counterId}',  ['uses' => 'CashierDepositeController@edit']);
Route::put('/cashier-deposite/{id}',  ['uses' => 'CashierDepositeController@update']);

Route::get('/payment-voucher', 'PaymentVoucherController@index');
Route::get('/payment-voucher/create/{counterId}', 'PaymentVoucherController@create');
Route::post('/payment-voucher/store', 'PaymentVoucherController@store');

Route::get('/bcrypt/{string}', ['uses' => 'TestController@testBcrypt']);
Route::get('/random', ['uses' => 'TestController@testStrRandom']);

Route::resource('/petty-cash-category', 'PettyCashCategoryController');
Route::resource('/petty-cash', 'PettyCashController');
Route::resource('/customer-reseller', 'CustomerResellerController');
Route::get('/sales-transaction-reseller-cashier', ['uses' => 'SalesTransactionResellerController@getSetupCashier']);
Route::get('/sales-transaction-reseller/product-reseller', ['uses' => 'SalesTransactionResellerController@getMstProductReseller']);
Route::get('/sales-transaction-reseller/product', ['uses' => 'SalesTransactionResellerController@getMstProduct']);
Route::get('/sales-transaction-reseller/product-concat', ['uses' => 'SalesTransactionResellerController@getMstProductConcat']);
Route::get('/sales-transaction-reseller/payment-methods', ['uses' => 'SalesTransactionResellerController@getMstPaymentMethods']);
Route::get('/sales-transaction-reseller/latest-receipt-no', ['uses' => 'SalesTransactionResellerController@getLastReceiptNo']);
Route::post('/sales-transaction-reseller/store', ['uses' => 'SalesTransactionResellerController@store']);
Route::post('/sales-transaction-reseller/storebundle', ['uses' => 'SalesTransactionResellerController@storebundle']);
Route::get('/sales-transaction-reseller/voucher', ['uses' => 'SalesTransactionResellerController@getVoucher']);
Route::get('/sales-transaction-reseller/promotion', ['uses' => 'SalesTransactionResellerController@getTodayProductPromotion']);
Route::get('/sales-transaction-reseller/currency', ['uses' => 'SalesTransactionResellerController@getForeignCurrencies']);
Route::get('/sales-transaction-reseller/edc', ['uses' => 'SalesTransactionResellerController@getEdcMachine']);
Route::get('/sales-transaction-reseller/member', ['uses' => 'SalesTransactionResellerController@getMember']);
Route::post('/sales-transaction-reseller-detail/store', ['uses' => 'SalesTransactionDetailsController@store']);
Route::post('/sales-transaction-reseller-payment/store', 'SalesTransactionPaymentController@store');
Route::get('/sales-transaction-reseller/reward-term', 'SalesTransactionResellerController@getRewardTerm');
Route::get('/sales-transaction-reseller/downline', 'SalesTransactionResellerController@getResellerDownline');
Route::get('/sales-transaction-reseller/reward-mutation', 'SalesTransactionResellerController@getRewardMutation');
Route::get('/sales-transaction-reseller/reward-reseller', 'SalesTransactionResellerController@getRewardReseller');
Route::get('/sales-transaction-reseller/get-reseler', 'SalesTransactionResellerController@getReseller');
Route::get('/sales-transaction-reseller/get-product', 'SalesTransactionResellerController@getProduct');
Route::get('/reward-point-reseller', 'RewardEndPointReseller@index');
Route::post('/reward-point-reseller/redem-point', 'RewardEndPointReseller@redemPoint');

Route::get('/bcrypt/{string}', ['uses' => 'TestController@bcryptString']);
Route::get('/test-data-reseller', ['uses' => 'TestController@testDataReseller']);
Route::get('/test-inject-data-reseller', ['uses' => 'TestController@testInjectDataReseller']);
Route::get('/test-create-file', ['uses' => 'TestController@testCreateFile']);

//sales-existing
Route::post('/sales-existing', ['uses' => 'SalesExistingController@index']);
Route::post('/sales-existing/check', ['uses' => 'SalesExistingController@check']);
Route::get('/sales-existing', ['uses' => 'SalesExistingController@getData']);
Route::get('/sales-existing/api-doc', ['uses' => 'SalesExistingController@apiDocumentation']);
Route::get('/sales-existing/api-doc/response', ['uses' => 'SalesExistingController@apiDocumentationResponse']);

//dashboard
Route::get('/dashboard', ['uses' => 'DashboardController@getData']);